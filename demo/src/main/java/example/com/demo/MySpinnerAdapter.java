package example.com.demo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Administrator on 10/25/2016.
 */
public class MySpinnerAdapter extends ArrayAdapter<SpinnerObject>

    {

        private List<SpinnerObject> mSpinnerObjectList;
        private LayoutInflater mLayoutInflater;
        private int mResId;

        public MySpinnerAdapter(Context context, int resource, List<SpinnerObject> spinnerObjectList) {
        super(context, resource, spinnerObjectList);
        this.mSpinnerObjectList = spinnerObjectList;
        this.mResId = resource;
        this.mLayoutInflater = LayoutInflater.from(context);
    }

        @Override
        public int getCount() {
        return mSpinnerObjectList.size();
    }

        @Override
        public SpinnerObject getItem(int position) {
        return mSpinnerObjectList.get(position);
    }

        @Override
        public long getItemId(int position) {
        return position;
    }

        private final class ViewHolder {

            private TextView tvSpinner;

            public ViewHolder(View parent) {
                tvSpinner = (TextView) parent.findViewById(android.R.id.text1);
            }
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(mResId, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        SpinnerObject spinnerObject = mSpinnerObjectList.get(position);
        holder.tvSpinner.setText(spinnerObject.getValue());
        // holder.tvSpinner.setTextColor(getContext().getResources().getColor(R.color.tm_accent_color));

        return convertView;
    }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        SpinnerObject spinnerObject = mSpinnerObjectList.get(position);
        holder.tvSpinner.setText(spinnerObject.getValue());
        // holder.tvSpinner.setTextColor(getContext().getResources().getColor(R.color.tm_accent_color));

        return convertView;
    }
}
