package example.com.demo;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.widget.*;

/**
 * Created by Administrator on 10/25/2016.
 */
public class MultiSelectionObjectSpinner extends Spinner {
    private List<SpinnerObject> mListItemOriginalDisplay = null;
    MySpinnerAdapter simple_adapter = null;
    boolean[] mSelection = null;
    private String titleDialog;
    ArrayList<SpinnerObject> listItems = new ArrayList<>();
    String[] _items = null;
    private Button mButtonChoose = null;
    private Button mButtonCancel = null;
    private AlertDialog mAlertDialog;
    private CheckBox checkBox = null;
    private SpinnerObject mSpinnerObjectDefault = null;
    private boolean mIsShowTitle = false;

    public MultiSelectionObjectSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setSpinnerMultiAdapter(SpinnerObject mSpinnerObjectOriginalDisplay, List<SpinnerObject> listItem, boolean isCustomTitle) {
        mListItemOriginalDisplay = listItem;
        mIsShowTitle = isCustomTitle;
        try {
            simple_adapter = new MySpinnerAdapter(getContext(), R.layout.textview_spinner, mListItemOriginalDisplay);
            simple_adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
            simple_adapter.add(mSpinnerObjectOriginalDisplay);
            setItems(listItem);
            this.mListItemOriginalDisplay = new ArrayList<>(mListItemOriginalDisplay);
            this.listItems = new ArrayList<>(listItem);
            super.setAdapter(simple_adapter);

        } catch (Exception e) {
            Log.e("tmt", "setSpinnerMultiAdapter " + e.getMessage());
        }
    }

    public void setItems(List<SpinnerObject> items) {
        BindingListItemDialog(items);
        mSelection = new boolean[_items.length];
        Arrays.fill(mSelection, false);
    }

    private void BindingListItemDialog(List<SpinnerObject> items) {
        _items = new String[items.size()];
        for (int i = 0; i < items.size(); i++) {
            _items[i] = items.get(i).getValue();
        }
    }

    @Override
    public boolean performClick() {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            if (mIsShowTitle) {
                View view = setViewTitleDialog(titleDialog, builder);
                builder.setCustomTitle(view);

            }
            builder.setMultiChoiceItems(_items, mSelection, new PrivateMultiChoice());
            LinearLayout layout = new LinearLayout(getContext());
            LinearLayout.LayoutParams parms = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            layout.setOrientation(LinearLayout.HORIZONTAL);
            layout.setLayoutParams(parms);
            layout.setPadding(10, 10, 10, 10);
            mButtonChoose = new Button(getContext());
            mButtonChoose.setGravity(Gravity.CENTER);
            mButtonChoose.setText("CHỌN");
            mButtonChoose.setTextSize(15);
            mButtonCancel = new Button(getContext());
            mButtonCancel.setGravity(Gravity.CENTER);
            mButtonCancel.setTextSize(15);
            mButtonCancel.setText("HỦY");
            LinearLayout.LayoutParams buttonLayoutParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            buttonLayoutParams.setMargins(0, 0, 10, 0);
            layout.addView(mButtonCancel, buttonLayoutParams);
            layout.addView(mButtonChoose, buttonLayoutParams);
            builder.setView(layout);
            mAlertDialog = builder.create();
            mButtonChoose.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    mAlertDialog.dismiss();
                }
            });
            mButtonCancel.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    mAlertDialog.cancel();
                }
            });
            mAlertDialog.show();
        } catch (Exception e) {
        }

        return true;
    }

    public void setSpinnerTitleDialog(String title) {
        this.titleDialog = title;
    }

    private View setViewTitleDialog(String titleDialog, final AlertDialog.Builder builder) {
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        View parent = layoutInflater.inflate(R.layout.entry_ticket_dialog_title, null, false);
        TextView textView = (TextView) parent.findViewById(R.id.tm_tv_dialog_title);
        checkBox = (CheckBox) parent.findViewById(R.id.tm_cb_dialog_title);
        textView.setText(titleDialog);
        return parent;
    }

    @Override
    public void setAdapter(SpinnerAdapter adapter) {
        throw new RuntimeException("setAdapter is not supported by MultiSelectSpinner.");
    }
    private final class PrivateMultiChoice implements DialogInterface.OnMultiChoiceClickListener {

        @Override
        public void onClick(DialogInterface dialog, int which, boolean isChecked) {
            // đánh dấu đang chọn 1 trong các checkbox con.
            Toast.makeText(getContext(),"Selected"+String.valueOf(which), Toast.LENGTH_SHORT).show();
        }
    }
}
