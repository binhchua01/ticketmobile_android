package example.com.demo;

/**
 * Created by Administrator on 10/25/2016.
 */
public class SpinnerObject {

    private String Key;
    private String Value;
    private String IsEffect;

    public String getKey() {
        return Key;
    }

    public void setKey(String key) {
        Key = key;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }

    public String getIsEffect() {
        return IsEffect;
    }

    public void setIsEffect(String isEffect) {
        IsEffect = isEffect;
    }

    public SpinnerObject() {
    }

    public SpinnerObject(String key, String value) {
        Key = key;
        Value = value;
    }

    public SpinnerObject(String key, String value, String isEffect) {
        Key = key;
        Value = value;
        IsEffect = isEffect;
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof SpinnerObject && getKey().equals(((SpinnerObject) o).getKey().toString()) && getValue().equals(((SpinnerObject) o).getValue());
    }
}
