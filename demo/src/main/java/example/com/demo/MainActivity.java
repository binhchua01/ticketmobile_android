package example.com.demo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private List<SpinnerObject> mServiceTypeList = new ArrayList<>();
    private SpinnerObject mServiceTypeObject = null;
    private Spinner mSpinnerTicketSupportRegionServiceType;
    private SpinnerObject mLocationAreaObject = null;
    private MultiSelectionObjectSpinner multiSelectionObjectSpinner;
    private final List<SpinnerObject> mListSpinner = new ArrayList<>();
    private Button bt_onclick;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        for (int i = 0; i < 20; i++) {
            mServiceTypeObject = new SpinnerObject(String.valueOf(i), getString(R.string.tm_choose_service_type) + ":" + i);
            mServiceTypeList.add(mServiceTypeObject);
        }
        mLocationAreaObject = new SpinnerObject("0","Hà Nội");
        mListSpinner.add(mLocationAreaObject);
        mListSpinner.add(new SpinnerObject("1","Trà Vinh"));
        mListSpinner.add(new SpinnerObject("2","Vĩnh Long"));
        mListSpinner.add(new SpinnerObject("3","Bạc Liêu"));
        mListSpinner.add(new SpinnerObject("4","Tiền Giang"));
        mListSpinner.add(new SpinnerObject("5","Long An"));
        mListSpinner.add(new SpinnerObject("6","Cần Thơ"));
        mListSpinner.add(new SpinnerObject("7","Vũng Tàu"));
        mListSpinner.add(new SpinnerObject("8","Cà Mau"));
        mSpinnerTicketSupportRegionServiceType = (Spinner) findViewById(R.id.tm_spinner_ticket_support_region_service_type);
        multiSelectionObjectSpinner = (MultiSelectionObjectSpinner)findViewById(R.id.tm_spinner_ticket_support_region_location);
        multiSelectionObjectSpinner.setSpinnerTitleDialog("Chọn Tỉnh Thành");
        multiSelectionObjectSpinner.setSpinnerMultiAdapter(mLocationAreaObject,mListSpinner,true);
        bt_onclick = (Button) findViewById(R.id.bt_onclick);
        bt_onclick.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SpinnerObject spinner = (SpinnerObject) mSpinnerTicketSupportRegionServiceType.getSelectedItem();
                Toast.makeText(getBaseContext(), spinner.getKey(), Toast.LENGTH_SHORT).show();
            }
        });
        mSpinnerTicketSupportRegionServiceType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // your code here
                Toast.makeText(getBaseContext(),"Item on Change:"+mServiceTypeList.get(position).getKey()+":"+mServiceTypeList.get(position).getValue(), Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        MySpinnerAdapter adapter = new MySpinnerAdapter(this, R.layout.textview_spinner, mServiceTypeList);
        mSpinnerTicketSupportRegionServiceType.setAdapter(adapter);
    }
}
