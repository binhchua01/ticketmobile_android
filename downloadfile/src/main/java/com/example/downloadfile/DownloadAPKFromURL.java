package com.example.downloadfile;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by HCM.TUANTT14 on 7/6/2017.
 */

public class DownloadAPKFromURL extends AsyncTask<String, String, String> {
    // "/sdcard/download/";
    String path_file = Environment.getExternalStorageDirectory()
            + "/download/";
    String file_name = "abc.apk";
    String linkdownload = "";
    String temp;
    ProgressDialog pDialog;
    @Override
    protected void onPreExecute() {
        // TODO Auto-generated method stub
        super.onPreExecute();
        pDialog = new ProgressDialog(context);
        pDialog.setTitle("Cập nhật phiên bản mới!");
        pDialog.setMessage("Đang cập nhật...");
        pDialog.setIndeterminate(false);
        pDialog.setMax(100);
        pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        pDialog.setCancelable(false);
        pDialog.show();
    }

    @Override
    protected String doInBackground(String... params) {
        // TODO Auto-generated method stub
        int count;
        int progress = 0;
        try {
            URL url = new URL(params[0]);
            URLConnection conection = url.openConnection();
            conection.connect();
            // Check folder
            File folder = new File(
                    Environment.getExternalStorageDirectory() + "/download");
            if (!folder.exists()) {
                folder.mkdir();
            }
            // Get file name
            file_name = url.getFile().substring(
                    url.getFile().lastIndexOf('/') + 1,
                    url.getFile().length());
            // Check exists file
            File file = new File(path_file, file_name);
            if (file.exists()) {
                file.delete();
            }
            try {
                // getting file length
                int lenghtOfFile = conection.getContentLength();
                // input stream to read file - with 8k buffer
                InputStream input = new BufferedInputStream(
                        url.openStream(), 8192);
                // Output stream to write file
                OutputStream output = new FileOutputStream(path_file
                        + file_name);
                byte data[] = new byte[1024];
                long total = 0;
                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    progress = (int) ((total * 100) / lenghtOfFile);
                    publishProgress("" + progress);
                    // writing data to file
                    output.write(data, 0, count);
                }
                // flushing output
                output.flush();
                // closing streams
                output.close();
                input.close();

            } catch (IOException ex) {
                ex.printStackTrace();
                temp = ex.getMessage().toString() + "ioconnect";
                return "DISCONNET_INTERNET";

            }

        } catch (Exception e) {
            temp = e.getMessage().toString() + "exception all";
            return "DISCONNET_INTERNET";
        }
        temp = "ok";
        return null;
    }

    @Override
    protected void onProgressUpdate(String... values) {
        // TODO Auto-generated method stub
        super.onProgressUpdate(values);
        pDialog.setProgress(Integer.parseInt(values[0]));
    }

    @Override
    protected void onPostExecute(String result) {
        // TODO Auto-generated method stub
        super.onPostExecute(result);
        // hide dialog
        pDialog.dismiss();
        if (result != null && result.equals("DISCONNET_INTERNET")) {
            return;
        }
        // Install app
        try {
//            // cai dat file apk moi download
//            Intent intent = new Intent(Intent.ACTION_VIEW);
//            intent.setDataAndType(
//                    Uri.fromFile(new File(path_file + file_name)),
//                    "application/vnd.android.package-archive");
//            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            startActivity(intent);
        } catch (Exception ex) {
        }
    }
}
