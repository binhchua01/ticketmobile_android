package app.fpt.ticketmobile.app.ui.adapter.libs;

import android.view.ViewGroup;

import java.util.HashMap;
import java.util.Map;

import app.fpt.ticketmobile.R;

/**
 * Created by Administrator on 7/12/2016.
 */
public class AdapterDelegatesManager<T, ADT, VH> {

    private final Binder<T, ADT, VH> mBinder;
    private final Map<Integer, AdapterDelegate<? extends  ADT, ? extends VH>> mViewTypeDelegateMap;

    public static abstract interface Binder<T, ADT, VH>{
        public abstract int getViewType(T t);

        public abstract void onBindViewHolder(T t, VH vh, AdapterDelegate<ADT, VH> adapterDelegate);
    }

    public AdapterDelegatesManager(Binder<T, ADT, VH> binder){
        this.mBinder = binder;
        this.mViewTypeDelegateMap = new HashMap<>();
    }

    public AdapterDelegatesManager addDelegate(AdapterDelegate<? extends ADT, ? extends VH> delegate){
        return adapterDelegate(delegate, false);
    }

    public AdapterDelegatesManager adapterDelegate(AdapterDelegate<? extends ADT, ? extends VH> delegate, boolean allowReplacingDelegate) {
        int viewType = delegate.getItemViewType();
        if (allowReplacingDelegate || mViewTypeDelegateMap.get(Integer.valueOf(viewType)) == null){
            mViewTypeDelegateMap.put(Integer.valueOf(viewType), delegate);
            return this;
        }
        throw new IllegalArgumentException("An AdapterDelegate is already registered for the viewType = " + viewType);
    }

    public AdapterDelegatesManager removeDelegate(AdapterDelegate<? extends ADT, ? extends VH> delegate){
        AdapterDelegate<? extends ADT, ? extends VH> existing = mViewTypeDelegateMap.get(Integer.valueOf(delegate.getItemViewType()));

        if (existing != null && existing == delegate){
            mViewTypeDelegateMap.remove(delegate.getItemViewType());
        }
        return this;
    }

    public AdapterDelegatesManager removeDelegate(int viewType){
        mViewTypeDelegateMap.remove(Integer.valueOf(viewType));
        return this;
    }

    public VH onCreateViewHolder(ViewGroup parent, int viewType){
        AdapterDelegate<?, ? extends VH> delegate = (AdapterDelegate) this.mViewTypeDelegateMap.get(Integer.valueOf(viewType));
        if (delegate != null) {
            return delegate.onCreateViewHolder(parent);
        }
        throw new NullPointerException("No AdapterDelegate added for ViewType " + viewType);
    }

    public void onBindViewHolder(T item, VH viewHolder){
        int viewType = this.mBinder.getViewType(item);
        AdapterDelegate<ADT, VH> delegate = (AdapterDelegate) this.mViewTypeDelegateMap.get(Integer.valueOf(viewType));
        if (delegate != null) {
            this.mBinder.onBindViewHolder(item, viewHolder, delegate);
            return;
        }
        throw new NullPointerException("No AdapterDelegate added for ViewType " + viewType);

    }
}
