package app.fpt.ticketmobile.app.lib;

/**
 * Created by ToanMaster on 6/14/2016.
 */
public interface TmConnectivityInfo {

    boolean isActiveNetworkMetered();

    boolean isInterfaceConnected();

}
