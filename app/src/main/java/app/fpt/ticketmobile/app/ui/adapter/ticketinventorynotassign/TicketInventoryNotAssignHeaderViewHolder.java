package app.fpt.ticketmobile.app.ui.adapter.ticketinventorynotassign;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ViewHolder.ParentViewHolder;

import app.fpt.ticketmobile.R;

/**
 * Created by Administrator on 7/1/2016.
 */
public class TicketInventoryNotAssignHeaderViewHolder extends ParentViewHolder {

    private static final float INITIAL_POSITION = 0.0f;
    private static final float ROTATED_POSITION = 180f;

    private final ImageView mExpandToggle;
    private TextView mTicketInventoryHeaderTittle;

    public TicketInventoryNotAssignHeaderViewHolder(View parentView) {
        super(parentView);
        mExpandToggle = (ImageView) parentView.findViewById(R.id.tm_expand_toggle);
        mTicketInventoryHeaderTittle = (TextView) parentView.findViewById(R.id.tm_header_tittle);
    }

    public void bind(TicketInventoryNotAssignHeader ticketInventoryNotAssignHeader){
        mTicketInventoryHeaderTittle.setText(ticketInventoryNotAssignHeader.getQueueName() + " (" + ticketInventoryNotAssignHeader.getTicketInventoryNotAssignChildList().size()+")");
    }

    @Override
    public void setExpanded(boolean expanded) {
        super.setExpanded(expanded);
    }

    @Override
    public void onExpansionToggled(boolean expanded) {
        super.onExpansionToggled(expanded);
    }
}
