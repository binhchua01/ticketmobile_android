package app.fpt.ticketmobile.app.di.module;

import android.content.Context;

import com.squareup.okhttp.OkHttpClient;


import app.fpt.ticketmobile.app.di.module.internal.TmApiEndPointProvideImpl;
import app.fpt.ticketmobile.app.lib.TmApiEndPointProvider;
import app.fpt.ticketmobile.app.networking.utils.OkHttpStack;

/**
 * Created by ToanMaster on 6/14/2016.
 */
public class NetworkModule {

//    public OkHttpClient provideOkHttpClient(){
//        OkHttpClient okHttpClient = new OkHttpClient();
//        okHttpClient.setConnectTimeout(15000, TimeUnit.MILLISECONDS);
//        okHttpClient.setReadTimeout(15000, TimeUnit.MILLISECONDS);
//        okHttpClient.setReadTimeout(15000, TimeUnit.MILLISECONDS);
//        return okHttpClient;
//    }

    public OkHttpStack provideOkHttpStack(){
        OkHttpStack okHttpStack = new OkHttpStack(new OkHttpClient());
        return okHttpStack;
    }

    public TmApiEndPointProvider provideTmApiEndPointProvider(Context context, OkHttpStack okHttpStack){
        return new TmApiEndPointProvideImpl(context, okHttpStack);
    }





}
