package app.fpt.ticketmobile.app.jsonParsers.Fortype;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import app.fpt.ticketmobile.app.model.MyTicketFeedbackDetailObject;

/**
 * Created by Administrator on 7/4/2016.
 */
public class MyTicketFeedbackDetailField {

    public static final class Deserializer implements JsonDeserializer<MyTicketFeedbackDetailObject>{

        @Override
        public MyTicketFeedbackDetailObject deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            Gson gson = new Gson();
            MyTicketFeedbackDetailObject feedbackDetailObject = null;

            try{
                JsonObject jsObj = json.getAsJsonObject();
                JsonObject jsResult = jsObj.getAsJsonObject("Result");

                if (jsResult.get("Data") instanceof JsonArray) {
                    JsonObject jsonFeedbackDetail = (JsonObject) jsResult.getAsJsonArray("Data").get(0);
                    feedbackDetailObject = gson.fromJson(jsonFeedbackDetail, MyTicketFeedbackDetailObject.class);

                }

            }catch(Exception e){
                Log.e("tmt", "MyTicketFeedbackDetailField : Deserializer : " + e.toString());
            }

            return feedbackDetailObject;
        }
    }
}
