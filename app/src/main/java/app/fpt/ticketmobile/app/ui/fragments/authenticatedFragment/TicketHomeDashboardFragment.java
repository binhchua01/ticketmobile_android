package app.fpt.ticketmobile.app.ui.fragments.authenticatedFragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.squareup.otto.Subscribe;

import app.fpt.ticketmobile.R;
import app.fpt.ticketmobile.app.eventBus.event.DashboardAccountEvent;
import app.fpt.ticketmobile.app.eventBus.event.DashboardTicketPrimaryFunctionEvent;
import app.fpt.ticketmobile.app.eventBus.event.DashboardTicketSupportEvent;
import app.fpt.ticketmobile.app.eventBus.event.DashbroadChangeItemBackground;
import app.fpt.ticketmobile.app.eventBus.event.DashbroadTicketSettingEvent;
import app.fpt.ticketmobile.app.model.User;
import app.fpt.ticketmobile.app.ui.adapter.HomeDashboardAdapter;
import app.fpt.ticketmobile.app.ui.fragments.TmBaseFragment;
import app.fpt.ticketmobile.app.ui.lib.TicketPrimaryFunction;
import app.fpt.ticketmobile.app.ui.lib.TicketSettingInfo;
import app.fpt.ticketmobile.app.ui.lib.TicketSupport;
import app.fpt.ticketmobile.widget.decorators.DividerItemDecoration;

/**
 * Created by Administrator on 7/7/2016.
 */
public class TicketHomeDashboardFragment extends TmBaseFragment<TicketHomeDashboardFragment.Callback> implements HomeDashboardAdapter.HomeDashboardCallback {


    private HomeDashboardAdapter mHomeDashboardAdapter;

    public interface Callback{

    }

    public TicketHomeDashboardFragment() {
        super(Callback.class, R.layout.fragment_ticket_home_dashboard, false, false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    protected void onTmActivityCreated(View parent, Bundle savedInstanceState) {
        super.onTmActivityCreated(parent, savedInstanceState);
        setupViews(parent);
    }

    private void setupViews(View parent) {
        mHomeDashboardAdapter = new HomeDashboardAdapter(getActivity(), getTmGraphics(), TicketPrimaryFunction.TICKET_INVENTORY_NOT_ASSIGN, this);
        RecyclerView recyclerView = (RecyclerView) parent.findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), 1, false));
        recyclerView.setAdapter(this.mHomeDashboardAdapter);


    }

    @Override
    public void onSelectionTicketPrimaryFunction(TicketPrimaryFunction ticketPrimaryFunction, String title) {
        getEventBus().post(new DashboardTicketPrimaryFunctionEvent(ticketPrimaryFunction, title));
    }

    @Override
    public void onSelectionTicketSupport(TicketSupport ticketSupport, String title) {
        getEventBus().post(new DashboardTicketSupportEvent(ticketSupport, title));
    }

    @Override
    public void onSelectionTicketSetting(TicketSettingInfo ticketSettingInfo, String title) {
        getEventBus().post(new DashbroadTicketSettingEvent(ticketSettingInfo, title));
    }

    @Override
    public void onSelectionAccount(User user) {
        getEventBus().post(new DashboardAccountEvent(user));
    }

    @Override
    public void onDataSetChanged() {
    }


    @Subscribe
    public void onDashbroadChangeItemBackground(DashbroadChangeItemBackground dashBroadChangeMenu){
        mHomeDashboardAdapter.onSetChangeMenuItemSupport(dashBroadChangeMenu);
    }


}
