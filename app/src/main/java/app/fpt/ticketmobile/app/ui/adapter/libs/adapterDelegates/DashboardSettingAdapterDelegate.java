package app.fpt.ticketmobile.app.ui.adapter.libs.adapterDelegates;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import app.fpt.ticketmobile.app.ui.adapter.libs.utils.ViewUtils;
import app.fpt.ticketmobile.R;
import app.fpt.ticketmobile.app.lib.Provider;
import app.fpt.ticketmobile.app.lib.TmGraphics;
import app.fpt.ticketmobile.app.ui.adapter.libs.DashGroupSettingInfo;
import app.fpt.ticketmobile.app.ui.lib.TicketSettingInfo;
import app.fpt.ticketmobile.widget.views.CircularImageView;

/**
 * Created by Administrator on 7/28/2016.
 */
public class DashboardSettingAdapterDelegate extends TmAdapterDelegates<DashGroupSettingInfo, DashboardSettingAdapterDelegate.DashboardSettingViewHolder> {

    private final TicketSettingCallback mCallback;
    private final TmGraphics mTmGraphics;
    private Provider<TicketSettingInfo> mSelectedTicketSettingProvider = null;

    public interface TicketSettingCallback{
        void onTicketSettingSelected(TicketSettingInfo ticketSettingInfo, String title);
    }

    public DashboardSettingAdapterDelegate(int itemViewType, TmGraphics tmGraphics, Provider<TicketSettingInfo> selectedTicketSettingProvider, TicketSettingCallback callback) {
        super(itemViewType);
        this.mTmGraphics = tmGraphics;
        this.mSelectedTicketSettingProvider = selectedTicketSettingProvider;
        this.mCallback = callback;
    }

    @Override
    public void onBindViewHolder(DashGroupSettingInfo groupTicketSettingInfo, DashboardSettingViewHolder holder) {
        holder.bind(groupTicketSettingInfo.getTicketSetting(), groupTicketSettingInfo.getTicketSettingTitle());
    }

    @Override
    public DashboardSettingViewHolder onCreateViewHolder(ViewGroup parent) {
        return new DashboardSettingViewHolder(ViewUtils.inflate(R.layout.entry_dashboard_ticket_support, parent, false));
    }


    public final class DashboardSettingViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private final TextView mTicketSupportTitle;
        private final CircularImageView mTicketSupportImage;
        private TicketSettingInfo mTicketSetting;
        private String mTitle;

        public DashboardSettingViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            mTicketSupportTitle = (TextView) itemView.findViewById(R.id.group_ticket_primary_function_title);
            mTicketSupportImage = (CircularImageView) itemView.findViewById(R.id.group_ticket_primary_function_image);
        }

        public void bind(TicketSettingInfo ticketSetting, String title) {
            this.mTitle = title;
            this.mTicketSetting = ticketSetting;
            if (mSelectedTicketSettingProvider.provider() != null) {
                boolean isSelectedGroup = ((TicketSettingInfo) mSelectedTicketSettingProvider.provider()).equals(ticketSetting);
                this.itemView.setActivated(isSelectedGroup);
            }else {
                this.itemView.setActivated(false);
            }
            this.mTicketSupportImage.setImageBitmap(mTmGraphics.getGroupTicketSettingBitmap(ticketSetting));
            this.mTicketSupportTitle.setText(title);
        }

        @Override
        public void onClick(View v) {
            v.setActivated(true);
            mCallback.onTicketSettingSelected(mTicketSetting, mTitle);
        }
    }
}
