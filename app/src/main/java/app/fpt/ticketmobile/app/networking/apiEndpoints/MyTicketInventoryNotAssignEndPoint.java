package app.fpt.ticketmobile.app.networking.apiEndpoints;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;

import java.util.HashMap;
import java.util.Map;

import app.fpt.ticketmobile.app.model.MyTicketInventoryNotAssignObject;
import app.fpt.ticketmobile.app.networking.utils.GsonRequest;

/**
 * Created by ToanMaster on 7/1/2016.
 */
public class MyTicketInventoryNotAssignEndPoint {

    private static Map<String, String> getHeader(String userId){
        Map<String, String>   params = new HashMap<String, String>();
        params.put("UserID", userId);
        return params;
    }

    public static GsonRequest<MyTicketInventoryNotAssignObject> getNewTicketInventory(final String userId, Response.Listener<MyTicketInventoryNotAssignObject> listener, Response.ErrorListener errorListener){
        GsonRequest<MyTicketInventoryNotAssignObject> gsonRequest = new GsonRequest<MyTicketInventoryNotAssignObject>(Constants.MY_GET_TICKET_INVENTORY_NOT_ASSIGN_END_POINT, MyTicketInventoryNotAssignObject.class, listener, errorListener){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return getHeader(userId);
            }
        };
        return gsonRequest;
    }
}
