package app.fpt.ticketmobile.app.ui.fragments.authenticatedFragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

import java.util.ArrayList;
import java.util.List;

import app.fpt.ticketmobile.app.model.support.MyTicketSupportOfUserCreatedObject;
import app.fpt.ticketmobile.app.ui.adapter.support.TicketSupportOfMeCreatedHeader;
import app.fpt.ticketmobile.R;
import app.fpt.ticketmobile.app.networking.apiEndpoints.MyTicketSupportEndPoint;
import app.fpt.ticketmobile.app.ui.adapter.TicketSupportOfMeCreatedAdapter;
import app.fpt.ticketmobile.app.ui.fragments.TmBaseFragment;
import app.fpt.ticketmobile.app.ui.lib.FragmentRefreshBasedSwipeRefreshLayoutHandler;
import app.fpt.ticketmobile.widget.decorators.DividerItemDecoration;

/**
 * Created by Administrator on 7/14/2016.
 */
public class TicketSupportOfMeCreatedFragment extends TmBaseFragment<TicketSupportOfMeCreatedFragment.Callback> implements SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG_MY_TICKET_OF_USER_CREATED_KEY = "tag_my_ticket_of_me_created_key";
    private RecyclerView mListTicketSupportOfMeCreated;
    private TicketSupportOfMeCreatedAdapter ticketSupportOfMeCreatedAdapter;
    private List<TicketSupportOfMeCreatedHeader> mTicketSupportOfUserCreatedHeaderList;

    public interface Callback {

    }

    public TicketSupportOfMeCreatedFragment() {
        super(Callback.class, Integer.valueOf(R.layout.fragment_ticket_support_of_me), false, true);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mTicketSupportOfUserCreatedHeaderList = new ArrayList<>();
    }

    @Override
    protected void onTmActivityCreated(View parent, Bundle savedInstanceState) {
        super.onTmActivityCreated(parent, savedInstanceState);
        setupSwipeRefreshLayoutView(parent, savedInstanceState);
    }

    private void setupSwipeRefreshLayoutView(View parent ,  Bundle savedInstanceState) {

        SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) parent.findViewById(R.id.swipe_refresh_layout);
        setFragmentRefreshAnimationHandler(new FragmentRefreshBasedSwipeRefreshLayoutHandler(swipeRefreshLayout, this, Integer.valueOf(R.color.tm_swipe_refresh_color)));
        mListTicketSupportOfMeCreated = (RecyclerView) parent.findViewById(R.id.list);
        mListTicketSupportOfMeCreated.setLayoutManager(new LinearLayoutManager(getActivity(), 1, false));
        mListTicketSupportOfMeCreated.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.ContainerType.VERTICAL_LIST));
        ticketSupportOfMeCreatedAdapter = new TicketSupportOfMeCreatedAdapter(getActivity(), mTicketSupportOfUserCreatedHeaderList);
        mListTicketSupportOfMeCreated.setAdapter(ticketSupportOfMeCreatedAdapter);
        // recyclerView.setAdapter();

        if (!isShowingRefreshAnimation()) {
            swipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    showRefreshAnimation();
                }
            }, 500);
        }
        ticketSupportOfMeCreatedAdapter.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        ticketSupportOfMeCreatedAdapter.onSaveInstanceState(savedInstanceState);
    }


    @Override
    public void onRefresh() {
        autoSynced();
    }


    @Override
    protected void autoSynced() {
        super.autoSynced();
        if (!isShowingRefreshAnimation()) {
            showRefreshAnimation();
        }

        getTmApiEndPointProvider().addRequest(MyTicketSupportEndPoint.getTicketSupportCreatedList(getTmSharePreferencesUtils().getEmailUserLogin(), new Response.Listener<MyTicketSupportOfUserCreatedObject>() {
            @Override
            public void onResponse(MyTicketSupportOfUserCreatedObject ticketCreatedObject) {
                if (getActivity() != null) {
                    if (ticketCreatedObject != null && ticketCreatedObject.getTicketSupportOfUserCreatedHeaderList() != null) {
                        if (ticketCreatedObject.getTicketSupportOfUserCreatedHeaderList().size() > 0) {
                            ticketSupportOfMeCreatedAdapter.clear();
                            mTicketSupportOfUserCreatedHeaderList = ticketCreatedObject.getTicketSupportOfUserCreatedHeaderList();
                            ticketSupportOfMeCreatedAdapter = new TicketSupportOfMeCreatedAdapter(getActivity(), mTicketSupportOfUserCreatedHeaderList);
                            mListTicketSupportOfMeCreated.setAdapter(ticketSupportOfMeCreatedAdapter);
                            mListTicketSupportOfMeCreated.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

                        } else {
                            setLayoutNoData(R.layout.fragment_no_data);
                        }
                    } else {
                        setLayoutNoData(R.layout.fragment_no_data);
                    }
                    stopRefreshAnimation();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onErrorListener(error);

            }
        }), TAG_MY_TICKET_OF_USER_CREATED_KEY);
    }

    @Override
    public void onStop() {
        super.onStop();
        getTmApiEndPointProvider().cancelAllRequest(TAG_MY_TICKET_OF_USER_CREATED_KEY);
    }

    private void onErrorListener(VolleyError error) {
        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
            getTmToast().makeToast(getString(R.string.tm_error_connect), Toast.LENGTH_SHORT);
        } else if (error instanceof AuthFailureError) {
            Log.d("tmt", "AuthFailureError");
        } else if (error instanceof ServerError) {
            Log.d("tmt", "ServerError");
        } else if (error instanceof NetworkError) {
            Log.d("tmt", "NetworkError");
        } else if (error instanceof ParseError) {
            Log.d("tmt", "ParseError");
        } else {
            Log.d("tmt", "onErrorResponse : " + error.getMessage());
        }
        stopRefreshAnimation();
    }

    private void setLayoutNoData(Integer layoutResId) {
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewGroup rootView = (ViewGroup) getView();
        View mainView = inflater.inflate(layoutResId.intValue(), rootView, false);
        rootView.removeAllViews();
        rootView.addView(mainView);
        SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) mainView.findViewById(R.id.swipe_refresh_layout);
        setFragmentRefreshAnimationHandler(new FragmentRefreshBasedSwipeRefreshLayoutHandler(swipeRefreshLayout, this, Integer.valueOf(R.color.tm_swipe_refresh_color)));

    }
}
