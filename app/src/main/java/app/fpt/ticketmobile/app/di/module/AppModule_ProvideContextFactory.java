package app.fpt.ticketmobile.app.di.module;

import android.content.Context;

import dagger.internal.Factory;

/**
 * Created by ToanMaster on 6/14/2016.
 */
public class AppModule_ProvideContextFactory implements Factory<Context> {
    private static final boolean $assertionDisabled = !AppModule_ProvideContextFactory.class.desiredAssertionStatus();
    private AppModule appModule;

    public AppModule_ProvideContextFactory(AppModule appModule){
        if ($assertionDisabled || appModule != null){
            this.appModule = appModule;
            return;
        }
        throw new AssertionError();
    }

    @Override
    public Context get() {

        Context provideContext = appModule.provideContext();
        if (provideContext != null){
            return provideContext;
        }
       throw new NullPointerException("Cannot return null from a non-@Nullable @Provides method");
    }

    public static Factory<Context> create(AppModule appModule){
        return new AppModule_ProvideContextFactory(appModule);
    }
}
