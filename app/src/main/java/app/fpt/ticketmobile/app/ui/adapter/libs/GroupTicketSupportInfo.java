package app.fpt.ticketmobile.app.ui.adapter.libs;

import app.fpt.ticketmobile.app.ui.lib.TicketSupport;

/**
 * Created by ToanMaster on 7/12/2016.
 */
public class GroupTicketSupportInfo {
    private final TicketSupport mTicketSupport;
    private final String mTicketSupportTitle;

    public GroupTicketSupportInfo(TicketSupport mTicketSupport, String mTicketSupportTitle) {
        this.mTicketSupport = mTicketSupport;
        this.mTicketSupportTitle = mTicketSupportTitle;
    }

    public TicketSupport getTicketSupport() {
        return mTicketSupport;
    }

    public String getTicketSupportTitle() {
        return mTicketSupportTitle;
    }
}
