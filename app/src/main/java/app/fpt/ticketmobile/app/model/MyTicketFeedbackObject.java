package app.fpt.ticketmobile.app.model;

import java.util.List;

/**
 * Created by Administrator on 7/4/2016.
 */
public class MyTicketFeedbackObject {

    private List<FeedbackObject> Data;

    public List<FeedbackObject> getFeedbackObjectList() {
        return Data;
    }


    public class FeedbackObject{
        private String CreatedDate;
        private String FromAdr;
        private Integer ID;
        private String Subject;


        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String createdDate) {
            CreatedDate = createdDate;
        }

        public String getFromAdr() {
            return FromAdr;
        }

        public void setFromAdr(String fromAdr) {
            FromAdr = fromAdr;
        }

        public Integer getID() {
            return ID;
        }

        public void setID(Integer ID) {
            this.ID = ID;
        }

        public String getSubject() {
            return Subject;
        }

        public void setSubject(String subject) {
            Subject = subject;
        }
    }

}
