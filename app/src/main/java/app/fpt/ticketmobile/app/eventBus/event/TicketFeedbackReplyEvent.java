package app.fpt.ticketmobile.app.eventBus.event;

import app.fpt.ticketmobile.app.model.MyTicketFeedbackDetailObject;
import app.fpt.ticketmobile.app.eventBus.Event;

/**
 * Created by Administrator on 7/4/2016.
 */
public class TicketFeedbackReplyEvent implements Event {
    private MyTicketFeedbackDetailObject myTicketFeedbackDetailObject;

    public MyTicketFeedbackDetailObject getMyTicketFeedbackDetailObject() {
        return myTicketFeedbackDetailObject;
    }

    public void setMyTicketFeedbackDetailObject(MyTicketFeedbackDetailObject myTicketFeedbackDetailObject) {
        this.myTicketFeedbackDetailObject = myTicketFeedbackDetailObject;
    }

    public TicketFeedbackReplyEvent(MyTicketFeedbackDetailObject myTicketFeedbackDetailObject) {
        this.myTicketFeedbackDetailObject = myTicketFeedbackDetailObject;
    }
}
