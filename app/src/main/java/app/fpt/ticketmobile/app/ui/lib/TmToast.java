package app.fpt.ticketmobile.app.ui.lib;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by Administrator on 6/13/2016.
 */
public class TmToast {

    private final Context mContext;
    private String mLastMessage;
    private Toast mLastToast;

    public TmToast(Context context) {
        this.mLastToast = null;
        this.mLastMessage = null;
        this.mContext = context;
    }

    public void makeToast(int textResourceId, int duration) {
        makeToast(this.mContext.getString(textResourceId), duration);
    }

    public void makeToast(int textResourceId, int duration, Object... formatArgs) {
        makeToast(this.mContext.getString(textResourceId, formatArgs), duration);
    }

    public void makeToast(String message, int duration) {
        makeToast(message, duration, false);
    }

    public void makeToast(String message, int duration, boolean showImmediately) {
        if (showImmediately || canShowToast(message)) {
            if (this.mLastToast != null) {
                this.mLastToast.cancel();
            }
            this.mLastMessage = message;
            this.mLastToast = Toast.makeText(this.mContext, message, duration);
            this.mLastToast.show();
        }
    }

    private boolean canShowToast(String message) {
        return (this.mLastMessage != null && this.mLastMessage.equals(message) && isToastBeingShown()) ? false : true;
    }

    private boolean isToastBeingShown() {
        return this.mLastToast.getView() != null && this.mLastToast.getView().isShown();
    }
}
