package app.fpt.ticketmobile.app.jsonParsers.Fortype;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import app.fpt.ticketmobile.app.model.SpinnerObject;
import app.fpt.ticketmobile.app.model.MyTicketQueueObject;

/**
 * Created by Administrator on 6/29/2016.
 */
public class MyTicketQueueField {

    public final static class Deserializer implements JsonDeserializer<MyTicketQueueObject>{

        @Override
        public MyTicketQueueObject deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            Gson gson = new Gson();
            MyTicketQueueObject myTicketQueueStatusObject = new MyTicketQueueObject();
            List<SpinnerObject> spinnerObjectList = new ArrayList<>();

            try{
                JsonObject jsObj = json.getAsJsonObject();
                JsonObject jsonObject = jsObj.getAsJsonObject("Result");
                myTicketQueueStatusObject = gson.fromJson(jsonObject, MyTicketQueueObject.class);

                if (myTicketQueueStatusObject.getData() != null && myTicketQueueStatusObject.getData().size() > 0) {
                    for (int i = 0; i < myTicketQueueStatusObject.getData().size(); i++){
                        MyTicketQueueObject.QueueObject queueObject = myTicketQueueStatusObject.getData().get(i);
                        SpinnerObject spinnerObject = new SpinnerObject(queueObject.getID(), queueObject.getName());
                        spinnerObjectList.add(spinnerObject);
                    }
                    myTicketQueueStatusObject.setSpinnerObjectList(spinnerObjectList);
                }

            }catch(Exception ex){
                Log.e("tmt", "MyTicketQueueStatusField : Deserializer " + ex.toString());
            }
            return myTicketQueueStatusObject;
        }
    }
}
