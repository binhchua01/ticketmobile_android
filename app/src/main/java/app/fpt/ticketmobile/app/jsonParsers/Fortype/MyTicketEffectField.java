package app.fpt.ticketmobile.app.jsonParsers.Fortype;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import app.fpt.ticketmobile.app.model.SpinnerObject;
import app.fpt.ticketmobile.app.model.MyTicketEffectObject;

/**
 * Created by ToanMaster on 6/28/2016.
 */
public class MyTicketEffectField {

    public static final class Deserializer implements JsonDeserializer<MyTicketEffectObject>{

        @Override
        public MyTicketEffectObject deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            Gson gson = new Gson();
            MyTicketEffectObject myTicketEffectObject = new MyTicketEffectObject();
            List<SpinnerObject> spinnerObjectList = new ArrayList<>();

            try{
                JsonObject jsObj = json.getAsJsonObject();
                JsonObject jsonObject = jsObj.getAsJsonObject("Result");

                myTicketEffectObject = gson.fromJson(jsonObject, MyTicketEffectObject.class);

                if (myTicketEffectObject.getData() != null && myTicketEffectObject.getData().size() > 0) {
                    for (int i = 0; i < myTicketEffectObject.getData().size(); i++) {
                        MyTicketEffectObject.EffectObject effectObject = myTicketEffectObject.getData().get(i);
                        SpinnerObject spinnerObject = new SpinnerObject(String.valueOf(effectObject.getEffectID()), effectObject.getEffectName(), String.valueOf(effectObject.getIsEffect()));
                        spinnerObjectList.add(spinnerObject);
                    }
                    myTicketEffectObject.setSpinnerObjectList(spinnerObjectList);
                }

            }catch (Exception e){
                Log.e("tmt", "MyTicketEffectField : " + e.toString());
            }

            return myTicketEffectObject;
        }
    }

}
