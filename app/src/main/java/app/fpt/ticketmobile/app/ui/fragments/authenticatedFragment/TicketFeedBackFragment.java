package app.fpt.ticketmobile.app.ui.fragments.authenticatedFragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

import app.fpt.ticketmobile.app.eventBus.event.TicketFeedbackDetailEvent;
import app.fpt.ticketmobile.app.model.MyTicketFeedbackObject;
import app.fpt.ticketmobile.app.networking.apiEndpoints.MyTicketResponseEndpoint;
import app.fpt.ticketmobile.app.ui.activities.authenticatedActivities.TicketFeedBackDetailActivity;
import app.fpt.ticketmobile.app.ui.adapter.MyTicketFeedbackAdapter;
import app.fpt.ticketmobile.app.ui.lib.FragmentRefreshBasedSwipeRefreshLayoutHandler;
import app.fpt.ticketmobile.R;
import app.fpt.ticketmobile.app.ui.fragments.TmBaseFragment;

/**
 * Created by Administrator on 7/4/2016.
 */
public class TicketFeedBackFragment extends TmBaseFragment<TicketFeedBackFragment.Callback> implements SwipeRefreshLayout.OnRefreshListener{

    private static final String TAG_MY_TICKET_FEED_BACK_KEY = "tag_my_ticket_feed_back_key";
    private static final String EXTRA_MY_TICKET_ID_KEY = "extra_my_ticket_id_key";

    private RecyclerView mListTicketFeedback;
    private MyTicketFeedbackAdapter myTicketFeedbackAdapter;
    private String TICKET_ID = null;

    private List<MyTicketFeedbackObject.FeedbackObject> mFeedbackObjectList;

    public interface Callback{

    }

    public TicketFeedBackFragment() {
        super(Callback.class, Integer.valueOf(R.layout.fragment_ticket_feedback), false , true);
    }

    public static TicketFeedBackFragment getInstances(String ticketId){
        Bundle bundle = new Bundle();
        bundle.putString(EXTRA_MY_TICKET_ID_KEY, ticketId);
        TicketFeedBackFragment ticketFeedBackFragment = new TicketFeedBackFragment();
        ticketFeedBackFragment.setArguments(bundle);
        return ticketFeedBackFragment;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mFeedbackObjectList = new ArrayList<>();
        extrasArguments();
    }

    @Override
    protected void onTmActivityCreated(View parent, Bundle savedInstanceState) {
        super.onTmActivityCreated(parent, savedInstanceState);
        mListTicketFeedback = (RecyclerView) parent.findViewById(R.id.list);
        mListTicketFeedback.setItemAnimator(new DefaultItemAnimator());
        myTicketFeedbackAdapter = new MyTicketFeedbackAdapter(getEventBus() ,mFeedbackObjectList);
        mListTicketFeedback.setAdapter(myTicketFeedbackAdapter);

        SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) parent.findViewById(R.id.swipe_refresh_layout);
        setFragmentRefreshAnimationHandler(new FragmentRefreshBasedSwipeRefreshLayoutHandler(swipeRefreshLayout, this, Integer.valueOf(R.color.tm_swipe_refresh_color)));
        if (!isShowingRefreshAnimation()) {
            swipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    showRefreshAnimation();
                }
            }, 500);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        autoSynced();
    }

    @Override
    public void onRefresh() {
        autoSynced();
    }

    @Override
    protected void autoSynced() {
        super.autoSynced();
        if (!isShowingRefreshAnimation()) {
            showRefreshAnimation();
        }

        if (TICKET_ID != null) {
            getTmApiEndPointProvider().addRequest(MyTicketResponseEndpoint.getMyTicketFeedbackList(TICKET_ID,

                    new Response.Listener<MyTicketFeedbackObject>() {
                        @Override
                        public void onResponse(final MyTicketFeedbackObject ticketFeedbackObject) {
                            if (getActivity() != null) {
                                if (ticketFeedbackObject != null && ticketFeedbackObject.getFeedbackObjectList() != null) {
                                    if (ticketFeedbackObject.getFeedbackObjectList().size() > 0) {
                                        mFeedbackObjectList.clear();
                                        mFeedbackObjectList = ticketFeedbackObject.getFeedbackObjectList();
                                        myTicketFeedbackAdapter = new MyTicketFeedbackAdapter(getEventBus(), mFeedbackObjectList);
                                        mListTicketFeedback.setAdapter(myTicketFeedbackAdapter);
                                        mListTicketFeedback.setLayoutManager(new LinearLayoutManager(getActivity()));
                                    } else {
                                        setLayoutNoData(R.layout.fragment_no_data);
                                    }

                                } else {
                                    setLayoutNoData(R.layout.fragment_no_data);
                                }

                                stopRefreshAnimation();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            onErrorListener(error);
                        }
                    }), TAG_MY_TICKET_FEED_BACK_KEY);
        }else{
            stopRefreshAnimation();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        getTmApiEndPointProvider().cancelAllRequest(TAG_MY_TICKET_FEED_BACK_KEY);
    }

    private void extrasArguments(){
        if (getArguments() != null){
            TICKET_ID = getArguments().getString(EXTRA_MY_TICKET_ID_KEY);
        }
    }


    private void onErrorListener(VolleyError error) {
        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
            getTmToast().makeToast(getString(R.string.tm_error_connect), Toast.LENGTH_SHORT);
        } else if (error instanceof AuthFailureError) {
            Log.d("tmt", "AuthFailureError");
        } else if (error instanceof ServerError) {
            Log.d("tmt", "ServerError");
        } else if (error instanceof NetworkError) {
            Log.d("tmt", "NetworkError");
        } else if (error instanceof ParseError) {
            Log.d("tmt", "ParseError");
        } else {
            Log.d("tmt", "onErrorResponse : " + error.getMessage());
        }
        stopRefreshAnimation();
    }

    private void setLayoutNoData(Integer layoutResId) {
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewGroup rootView = (ViewGroup) getView();
        View mainView = inflater.inflate(layoutResId.intValue(), rootView, false);
        rootView.removeAllViews();
        rootView.addView(mainView);
        SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) mainView.findViewById(R.id.swipe_refresh_layout);
        setFragmentRefreshAnimationHandler(new FragmentRefreshBasedSwipeRefreshLayoutHandler(swipeRefreshLayout, this, Integer.valueOf(R.color.tm_swipe_refresh_color)));
    }

    @Subscribe
    public void onEvent(TicketFeedbackDetailEvent ticketFeedbackDetailEvent){
        getActivity().startActivity(TicketFeedBackDetailActivity.buildIntent(getActivity(), TICKET_ID, ticketFeedbackDetailEvent.getTicketResponseId()));
    }
}
