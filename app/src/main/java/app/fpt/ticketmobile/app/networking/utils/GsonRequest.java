package app.fpt.ticketmobile.app.networking.utils;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.Map;

import app.fpt.ticketmobile.app.jsonParsers.Fortype.MyDeviceField;
import app.fpt.ticketmobile.app.jsonParsers.Fortype.MyResponseJsonField;
import app.fpt.ticketmobile.app.jsonParsers.Fortype.MyTicketCusTypeField;
import app.fpt.ticketmobile.app.jsonParsers.Fortype.MyTicketDetailField;
import app.fpt.ticketmobile.app.jsonParsers.Fortype.MyTicketEffectField;
import app.fpt.ticketmobile.app.jsonParsers.Fortype.MyTicketEstimatedDateField;
import app.fpt.ticketmobile.app.jsonParsers.Fortype.MyTicketFeedbackDetailField;
import app.fpt.ticketmobile.app.jsonParsers.Fortype.MyTicketFeedbackField;
import app.fpt.ticketmobile.app.jsonParsers.Fortype.MyTicketField;
import app.fpt.ticketmobile.app.jsonParsers.Fortype.MyTicketOfMeCreatedField;
import app.fpt.ticketmobile.app.jsonParsers.Fortype.MyTicketOperateStaffField;
import app.fpt.ticketmobile.app.jsonParsers.Fortype.MyTicketProcessStaffField;
import app.fpt.ticketmobile.app.jsonParsers.Fortype.MyTicketQueueField;
import app.fpt.ticketmobile.app.jsonParsers.Fortype.MyTicketReasonDetailField;
import app.fpt.ticketmobile.app.jsonParsers.Fortype.MyTicketReasonField;
import app.fpt.ticketmobile.app.jsonParsers.Fortype.MyTicketServiceTypeField;
import app.fpt.ticketmobile.app.jsonParsers.Fortype.MyTicketStatusField;
import app.fpt.ticketmobile.app.jsonParsers.Fortype.UserField;
import app.fpt.ticketmobile.app.jsonParsers.Fortype.UserIAMField;
import app.fpt.ticketmobile.app.jsonParsers.Fortype.checkversion.CheckVersionField;
import app.fpt.ticketmobile.app.jsonParsers.Fortype.support.MyOperateStaffSupportField;
import app.fpt.ticketmobile.app.jsonParsers.Fortype.support.MyProcessStaffSupportField;
import app.fpt.ticketmobile.app.jsonParsers.Fortype.support.MyQueueField;
import app.fpt.ticketmobile.app.jsonParsers.Fortype.support.MyTicketCreatedField;
import app.fpt.ticketmobile.app.jsonParsers.Fortype.support.MyTicketCustomTypeField;
import app.fpt.ticketmobile.app.jsonParsers.Fortype.support.MyTicketPriorityField;
import app.fpt.ticketmobile.app.jsonParsers.Fortype.support.MyTicketReasonSupportField;
import app.fpt.ticketmobile.app.jsonParsers.Fortype.support.MyTicketSupportServiceTypeField;
import app.fpt.ticketmobile.app.jsonParsers.Fortype.support.MyVisorStaffSupportField;
import app.fpt.ticketmobile.app.model.MyDeviceObject;
import app.fpt.ticketmobile.app.model.MyResponseObject;
import app.fpt.ticketmobile.app.model.MyTicketCusTypeObject;
import app.fpt.ticketmobile.app.model.MyTicketDetailObject;
import app.fpt.ticketmobile.app.model.MyTicketFeedbackDetailObject;
import app.fpt.ticketmobile.app.model.MyTicketFeedbackObject;
import app.fpt.ticketmobile.app.model.MyTicketInventoryNotAssignObject;
import app.fpt.ticketmobile.app.model.MyTicketInventoryUnFinishObject;
import app.fpt.ticketmobile.app.model.MyTicketIssueObject;
import app.fpt.ticketmobile.app.model.MyTicketObject;
import app.fpt.ticketmobile.app.model.MyTicketOfUserCreatedObject;
import app.fpt.ticketmobile.app.model.MyTicketOperateStaffObject;
import app.fpt.ticketmobile.app.model.MyTicketProcessStaffObject;
import app.fpt.ticketmobile.app.model.MyTicketQueueObject;
import app.fpt.ticketmobile.app.model.MyTicketReasonDetailObject;
import app.fpt.ticketmobile.app.model.MyTicketReasonObject;
import app.fpt.ticketmobile.app.model.MyTicketServiceTypeObject;
import app.fpt.ticketmobile.app.model.MyTicketStatusObject;
import app.fpt.ticketmobile.app.model.MyTicketVisorStaffObject;
import app.fpt.ticketmobile.app.model.User;
import app.fpt.ticketmobile.app.model.UserIam;
import app.fpt.ticketmobile.app.model.checkversion.CheckVersionObject;
import app.fpt.ticketmobile.app.model.support.MyImportContractObject;
import app.fpt.ticketmobile.app.model.support.MyOperateStaffSupportObject;
import app.fpt.ticketmobile.app.model.support.MyProcessStaffSupportObject;
import app.fpt.ticketmobile.app.model.support.MyQueueObject;
import app.fpt.ticketmobile.app.model.support.MyTicketBranchLocationObject;
import app.fpt.ticketmobile.app.model.support.MyTicketBusinessAreaObject;
import app.fpt.ticketmobile.app.model.support.MyTicketCreatedObject;
import app.fpt.ticketmobile.app.model.support.MyTicketCustomTypeObject;
import app.fpt.ticketmobile.app.model.support.MyTicketDescriptionObject;
import app.fpt.ticketmobile.app.model.support.MyTicketPriorityObject;
import app.fpt.ticketmobile.app.model.support.MyTicketSupportOfUserCreatedObject;
import app.fpt.ticketmobile.app.model.support.MyTicketSupportServiceTypeObject;
import app.fpt.ticketmobile.app.model.support.MyVisorStaffSupportObject;
import app.fpt.ticketmobile.app.jsonParsers.Fortype.support.MyImportContractField;
import app.fpt.ticketmobile.app.jsonParsers.Fortype.support.MyTicketBranchLocationField;
import app.fpt.ticketmobile.app.jsonParsers.Fortype.support.MyTicketBusinessAreaField;
import app.fpt.ticketmobile.app.jsonParsers.Fortype.MyTicketInventoryNotAssignField;
import app.fpt.ticketmobile.app.jsonParsers.Fortype.MyTicketInventoryUnFinishedField;
import app.fpt.ticketmobile.app.jsonParsers.Fortype.MyTicketIssueField;
import app.fpt.ticketmobile.app.jsonParsers.Fortype.support.MyTicketDescriptionField;
import app.fpt.ticketmobile.app.jsonParsers.Fortype.support.MyTicketSupportOfMeCreatedField;
import app.fpt.ticketmobile.app.jsonParsers.Fortype.MyTicketVisorStaffField;
import app.fpt.ticketmobile.app.model.MyTicketEffectObject;
import app.fpt.ticketmobile.app.model.MyTicketEstimatedDateObject;
import app.fpt.ticketmobile.app.model.support.MyTicketReasonSupportObject;

/**
 * Created by ToanMaster on 6/15/2016.
 */
public class GsonRequest<T> extends Request<T> {

    private  Gson gson = new GsonBuilder()
            .registerTypeAdapter(User.class, new UserField.Deserializer())
            .registerTypeAdapter(UserIam.class, new UserIAMField.Serializer())
            .registerTypeAdapter(UserIam.class, new UserIAMField.Deserializer())
            .registerTypeAdapter(User.class, new UserField.Serializer())
            .registerTypeAdapter(MyTicketObject.class, new MyTicketField.Deserializer())
            .registerTypeAdapter(MyTicketOfUserCreatedObject.class, new MyTicketOfMeCreatedField.Deserializer())
            .registerTypeAdapter(MyTicketInventoryUnFinishObject.class, new MyTicketInventoryUnFinishedField.Deserializer())
            .registerTypeAdapter(MyTicketInventoryNotAssignObject.class, new MyTicketInventoryNotAssignField.Deserializer())
            .registerTypeAdapter(MyTicketDetailObject.class, new MyTicketDetailField.Deserializer())
            .registerTypeAdapter(MyDeviceObject.class, new MyDeviceField.Deserializer())
            // ticket detail request description
            .registerTypeAdapter(MyTicketIssueObject.class, new MyTicketIssueField.Deserializer())
            .registerTypeAdapter(MyTicketEffectObject.class, new MyTicketEffectField.Deserializer())
            .registerTypeAdapter(MyTicketReasonObject.class, new MyTicketReasonField.Deserializer())
            .registerTypeAdapter(MyTicketReasonDetailObject.class, new MyTicketReasonDetailField.Deserializer())
            .registerTypeAdapter(MyTicketServiceTypeObject.class, new MyTicketServiceTypeField.Deserializer())
            .registerTypeAdapter(MyTicketCusTypeObject.class, new MyTicketCusTypeField.Deserializer())
            // ticket detail request employee
            .registerTypeAdapter(MyTicketQueueObject.class, new MyTicketQueueField.Deserializer())
            .registerTypeAdapter(MyTicketStatusObject.class, new MyTicketStatusField.Deserializer())
            .registerTypeAdapter(MyTicketOperateStaffObject.class, new MyTicketOperateStaffField.Deserializer())
            .registerTypeAdapter(MyTicketVisorStaffObject.class, new MyTicketVisorStaffField.Deserializer())
            .registerTypeAdapter(MyTicketProcessStaffObject.class, new MyTicketProcessStaffField.Deserializer())
            .registerTypeAdapter(MyResponseObject.class, new MyResponseJsonField.Deserializer())

            .registerTypeAdapter(MyTicketEstimatedDateObject.class, new MyTicketEstimatedDateField.Deserializer())
            // ticket detail response
            .registerTypeAdapter(MyTicketFeedbackObject.class, new MyTicketFeedbackField.Deserializer())
            .registerTypeAdapter(MyTicketFeedbackDetailObject.class, new MyTicketFeedbackDetailField.Deserializer())
            // ticket support
            .registerTypeAdapter(MyTicketBusinessAreaObject.class, new MyTicketBusinessAreaField.Deserializer())
            .registerTypeAdapter(MyTicketBranchLocationObject.class, new MyTicketBranchLocationField.Deserializer())
            .registerTypeAdapter(MyTicketSupportServiceTypeObject.class, new MyTicketSupportServiceTypeField.Deserializer())
            .registerTypeAdapter(MyTicketCustomTypeObject.class, new MyTicketCustomTypeField.Deserializer())
            .registerTypeAdapter(MyTicketDescriptionObject.class, new MyTicketDescriptionField.Deserializer())
            .registerTypeAdapter(MyTicketReasonSupportObject.class, new MyTicketReasonSupportField.Deserializer())
            .registerTypeAdapter(MyTicketPriorityObject.class, new MyTicketPriorityField.Deserializer())
            .registerTypeAdapter(MyTicketCreatedObject.class, new MyTicketCreatedField.Deserializer())
            .registerTypeAdapter(MyQueueObject.class, new MyQueueField.Deserializer())
            .registerTypeAdapter(MyVisorStaffSupportObject.class, new MyVisorStaffSupportField.Deserializer())
            .registerTypeAdapter(MyOperateStaffSupportObject.class,new MyOperateStaffSupportField.Deserializer())
            .registerTypeAdapter(MyProcessStaffSupportObject.class, new MyProcessStaffSupportField.Deserializer())
            .registerTypeAdapter(MyTicketSupportOfUserCreatedObject.class, new MyTicketSupportOfMeCreatedField.Deserializer())
            .registerTypeAdapter(MyImportContractObject.class, new MyImportContractField.Deserializer())
            .registerTypeAdapter(CheckVersionObject.class, new CheckVersionField.Deserializer())
            .registerTypeAdapter(CheckVersionObject.class, new CheckVersionField.Deserializer())
            .create();


    private Map<String , String> header;
    private final Type type;
    private final Response.Listener<T> listener;

    public GsonRequest(String url, Class<T> type ,Response.Listener<T> listener, Response.ErrorListener errorListener) {
        super(Method.POST, url, errorListener);
        this.type =  type;
        this.listener = listener;
    }


    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        try {
//            String json = new String(
//                    response.data,
//                    HttpHeaderParser.parseCharset(response.headers));

           String json = new String(response.data, "UTF-8");
            return (Response<T>) Response.success(
                    gson.fromJson(json, type),
                    HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JsonSyntaxException e) {
            return Response.error(new ParseError(e));
        }
    }

    @Override
    protected void deliverResponse(T response) {
        listener.onResponse(response);
    }


    @Override
    protected VolleyError parseNetworkError(VolleyError volleyError) {
        return super.parseNetworkError(volleyError);
    }



}
