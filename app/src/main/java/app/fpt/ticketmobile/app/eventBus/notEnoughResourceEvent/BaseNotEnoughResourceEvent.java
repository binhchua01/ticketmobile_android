package app.fpt.ticketmobile.app.eventBus.notEnoughResourceEvent;

import app.fpt.ticketmobile.app.eventBus.Event;

/**
 * Created by ToanMaster on 6/13/2016.
 */
public abstract class BaseNotEnoughResourceEvent implements Event {

    private int mStringResId;

    protected BaseNotEnoughResourceEvent(int stringResId) {
        this.mStringResId = stringResId;
    }

    public int getStringResId() {
        return this.mStringResId;
    }
}
