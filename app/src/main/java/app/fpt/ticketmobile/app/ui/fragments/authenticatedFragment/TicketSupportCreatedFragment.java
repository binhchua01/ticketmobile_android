package app.fpt.ticketmobile.app.ui.fragments.authenticatedFragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

import app.fpt.ticketmobile.app.eventBus.event.ImportContractObjectEvent;
import app.fpt.ticketmobile.app.model.MyResponseObject;
import app.fpt.ticketmobile.R;
import app.fpt.ticketmobile.app.eventBus.event.DashBroadChangeMenu;
import app.fpt.ticketmobile.app.lib.utils.TmUtils;
import app.fpt.ticketmobile.app.model.SpinnerObject;
import app.fpt.ticketmobile.app.model.support.ImportContractObject;
import app.fpt.ticketmobile.app.model.support.MyImportContractObject;
import app.fpt.ticketmobile.app.model.support.MyOperateStaffSupportObject;
import app.fpt.ticketmobile.app.model.support.MyProcessStaffSupportObject;
import app.fpt.ticketmobile.app.model.support.MyQueueObject;
import app.fpt.ticketmobile.app.model.support.MyTicketBranchLocationObject;
import app.fpt.ticketmobile.app.model.support.MyTicketBusinessAreaObject;
import app.fpt.ticketmobile.app.model.support.MyTicketCustomTypeObject;
import app.fpt.ticketmobile.app.model.support.MyTicketDescriptionObject;
import app.fpt.ticketmobile.app.model.support.MyTicketPriorityObject;
import app.fpt.ticketmobile.app.model.support.MyTicketReasonSupportObject;
import app.fpt.ticketmobile.app.model.support.MyTicketSupportObject;
import app.fpt.ticketmobile.app.model.support.MyTicketSupportServiceTypeObject;
import app.fpt.ticketmobile.app.model.support.MyVisorStaffSupportObject;
import app.fpt.ticketmobile.app.networking.apiEndpoints.MyTicketDetailEndPoint;
import app.fpt.ticketmobile.app.networking.apiEndpoints.MyTicketSupportEndPoint;
import app.fpt.ticketmobile.app.ui.fragments.TmBaseFragment;
import app.fpt.ticketmobile.app.ui.fragments.dialogFragment.ImportContractDialogFragment;
import app.fpt.ticketmobile.app.ui.lib.CredentialsValidator;
import app.fpt.ticketmobile.app.ui.lib.FragmentRefreshBasedSwipeRefreshLayoutHandler;
import app.fpt.ticketmobile.app.ui.lib.TicketSupport;
import app.fpt.ticketmobile.widget.views.MultiSelectionObjectSpinner;

/**
 * Created by tmt on 7/14/2016.
 */
public class TicketSupportCreatedFragment extends TmBaseFragment<TicketSupportCreatedFragment.TicketSupportCreatedCallback>
        implements SwipeRefreshLayout.OnRefreshListener {

    private static final String MY_TICKET_SUPPORT_REQUEST_BUSINESS_AREA_TAG = "my_ticket_support_request_business_area_tag";
    private static final String MY_TICKET_SUPPORT_REQUEST_BRANCH_LOCATION_TAG = "my_ticket_support_request_branch_location_tag";
    private static final String MY_TICKET_SUPPORT_REQUEST_SERVICE_TYPE_TAG = "my_ticket_support_request_service_type_tag";
    private static final String MY_TICKET_SUPPORT_REQUEST_CUSTOM_TYPE_TAG = "my_ticket_support_request_custom_type_tag";
    private static final String MY_TICKET_SUPPORT_REQUEST_DESCRIPTION_TAG = "my_ticket_support_request_description_tag";
    private static final String MY_TICKET_SUPPORT_REQUEST_PRIORITY_TAG = "my_ticket_support_request_priority_tag";
    private static final String MY_TICKET_SUPPORT_REQUEST_REASON_TAG = "my_ticket_support_request_reason_tag";
    private static final String MY_TICKET_SUPPORT_REQUEST_QUEUE_TAG = "my_ticket_support_request_queue_tag";
    private static final String MY_TICKET_SUPPORT_REQUEST_OPERATE_STAFF_TAG = "my_ticket_support_request_operate_staff_tag";
    private static final String MY_TICKET_SUPPORT_REQUEST_VISOR_STAFF_TAG = "my_ticket_support_request_visor_staff_tag";
    private static final String MY_TICKET_SUPPORT_REQUEST_HT_ESTIMATED_TIME_TAG = "my_ticket_support_request_ht_estimated_time_tag";
    private static final String MY_TICKET_SUPPORT_REQUEST_PROCESS_STAFF_TAG = "my_ticket_support_request_process_staff_tag";
    private static final String MY_TICKET_SUPPORT_UPDATED_CREATED_TAG = "my_ticket_support_updated_created_tag";

    private static final String FRAGMENT_IMPORT_CONTRACT_DIALOG_TAG = "fragment_import_contract_dialog_tag";

    private static final String EXTRA_TICKET_USER_ASSIGN_KEY = "1";
    private static final int EXTRA_TICKET_KIND_KEY = 2;
    private static final int EXTRA_TICKET_STATUS_KEY = -1;
    private static final String EXTRA_TICKET_CRICLEV_KEY = "1";
    private static final String EXTRA_TICKET_REMIND_STATUS_KEY = "Email";
    private static final int EXTRA_TICKET_HAS_WARNING_KEY = 0;
    private static final int EXTRA_TICKET_PARENT_ID = 0;
    private int EstimatedTimeValue = 0;

    private static int CURRENT_SERVICE_TYPE_SPINNER_SELECTION = 0;
    private static int CURRENT_CUSTOM_TYPE_SPINNER_SELECTION = 0;
    private static int CURRENT_ISSUE_DESCRIPTION_SPINNER_SELECTION = 0;
    private static int CURRENT_QUEUE_SPINNER_SELECTION = 0;
    private static int CURRENT_PROCESS_STAFF_SELECTION = 0;


    private MultiSelectionObjectSpinner mSpinnerTicketSupportLocationArea;
    private MultiSelectionObjectSpinner mSpinnerTicketSupportBranchLocation;
    private TextView tvTicketSupportRegionRequirement;
    private TextView tvTicketSupportRegionCricLev;
    private TextView tvTicketSupportRegionPriority;
    private Spinner mSpinnerTicketSupportRegionServiceType;
    private Spinner mSpinnerTicketSupportCusType;
    private Spinner mSpinnerTicketSupportIssueDescription;
    private AppCompatButton btnTicketSupportImportContract;
    private AppCompatEditText tvTicketSupportComment;
    private Spinner mSpinnerTicketSupportReason;
    private Spinner mSpinnerTicketSupportEmployeeNotification;
    private TextView tvTicketSupportMobileNumber;
    private TextView tvTicketSupportMobileIP;
    private Spinner mSpinnerTicketSupportDepartmentNotification;
    private Spinner mSpinnerTicketSupportStateBreakDown;
    private Spinner mSpinnerTicketSupportQueue;
    private Spinner mSpinnerTicketSupportOperateStaff;
    private Spinner mSpinnerTicketSupportVisorStaff;
    private Spinner mSpinnerTicketSupportProcessStaff;
    private TextView tvTicketSupportProcessStaffPhoneNumber;
    private TextView tvTicketSupportProcessStaffIpPhone;

    private Button btnSave;
    private Button btnInputReply;
    private ProgressDialog mProgressDialog;
    private View mLayoutEmployeeProcessStaff = null;

    private final List<SpinnerObject> mListSpinner = new ArrayList<>();
    private List<SpinnerObject> mServiceTypeList = new ArrayList<>();
    private List<SpinnerObject> mCusTypeList = new ArrayList<>();
    private List<SpinnerObject> mDescriptionList = new ArrayList<>();
    private List<SpinnerObject> mReasonList = new ArrayList<>();
    private List<SpinnerObject> mEmployeeNotificationList = new ArrayList<>();
    private List<SpinnerObject> mDepartmentNotificationList = new ArrayList<>();
    private List<SpinnerObject> mStateBreakDownList = new ArrayList<>();
    private List<SpinnerObject> mQueueList = new ArrayList<>();
    private List<SpinnerObject> mOperateStaffList = new ArrayList<>();
    private List<SpinnerObject> mVisorStaffList = new ArrayList<>();
    private List<SpinnerObject> mProcessStaffList = new ArrayList<>();

    private SpinnerObject mLocationAreaObject = null;
    private SpinnerObject mBranchLocationObject = null;
    private SpinnerObject mServiceTypeObject = null;
    private SpinnerObject mCusTypeObject = null;
    private SpinnerObject mDescriptionObject = null;
    private SpinnerObject mReasonObject = null;
    private SpinnerObject mEmployeeNotificationObject = null;
    private SpinnerObject mDepartmentNotificationObject = null;
    private SpinnerObject mStateBreakDownObject = null;
    private SpinnerObject mQueueObject = null;
    private SpinnerObject mOperateStaffObject = null;
    private SpinnerObject mVisorStaffObject = null;
    private SpinnerObject mProcessStaffObject = null;

    private List<ImportContractObject> mImportContractObjectList = new ArrayList<>();
    private MyImportContractObject mMyImportContractObject = null;


    public interface TicketSupportCreatedCallback {

    }

    public TicketSupportCreatedFragment() {
        super(TicketSupportCreatedCallback.class, Integer.valueOf(R.layout.fragment_ticket_support_created), false, true);

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mLocationAreaObject = new SpinnerObject("0", getString(R.string.tm_choose_location));
        mBranchLocationObject = new SpinnerObject("0", getString(R.string.tm_choose_branch_location));
        mServiceTypeObject = new SpinnerObject("0", getString(R.string.tm_choose_service_type));
        mCusTypeObject = new SpinnerObject("0", getString(R.string.tm_choose_cus_type));
        mDescriptionObject = new SpinnerObject("0", getString(R.string.tm_choose_description));
        mReasonObject = new SpinnerObject("0", getString(R.string.tm_choose_reason));
        mEmployeeNotificationObject = new SpinnerObject("0", getTmSharePreferencesUtils().getUserName());
        mDepartmentNotificationObject = new SpinnerObject("0", getTmSharePreferencesUtils().getDivisionUserLogin());
        mStateBreakDownObject = new SpinnerObject("0", getString(R.string.tm_choose_state_break_down));
        mQueueObject = new SpinnerObject("0", getString(R.string.tm_choose_queue));
        mOperateStaffObject = new SpinnerObject("0", getString(R.string.tm_choose_operate_staff));
        mVisorStaffObject = new SpinnerObject("0", getString(R.string.tm_choose_visor_staff));
        mProcessStaffObject = new SpinnerObject("0", getString(R.string.tm_choose_process_staff));

        mServiceTypeList.add(0, mServiceTypeObject);
        mCusTypeList.add(0, mCusTypeObject);
        mDescriptionList.add(0, mDescriptionObject);
        mReasonList.add(0, mReasonObject);
        mEmployeeNotificationList.add(0, mEmployeeNotificationObject);
        mDepartmentNotificationList.add(0, mDepartmentNotificationObject);
        mStateBreakDownList.add(0, mStateBreakDownObject);
        mQueueList.add(0, mQueueObject);
        mOperateStaffList.add(0, mOperateStaffObject);
        mVisorStaffList.add(0, mVisorStaffObject);
        mProcessStaffList.add(0, mProcessStaffObject);
        mMyImportContractObject = new MyImportContractObject(mImportContractObjectList);

    }

    @Override
    protected void onTmActivityCreated(View parent, Bundle savedInstanceState) {
        super.onTmActivityCreated(parent, savedInstanceState);
        setupViews(parent);
        if (parent != null && getActivity() != null) {
            TmUtils.setupHideKeyBoardParent(parent, getActivity());
        }
    }

    private void setupViews(View parent) {
        mProgressDialog = new ProgressDialog(getContext());
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setCancelable(false);
        mLayoutEmployeeProcessStaff = parent.findViewById(R.id.tm_layout_employee_process_staff);

        SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) parent.findViewById(R.id.swipe_refresh_layout);
        setFragmentRefreshAnimationHandler(new FragmentRefreshBasedSwipeRefreshLayoutHandler(swipeRefreshLayout, this, Integer.valueOf(R.color.tm_swipe_refresh_color)));

        mSpinnerTicketSupportLocationArea = (MultiSelectionObjectSpinner) parent.findViewById(R.id.tm_spinner_ticket_support_region_location);
        mSpinnerTicketSupportBranchLocation = (MultiSelectionObjectSpinner) parent.findViewById(R.id.tm_spinner_ticket_support_region_agent);
        tvTicketSupportRegionRequirement = (TextView) parent.findViewById(R.id.tm_edt_ticket_support_region_requirement);
        tvTicketSupportRegionCricLev = (TextView) parent.findViewById(R.id.tm_tv_ticket_support_region_cricLev);
        tvTicketSupportRegionPriority = (TextView) parent.findViewById(R.id.tm_tv_ticket_support_region_priority);
        mSpinnerTicketSupportRegionServiceType = (Spinner) parent.findViewById(R.id.tm_spinner_ticket_support_region_service_type);
        CredentialsValidator.setColorPromptSpinner(mSpinnerTicketSupportRegionServiceType, getResources().getString(R.string.tm_tv_ticket_support_service_type));
        mSpinnerTicketSupportCusType = (Spinner) parent.findViewById(R.id.tm_spinner_ticket_support_region_cus_type);
        CredentialsValidator.setColorPromptSpinner(mSpinnerTicketSupportCusType, getResources().getString(R.string.tm_tv_ticket_support_cus_type));
        mSpinnerTicketSupportIssueDescription = (Spinner) parent.findViewById(R.id.tm_spinner_ticket_support_region_issue_description);
        CredentialsValidator.setColorPromptSpinner(mSpinnerTicketSupportIssueDescription, getResources().getString(R.string.tm_tv_ticket_support_description));
        btnTicketSupportImportContract = (AppCompatButton) parent.findViewById(R.id.tm_button_ticket_support_import_contract);
        tvTicketSupportComment = (AppCompatEditText) parent.findViewById(R.id.tm_tv_comment);
        mSpinnerTicketSupportReason = (Spinner) parent.findViewById(R.id.tm_spinner_reason);
        CredentialsValidator.setColorPromptSpinner(mSpinnerTicketSupportReason, getResources().getString(R.string.tm_ticket_detail_description_reason_spinner));
        mSpinnerTicketSupportEmployeeNotification = (Spinner) parent.findViewById(R.id.tm_spinner_employee_notification);
        CredentialsValidator.setColorPromptSpinner(mSpinnerTicketSupportEmployeeNotification, getResources().getString(R.string.tm_ticket_detail_employee_notification));
        tvTicketSupportMobileNumber = (TextView) parent.findViewById(R.id.tm_tv_mobile_number);
        tvTicketSupportMobileIP = (TextView) parent.findViewById(R.id.tm_tv_mobile_ip);
        mSpinnerTicketSupportDepartmentNotification = (Spinner) parent.findViewById(R.id.tm_spinner_department_notification);
        CredentialsValidator.setColorPromptSpinner(mSpinnerTicketSupportDepartmentNotification, getResources().getString(R.string.tm_department_notification));
        mSpinnerTicketSupportStateBreakDown = (Spinner) parent.findViewById(R.id.tm_spinner_state_breakdown);
        CredentialsValidator.setColorPromptSpinner(mSpinnerTicketSupportStateBreakDown, getResources().getString(R.string.tm_department_notification));
        mSpinnerTicketSupportQueue = (Spinner) parent.findViewById(R.id.tm_spinner_queue);
        CredentialsValidator.setColorPromptSpinner(mSpinnerTicketSupportQueue, getResources().getString(R.string.tm_queue));
        mSpinnerTicketSupportOperateStaff = (Spinner) parent.findViewById(R.id.tm_spinner_operate_staff);
        CredentialsValidator.setColorPromptSpinner(mSpinnerTicketSupportOperateStaff, getResources().getString(R.string.tm_ticket_operate_staff));
        mSpinnerTicketSupportProcessStaff = (Spinner) parent.findViewById(R.id.tm_spinner_employee_process_staff);
        CredentialsValidator.setColorPromptSpinner(mSpinnerTicketSupportProcessStaff, getResources().getString(R.string.tm_ticket_visor_staff));
        mSpinnerTicketSupportVisorStaff = (Spinner) parent.findViewById(R.id.tm_spinner_visor_staff);
        CredentialsValidator.setColorPromptSpinner(mSpinnerTicketSupportVisorStaff, getResources().getString(R.string.tm_visor_staff));
        tvTicketSupportProcessStaffPhoneNumber = (TextView) parent.findViewById(R.id.tm_tv_employee_process_staff_phone_number);
        tvTicketSupportProcessStaffIpPhone = (TextView) parent.findViewById(R.id.tm_tv_employee_process_staff_ip_phone);

        btnSave = (Button) parent.findViewById(R.id.tm_ticket_save);
        btnInputReply = (Button) parent.findViewById(R.id.tm_ticket_feedback);
        btnSave.setOnClickListener(new PrivateCreatedTicketSupportListener());
        btnInputReply.setOnClickListener(new PrivateInputReplyListener());

        //
        mSpinnerTicketSupportLocationArea.setEnabled(true);
        mSpinnerTicketSupportBranchLocation.setEnabled(false);
        mSpinnerTicketSupportRegionServiceType.setEnabled(false);
        mSpinnerTicketSupportCusType.setEnabled(false);
        mSpinnerTicketSupportIssueDescription.setEnabled(false);
        mSpinnerTicketSupportReason.setEnabled(false);

//        mSpinnerTicketSupportOperateStaff.setEnabled(false);
//        mSpinnerTicketSupportVisorStaff.setEnabled(false);


        mSpinnerTicketSupportRegionServiceType.setOnItemSelectedListener(new PrivateSpinnerItemSelectedListener());
        mSpinnerTicketSupportCusType.setOnItemSelectedListener(new PrivateSpinnerItemSelectedListener());
        mSpinnerTicketSupportIssueDescription.setOnItemSelectedListener(new PrivateSpinnerItemSelectedListener());
        mSpinnerTicketSupportProcessStaff.setOnItemSelectedListener(new PrivateSpinnerItemSelectedListener());
        mSpinnerTicketSupportQueue.setOnItemSelectedListener(new PrivateSpinnerItemSelectedListener());

        btnTicketSupportImportContract.setOnClickListener(new PrivateImportContractsListener());

        setupLoadDataLocal();

        if (!isShowingRefreshAnimation()) {
            swipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    showRefreshAnimation();
                }
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!isShowingRefreshAnimation()) {
            showRefreshAnimation();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        getTmApiEndPointProvider().cancelAllRequest(MY_TICKET_SUPPORT_REQUEST_BUSINESS_AREA_TAG);
        getTmApiEndPointProvider().cancelAllRequest(MY_TICKET_SUPPORT_REQUEST_BRANCH_LOCATION_TAG);
        getTmApiEndPointProvider().cancelAllRequest(MY_TICKET_SUPPORT_REQUEST_SERVICE_TYPE_TAG);
        getTmApiEndPointProvider().cancelAllRequest(MY_TICKET_SUPPORT_REQUEST_CUSTOM_TYPE_TAG);
        getTmApiEndPointProvider().cancelAllRequest(MY_TICKET_SUPPORT_REQUEST_DESCRIPTION_TAG);
        getTmApiEndPointProvider().cancelAllRequest(MY_TICKET_SUPPORT_REQUEST_PRIORITY_TAG);
        getTmApiEndPointProvider().cancelAllRequest(MY_TICKET_SUPPORT_REQUEST_REASON_TAG);
        getTmApiEndPointProvider().cancelAllRequest(MY_TICKET_SUPPORT_REQUEST_OPERATE_STAFF_TAG);
        getTmApiEndPointProvider().cancelAllRequest(MY_TICKET_SUPPORT_REQUEST_VISOR_STAFF_TAG);
        getTmApiEndPointProvider().cancelAllRequest(MY_TICKET_SUPPORT_REQUEST_HT_ESTIMATED_TIME_TAG);
        getTmApiEndPointProvider().cancelAllRequest(MY_TICKET_SUPPORT_REQUEST_PROCESS_STAFF_TAG);
        getTmApiEndPointProvider().cancelAllRequest(MY_TICKET_SUPPORT_UPDATED_CREATED_TAG);
    }

    @Override
    public void onRefresh() {
        autoSynced();
    }

    @Override
    protected void autoSynced() {
        super.autoSynced();
        if (!isShowingRefreshAnimation()) {
            showRefreshAnimation();
        }
        setupLoadDataToView();
        stopRefreshAnimation();
    }

    private void setupLoadDataLocal() {
        loadDataLocalLocationArea();
        loadDataLocalBranchLocation();
        loadDataLocalServiceType();
        loadDataLocalSpinnerCusType();
        loadDataLocalSpinnerDescriptionType();
        loadDataLocalSpinnerReasonType();
        loadDataLocalSpinnerEmployeeInformation();
        loadDataLocalSpinnerDepartment();
        loadDataLocalSpinnerStateBreakDown();
        loadDataLocalSpinnerQueue();
        loadDataLocalSpinnerOperateStaff();
        loadDataLocalSpinnerVisorStaff();
        loadDataLocalTvEmployeeMobileNumber();
        loadDataLocalTvEmployeeIpMobile();
    }

    private void loadDataLocalLocationArea() {
        mSpinnerTicketSupportLocationArea.setSpinnerMultiAdapter(mLocationAreaObject, mListSpinner, true);
        if (mSpinnerTicketSupportLocationArea.getCheckBoxAll() != null) {
            mSpinnerTicketSupportLocationArea.getCheckBoxAll().setChecked(false);
        }
        mSpinnerTicketSupportBranchLocation.setEnabled(false);
    }

    private void loadDataLocalBranchLocation() {
        mSpinnerTicketSupportBranchLocation.setSpinnerMultiAdapter(mBranchLocationObject, mListSpinner, true);
    }

    private void loadDataLocalServiceType() {
        TmUtils.setSpinnerAdapter(getActivity(), mSpinnerTicketSupportRegionServiceType, mServiceTypeList);
    }

    private void loadDataLocalSpinnerQueue() {
        mQueueList.clear();
        mQueueList.add(0, mQueueObject);
        TmUtils.setSpinnerAdapter(getActivity(), mSpinnerTicketSupportQueue, mQueueList);
    }

    private void loadDataLocalSpinnerOperateStaff() {
        mOperateStaffList.clear();
        mOperateStaffList.add(0, mOperateStaffObject);
        TmUtils.setSpinnerAdapter(getActivity(), mSpinnerTicketSupportOperateStaff, mOperateStaffList);
    }

    private void loadDataLocalSpinnerVisorStaff() {
        mVisorStaffList.clear();
        mVisorStaffList.add(0, mVisorStaffObject);
        TmUtils.setSpinnerAdapter(getActivity(), mSpinnerTicketSupportVisorStaff, mVisorStaffList);
    }

    private void loadDataLocalSpinnerStateBreakDown() {
        mStateBreakDownList.clear();
        mStateBreakDownList.add(0, mStateBreakDownObject);
        TmUtils.setSpinnerAdapter(getActivity(), mSpinnerTicketSupportStateBreakDown, mStateBreakDownList);
    }

    private void loadDataLocalTvEmployeeIpMobile() {
        tvTicketSupportMobileIP.setText(getTmSharePreferencesUtils().getIpPhoneUserLogin());
    }

    private void loadDataLocalTvEmployeeMobileNumber() {
        tvTicketSupportMobileNumber.setText(getTmSharePreferencesUtils().getPhoneUserLogin());
    }

    private void loadDataLocalSpinnerEmployeeInformation() {
        mEmployeeNotificationList.clear();
        mEmployeeNotificationList.add(0, mEmployeeNotificationObject);
        TmUtils.setSpinnerAdapter(getActivity(), mSpinnerTicketSupportEmployeeNotification, mEmployeeNotificationList);
    }

    private void loadDataLocalSpinnerReasonType() {
        mReasonList.clear();
        mReasonList.add(0, mReasonObject);
        TmUtils.setSpinnerAdapter(getActivity(), mSpinnerTicketSupportReason, mReasonList);
    }

    private void loadDataLocalSpinnerDepartment() {
        mDepartmentNotificationList.clear();
        mDepartmentNotificationList.add(0, mDepartmentNotificationObject);
        TmUtils.setSpinnerAdapter(getActivity(), mSpinnerTicketSupportDepartmentNotification, mDepartmentNotificationList);
    }

    private void setupLoadDataToView() {
        getBusinessAreaList();
        getSupportServiceTypeList();
        getSupportQueueList();
    }

    private void getSupportQueueList() {
        mProgressDialog.show();
        getTmApiEndPointProvider().addRequest(MyTicketSupportEndPoint.getQueueList(new Response.Listener<MyQueueObject>() {
            @Override
            public void onResponse(MyQueueObject myQueueObject) {
                if (getActivity() != null) {
                    if (myQueueObject != null && myQueueObject.getSpinnerObjectList() != null) {
                        if (myQueueObject.getSpinnerObjectList().size() > 0) {
                            mQueueList.clear();
                            mQueueList = new ArrayList<SpinnerObject>(myQueueObject.getSpinnerObjectList());
                            mQueueList.add(0, mQueueObject);
                            TmUtils.setSpinnerAdapter(getActivity(), mSpinnerTicketSupportQueue, mQueueList);
                            CURRENT_QUEUE_SPINNER_SELECTION = mSpinnerTicketSupportQueue.getSelectedItemPosition();
                        }
                    }
                    mProgressDialog.dismiss();

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onErrorListener(error);
            }
        }), MY_TICKET_SUPPORT_REQUEST_QUEUE_TAG);
    }

    private void getBusinessAreaList() {
        mProgressDialog.show();
        getTmApiEndPointProvider().addRequest(MyTicketSupportEndPoint.getBusinessAreaList(getTmSharePreferencesUtils().getUserIdLogin(), new Response.Listener<MyTicketBusinessAreaObject>() {
            @Override
            public void onResponse(MyTicketBusinessAreaObject businessAreaObject) {
                if (getActivity() != null) {
                    if (businessAreaObject != null && businessAreaObject.getSpinnerObjectList() != null) {
                        if (businessAreaObject.getSpinnerObjectList().size() > 0) {
                            mSpinnerTicketSupportLocationArea.setEnabled(true);
                            mSpinnerTicketSupportLocationArea.setSpinnerTitleDialog(getString(R.string.tm_choose_location));
                            mSpinnerTicketSupportLocationArea.setSpinnerMultiAdapter(mLocationAreaObject, businessAreaObject.getSpinnerObjectList(), false);
                            mSpinnerTicketSupportLocationArea.setSpinnerUpdateCallback(new PrivateSpinnerCallbackInterface());
                        }
                    }
                    mProgressDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onErrorListener(error);
            }
        }), MY_TICKET_SUPPORT_REQUEST_BUSINESS_AREA_TAG);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // get BranchLocation (String keyLocation)
    private void getBranchLocationList(String keyLocation) {
        mProgressDialog.show();
        getTmApiEndPointProvider().addRequest(MyTicketSupportEndPoint.getBranchLocationList(keyLocation, new Response.Listener<MyTicketBranchLocationObject>() {
            @Override
            public void onResponse(MyTicketBranchLocationObject branchLocationObject) {
                if (getActivity() != null) {
                    if (branchLocationObject != null && branchLocationObject.getSpinnerObjectList() != null) {
                        if (branchLocationObject.getSpinnerObjectList().size() > 0) {
                            mSpinnerTicketSupportBranchLocation.setEnabled(true);
                            mSpinnerTicketSupportBranchLocation.setSpinnerTitleDialog(getString(R.string.tm_choose_branch_location));
                            mSpinnerTicketSupportBranchLocation.setSpinnerMultiAdapter(mBranchLocationObject, branchLocationObject.getSpinnerObjectList(), true);
                            mSpinnerTicketSupportBranchLocation.setSpinnerUpdateCallback(new PrivateSpinnerCallbackUpdatePriority());
                            mSpinnerTicketSupportBranchLocation.setCheckBox();
                        }
                    }
                    mProgressDialog.dismiss();

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onErrorListener(error);
            }
        }), MY_TICKET_SUPPORT_REQUEST_BRANCH_LOCATION_TAG);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // get ServiceType
    private void getSupportServiceTypeList() {
        mProgressDialog.show();
        getTmApiEndPointProvider().addRequest(MyTicketSupportEndPoint.getSupportServiceTypeList(new Response.Listener<MyTicketSupportServiceTypeObject>() {
            @Override
            public void onResponse(MyTicketSupportServiceTypeObject serviceTypeObject) {
                if (getActivity() != null) {
                    if (serviceTypeObject != null && serviceTypeObject.getSpinnerObjectList() != null) {
                        if (serviceTypeObject.getSpinnerObjectList().size() > 0) {
                            mSpinnerTicketSupportRegionServiceType.setEnabled(true);
                            mServiceTypeList.clear();
                            mServiceTypeList = new ArrayList<>(serviceTypeObject.getSpinnerObjectList());
                            mServiceTypeList.add(0, mServiceTypeObject);
                            TmUtils.setSpinnerAdapter(getActivity(), mSpinnerTicketSupportRegionServiceType, mServiceTypeList);
                            CURRENT_SERVICE_TYPE_SPINNER_SELECTION = mSpinnerTicketSupportRegionServiceType.getSelectedItemPosition();
                        }
                    }
                    mProgressDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onErrorListener(error);
            }
        }), MY_TICKET_SUPPORT_REQUEST_SERVICE_TYPE_TAG);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // getCustomTypeList(String serviceType)
    private void getCustomTypeList(String serviceType) {
        mProgressDialog.show();
        getTmApiEndPointProvider().addRequest(MyTicketSupportEndPoint.getCustomTypeList(serviceType, new Response.Listener<MyTicketCustomTypeObject>() {
            @Override
            public void onResponse(MyTicketCustomTypeObject customTypeObject) {
                if (getActivity() != null) {
                    if (customTypeObject != null && customTypeObject.getSpinnerObjectList() != null) {
                        if (customTypeObject.getSpinnerObjectList().size() > 0) {
                            mCusTypeList.clear();
                            mCusTypeList = new ArrayList<>(customTypeObject.getSpinnerObjectList());
                            mCusTypeList.add(0, mCusTypeObject);
                            TmUtils.setSpinnerAdapter(getActivity(), mSpinnerTicketSupportCusType, mCusTypeList);
                            CURRENT_CUSTOM_TYPE_SPINNER_SELECTION = mSpinnerTicketSupportCusType.getSelectedItemPosition();
                        }
                    }
                    mProgressDialog.dismiss();

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onErrorListener(error);
            }
        }), MY_TICKET_SUPPORT_REQUEST_CUSTOM_TYPE_TAG);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // getCustomTypeList(String serviceType)
    private void getIssueDescriptionList(String cusType) {
        mProgressDialog.show();
        getTmApiEndPointProvider().addRequest(MyTicketSupportEndPoint.getIssueDescriptionList(cusType, new Response.Listener<MyTicketDescriptionObject>() {
            @Override
            public void onResponse(MyTicketDescriptionObject descriptionObject) {
                if (getActivity() != null) {
                    if (descriptionObject != null && descriptionObject.getSpinnerObjectList() != null) {
                        if (descriptionObject.getSpinnerObjectList().size() > 0) {
                            mDescriptionList.clear();
                            mDescriptionList = new ArrayList<>(descriptionObject.getSpinnerObjectList());
                            mDescriptionList.add(0, mDescriptionObject);
                            TmUtils.setSpinnerAdapter(getActivity(), mSpinnerTicketSupportIssueDescription, mDescriptionList);
                            CURRENT_ISSUE_DESCRIPTION_SPINNER_SELECTION = mSpinnerTicketSupportIssueDescription.getSelectedItemPosition();
                        }
                    }
                    mProgressDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onErrorListener(error);
            }
        }), MY_TICKET_SUPPORT_REQUEST_DESCRIPTION_TAG);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // getCustomTypeList(String serviceType)
    private void getPriorityList(String locationArea, String branchLocation, String issueType) {
        mProgressDialog.show();
        getTmApiEndPointProvider().addRequest(MyTicketSupportEndPoint.getPriorityList(locationArea, branchLocation, issueType, new Response.Listener<MyTicketPriorityObject>() {
            @Override
            public void onResponse(MyTicketPriorityObject ticketPriorityObject) {
                if (getActivity() != null) {
                    if (ticketPriorityObject != null && ticketPriorityObject.getPriorityList() != null) {
                        if (ticketPriorityObject.getPriorityList().size() > 0) {
                            tvTicketSupportRegionPriority.setText(String.valueOf(ticketPriorityObject.getPriorityList().get(0).getPriorityValue().intValue()));
                        }
                    }
                    mProgressDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onErrorListener(error);
            }
        }), MY_TICKET_SUPPORT_REQUEST_PRIORITY_TAG);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // getCustomTypeList(String serviceType)
    private void getReasonList(String cusType) {
        mProgressDialog.show();
        getTmApiEndPointProvider().addRequest(MyTicketSupportEndPoint.getReasonList(cusType, new Response.Listener<MyTicketReasonSupportObject>() {
            @Override
            public void onResponse(MyTicketReasonSupportObject reasonSupportObject) {
                if (getActivity() != null) {
                    if (reasonSupportObject != null && reasonSupportObject.getSpinnerObjectList() != null) {
                        if (reasonSupportObject.getSpinnerObjectList().size() > 0) {
                            mSpinnerTicketSupportReason.setEnabled(true);
                            mReasonList.clear();
                            mReasonList = new ArrayList<>(reasonSupportObject.getSpinnerObjectList());
                            mReasonList.add(0, mReasonObject);
                            TmUtils.setSpinnerAdapter(getActivity(), mSpinnerTicketSupportReason, mReasonList);
                        }
                    }
                    mProgressDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onErrorListener(error);
            }
        }), MY_TICKET_SUPPORT_REQUEST_REASON_TAG);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // getOperateStaffList(String queue, String locationArea, String branchLocation, String criLev)
    // nhân viên chủ trì
    private void getOperateStaffList(String queue, String locationArea, String branchLocation, String criLev) {
        mProgressDialog.show();
        getTmApiEndPointProvider().addRequest(MyTicketSupportEndPoint.getOperateStaffList(queue, locationArea, branchLocation, criLev, new Response.Listener<MyOperateStaffSupportObject>() {
            @Override
            public void onResponse(MyOperateStaffSupportObject operateStaffObject) {
                if (getActivity() != null) {
                    if (operateStaffObject != null && operateStaffObject.getSpinnerObjectList() != null) {
                        if (operateStaffObject.getSpinnerObjectList().size() > 0) {
                            //mSpinnerTicketSupportOperateStaff.setEnabled(true);
                            mOperateStaffList.clear();
                            mOperateStaffList = new ArrayList<>(operateStaffObject.getSpinnerObjectList());
                            //mOperateStaffList.add(0, mOperateStaffObject);
                            TmUtils.setSpinnerAdapter(getActivity(), mSpinnerTicketSupportOperateStaff, mOperateStaffList);
                        }
                    }
                    mProgressDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onErrorListener(error);
            }
        }), MY_TICKET_SUPPORT_REQUEST_OPERATE_STAFF_TAG);
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////
    // getVisorStaffList(String queue, String locationArea, String branchLocation)
    private void getVisorStaffList(String queue, String locationArea, String branchLocation) {
        mProgressDialog.show();
        getTmApiEndPointProvider().addRequest(MyTicketSupportEndPoint.getVisorStaffList(queue, locationArea, branchLocation, new Response.Listener<MyVisorStaffSupportObject>() {
            @Override
            public void onResponse(MyVisorStaffSupportObject visorStaffObject) {
                if (getActivity() != null) {
                    if (visorStaffObject != null && visorStaffObject.getSpinnerObjectList() != null) {
                        if (visorStaffObject.getSpinnerObjectList().size() > 0) {
                            //mSpinnerTicketSupportVisorStaff.setEnabled(true);
                            mVisorStaffList.clear();
                            mVisorStaffList = new ArrayList<>(visorStaffObject.getSpinnerObjectList());
                            //mVisorStaffList.add(0, mVisorStaffObject);
                            TmUtils.setSpinnerAdapter(getActivity(), mSpinnerTicketSupportVisorStaff, mVisorStaffList);
                        }
                    }
                    mProgressDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onErrorListener(error);
            }
        }), MY_TICKET_SUPPORT_REQUEST_VISOR_STAFF_TAG);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    //  getEstimatedTimeValue(String cricLev, String issueId)
    private void getEstimatedTimeValue(String cricLev, String issueId) {
        mProgressDialog.show();
        getTmApiEndPointProvider().addRequest(MyTicketSupportEndPoint.getEstimatedTimeValue(cricLev, issueId, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (getActivity() != null) {
                    JsonElement jsonElement = new JsonParser().parse(response);
                    JsonObject jsonResult = jsonElement.getAsJsonObject();
                    jsonResult = jsonResult.getAsJsonObject("Result");
                    JsonArray jsonData = jsonResult.getAsJsonArray("Data");
                    if (jsonData.size() > 0) {
                        jsonResult = jsonData.get(0).getAsJsonObject();
                        if (jsonResult.has("EstimatedTimeValue")) {
                            EstimatedTimeValue = jsonResult.get("EstimatedTimeValue").getAsInt();
                        }
                    }
                    mProgressDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }), MY_TICKET_SUPPORT_REQUEST_HT_ESTIMATED_TIME_TAG);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    //  getMyTicketProcessStaffList(String queue, String locationId, String branchId)
    private void getMyTicketProcessStaffList(String queue, String locationId, String branchId) {
        mProgressDialog.show();
        getTmApiEndPointProvider().addRequest(MyTicketSupportEndPoint.getProcessStaffSupportList(queue, locationId, branchId, new Response.Listener<MyProcessStaffSupportObject>() {
            @Override
            public void onResponse(MyProcessStaffSupportObject ticketProcessStaffObject) {
                if (getActivity() != null) {
                    if (ticketProcessStaffObject != null && ticketProcessStaffObject.getSpinnerObjectList() != null) {
                        if (ticketProcessStaffObject.getSpinnerObjectList().size() > 0) {
                            mProcessStaffList.clear();
                            mProcessStaffList = new ArrayList<SpinnerObject>(ticketProcessStaffObject.getSpinnerObjectList());
                            mProcessStaffList.add(0, mProcessStaffObject);
                            TmUtils.setSpinnerAdapter(getActivity(), mSpinnerTicketSupportProcessStaff, mProcessStaffList);
                            CURRENT_PROCESS_STAFF_SELECTION = mSpinnerTicketSupportProcessStaff.getSelectedItemPosition();
                        }
                    }
                    mProgressDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onErrorListener(error);
            }
        }));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    //  getMyTicketPhoneStaffStaffList(String staffEmail)
    private void getMyTicketPhoneStaffStaffList(String staffEmail) {
        getTmApiEndPointProvider().addRequest(MyTicketDetailEndPoint.getMyTicketProcessStaff(staffEmail, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (getActivity() != null) {
                    JsonElement jsonElement = new JsonParser().parse(response);
                    JsonObject jsonObject = jsonElement.getAsJsonObject();
                    if (jsonObject.has("Result")) {
                        jsonObject = jsonObject.getAsJsonObject("Result");
                        if (jsonObject.has("Data")) {
                            jsonElement = jsonObject.get("Data");
                            if (jsonElement.isJsonArray()) {
                                JsonArray jsonData = jsonElement.getAsJsonArray();
                                if (jsonData.size() > 0) {
                                    jsonObject = jsonData.get(0).getAsJsonObject();
                                    if (jsonObject.has("IPPhone") && jsonObject.has("Mobile")) {
                                        tvTicketSupportProcessStaffIpPhone.setText(jsonObject.get("IPPhone").getAsString().toString());
                                        tvTicketSupportProcessStaffPhoneNumber.setText(jsonObject.get("Mobile").getAsString().toString());
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onErrorListener(error);
            }
        }));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // updatedCreatedTicketSupport(MyTicketSupportObject ticketSupportObject)
    private void updatedCreatedTicketSupport(MyTicketSupportObject ticketSupportObject) {
        getTmApiEndPointProvider().addRequest(MyTicketSupportEndPoint.MyTicketSupportCreated(ticketSupportObject, new Response.Listener<MyResponseObject>() {
            @Override
            public void onResponse(MyResponseObject myResponseObject) {
                if (getActivity() != null) {
                    getTmToast().makeToast(myResponseObject.getErrorDescription(), Toast.LENGTH_SHORT);
                    mProgressDialog.dismiss();
                    if (myResponseObject.getErrorCode().intValue() == 1) {
                        getEventBus().post(new DashBroadChangeMenu(TicketSupport.TICKET_SUPPORT_OF_ME, getActivity().getString(R.string.tm_ticket_support_of_me)));
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onErrorListener(error);
                clearData();
            }
        }), MY_TICKET_SUPPORT_UPDATED_CREATED_TAG);
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////
    // loadDataLocalSpinnerCusType()
    private void loadDataLocalSpinnerCusType() {
        mCusTypeList.clear();
        mCusTypeList.add(0, mCusTypeObject);
        TmUtils.setSpinnerAdapter(getActivity(), mSpinnerTicketSupportCusType, mCusTypeList);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // loadDataLocalSpinnerDescriptionType()
    private void loadDataLocalSpinnerDescriptionType() {
        mDescriptionList.clear();
        mDescriptionList.add(0, mDescriptionObject);
        TmUtils.setSpinnerAdapter(getActivity(), mSpinnerTicketSupportIssueDescription, mDescriptionList);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // loadDataLocalSpinnerReasonList()
    private void loadDataLocalSpinnerReasonList() {
        mReasonList.clear();
        mReasonList.add(0, mReasonObject);
        TmUtils.setSpinnerAdapter(getActivity(), mSpinnerTicketSupportReason, mReasonList);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // getLocationAreaId()
    private String getLocationAreaId() {
        String mLocationAreaId = mSpinnerTicketSupportLocationArea.getSelectedKeyItemsAsString() != ""
                ? mSpinnerTicketSupportLocationArea.getSelectedKeyItemsAsString() : "0";
        return mLocationAreaId;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // getLocationAreaName()
    private String getLocationAreaName() {
        return mSpinnerTicketSupportLocationArea.getSelectedItemsAsString();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // getBranchLocationId()
    private String getBranchLocationId() {
        String mBranchLocationId = mSpinnerTicketSupportBranchLocation.getSelectedKeyItemsAsString() != ""
                ? mSpinnerTicketSupportBranchLocation.getSelectedKeyItemsAsString() : "0";
        return mBranchLocationId;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // getBranchLocationName()
    private String getBranchLocationName() {
        return mSpinnerTicketSupportBranchLocation.getSelectedItemsAsString();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // PrivateImportContractsListener
    private final class PrivateImportContractsListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            ImportContractDialogFragment contractDialogFragment = (ImportContractDialogFragment) getChildFragmentManager().findFragmentByTag(FRAGMENT_IMPORT_CONTRACT_DIALOG_TAG);
            if (contractDialogFragment == null) {
                contractDialogFragment = ImportContractDialogFragment.newInstance(mMyImportContractObject);
            }
            contractDialogFragment.show(getChildFragmentManager(), FRAGMENT_IMPORT_CONTRACT_DIALOG_TAG);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // PrivateSpinnerCallbackInterface selected item selected Spinner Location
    private final class PrivateSpinnerCallbackInterface implements MultiSelectionObjectSpinner.SpinnerUpdateInterfaceCallback {

        @Override
        public void onMultiChooseCallback(String locationKey) {
            if (locationKey.equalsIgnoreCase("")) {
                mSpinnerTicketSupportBranchLocation.setEnabled(false);
                mSpinnerTicketSupportBranchLocation.setSpinnerMultiAdapter(mBranchLocationObject, new ArrayList<SpinnerObject>(), true);
            } else {
                getBranchLocationList(locationKey);
            }

            if (checkedShowOrHideProcessStaff()) {
                SpinnerObject spinnerObject = (SpinnerObject) mSpinnerTicketSupportQueue.getSelectedItem();
                String id = spinnerObject.getKey();
                getMyTicketProcessStaffList(id, getLocationAreaId(), getBranchLocationId());
                tvTicketSupportProcessStaffIpPhone.setText("");
                tvTicketSupportProcessStaffPhoneNumber.setText("");
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // PrivateSpinnerCallbackInterface selected item selected Spinner Location
    private final class PrivateSpinnerCallbackUpdatePriority implements MultiSelectionObjectSpinner.SpinnerUpdateInterfaceCallback {

        @Override
        public void onMultiChooseCallback(String branchLocationId) {
            SpinnerObject issueSpinnerObject = (SpinnerObject) mSpinnerTicketSupportIssueDescription.getSelectedItem();
            SpinnerObject queueSpinnerObject = (SpinnerObject) mSpinnerTicketSupportQueue.getSelectedItem();
            String issueId = issueSpinnerObject.getKey();
            String queueId = queueSpinnerObject.getKey();

            getPriorityList(getLocationAreaId(), getBranchLocationId(), issueId);
            if (!queueId.equals("0")) {
                getOperateStaffList(queueId, getLocationAreaId(), getBranchLocationId(), EXTRA_TICKET_CRICLEV_KEY);
                getVisorStaffList(queueId, getLocationAreaId(), getBranchLocationId());
            }

            if (checkedShowOrHideProcessStaff()) {
                SpinnerObject spinnerObject = (SpinnerObject) mSpinnerTicketSupportQueue.getSelectedItem();
                String id = spinnerObject.getKey();
                getMyTicketProcessStaffList(id, getLocationAreaId(), getBranchLocationId());
                tvTicketSupportProcessStaffIpPhone.setText("");
                tvTicketSupportProcessStaffPhoneNumber.setText("");
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // PrivateSpinnerItemSelectedListener
    private final class PrivateSpinnerItemSelectedListener implements Spinner.OnItemSelectedListener {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            Spinner spinner = (Spinner) parent;
            SpinnerObject spinnerObject = (SpinnerObject) parent.getItemAtPosition(position);
            switch (spinner.getId()) {
                case R.id.tm_spinner_ticket_support_region_service_type:
                    if (CURRENT_SERVICE_TYPE_SPINNER_SELECTION != position) {
                        onSelectionItemSpinnerServiceType(position, spinnerObject);
                        CURRENT_SERVICE_TYPE_SPINNER_SELECTION = position;
                    }
                    break;

                case R.id.tm_spinner_ticket_support_region_cus_type:
                    if (CURRENT_CUSTOM_TYPE_SPINNER_SELECTION != position) {
                        onSelectionItemSpinnerCustomType(position, spinnerObject);
                        CURRENT_CUSTOM_TYPE_SPINNER_SELECTION = position;
                    }
                    break;

                case R.id.tm_spinner_ticket_support_region_issue_description:
                    if (CURRENT_ISSUE_DESCRIPTION_SPINNER_SELECTION != position) {
                        onSelectionItemSpinnerDescription(position, spinnerObject);
                        CURRENT_ISSUE_DESCRIPTION_SPINNER_SELECTION = position;
                    }
                    break;

                case R.id.tm_spinner_queue:
                    if (CURRENT_QUEUE_SPINNER_SELECTION != position) {
                        onSelectionItemSpinnerQueue(position, spinnerObject);
                        CURRENT_QUEUE_SPINNER_SELECTION = position;
                    }
                    break;
                case R.id.tm_spinner_employee_process_staff:
                    if (CURRENT_PROCESS_STAFF_SELECTION != position) {
                        onSelectionItemSpinnerProcessStaff(position, spinnerObject);
                        CURRENT_PROCESS_STAFF_SELECTION = position;
                    }
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }

        private void onSelectionItemSpinnerServiceType(int position, SpinnerObject spinnerObject) {
            if (position == 0) {
                mSpinnerTicketSupportCusType.setEnabled(false);
                mSpinnerTicketSupportIssueDescription.setEnabled(false);
                mSpinnerTicketSupportReason.setEnabled(false);
                loadDataLocalSpinnerCusType();
                loadDataLocalSpinnerDescriptionType();
            } else {
                mSpinnerTicketSupportCusType.setEnabled(true);
                mSpinnerTicketSupportIssueDescription.setEnabled(false);
                mSpinnerTicketSupportReason.setEnabled(false);
                loadDataLocalSpinnerDescriptionType();
                loadDataLocalSpinnerReasonType();
                getCustomTypeList(spinnerObject.getKey());
            }
        }

        private void onSelectionItemSpinnerCustomType(int position, SpinnerObject spinnerObject) {
            if (position == 0) {
                mSpinnerTicketSupportIssueDescription.setEnabled(false);
                mSpinnerTicketSupportReason.setEnabled(false);
                loadDataLocalSpinnerReasonList();
                loadDataLocalSpinnerDescriptionType();
            } else {
                mSpinnerTicketSupportIssueDescription.setEnabled(true);
                mSpinnerTicketSupportReason.setEnabled(true);
                getIssueDescriptionList(spinnerObject.getKey());
                getReasonList(spinnerObject.getKey());
            }
        }

        private void onSelectionItemSpinnerDescription(int position, SpinnerObject spinnerObject) {
            if (position == 0) {
                tvTicketSupportRegionPriority.setText("0");
            } else {
                getPriorityList(getLocationAreaId(), getBranchLocationId(), spinnerObject.getKey());
                getEstimatedTimeValue(EXTRA_TICKET_CRICLEV_KEY, spinnerObject.getKey());
            }
        }

        private void onSelectionItemSpinnerQueue(int position, SpinnerObject spinnerObject) {
            if (position == 0) {
                //mSpinnerTicketSupportOperateStaff.setEnabled(false);
                //mSpinnerTicketSupportVisorStaff.setEnabled(false);
                loadDataLocalSpinnerVisorStaff();
                loadDataLocalSpinnerOperateStaff();
            } else {
                // mSpinnerTicketSupportOperateStaff.setEnabled(true);
                //mSpinnerTicketSupportVisorStaff.setEnabled(true);
                String queue = spinnerObject.getKey();
                String locationArea = getLocationAreaId();
                String branchLocation = getBranchLocationId();
                String criLev = EXTRA_TICKET_CRICLEV_KEY;
                getOperateStaffList(queue, locationArea, branchLocation, criLev);
                getVisorStaffList(queue, locationArea, branchLocation);
            }
            //check is allow choose operate, visitor staff
            if (checkedShowOrHideProcessStaff()) {
                mSpinnerTicketSupportOperateStaff.setEnabled(true);
                mSpinnerTicketSupportVisorStaff.setEnabled(true);
                //check is allow choose process staff
                if (getTmSharePreferencesUtils().getUserLogin().getAssign().equalsIgnoreCase(EXTRA_TICKET_USER_ASSIGN_KEY)) {
                    mLayoutEmployeeProcessStaff.setVisibility(View.VISIBLE);
                    getMyTicketProcessStaffList(spinnerObject.getKey(), getLocationAreaId(), getBranchLocationId());
                } else {
                    mLayoutEmployeeProcessStaff.setVisibility(View.GONE);
                }
            } else {
                mSpinnerTicketSupportOperateStaff.setEnabled(false);
                mSpinnerTicketSupportVisorStaff.setEnabled(false);
            }
            if (getTmSharePreferencesUtils().getUserLogin().getAssign().equalsIgnoreCase(EXTRA_TICKET_USER_ASSIGN_KEY)) {

                if (checkedShowOrHideProcessStaff()) {
                    mLayoutEmployeeProcessStaff.setVisibility(View.VISIBLE);
                    getMyTicketProcessStaffList(spinnerObject.getKey(), getLocationAreaId(), getBranchLocationId());
                } else {
                    mLayoutEmployeeProcessStaff.setVisibility(View.GONE);
                }
            } else {
                mLayoutEmployeeProcessStaff.setVisibility(View.GONE);
            }


        }

        private void onSelectionItemSpinnerProcessStaff(int position, SpinnerObject spinnerObject) {
            if (position == 0) {
                tvTicketSupportProcessStaffPhoneNumber.setText("");
                tvTicketSupportProcessStaffIpPhone.setText("");
            } else {
                // spinnerObject.getIsEffect() = get Email
                getMyTicketPhoneStaffStaffList(spinnerObject.getIsEffect());
            }
        }
    }

    private boolean checkedShowOrHideProcessStaff() {
        SpinnerObject spinnerObject = (SpinnerObject) mSpinnerTicketSupportQueue.getSelectedItem();
        String id = spinnerObject.getKey();
        String value = spinnerObject.getValue();
        if (value.equalsIgnoreCase(getTmSharePreferencesUtils().getDivisionUserLogin()) && id.equalsIgnoreCase(String.valueOf(getTmSharePreferencesUtils().getUserLogin().getParentQueue()))) {
            return true;
        }
        return false;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // PrivateCreatedTicketSupportListener
    private final class PrivateCreatedTicketSupportListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            if (checkNullIfNeed()) {
                onConfirmCreatedTicketSupport();
            }
        }
    }

    private void onConfirmCreatedTicketSupport() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(getString(R.string.tm_alert_dialog_updated_ticket_support));
        builder.setMessage(getString(R.string.tm_alert_dialog_want_updated_support));
        builder.setPositiveButton(getString(R.string.tm_alert_dialog_confirm_updated_support), new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                mProgressDialog.setMessage(getString(R.string.tm_progress_dialog_updated_ticket_detail_support));
                mProgressDialog.show();
                updatedCreatedTicketSupport(getMyTicketSupportObject());
                dialog.dismiss();
            }

        });

        builder.setNegativeButton(getString(R.string.tm_alert_dialog_cancel_updated), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    private void showDialogNotCheckSpinner(String titleDialog) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        builder.setTitle("Bạn chưa " + titleDialog.toLowerCase());
        builder.setPositiveButton("Hủy", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }

        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private boolean checkNullIfNeed() {
        TextView tLocationAreaId = (TextView) mSpinnerTicketSupportLocationArea.getChildAt(0);
        TextView tLocationId = (TextView) mSpinnerTicketSupportBranchLocation.getChildAt(0);
        TextView tSupportRegionServiceType = (TextView) mSpinnerTicketSupportRegionServiceType.getChildAt(0);
        TextView tTicketSupportCusType = (TextView) mSpinnerTicketSupportCusType.getChildAt(0);
        TextView tTicketSupportIssueDescription = (TextView) mSpinnerTicketSupportIssueDescription.getChildAt(0);
        TextView tTicketSupportReason = (TextView) mSpinnerTicketSupportReason.getChildAt(0);
        TextView tTicketSupportQueue = (TextView) mSpinnerTicketSupportQueue.getChildAt(0);
        TextView tTicketSupportOperateStaff = (TextView) mSpinnerTicketSupportOperateStaff.getChildAt(0);
        TextView tTicketSupportVisorStaff = (TextView) mSpinnerTicketSupportVisorStaff.getChildAt(0);
        if (getLocationAreaId().equalsIgnoreCase("0")) {
            showDialogNotCheckSpinner(tLocationAreaId.getText().toString());
            tLocationAreaId.setTextColor(Color.RED);
            return false;
        } else {
            tLocationAreaId.setTextColor(Color.BLACK);
        }
        if (getBranchLocationId().equalsIgnoreCase("0")) {
            showDialogNotCheckSpinner(tLocationId.getText().toString());
            tLocationId.setTextColor(Color.RED);
            return false;
        } else {
            tLocationId.setTextColor(Color.BLACK);
        }

        if (tvTicketSupportRegionRequirement.getText().toString().equalsIgnoreCase("")) {
            showDialogNotCheckSpinner("yêu cầu");
            tvTicketSupportRegionRequirement.setFocusable(true);
            return false;
        }

        if (mSpinnerTicketSupportRegionServiceType.getSelectedItemPosition() == 0) {
            showDialogNotCheckSpinner(tSupportRegionServiceType.getText().toString());
            tSupportRegionServiceType.setTextColor(Color.RED);
            return false;
        } else {
            tSupportRegionServiceType.setTextColor(Color.BLACK);
        }
        if (mSpinnerTicketSupportCusType.getSelectedItemPosition() == 0) {
            showDialogNotCheckSpinner(tTicketSupportCusType.getText().toString());
            tTicketSupportCusType.setTextColor(Color.RED);
            return false;
        } else {
            tTicketSupportCusType.setTextColor(Color.BLACK);
        }
        if (mSpinnerTicketSupportIssueDescription.getSelectedItemPosition() == 0) {
            showDialogNotCheckSpinner(tTicketSupportIssueDescription.getText().toString());
            tTicketSupportIssueDescription.setTextColor(Color.RED);
            return false;
        } else {
            tTicketSupportIssueDescription.setTextColor(Color.BLACK);
        }
        if (mSpinnerTicketSupportReason.getSelectedItemPosition() == 0) {
            showDialogNotCheckSpinner(tTicketSupportReason.getText().toString());
            tTicketSupportReason.setTextColor(Color.RED);
            return false;
        } else {
            tTicketSupportReason.setTextColor(Color.BLACK);
        }

        if (mSpinnerTicketSupportQueue.getSelectedItemPosition() == 0) {
            showDialogNotCheckSpinner(tTicketSupportQueue.getText().toString());
            tTicketSupportQueue.setTextColor(Color.RED);
            return false;
        } else {
            tTicketSupportQueue.setTextColor(Color.BLACK);
        }
        return true;
    }

    private MyTicketSupportObject getMyTicketSupportObject() {
        SpinnerObject mOperateStaffObject = (SpinnerObject) mSpinnerTicketSupportOperateStaff.getSelectedItem();
        SpinnerObject mVisorStaffObject = (SpinnerObject) mSpinnerTicketSupportVisorStaff.getSelectedItem();
        SpinnerObject mQueueStaffObject = (SpinnerObject) mSpinnerTicketSupportQueue.getSelectedItem();
        SpinnerObject mServiceTypeObject = (SpinnerObject) mSpinnerTicketSupportRegionServiceType.getSelectedItem();
        SpinnerObject mCusTypeObject = (SpinnerObject) mSpinnerTicketSupportCusType.getSelectedItem();
        SpinnerObject mIssueDescription = (SpinnerObject) mSpinnerTicketSupportIssueDescription.getSelectedItem();
        SpinnerObject mReasonObject = (SpinnerObject) mSpinnerTicketSupportReason.getSelectedItem();

        String Kind = String.valueOf(EXTRA_TICKET_KIND_KEY);
        String TicketStatus = String.valueOf(EXTRA_TICKET_STATUS_KEY);
        String Title = tvTicketSupportRegionRequirement.getText().toString();
        String Description = tvTicketSupportComment.getText().toString();
        String Priority = tvTicketSupportRegionPriority.getText().toString();
        String CricLev = tvTicketSupportRegionCricLev.getText().toString();
        String EstimatedTime = String.valueOf(EstimatedTimeValue);
        String FoundStaff = getTmSharePreferencesUtils().getUserIdLogin();
        String FoundDivision = String.valueOf(getTmSharePreferencesUtils().getUserLogin().getDivisionID());
        String FoundPhone = getTmSharePreferencesUtils().getUserLogin().getMobile();
        String FoundIP = getTmSharePreferencesUtils().getUserLogin().getIPPhone();
        String RemindStatus = EXTRA_TICKET_REMIND_STATUS_KEY;
        String HasWarning = String.valueOf(EXTRA_TICKET_HAS_WARNING_KEY);
        String OperateStaff = mOperateStaffObject.getKey();
        String VisorStaff = mVisorStaffObject.getKey();
        String ProcessStaff = "";
        String ProcessPhone = "";
        String ProcessIP = "";
        String Queue = mQueueStaffObject.getKey();
        String ParentID = String.valueOf(EXTRA_TICKET_PARENT_ID);
        String LocationID = getLocationAreaId();
        String EffBranch = getBranchLocationId();
        String ServiceType = mServiceTypeObject.getKey();
        String CusType = mCusTypeObject.getKey();
        String IssueID = mIssueDescription.getKey();
        String ReasonGroup = mReasonObject.getKey();
        String ProcessStep = "";
        String ProcessName = "";
        String IssueName = mIssueDescription.getValue();
        String LocationName = getLocationAreaName();
        String BranchName = getBranchLocationName();
        String ServiceName = mServiceTypeObject.getValue();
        String CusName = mCusTypeObject.getValue();
        String FoundDivName = getTmSharePreferencesUtils().getDivisionUserLogin();
        String FoundName = getTmSharePreferencesUtils().getUserName();
        String Options = "";

        if (checkedShowOrHideProcessStaff() && getTmSharePreferencesUtils().getUserLogin().getAssign().equalsIgnoreCase(EXTRA_TICKET_USER_ASSIGN_KEY)  && mSpinnerTicketSupportProcessStaff.getSelectedItemPosition() != 0) {
            SpinnerObject spinnerObject = (SpinnerObject) mSpinnerTicketSupportProcessStaff.getSelectedItem();
                ProcessStaff = spinnerObject.getKey();
                ProcessName = spinnerObject.getValue();
            ProcessPhone = tvTicketSupportProcessStaffPhoneNumber.getText().toString();
            ProcessIP = tvTicketSupportProcessStaffIpPhone.getText().toString();
        } else {
            ProcessStaff = "";
            ProcessName = "";
            ProcessPhone = "";
            ProcessIP = "";
        }

        if (mMyImportContractObject.getImportContractObjectList().size() > 0) {
            StringBuilder stringBuilder = new StringBuilder();
            boolean foundOne = false;

            for (int i = 0; i < this.mMyImportContractObject.getImportContractObjectList().size(); ++i) {
                if (foundOne) {
                    stringBuilder.append(";");
                }
                foundOne = true;
                stringBuilder.append(this.mMyImportContractObject.getImportContractObjectList().get(i).toString());
            }
            Options = stringBuilder.toString();
            Log.d("tmt", "stringbuilder : " + stringBuilder.toString());

        } else {
            Options = "";
        }

        MyTicketSupportObject ticketSupportObject = new MyTicketSupportObject();
        ticketSupportObject.setKind(Kind);
        ticketSupportObject.setTicketStatus(TicketStatus);
        ticketSupportObject.setTitle(Title);
        ticketSupportObject.setDescription(Description);
        ticketSupportObject.setPriority(Priority);
        ticketSupportObject.setCricLev(CricLev);
        ticketSupportObject.setEstimatedTime(EstimatedTime);
        ticketSupportObject.setFoundStaff(FoundStaff);
        ticketSupportObject.setFoundDivision(FoundDivision);
        ticketSupportObject.setFoundPhone(FoundPhone);
        ticketSupportObject.setFoundIP(FoundIP);
        ticketSupportObject.setRemindStatus(RemindStatus);
        ticketSupportObject.setHasWarning(HasWarning);
        ticketSupportObject.setOperateStaff(OperateStaff);
        ticketSupportObject.setVisorStaff(VisorStaff);
        ticketSupportObject.setProcessStaff(ProcessStaff);
        ticketSupportObject.setProcessPhone(ProcessPhone);
        ticketSupportObject.setProcessIP(ProcessIP);
        ticketSupportObject.setQueue(Queue);
        ticketSupportObject.setParentID(ParentID);
        ticketSupportObject.setLocationID(LocationID);
        ticketSupportObject.setEffBranch(EffBranch);
        ticketSupportObject.setServiceType(ServiceType);
        ticketSupportObject.setCusType(CusType);
        ticketSupportObject.setIssueID(IssueID);
        ticketSupportObject.setReasonGroup(ReasonGroup);
        ticketSupportObject.setProcessStep(ProcessStep);
        ticketSupportObject.setProcessName(ProcessName);
        ticketSupportObject.setIssueName(IssueName);
        ticketSupportObject.setLocationName(LocationName);
        ticketSupportObject.setBranchName(BranchName);
        ticketSupportObject.setServiceName(ServiceName);
        ticketSupportObject.setCusName(CusName);
        ticketSupportObject.setFoundDivName(FoundDivName);
        ticketSupportObject.setFoundName(FoundName);
        ticketSupportObject.setOptions(Options);

        return ticketSupportObject;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // PrivateInputReplyListener
    private final class PrivateInputReplyListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            clearData();
        }
    }

    private void clearData() {
        setupLoadDataLocal();
        setupLoadDataToView();
        tvTicketSupportRegionRequirement.setText("");
        tvTicketSupportComment.setText("");
        mMyImportContractObject = null;
    }

    private void onErrorListener(VolleyError error) {
        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
            getTmToast().makeToast(getString(R.string.tm_error_connect), Toast.LENGTH_SHORT);
        } else if (error instanceof AuthFailureError) {
            Log.d("tmt", "AuthFailureError");
        } else if (error instanceof ServerError) {
            Log.d("tmt", "ServerError");
        } else if (error instanceof NetworkError) {
            Log.d("tmt", "NetworkError");
        } else if (error instanceof ParseError) {
            Log.d("tmt", "ParseError");
        } else {
            Log.d("tmt", "onErrorResponse : " + error.getMessage());
        }
        //   TmUtilsVolleyErrorListener.onErrorListener(getActivity(), getView() ,error);
        mProgressDialog.dismiss();
    }


    @Subscribe
    public void onImportContractEvent(ImportContractObjectEvent importContractObjectEvent) {
        mImportContractObjectList.clear();
        mImportContractObjectList = new ArrayList<>(importContractObjectEvent.getImportContractObjectList());
        mMyImportContractObject.setImportContractObjectList(mImportContractObjectList);
    }
}
