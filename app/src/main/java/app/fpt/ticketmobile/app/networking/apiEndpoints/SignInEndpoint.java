package app.fpt.ticketmobile.app.networking.apiEndpoints;

import android.support.annotation.NonNull;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.util.HashMap;
import java.util.Map;

import app.fpt.ticketmobile.app.model.User;
import app.fpt.ticketmobile.app.networking.utils.GsonRequest;

/**
 * Created by ToanMaster on 6/14/2016.
 */
public abstract class SignInEndpoint {

    private static String PLATFORM = "1";

    private static  Map<String, String> getHeader(String username, String password, String otp){
        Map<String, String>   params = new HashMap<String, String>();
        params.put("UserName", username);
        params.put("Password", password);
        params.put("OTP", otp);
        params.put("Platform", PLATFORM);
        return params;
    }

    public static GsonRequest<User> getUserObject(final String userName, final String password , final String otp, @NonNull final Response.Listener<User> listener, @NonNull final Response.ErrorListener errorListener) throws AuthFailureError {
        GsonRequest<User> userGsonRequest =   new GsonRequest<User>( Constants.SING_IN_ENDPOINT,  User.class, listener, errorListener){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return getHeader(userName, password, otp);
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                return super.parseNetworkError(volleyError);
            }
        };
        return userGsonRequest;
    }



}
