package app.fpt.ticketmobile.app.ui.adapter;

import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Administrator on 7/12/2016.
 */
public abstract class TmRecycleViewAdapter<VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> {

    private static final AtomicInteger VIEW_TYPE_ASSIGNER = new AtomicInteger(0);
    private final Context mContext;

    public static abstract interface Callback{
        public abstract void onDataSetChanged();
    }

    public TmRecycleViewAdapter(Context context, Callback callback){
        this.mContext = context.getApplicationContext();
        if (context != null){
            registerAdapterDataObserver(new PrivateDataObserver(callback));
        }
    }

    protected static int assignUniqueViewType(){
        return VIEW_TYPE_ASSIGNER.getAndIncrement();
    }

    protected final int getColor(int resId){
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return mContext.getResources().getColor(resId);
        }else {
            return mContext.getResources().getColor(resId, mContext.getTheme());
        }
    }

    protected final String getString(int resId){
        return mContext.getString(resId);
    }

    protected final String getString(int resId, Object... formatArgs){
        return mContext.getString(resId, formatArgs);
    }

    public static class PrivateDataObserver extends RecyclerView.AdapterDataObserver{

        private Callback mCallback;

        public PrivateDataObserver(Callback callback){
            this.mCallback = callback;
        }

        private void informCallback(){
            mCallback.onDataSetChanged();
        }

        @Override
        public void onChanged() {
            super.onChanged();
            informCallback();
        }

        @Override
        public void onItemRangeChanged(int positionStart, int itemCount) {
            super.onItemRangeChanged(positionStart, itemCount);
            informCallback();
        }

        @Override
        public void onItemRangeChanged(int positionStart, int itemCount, Object payload) {
            super.onItemRangeChanged(positionStart, itemCount, payload);
            informCallback();
        }

        @Override
        public void onItemRangeInserted(int positionStart, int itemCount) {
            super.onItemRangeInserted(positionStart, itemCount);
            informCallback();
        }

        @Override
        public void onItemRangeMoved(int fromPosition, int toPosition, int itemCount) {
            super.onItemRangeMoved(fromPosition, toPosition, itemCount);
            informCallback();
        }

        @Override
        public void onItemRangeRemoved(int positionStart, int itemCount) {
            super.onItemRangeRemoved(positionStart, itemCount);
            informCallback();
        }
    }


}
