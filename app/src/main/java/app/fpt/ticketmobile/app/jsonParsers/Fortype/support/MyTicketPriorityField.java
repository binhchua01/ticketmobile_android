package app.fpt.ticketmobile.app.jsonParsers.Fortype.support;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import app.fpt.ticketmobile.app.model.support.MyTicketPriorityObject;

/**
 * Created by Administrator on 7/7/2016.
 */
public class MyTicketPriorityField {

    public static final class Deserializer implements JsonDeserializer<MyTicketPriorityObject>{

        @Override
        public MyTicketPriorityObject deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            Gson gson = new Gson();
            MyTicketPriorityObject ticketPriorityObject = null;

            try{
                JsonObject jsResult = json.getAsJsonObject();
                if (jsResult.has("Result")) {
                    jsResult = jsResult.getAsJsonObject("Result");
                    if (jsResult.has("Data")) {
                        if (jsResult.get("Data").isJsonArray()) {
                            ticketPriorityObject = gson.fromJson(jsResult, MyTicketPriorityObject.class);
                        }
                    }
                }

            }catch(Exception e){
                Log.e("tmt", "MyTicketPriorityField : Deserializer : " + e.toString());
            }

            return ticketPriorityObject;
        }
    }
}
