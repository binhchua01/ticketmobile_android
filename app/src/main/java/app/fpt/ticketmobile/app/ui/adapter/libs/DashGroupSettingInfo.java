package app.fpt.ticketmobile.app.ui.adapter.libs;

import app.fpt.ticketmobile.app.ui.lib.TicketSettingInfo;

/**
 * Created by Administrator on 7/28/2016.
 */
public class DashGroupSettingInfo {
    private final TicketSettingInfo mTicketSettingInfo;
    private final String mTicketSupportTitle;

    public DashGroupSettingInfo(TicketSettingInfo ticketSettingInfo, String mTicketSupportTitle) {
        this.mTicketSettingInfo = ticketSettingInfo;
        this.mTicketSupportTitle = mTicketSupportTitle;
    }

    public TicketSettingInfo getTicketSetting() {
        return mTicketSettingInfo;
    }

    public String getTicketSettingTitle() {
        return mTicketSupportTitle;
    }
}
