package app.fpt.ticketmobile.app.model;

import java.util.List;

/**
 * Created by ToanMaster on 6/29/2016.
 */
public class MyTicketCusTypeObject {

    private List<CusTypeObject> Data;
    private List<SpinnerObject> mSpinnerObjectList;

    public List<CusTypeObject> getData() {
        return Data;
    }

    public void setData(List<CusTypeObject> data) {
        Data = data;
    }

    public List<SpinnerObject> getSpinnerObjectList() {
        return mSpinnerObjectList;
    }

    public void setSpinnerObjectList(List<SpinnerObject> mSpinnerObjectList) {
        this.mSpinnerObjectList = mSpinnerObjectList;
    }

    public class CusTypeObject{
        private String ID;
        private String Description;

        public String getDescription() {
            return Description;
        }

        public void setDescription(String description) {
            Description = description;
        }

        public String getID() {
            return ID;
        }

        public void setID(String ID) {
            this.ID = ID;
        }

        public CusTypeObject(String ID, String description) {
            Description = description;
            this.ID = ID;
        }
    }


}
