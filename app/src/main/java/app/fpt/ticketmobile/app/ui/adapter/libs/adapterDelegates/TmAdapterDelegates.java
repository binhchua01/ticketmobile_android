package app.fpt.ticketmobile.app.ui.adapter.libs.adapterDelegates;

import android.support.v7.widget.RecyclerView;

import app.fpt.ticketmobile.app.ui.adapter.libs.AdapterDelegate;

/**
 * Created by Administrator on 7/12/2016.
 */
public abstract class TmAdapterDelegates<T, VH extends RecyclerView.ViewHolder> implements AdapterDelegate<T, VH> {

    private final int mItemViewType;

    public TmAdapterDelegates(int itemViewType) {
        this.mItemViewType = itemViewType;
    }

    @Override
    public int getItemViewType() {
        return mItemViewType;
    }
}
