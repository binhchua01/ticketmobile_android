package app.fpt.ticketmobile.app.model.support;

import java.util.List;

import app.fpt.ticketmobile.app.model.SpinnerObject;

/**
 * Created by Administrator on 7/7/2016.
 */
public class MyTicketBranchLocationObject {

    private List<SpinnerObject> mSpinnerObjectList;
    private List<BranchLocationObject> Data;

    public List<BranchLocationObject> getBranchLocationList() {
        return Data;
    }

    public void setBranchLocationList(List<BranchLocationObject> data) {
        Data = data;
    }

    public List<SpinnerObject> getSpinnerObjectList() {
        return mSpinnerObjectList;
    }

    public void setSpinnerObjectList(List<SpinnerObject> mSpinnerObjectList) {
        this.mSpinnerObjectList = mSpinnerObjectList;
    }

    public class BranchLocationObject{
        private Integer BranchID;
        private String BranchName;

        public Integer getBranchID() {
            return BranchID;
        }

        public void setBranchID(Integer branchID) {
            BranchID = branchID;
        }

        public String getBranchName() {
            return BranchName;
        }

        public void setBranchName(String branchName) {
            BranchName = branchName;
        }
    }

}
