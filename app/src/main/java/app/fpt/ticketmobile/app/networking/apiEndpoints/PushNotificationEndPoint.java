package app.fpt.ticketmobile.app.networking.apiEndpoints;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;

import java.util.HashMap;
import java.util.Map;

import app.fpt.ticketmobile.app.model.MyResponseObject;
import app.fpt.ticketmobile.app.networking.utils.GsonRequest;

/**
 * Created by Administrator on 7/29/2016.
 */
public class PushNotificationEndPoint {

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // register deviceToken
    public static GsonRequest<MyResponseObject> registerDeviceToken(final String email, final String device_Token, final String platform, final String signature, Response.Listener<MyResponseObject> listener, Response.ErrorListener errorListener){
        GsonRequest<MyResponseObject> request = new GsonRequest<MyResponseObject>(Constants.MY_TICKET_REGISTER_PUSH_NOTIFICATION, MyResponseObject.class, listener, errorListener){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return getParamRegisterDeviceToken(email, device_Token, platform, signature);
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return getHeaderRegisterDeviceToken();
            }
        };

        return  request;
    }

    private static Map<String, String> getParamRegisterDeviceToken(String email, String device_Token, String platform, String signature){
        Map<String, String> params = new HashMap<>();
        params.put("email", email);
        params.put("device_token", device_Token);
        params.put("platform", platform);
        params.put("signature", signature);

        return params;
    }

    private static Map<String, String> getHeaderRegisterDeviceToken(){
        Map<String, String> params = new HashMap<>();
        params.put("PublicKey", "1417124836");
        return params;
    }

}
