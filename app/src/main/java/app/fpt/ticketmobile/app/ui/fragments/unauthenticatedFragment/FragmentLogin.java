package app.fpt.ticketmobile.app.ui.fragments.unauthenticatedFragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import app.fpt.ticketmobile.app.db.TmSharePreferencesUtils;
import app.fpt.ticketmobile.app.model.IAMDeviceTokenModel;
import app.fpt.ticketmobile.app.model.User;
import app.fpt.ticketmobile.R;
import app.fpt.ticketmobile.app.lib.utils.TmUtils;
import app.fpt.ticketmobile.app.model.UserIam;
import app.fpt.ticketmobile.app.model.checkversion.CheckVersionObject;
import app.fpt.ticketmobile.app.networking.apiEndpoints.CheckApkVersionEndpoint;
import app.fpt.ticketmobile.app.networking.apiEndpoints.Constants;
import app.fpt.ticketmobile.app.networking.apiEndpoints.LoginWithIamWcf;
import app.fpt.ticketmobile.app.networking.apiEndpoints.SignInEndpoint;
import app.fpt.ticketmobile.app.ui.activities.authenticatedActivities.IAMLoginActivity;
import app.fpt.ticketmobile.app.ui.activities.unauthenticatedActivities.AuthActivity;
import app.fpt.ticketmobile.app.ui.activities.unauthenticatedActivities.SplashScreenActivity;
import app.fpt.ticketmobile.app.ui.fragments.TmBaseFragment;

/**
 * Created by ToanMaster on 6/15/2016.
 */
public class FragmentLogin extends TmBaseFragment {

    private static final String EXTRA_TICKET_REQUEST_LOGIN_KEY = "extra_ticket_request_login_key";
    private static final String TAG_SIGN_IN_KEY = "tag_sign_in_key";

    private static final String EMAIL_ERRORS_KEY = "email_errors_key";
    private static final String LAST_ENTERED_EMAIL_KEY = "last_entered_email_key";
    private static final String LAST_ENTERED_PASSWORD_KEY = "last_entered_password_key";
    private static final String PASSWORD_ERRORS_KEY = "password_errors_key";

    private static final String LOGGED_IN_USER_EMAIL_KEY = "logger_in_user_email_key";
    private static final String IAM_IN_AUTH = "iam_in_auth";

    private TextInputLayout mEmailTextInputLayout;
    private AutoCompleteTextView mEmailView;
    private TextInputLayout mPasswordTextInputLayout;
    private TextView mPasswordView;
    private TextInputLayout mOtpTextInputLayout;
    private TextView mOtpView, mLoginIAM;

    private ProgressDialog progressDialog;
    private JsonObjectRequest mDeviceIAMRequest;
    private RequestQueue mRequestQueue;

    private static final int LOGIN_REQUEST_CODE = 101;
    private static final int REQUEST_CODE_AIM_SUCCESS = 102;
    private static final String EXTRA_IAM_TAG = "extra_iam_tag";



    private IAMDeviceTokenModel modelsIAM = new IAMDeviceTokenModel();

    public FragmentLogin() {
        super(null, Integer.valueOf(R.layout.fragment_login), false, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getView() != null) {
            setupViews(getView());
            getIAMTokenDevice(getTmSharePreferencesUtils().getKongToken());
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        getTmApiEndPointProvider().cancelAllRequest(TAG_SIGN_IN_KEY);
    }


    private void setupViews(View parent) {
        if (parent != null && getActivity() != null) {
            TmUtils.setupHideKeyBoardParent(parent, getActivity());
        }
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);

        mEmailTextInputLayout = (TextInputLayout) parent.findViewById(R.id.email_container);
        mEmailView = (AutoCompleteTextView) parent.findViewById(R.id.tm_email);
        mPasswordTextInputLayout = (TextInputLayout) parent.findViewById(R.id.password_container);
        mPasswordView = (TextView) parent.findViewById(R.id.tm_password);

        mOtpTextInputLayout = (TextInputLayout) parent.findViewById(R.id.otp_container);
        mOtpView = (TextView) parent.findViewById(R.id.tm_otp);

        mLoginIAM = (TextView) parent.findViewById(R.id.sign_in_iam);

        parent.findViewById(R.id.sign_in).setOnClickListener(new PrivateSignInListener());
        parent.findViewById(R.id.sign_in_iam).setOnClickListener(new PrivateSignInIAMListener());
    }

    private void getIAMTokenDevice(final String kongToken) {
        mRequestQueue = Volley.newRequestQueue(getContext());
        mDeviceIAMRequest = new JsonObjectRequest(Request.Method.GET, Constants.URL_STAGING_DEVICE_TOKEN, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Gson gson = new Gson();
                modelsIAM = gson.fromJson(jsonObject.toString(), IAMDeviceTokenModel.class);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("TAG", "Error :" + error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                return getHeaderIAM(kongToken);
            }
        };
        mRequestQueue.add(mDeviceIAMRequest);
    }

    private static Map<String, String> getHeaderIAM(String token) {
        Map<String, String> params = new HashMap<>();
        params.put("Authorization", "Bearer " + token.replace('"', ' '));
        return params;
    }

    private final class PrivateSignInListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {

            String username = mEmailView.getText().toString();
            final String password = mPasswordView.getText().toString();

            String mOtp = null;
            mOtp = mOtpView.getText().toString() != "" ? mOtpView.getText().toString() : "0";

            //  if (CredentialsValidator.EmailValidationResult(username) && CredentialsValidator.PasswordValidateResult(password)) {
            progressDialog.setMessage("Đang đăng nhập...");
            progressDialog.show();
            try {
                getTmApiEndPointProvider().addRequest(SignInEndpoint.getUserObject(username, password, mOtp, new Response.Listener<User>() {
                    @Override
                    public void onResponse(User user) {
                        if (user.getErrorDescription() != null) {
                            getTmToast().makeToast(user.getErrorDescription(), Toast.LENGTH_SHORT);
                            progressDialog.dismiss();
                        } else {
                            getTmSharePreferencesUtils().saveUserLogin(user);
                            getTmSharePreferencesUtils().saveCheckUserLogin(user.getEmail(), password, true);
                            progressDialog.dismiss();
                            Intent intent = new Intent();
                            intent.putExtra(LOGGED_IN_USER_EMAIL_KEY, user.getEmail());
                            getActivity().setResult(Activity.RESULT_OK, intent);
                            getActivity().finish();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        progressDialog.dismiss();
                        if (volleyError instanceof TimeoutError || volleyError instanceof NoConnectionError) {
                            getTmToast().makeToast(getString(R.string.tm_error_connect), Toast.LENGTH_SHORT);
                        } else if (volleyError instanceof AuthFailureError) {
                            Log.d("tmt", "AuthFailureError");
                        } else if (volleyError instanceof ServerError) {
                            Log.d("tmt", "ServerError");
                        } else if (volleyError instanceof NetworkError) {
                            Log.d("tmt", "NetworkError");
                        } else if (volleyError instanceof ParseError) {
                            Log.d("tmt", "ParseError");
                        } else {
                            Log.d("tmt", "onErrorResponse : " + volleyError.getMessage());
                        }
                    }
                }), TAG_SIGN_IN_KEY);
            } catch (AuthFailureError authFailureError) {
                authFailureError.printStackTrace();
                Log.d("tmt", "authFailureError");
            }
        }
    }

    private final class PrivateSignInIAMListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(getContext(), IAMLoginActivity.class);
            intent.putExtra(IAM_IN_AUTH, (Serializable) modelsIAM.getData().get(0));
            startActivityForResult(intent, REQUEST_CODE_AIM_SUCCESS);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_AIM_SUCCESS) {
            if (resultCode == Activity.RESULT_OK) {
                final String session_state = data.getStringExtra(IAMLoginActivity.SESSION_STATE);
                final String code = data.getStringExtra(IAMLoginActivity.URL_CODE);
                loginIAMWcf(code, getTmSharePreferencesUtils().getKongToken());
            }
        }
    }

    private void loginIAMWcf(String code, String kong) {
        getTmApiEndPointProvider().addRequest(LoginWithIamWcf.getIamWcf(code, modelsIAM.getData().get(0).getClientSecret(),kong, new Response.Listener<UserIam>() {
            @Override
            public void onResponse(UserIam user) {
                Log.i("TAG", user.getEmail());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onErrorListener(error);
            }
        } ), EXTRA_IAM_TAG);
    }

    private void onErrorListener(VolleyError error) {
        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
            getTmToast().makeToast(getString(R.string.tm_error_connect), Toast.LENGTH_SHORT);
        } else if (error instanceof AuthFailureError) {
            Log.d("tmt", "AuthFailureError");
        } else if (error instanceof ServerError) {
            Log.d("tmt", "ServerError");
        } else if (error instanceof NetworkError) {
            Log.d("tmt", "NetworkError");
        } else if (error instanceof ParseError) {
            Log.d("tmt", "ParseError");
        } else {
            Log.d("tmt", "onErrorResponse : " + error.getMessage());
        }
        show_Message(error.getMessage().toString());
    }

    public void show_Message(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Thông báo");
        builder.setMessage(msg);
        builder.setCancelable(false);

        builder.setPositiveButton("Thoát",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }

                });

        AlertDialog alert = builder.create();
        alert.getWindow().setGravity(Gravity.TOP);
        alert.show();
    }
}
