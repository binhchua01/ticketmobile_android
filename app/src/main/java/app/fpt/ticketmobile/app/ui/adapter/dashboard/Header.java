package app.fpt.ticketmobile.app.ui.adapter.dashboard;

/**
 * Created by Administrator on 7/8/2016.
 */
public class Header {

    private String mUserName;
    private String mUserEmail;
    private String mUserImage;

    public Header(){}

    public Header( String mUserName, String mUserEmail, String mUserImage) {
        this.mUserEmail = mUserEmail;
        this.mUserImage = mUserImage;
        this.mUserName = mUserName;
    }

    public String getUserEmail() {
        return mUserEmail;
    }

    public void setUserEmail(String mUserEmail) {
        this.mUserEmail = mUserEmail;
    }

    public String getUserImage() {
        return mUserImage;
    }

    public void setUserImage(String mUserImage) {
        this.mUserImage = mUserImage;
    }

    public String getUserName() {
        return mUserName;
    }

    public void setUserName(String mUserName) {
        this.mUserName = mUserName;
    }
}
