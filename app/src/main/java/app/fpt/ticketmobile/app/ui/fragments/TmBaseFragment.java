package app.fpt.ticketmobile.app.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

import app.fpt.ticketmobile.app.db.TmSharePreferencesUtils;
import app.fpt.ticketmobile.app.di.component.AppComponent;
import app.fpt.ticketmobile.app.eventBus.EventBus;
import app.fpt.ticketmobile.app.lib.TmApiEndPointProvider;
import app.fpt.ticketmobile.app.lib.TmConnectivityInfo;
import app.fpt.ticketmobile.app.lib.TmGraphics;
import app.fpt.ticketmobile.app.tracker.interfaces.TmTrackerAnalytics;
import app.fpt.ticketmobile.app.ui.lib.FragmentRefreshAnimationHandler;
import app.fpt.ticketmobile.app.ui.lib.TmToast;
import app.fpt.ticketmobile.app.core.TmApplication;

/**
 * Created by ToanMaster on 6/15/2016.
 */
public abstract class TmBaseFragment<C> extends Fragment {

    private final Class<C> callbackInterface;
    private final Collection<Object> eventBusListeners;
    private FragmentRefreshAnimationHandler fragmentRefreshAnimationHandler;
    private final boolean hasOptionsMenu;
    private boolean isEventBusRegistrationDone;
    private final Integer layoutResId;
    private final boolean shouldRetainInstance;


    protected TmBaseFragment(Class<C> callbackInterface, Integer layoutResId, boolean shouldRetainInstance, boolean hasOptionsMenu){
        this.callbackInterface = callbackInterface;
        this.layoutResId = layoutResId;
        this.hasOptionsMenu = hasOptionsMenu;
        this.shouldRetainInstance = shouldRetainInstance;
        this.eventBusListeners = new HashSet<>();
        addEventBusListeners(this);
    }

    protected final C getCallback(){
       if (getActivity() != null) {
           return (C) getActivity();
       }
        return null;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(hasOptionsMenu);
        setRetainInstance(shouldRetainInstance);
    }

    protected final void addEventBusListeners(Object... listeners){
        Collections.addAll(this.eventBusListeners, Arrays.copyOf(listeners, listeners.length));
        if (isEventBusRegistrationDone){
            for (Object listener : listeners) {
                getEventBus().register(listener);
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (layoutResId == null){
            return null;
        }
        return inflater.inflate(layoutResId.intValue(), container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        onTmActivityCreated(getView(), savedInstanceState);

    }

    protected void onTmActivityCreated(View parent, Bundle savedInstanceState) {

    }

    @Override
    public void onResume() {
        super.onResume();
        performEventBusRegistration();
    }

    @Override
    public void onPause() {
        performEventBusUnregistration();
        super.onPause();
    }

    protected final FragmentRefreshAnimationHandler getFragmentRefreshAnimationHandler() {
        return this.fragmentRefreshAnimationHandler;
    }

    protected final void setFragmentRefreshAnimationHandler(FragmentRefreshAnimationHandler fragmentRefreshAnimationHandler) {
        this.fragmentRefreshAnimationHandler = fragmentRefreshAnimationHandler;
    }

    protected final boolean isShowingRefreshAnimation(){
        FragmentRefreshAnimationHandler fragmentRefreshAnimationHandler = this.fragmentRefreshAnimationHandler;
        return fragmentRefreshAnimationHandler != null ? fragmentRefreshAnimationHandler.isFragmentShowingRefreshAnimation() : false;
    }

    protected final void showRefreshAnimation(){
        FragmentRefreshAnimationHandler fragmentRefreshAnimationHandler = this.fragmentRefreshAnimationHandler;
        if (fragmentRefreshAnimationHandler != null){
            fragmentRefreshAnimationHandler.showRefreshAnimationInFragment();
            autoSynced();
        }
    }

    protected final void stopRefreshAnimation(){
        FragmentRefreshAnimationHandler fragmentRefreshAnimationHandler = this.fragmentRefreshAnimationHandler;
        if (fragmentRefreshAnimationHandler != null){
            fragmentRefreshAnimationHandler.stopRefreshAnimationInFragment();
        }
    }

    protected void autoSynced(){

    }

    protected final void removeEventBusListeners(Object... listeners) {
        for (Object listener : listeners) {
            getEventBus().unregister(listener);
            this.eventBusListeners.remove(listener);
        }
    }

    private final void performEventBusRegistration() {
        for (Object listener : this.eventBusListeners) {
            getEventBus().register(listener);
        }
        this.isEventBusRegistrationDone = true;
    }

    private final void performEventBusUnregistration() {
        for (Object listener : this.eventBusListeners) {
            getEventBus().unregister(listener);
        }
        this.isEventBusRegistrationDone = false;
    }

    protected EventBus getEventBus(){
        checkIfNeedIsNull();
        return getAppComponent().getEventBus();
    }

    protected TmToast getTmToast(){
        checkIfNeedIsNull();
        return getAppComponent().getTmToast();
    }

    protected TmApiEndPointProvider getTmApiEndPointProvider(){
        checkIfNeedIsNull();
        return getAppComponent().getTmApiEndPointProvider();
    }

    protected TmConnectivityInfo getTmConnectivityInfo(){
        checkIfNeedIsNull();
        return getAppComponent().getTmConnectivityInfo();
    }

    protected TmSharePreferencesUtils getTmSharePreferencesUtils(){
        checkIfNeedIsNull();
        return  getAppComponent().getTmSharePreferencesUtils();
    }

    protected TmGraphics getTmGraphics(){
        checkIfNeedIsNull();
        return getAppComponent().getTmGraphics();
    }

    protected final TmTrackerAnalytics getTmTrackerAnalytics(){
        checkIfNeedIsNull();
        return getAppComponent().getTmTrackerAnalytics();
    }

    private AppComponent getAppComponent(){
        AppComponent appComponent = ((TmApplication) getActivity().getApplication()).getAppComponent();
        return appComponent;
    }

    private void checkIfNeedIsNull(){
        if (getAppComponent() == null){
            throw new RuntimeException("AppComponent is null");
        }
    }
}
