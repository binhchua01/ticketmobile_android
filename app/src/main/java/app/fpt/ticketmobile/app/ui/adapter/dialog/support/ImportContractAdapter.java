package app.fpt.ticketmobile.app.ui.adapter.dialog.support;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import app.fpt.ticketmobile.app.model.support.ImportContractObject;
import app.fpt.ticketmobile.R;
import app.fpt.ticketmobile.app.ui.activities.authenticatedActivities.HomeActivity;

/**
 * Created by Administrator on 7/25/2016.
 */
public class ImportContractAdapter extends BaseAdapter {

    private List<ImportContractObject> mImportContractObjectList;
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private Button bDelete;
    public ImportContractAdapter(Context mContext, List<ImportContractObject> importContractObjectList ) {
        this.mContext = mContext;
        this.mImportContractObjectList = importContractObjectList;
        this.mLayoutInflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getCount() {
        return mImportContractObjectList.size();
    }

    @Override
    public Object getItem(int position) {
        return mImportContractObjectList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    private final class ViewHolder{
        private TextView mTvContract;
        private TextView mTvContractName;
        private TextView mDomainIp;
        private TextView mAddress;
        public ViewHolder(View parent){
            mTvContract = (TextView) parent.findViewById(R.id.tm_tv_dialog_contracts);
            mTvContractName = (TextView) parent.findViewById(R.id.tm_tv_dialog_contracts_name);
            mDomainIp = (TextView) parent.findViewById(R.id.tm_tv_dialog_contracts_ip_domain);
            mAddress = (TextView) parent.findViewById(R.id.tm_tv_dialog_contracts_address);
            bDelete =  (Button) parent.findViewById(R.id.delete_contract);
        }

        public void onBind(ImportContractObject contractObject){
            mTvContract.setText(contractObject.getContract());
            mTvContractName.setText(contractObject.getContractName());
            mDomainIp.setText(contractObject.getIpDomain());
            mAddress.setText(contractObject.getAddress());
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null){
            convertView = mLayoutInflater.inflate(R.layout.entry_import_contract, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder) convertView.getTag();
        }
        bDelete.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // TODO Auto-generated method stub
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext).setTitle("Bạn muốn xóa hợp đồng này?").setPositiveButton("Đồng ý", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mImportContractObjectList.remove(position);
                        notifyDataSetChanged();
                        dialog.dismiss();
                    }
                }).setNegativeButton("Hủy", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.create().show();
            }
        });
        ImportContractObject contractObject = mImportContractObjectList.get(position);
        holder.onBind(contractObject);
        notifyDataSetChanged();

        return convertView;
    }
}
