package app.fpt.ticketmobile.app.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import app.fpt.ticketmobile.R;
import app.fpt.ticketmobile.app.model.MyDeviceObject;
import app.fpt.ticketmobile.app.model.SpinnerObject;
import app.fpt.ticketmobile.app.ui.adapter.spinner.MySpinnerAdapter;
import app.fpt.ticketmobile.widget.views.libs.SpinnerCallbackInterface;

/**
 * Created by ToanMaster on 6/27/2016.
 */
public class MyDeviceAdapter extends RecyclerView.Adapter<MyDeviceAdapter.MyDeviceHolder> {

    private List<MyDeviceObject.DeviceObject> mDeviceObjectList = null;
    private static SpinnerCallbackInterface mCallbackInterface;

    private  final class PrivateSpinnerItemSelectionListener implements AdapterView.OnItemSelectedListener {
        private MyDeviceObject.DeviceObject deviceObject;
        private String mKey = null;

        public PrivateSpinnerItemSelectionListener(MyDeviceObject.DeviceObject deviceObject, String key) {
            this.deviceObject = deviceObject;
            this.mKey = key;
        }

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            SpinnerObject deviceStatusOBJ = (SpinnerObject) parent.getAdapter().getItem(position);
            if (deviceStatusOBJ != null) {
                if (mCallbackInterface != null && !mKey.equalsIgnoreCase(deviceStatusOBJ.getKey())) {
                    mCallbackInterface.UpdateDeviceStatus(deviceObject.getDeviceName(), deviceStatusOBJ.getKey());
                }
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

    public final class MyDeviceHolder extends RecyclerView.ViewHolder {
        private TextView mDeviceName, mDeviceUpdatedDate, mDeviceUpdatedByUser, mDeviceErrorNumber;
        private Spinner mSpinnerDeviceStatus;
        private Context mContext;

        public MyDeviceHolder(View parent) {
            super(parent);
            mContext = parent.getContext();
            mDeviceName = (TextView) parent.findViewById(R.id.tm_tv_ticket_detail_device_name);
            mDeviceUpdatedDate = (TextView) parent.findViewById(R.id.tm_tv_ticket_device_updated_date);
            mDeviceUpdatedByUser = (TextView) parent.findViewById(R.id.tm_tv_ticket_device_updated_user);
            mDeviceErrorNumber = (TextView) parent.findViewById(R.id.tm_tv_ticket_device_number_error);
            mSpinnerDeviceStatus = (Spinner) parent.findViewById(R.id.tm_spinner_ticket_device_status);

        }

        public void bind(MyDeviceObject.DeviceObject deviceObject) {
            mDeviceName.setText(deviceObject.getDeviceName());
            mDeviceUpdatedDate.setText(deviceObject.getUpdatedDate());
            mDeviceUpdatedByUser.setText(deviceObject.getUpdatedBy());
            mDeviceErrorNumber.setText(String.valueOf(deviceObject.getErrorTimes().intValue()));
            setAdapterSpinner(mSpinnerDeviceStatus, deviceObject.getStatus());
            mSpinnerDeviceStatus.setOnItemSelectedListener(new PrivateSpinnerItemSelectionListener(deviceObject, deviceObject.getStatus()));
        }

        private void setAdapterSpinner(Spinner mSpinnerDeviceStatus, String key) {
            List<SpinnerObject> myDeviceStatusObjectList = new ArrayList<>();
            myDeviceStatusObjectList.add(new SpinnerObject("New", "Chưa xử lý"));
            myDeviceStatusObjectList.add(new SpinnerObject("In", "Đang xử lý"));
            myDeviceStatusObjectList.add(new SpinnerObject("OK", "Đã hoàn thành"));
            myDeviceStatusObjectList.add(new SpinnerObject("NotOK", "Chưa hoàn thành"));
            MySpinnerAdapter statusAdapter = new MySpinnerAdapter(mContext, R.layout.textview_spinner, myDeviceStatusObjectList);
            statusAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerDeviceStatus.setAdapter(statusAdapter);
            setSpinnerSelection(mSpinnerDeviceStatus, statusAdapter, key);
        }

        private void setSpinnerSelection(Spinner sn, ArrayAdapter<SpinnerObject> adapter, String key) {
            Log.d("tmt", " key " + key);
            for (int i = 0; i < adapter.getCount(); i++) {
                if (key.equals(adapter.getItem(i).getKey().toString())) {
                    sn.setSelection(i);
                    break;
                }
            }
        }
    }

    public MyDeviceAdapter(List<MyDeviceObject.DeviceObject> deviceObjectList, SpinnerCallbackInterface callbackInterface) {
        this.mDeviceObjectList = deviceObjectList;
        this.mCallbackInterface = callbackInterface;
    }


    @Override
    public MyDeviceHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.entry_ticket_device_row_one, parent, false);
        return new MyDeviceHolder(view);
    }

    @Override
    public void onBindViewHolder(MyDeviceHolder holder, int position) {
        MyDeviceObject.DeviceObject  deviceObject = mDeviceObjectList.get(position);
        holder.bind(deviceObject);
    }

    @Override
    public int getItemCount() {
        return mDeviceObjectList.size();
    }


    public List<MyDeviceObject.DeviceObject> getDeviceObjectList() {
        return mDeviceObjectList;
    }


}
