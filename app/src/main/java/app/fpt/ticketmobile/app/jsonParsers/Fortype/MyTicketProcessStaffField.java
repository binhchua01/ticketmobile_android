package app.fpt.ticketmobile.app.jsonParsers.Fortype;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import app.fpt.ticketmobile.app.model.SpinnerObject;
import app.fpt.ticketmobile.app.model.MyTicketProcessStaffObject;

/**
 * Created by Administrator on 6/29/2016.
 */
public class MyTicketProcessStaffField {

    public static final class Deserializer implements JsonDeserializer<MyTicketProcessStaffObject>{

        @Override
        public MyTicketProcessStaffObject deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            Gson gson = new Gson();
            MyTicketProcessStaffObject myTicketProcessStaffObject = new MyTicketProcessStaffObject();
            List<SpinnerObject> spinnerObjectList = new ArrayList<>();

            try{
                JsonObject jsObj = json.getAsJsonObject();
                JsonObject jsonObject = jsObj.getAsJsonObject("Result");
                myTicketProcessStaffObject = gson.fromJson(jsonObject, MyTicketProcessStaffObject.class);

                if (myTicketProcessStaffObject.getData() != null && myTicketProcessStaffObject.getData().size() > 0){
                    for (int i = 0; i < myTicketProcessStaffObject.getData().size(); i++){
                        MyTicketProcessStaffObject.ProcessStaffObject processStaffObject = myTicketProcessStaffObject.getData().get(i);
                        SpinnerObject spinnerObject = new SpinnerObject(processStaffObject.getEmail(), processStaffObject.getName());
                        spinnerObjectList.add(spinnerObject);
                    }
                    myTicketProcessStaffObject.setSpinnerObjectList(spinnerObjectList);
                }

            }catch(Exception e){
                Log.e("tmt", "MyTicketProcessStaffField : Deserializer " + e.toString());
            }
            return myTicketProcessStaffObject;
        }
    }
}
