package app.fpt.ticketmobile.app.ui.adapter.ticketofusercreated;

import app.fpt.ticketmobile.app.model.MyTicketOfUserCreatedObject;

/**
 * Created by Administrator on 7/1/2016.
 */
public class TicketOfUserCreatedChild {

    private MyTicketOfUserCreatedObject.TicketOfUserCreated ticketOfUserCreated;

    public MyTicketOfUserCreatedObject.TicketOfUserCreated getTicketOfUserCreated() {
        return ticketOfUserCreated;
    }

    public void setTicketOfUserCreated(MyTicketOfUserCreatedObject.TicketOfUserCreated ticketOfUserCreated) {
        this.ticketOfUserCreated = ticketOfUserCreated;
    }

    public TicketOfUserCreatedChild(MyTicketOfUserCreatedObject.TicketOfUserCreated ticketOfUserCreated) {
        this.ticketOfUserCreated = ticketOfUserCreated;
    }
}
