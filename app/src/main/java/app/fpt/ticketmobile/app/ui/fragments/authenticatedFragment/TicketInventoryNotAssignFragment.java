package app.fpt.ticketmobile.app.ui.fragments.authenticatedFragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

import java.util.ArrayList;
import java.util.List;

import app.fpt.ticketmobile.app.model.MyTicketInventoryNotAssignObject;
import app.fpt.ticketmobile.app.networking.apiEndpoints.MyTicketInventoryNotAssignEndPoint;
import app.fpt.ticketmobile.app.ui.adapter.MyTicketInventoryNotAssignAdapter;
import app.fpt.ticketmobile.app.ui.adapter.ticketinventorynotassign.TicketInventoryNotAssignHeader;
import app.fpt.ticketmobile.app.ui.lib.FragmentRefreshBasedSwipeRefreshLayoutHandler;
import app.fpt.ticketmobile.R;
import app.fpt.ticketmobile.app.ui.fragments.TmBaseFragment;

/**
 * Created by Administrator on 7/1/2016.
 */
public class TicketInventoryNotAssignFragment extends TmBaseFragment<TicketInventoryNotAssignFragment.Callback> implements SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG_MY_TICKET_INVENTORY_NOT_ASSIGN_KEY = "tag_my_ticket_inventory_not_assign_key";
    private RecyclerView mListTicketInventoryNotAssign;
    private MyTicketInventoryNotAssignAdapter myTicketInventoryNotAssignAdapter;
    private List<TicketInventoryNotAssignHeader> mTicketInventoryNotAssignHeaderList;

    public interface Callback{

    }

    public TicketInventoryNotAssignFragment() {
        super(Callback.class, Integer.valueOf(R.layout.fragment_ticket_inventory_not_assign), false, true);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mTicketInventoryNotAssignHeaderList = new ArrayList<>();
    }

    @Override
    protected void onTmActivityCreated(View parent, Bundle savedInstanceState) {
        super.onTmActivityCreated(parent, savedInstanceState);
        mListTicketInventoryNotAssign = (RecyclerView) parent.findViewById(R.id.list);
        mListTicketInventoryNotAssign.setItemAnimator(new DefaultItemAnimator());
        myTicketInventoryNotAssignAdapter = new MyTicketInventoryNotAssignAdapter(getActivity(), mTicketInventoryNotAssignHeaderList);
        mListTicketInventoryNotAssign.setAdapter(myTicketInventoryNotAssignAdapter);

        SwipeRefreshLayout swipeRefreshLayout =  (SwipeRefreshLayout) parent.findViewById(R.id.swipe_refresh_layout);
        setFragmentRefreshAnimationHandler(new FragmentRefreshBasedSwipeRefreshLayoutHandler(swipeRefreshLayout, this, Integer.valueOf(R.color.tm_swipe_refresh_color)));
        if (!isShowingRefreshAnimation()) {
            swipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    showRefreshAnimation();
                }
            }, 500);
        }
        myTicketInventoryNotAssignAdapter.onRestoreInstanceState(savedInstanceState);
    }


    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        myTicketInventoryNotAssignAdapter.onSaveInstanceState(savedInstanceState);
    }



    @Override
    public void onRefresh() {
        autoSynced();
    }

    @Override
    protected void autoSynced() {
        super.autoSynced();
        if (!isShowingRefreshAnimation()) {
            showRefreshAnimation();
        }

        getTmApiEndPointProvider().addRequest( MyTicketInventoryNotAssignEndPoint.getNewTicketInventory(String.valueOf(getTmSharePreferencesUtils().getUserLogin().getID()),

                new Response.Listener<MyTicketInventoryNotAssignObject>() {
                    @Override
                    public void onResponse(final MyTicketInventoryNotAssignObject myTicketInventoryNotAssignObject) {
                        if (getActivity() != null) {
                            if (myTicketInventoryNotAssignObject != null && myTicketInventoryNotAssignObject.getInventoryNotAssignHeaderList() != null) {
                                if (myTicketInventoryNotAssignObject.getInventoryNotAssignHeaderList().size() > 0) {
                                    myTicketInventoryNotAssignAdapter.clear();
                                    mTicketInventoryNotAssignHeaderList = myTicketInventoryNotAssignObject.getInventoryNotAssignHeaderList();
                                    myTicketInventoryNotAssignAdapter = new MyTicketInventoryNotAssignAdapter(getActivity(), mTicketInventoryNotAssignHeaderList);
                                    mListTicketInventoryNotAssign.setAdapter(myTicketInventoryNotAssignAdapter);
                                    mListTicketInventoryNotAssign.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
                                    stopRefreshAnimation();
                                } else {
                                    setLayoutNoData(R.layout.fragment_no_data);
                                    stopRefreshAnimation();
                                }
                            } else {
                                setLayoutNoData(R.layout.fragment_no_data);
                                stopRefreshAnimation();
                            }
                            stopRefreshAnimation();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        onErrorListener(error);
                    }
                }), TAG_MY_TICKET_INVENTORY_NOT_ASSIGN_KEY);


    }


    @Override
    public void onStop() {
        super.onStop();
        getTmApiEndPointProvider().cancelAllRequest(TAG_MY_TICKET_INVENTORY_NOT_ASSIGN_KEY);
    }

    private void onErrorListener(VolleyError error){
        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
            getTmToast().makeToast(getString(R.string.tm_error_connect), Toast.LENGTH_SHORT);
        } else if (error instanceof AuthFailureError) {
            Log.d( "tmt","AuthFailureError");
        } else if (error instanceof ServerError) {
            Log.d( "tmt","ServerError");
        } else if (error instanceof NetworkError) {
            Log.d( "tmt","NetworkError");
        } else if (error instanceof ParseError) {
            Log.d( "tmt","ParseError");
        }else {
            Log.d( "tmt","onErrorResponse : " + error.getMessage());
        }
        stopRefreshAnimation();
    }

    private void setLayoutNoData(Integer layoutResId) {
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewGroup rootView = (ViewGroup) getView();
        View mainView = inflater.inflate(layoutResId.intValue(), rootView, false);
        rootView.removeAllViews();
        rootView.addView(mainView);
        SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) mainView.findViewById(R.id.swipe_refresh_layout);
        setFragmentRefreshAnimationHandler(new FragmentRefreshBasedSwipeRefreshLayoutHandler(swipeRefreshLayout, this, Integer.valueOf(R.color.tm_swipe_refresh_color)));
    }

}
