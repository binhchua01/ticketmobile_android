package app.fpt.ticketmobile.app.model.support;

import java.util.List;

import app.fpt.ticketmobile.app.model.SpinnerObject;

/**
 * Created by Administrator on 7/21/2016.
 */
public class MyQueueObject {

    private List<QueueObject> Data;
    private List<SpinnerObject> mSpinnerObjectList;

    public List<QueueObject> getQueueList() {
        return Data;
    }

    public void setQueueList(List<QueueObject> data) {
        Data = data;
    }

    public List<SpinnerObject> getSpinnerObjectList() {
        return mSpinnerObjectList;
    }

    public void setSpinnerObjectList(List<SpinnerObject> mSpinnerObjectList) {
        this.mSpinnerObjectList = mSpinnerObjectList;
    }

    public class QueueObject{
        private String ID;
        private String Name;

        public String getID() {
            return ID;
        }

        public void setID(String ID) {
            this.ID = ID;
        }

        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }
    }
}
