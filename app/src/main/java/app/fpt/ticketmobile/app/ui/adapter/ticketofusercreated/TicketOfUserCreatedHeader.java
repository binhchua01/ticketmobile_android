package app.fpt.ticketmobile.app.ui.adapter.ticketofusercreated;

import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;

import java.io.Serializable;
import java.util.List;


/**
 * Created by Administrator on 7/1/2016.
 */
public class TicketOfUserCreatedHeader implements ParentListItem, Serializable {

    private String QueueName;
    private List<TicketOfUserCreatedChild> ticketOfUserCreatedChildList;

    public TicketOfUserCreatedHeader(){}

    public TicketOfUserCreatedHeader(String queueName, List<TicketOfUserCreatedChild> ticketOfUserCreatedChildList) {
        QueueName = queueName;
        this.ticketOfUserCreatedChildList = ticketOfUserCreatedChildList;
    }


    public String getQueueName() {
        return QueueName;
    }

    public void setQueueName(String queueName) {
        QueueName = queueName;
    }

    public List<TicketOfUserCreatedChild> getTicketOfUserCreatedChildList() {
        return ticketOfUserCreatedChildList;
    }

    public void setTicketOfUserCreatedChildList(List<TicketOfUserCreatedChild> ticketOfUserCreatedChildList) {
        this.ticketOfUserCreatedChildList = ticketOfUserCreatedChildList;
    }

    @Override
    public List<?> getChildItemList() {
        return ticketOfUserCreatedChildList;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }
}

