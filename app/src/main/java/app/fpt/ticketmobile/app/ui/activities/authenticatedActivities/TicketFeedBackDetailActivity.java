package app.fpt.ticketmobile.app.ui.activities.authenticatedActivities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.otto.Subscribe;

import app.fpt.ticketmobile.app.eventBus.event.TicketFeedbackReplyAllEvent;
import app.fpt.ticketmobile.app.model.MyTicketFeedbackDetailObject;
import app.fpt.ticketmobile.app.ui.activities.TmBaseActivity;
import app.fpt.ticketmobile.app.ui.fragments.authenticatedFragment.TicketFeedBackDetailFragment;
import app.fpt.ticketmobile.R;
import app.fpt.ticketmobile.app.eventBus.event.TicketFeedbackReplyEvent;
import app.fpt.ticketmobile.app.eventBus.event.TicketShowViewEvent;

/**
 * Created by Administrator on 7/4/2016.
 */
public class TicketFeedBackDetailActivity extends TmBaseActivity {

    private static final String EXTRA_TICKET_RESPONSE_ID_KEY = "extra_ticket_response_id_key";
    private static final String EXTRA_TICKET_ID_KEY = "extra_ticket_id_key";
    private static final String FRAGMENT_FEED_BACK_DETAIL_TAG = "fragment_feed_back_detail_tag";

    private String TICKET_ID = null;
    private String RESPONSE_ID = null;
    private MyTicketFeedbackDetailObject myTicketFeedbackDetailObject = null;


    private final class PrivateOnClickFeedbackEvent implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.tm_btn_ticket_reply :
                    getEventBus().post(new TicketFeedbackReplyEvent(myTicketFeedbackDetailObject));
                    break;

                case R.id.tm_btn_ticket_reply_all:
                    getEventBus().post(new TicketFeedbackReplyAllEvent(myTicketFeedbackDetailObject));
                    break;
            }
        }
    }

    public TicketFeedBackDetailActivity() {
        super(Integer.valueOf(R.layout.activity_ticket_feedback_detail));
    }

    public static Intent buildIntent(Context context, String ticketId, String ticketResponseId) {
        Intent intent = new Intent(context, TicketFeedBackDetailActivity.class);
        intent.putExtra(EXTRA_TICKET_RESPONSE_ID_KEY, ticketResponseId);
        intent.putExtra(EXTRA_TICKET_ID_KEY, ticketId);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        extraIntent();
    }

    private void extraIntent() {
        if (getIntent() != null) {
            TICKET_ID = getIntent().getStringExtra(EXTRA_TICKET_ID_KEY);
            RESPONSE_ID = getIntent().getStringExtra(EXTRA_TICKET_RESPONSE_ID_KEY);
            setupViews();
            setupIsFragmentIfNeed(TICKET_ID, RESPONSE_ID);
        } else {
            setLayoutNoData(R.layout.fragment_no_data);
        }
    }

    private void setupViews(){

        findViewById(R.id.tm_btn_ticket_reply).setOnClickListener(new PrivateOnClickFeedbackEvent());
        findViewById(R.id.tm_btn_ticket_reply_all).setOnClickListener(new PrivateOnClickFeedbackEvent());
    }

    private void setupIsFragmentIfNeed(String ticketId, String ticketResponseId) {
        TicketFeedBackDetailFragment ticketFeedBackDetailFragment = (TicketFeedBackDetailFragment) getSupportFragmentManager().findFragmentByTag(FRAGMENT_FEED_BACK_DETAIL_TAG);
        if (ticketFeedBackDetailFragment == null) {
            ticketFeedBackDetailFragment = TicketFeedBackDetailFragment.buildIntent(ticketId, ticketResponseId);
        }

        getSupportFragmentManager().beginTransaction().replace(R.id.content_fragment, ticketFeedBackDetailFragment, FRAGMENT_FEED_BACK_DETAIL_TAG).commit();
    }

    @Override
    protected void setupToolbar(Toolbar mToolbar) {
        super.setupToolbar(mToolbar);
        ((TextView) mToolbar.findViewById(R.id.main_toolbar_title)).setText(getString(R.string.tm_ticket_response));
    }

    private void setLayoutNoData(Integer layoutResId) {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewGroup rootView = (ViewGroup) ((ViewGroup) this.findViewById(android.R.id.content)).getChildAt(0);
        View mainView = inflater.inflate(layoutResId.intValue(), rootView, false);
        rootView.removeAllViews();
        rootView.addView(mainView);
    }

    @Subscribe
    public void onShowViewEvent(TicketShowViewEvent ticketShowViewEvent){
        myTicketFeedbackDetailObject = ticketShowViewEvent.getMyTicketFeedbackDetailObject();
        ((Toolbar) findViewById(R.id.tm_toolbar_app_bottom)).setVisibility(View.VISIBLE);
    }
}
