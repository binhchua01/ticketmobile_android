package app.fpt.ticketmobile.app.networking.apiEndpoints;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

import app.fpt.ticketmobile.app.model.MyResponseObject;
import app.fpt.ticketmobile.app.model.support.MyImportContractObject;
import app.fpt.ticketmobile.app.model.support.MyOperateStaffSupportObject;
import app.fpt.ticketmobile.app.model.support.MyProcessStaffSupportObject;
import app.fpt.ticketmobile.app.model.support.MyQueueObject;
import app.fpt.ticketmobile.app.model.support.MyTicketBranchLocationObject;
import app.fpt.ticketmobile.app.model.support.MyTicketBusinessAreaObject;
import app.fpt.ticketmobile.app.model.support.MyTicketCustomTypeObject;
import app.fpt.ticketmobile.app.model.support.MyTicketDescriptionObject;
import app.fpt.ticketmobile.app.model.support.MyTicketPriorityObject;
import app.fpt.ticketmobile.app.model.support.MyTicketSupportObject;
import app.fpt.ticketmobile.app.model.support.MyTicketSupportOfUserCreatedObject;
import app.fpt.ticketmobile.app.model.support.MyTicketSupportServiceTypeObject;
import app.fpt.ticketmobile.app.model.support.MyVisorStaffSupportObject;
import app.fpt.ticketmobile.app.networking.utils.GsonRequest;
import app.fpt.ticketmobile.app.model.support.MyTicketReasonSupportObject;

/**
 * Created by Administrator on 7/7/2016.
 */
public class MyTicketSupportEndPoint {


    ////////////////////////////////////////////////////////////////////////////////////////////////
    // api getBusinessAreaList
    public static GsonRequest<MyTicketBusinessAreaObject> getBusinessAreaList(final String userId, Response.Listener<MyTicketBusinessAreaObject> listener, Response.ErrorListener errorListener) {
        GsonRequest<MyTicketBusinessAreaObject> request = new GsonRequest<MyTicketBusinessAreaObject>(Constants.MY_GET_SUPPORT_BUSINESS_AREA_LIST, MyTicketBusinessAreaObject.class, listener, errorListener) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return getHeaderBusinessAreaList(userId);
            }
        };
        return request;
    }

    private static Map<String, String> getHeaderBusinessAreaList(String userId) {
        Map<String, String> params = new HashMap<>();
        //  params.put("Content-Type", "application/json; charset=utf-8");
        params.put("UserID", userId);
        return params;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // api getBranchLocationList
    public static GsonRequest<MyTicketBranchLocationObject> getBranchLocationList(final String locationId, Response.Listener<MyTicketBranchLocationObject> listener, Response.ErrorListener errorListener) {
        GsonRequest<MyTicketBranchLocationObject> request = new GsonRequest<MyTicketBranchLocationObject>(Constants.MY_GET_SUPPORT_BRANCH_LOCATION_LIST, MyTicketBranchLocationObject.class, listener, errorListener) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return getHeaderBranchLocationList(locationId);
            }
        };

        return request;
    }

    private static Map<String, String> getHeaderBranchLocationList(String locationId) {
        Map<String, String> params = new HashMap<>();
        params.put("LocationID", locationId);
        return params;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // api getServiceTypeList
    public static GsonRequest<MyTicketSupportServiceTypeObject> getSupportServiceTypeList(Response.Listener<MyTicketSupportServiceTypeObject> listener, Response.ErrorListener errorListener) {
        GsonRequest<MyTicketSupportServiceTypeObject> request = new GsonRequest<MyTicketSupportServiceTypeObject>(Constants.MY_GET_SUPPORT_SERVICE_TYPE_LIST, MyTicketSupportServiceTypeObject.class, listener, errorListener);
        return request;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // api getCustomTypeList
    public static GsonRequest<MyTicketCustomTypeObject> getCustomTypeList(final String serviceType, Response.Listener<MyTicketCustomTypeObject> listener, Response.ErrorListener errorListener) {
        GsonRequest<MyTicketCustomTypeObject> request = new GsonRequest<MyTicketCustomTypeObject>(Constants.MY_GET_SUPPORT_CUSTOM_TYPE_LIST, MyTicketCustomTypeObject.class, listener, errorListener) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return getHeaderCustomTypeList(serviceType);
            }
        };
        return request;
    }

    private static Map<String, String> getHeaderCustomTypeList(String serviceType) {
        Map<String, String> params = new HashMap<>();
        params.put("ServiceType", serviceType);
        return params;
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////
    // api getDescriptionList
    public static GsonRequest<MyTicketDescriptionObject> getIssueDescriptionList(final String cusType, Response.Listener<MyTicketDescriptionObject> listener, Response.ErrorListener errorListener) {
        GsonRequest<MyTicketDescriptionObject> request = new GsonRequest<MyTicketDescriptionObject>(Constants.MY_GET_SUPPORT_DESCRIPTION_LIST, MyTicketDescriptionObject.class, listener, errorListener) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return getHeaderDescriptionList(cusType);
            }
        };

        return request;
    }

    private static Map<String, String> getHeaderDescriptionList(String cusType) {
        Map<String, String> params = new HashMap<>();
        params.put("CusType", cusType);
        return params;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // api getReasonList
    public static GsonRequest<MyTicketReasonSupportObject> getReasonList(final String cusType, Response.Listener<MyTicketReasonSupportObject> listener, Response.ErrorListener errorListener) {
        GsonRequest<MyTicketReasonSupportObject> request = new GsonRequest<MyTicketReasonSupportObject>(Constants.MY_GET_SUPPORT_REASON_LIST, MyTicketReasonSupportObject.class, listener, errorListener) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return getHeaderReasonList(cusType);
            }
        };

        return request;
    }

    private static Map<String, String> getHeaderReasonList(String cusType) {
        Map<String, String> params = new HashMap<>();
        params.put("CusType", cusType);
        return params;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // api getPriorityList
    public static GsonRequest<MyTicketPriorityObject> getPriorityList(final String locationId, final String branchId, final String issueId, Response.Listener<MyTicketPriorityObject> listener, Response.ErrorListener errorListener) {
        GsonRequest<MyTicketPriorityObject> request = new GsonRequest<MyTicketPriorityObject>(Constants.MY_GET_SUPPORT_PRIORITY_LIST, MyTicketPriorityObject.class, listener, errorListener) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return getHeaderPriorityList(locationId, branchId, issueId);
            }
        };
        return request;
    }

    private static Map<String, String> getHeaderPriorityList(String locationId, String branchId, String issueId) {
        Map<String, String> params = new HashMap<>();
        params.put("Location", locationId);
        params.put("Branch", branchId);
        params.put("IssueID", issueId);
        return params;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // api getCreatedTicketList
    public static GsonRequest<MyTicketSupportOfUserCreatedObject> getTicketSupportCreatedList(final String userEmail, Response.Listener<MyTicketSupportOfUserCreatedObject> listener, Response.ErrorListener errorListener) {
        GsonRequest<MyTicketSupportOfUserCreatedObject> request = new GsonRequest<MyTicketSupportOfUserCreatedObject>(Constants.MY_GET_SUPPORT_CREATED_TICKET_LIST, MyTicketSupportOfUserCreatedObject.class, listener, errorListener) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return getHeaderTicketCreatedList(userEmail);
            }
        };
        return request;
    }

    private static Map<String, String> getHeaderTicketCreatedList(String userEmail) {
        Map<String, String> params = new HashMap<>();
        params.put("UserEmail", userEmail);
        return params;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // api getQueueList
    public static GsonRequest<MyQueueObject> getQueueList(Response.Listener<MyQueueObject> listener, Response.ErrorListener errorListener) {
        GsonRequest<MyQueueObject> request = new GsonRequest<>(Constants.MY_GET_SUPPORT_QUEUE_LIST, MyQueueObject.class, listener, errorListener);
        return request;
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////
    // api getOperateStaffList(final String queue, final String locationId, final String branchId, final String cricLev,
    // Response.Listener<MyTicketOperateStaffObject> listener, Response.ErrorListener errorListener)

    public static GsonRequest<MyOperateStaffSupportObject> getOperateStaffList(final String queue, final String locationId, final String branchId, final String cricLev, Response.Listener<MyOperateStaffSupportObject> listener, Response.ErrorListener errorListener) {
        GsonRequest<MyOperateStaffSupportObject> request = new GsonRequest<MyOperateStaffSupportObject>(Constants.MY_GET_SUPPORT_OPERATE_STAFF, MyOperateStaffSupportObject.class, listener, errorListener) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return getHeaderTicketOperateStaff(queue, locationId, branchId, cricLev);
            }
        };
        return request;
    }

    private static Map<String, String> getHeaderTicketOperateStaff(String queue, String locationId, String branchId, String crivLev) {
        Map<String, String> params = new HashMap<>();
        params.put("Queue", queue);
        params.put("LocationID", locationId);
        params.put("BranchID", branchId);
        params.put("CricLev", crivLev);
        return params;
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////
    // api getVisorStaffList
    public static GsonRequest<MyVisorStaffSupportObject> getVisorStaffList(final String queue, final String locationId, final String branchId, Response.Listener<MyVisorStaffSupportObject> listener, Response.ErrorListener errorListener) {
        GsonRequest<MyVisorStaffSupportObject> request = new GsonRequest<MyVisorStaffSupportObject>(Constants.MY_GET_SUPPORT_VISOR_STAFF, MyVisorStaffSupportObject.class, listener, errorListener) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return getHeaderTicketVisorStaff(queue, locationId, branchId);
            }

        };
        return request;
    }

    private static Map<String, String> getHeaderTicketVisorStaff(String queue, String locationId, String branchId) {
        Map<String, String> params = new HashMap<>();
        params.put("Queue", queue);
        params.put("LocationID", locationId);
        params.put("BranchID", branchId);
        return params;
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////
    // api getVisorStaffList
    public static GsonRequest<MyProcessStaffSupportObject> getProcessStaffSupportList(final String queue, final String locationId, final String branchId, Response.Listener<MyProcessStaffSupportObject> listener, Response.ErrorListener errorListener){
        GsonRequest<MyProcessStaffSupportObject> request = new GsonRequest<MyProcessStaffSupportObject>(Constants.MY_GET_PROCESS_STAFF_LIST, MyProcessStaffSupportObject.class, listener, errorListener){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return getHeaderProcessStaffList(queue, locationId, branchId);
            }
        };

        return request;
    }

    private static Map<String, String> getHeaderProcessStaffList(String queue, String locationId, String branchId){

        Map<String, String> params = new HashMap<>();
        params.put("Queue", queue);
        params.put("LocationID", locationId);
        params.put("BranchID", branchId);
        return params;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // api getEstimatedTimeValue(String cricLev,String issueId)
    public static StringRequest getEstimatedTimeValue(final String cricLev, final String issueId, Response.Listener<String> listener, Response.ErrorListener errorListener){
        StringRequest jsonObjectRequest = new StringRequest( Request.Method.POST, Constants.MY_GET_SUPPORT_HT_ESTIMATED_TIME, listener, errorListener){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return getHeaderEstimatedTimeTime(cricLev, issueId);
            }
        };

        return jsonObjectRequest;
    }

    private static Map<String, String> getHeaderEstimatedTimeTime(String cricLev, String issueId) {
        Map<String, String> params = new HashMap<>();
        params.put("CricLev", cricLev);
        params.put("IssueID", issueId);
        return params;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // api getEstimatedTimeValue(String cricLev,String issueId)
    public static GsonRequest<MyImportContractObject> getImportContractList(final String ticketId, Response.Listener<MyImportContractObject> listener, Response.ErrorListener errorListener){
        GsonRequest<MyImportContractObject> request = new GsonRequest<MyImportContractObject>(Constants.MY_GET_SUPPORT_CUSTOMER_LIST, MyImportContractObject.class, listener, errorListener) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return getHeaderImportContractList(ticketId);
            }
        };

        return request;
    }

    private static Map<String, String> getHeaderImportContractList(String ticketId) {
        Map<String, String> params = new HashMap<>();
        params.put("TicketID", ticketId);
        return params;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // api MyTicketSupportCreated(MyTicketSupportObject ticketSupportObject)
    public static GsonRequest<MyResponseObject> MyTicketSupportCreated(final MyTicketSupportObject ticketSupportObject, Response.Listener<MyResponseObject> listener, Response.ErrorListener errorListener) {
        GsonRequest<MyResponseObject> request = new GsonRequest<MyResponseObject>(Constants.MY_UPDATED_CREATED_TICKET_SUPPORT, MyResponseObject.class, listener, errorListener) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return getHeaderCreatedTicketSupport(ticketSupportObject);
            }
        };

        return request;
    }

    private static Map<String, String> getHeaderCreatedTicketSupport(MyTicketSupportObject ticketSupportObject) {
        Map<String, String> params = new HashMap<>();
        params.put("Kind", ticketSupportObject.getKind() );
        params.put("TicketStatus", ticketSupportObject.getTicketStatus());
        params.put("Title", ticketSupportObject.getTitle());
        params.put("Description", ticketSupportObject.getDescription());
        params.put("Priority", ticketSupportObject.getPriority());
        params.put("EstimatedTime", ticketSupportObject.getEstimatedTime() );
        params.put("CricLev", ticketSupportObject.getCricLev());
        params.put("FoundStaff", ticketSupportObject.getFoundStaff());
        params.put("FoundDivision", ticketSupportObject.getFoundDivision());
        params.put("FoundPhone", ticketSupportObject.getFoundPhone());
        params.put("FoundIP", ticketSupportObject.getFoundIP());
        params.put("RemindStatus", ticketSupportObject.getRemindStatus());
        params.put("HasWarning", ticketSupportObject.getHasWarning() );
        params.put("OperateStaff", ticketSupportObject.getOperateStaff());
        params.put("VisorStaff", ticketSupportObject.getVisorStaff());
        params.put("ProcessStaff", ticketSupportObject.getProcessStaff());
        params.put("ProcessPhone", ticketSupportObject.getProcessPhone());
        params.put("ProcessIP", ticketSupportObject.getProcessIP());
        params.put("Queue", ticketSupportObject.getQueue() );
        params.put("ParentID", ticketSupportObject.getParentID());
        params.put("LocationID", ticketSupportObject.getLocationID());
        params.put("EffBranch", ticketSupportObject.getEffBranch());
        params.put("ServiceType", ticketSupportObject.getServiceType());
        params.put("CusType", ticketSupportObject.getCusType());
        params.put("IssueID", ticketSupportObject.getIssueID() );
        params.put("ReasonGroup", ticketSupportObject.getReasonGroup());
        params.put("ProcessStep", ticketSupportObject.getProcessStep());
        params.put("ProcessName", ticketSupportObject.getProcessName());
        params.put("IssueName", ticketSupportObject.getIssueName());
        params.put("LocationName", ticketSupportObject.getLocationName());
        params.put("BranchName", ticketSupportObject.getBranchName() );
        params.put("ServiceName", ticketSupportObject.getServiceName());
        params.put("CusName", ticketSupportObject.getCusName());
        params.put("FoundDivName", ticketSupportObject.getFoundDivName());
        params.put("FoundName", ticketSupportObject.getFoundName());
        params.put("Options", ticketSupportObject.getOptions());
        return params;
    }


}
