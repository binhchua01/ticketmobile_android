package app.fpt.ticketmobile.app.ui.fragments.dialogFragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import app.fpt.ticketmobile.app.model.support.ImportContractObject;
import app.fpt.ticketmobile.app.model.support.MyImportContractObject;
import app.fpt.ticketmobile.app.ui.adapter.dialog.support.ImportContractAdapter;
import app.fpt.ticketmobile.R;

/**
 * Created by Administrator on 7/20/2016.
 */
public class ImportContractDialogFragment extends TmBaseDialogFragment<ImportContractDialogFragment.ImportContractDialogCallback> {

    private static final String EXTRA_IMPORT_CONTRACT_KEY = "extra_import_contract_key";

    private SwitchCompat mSwitchScroll = null;
    private View mView;
    private TextView tvContract = null;
    private TextView tvContractName = null;
    private TextView tvIpDomain = null;
    private TextView tvAddress = null;
    private Button btnImportContract = null;
    private Button btnImportFinish = null;
    private Button btnImportCancel = null;
    private List<ImportContractObject> importContractList = null;
    private ListView mListView;
    private TextView mTvShowLog = null;
    private ImportContractAdapter contractAdapter = null;
    private MyImportContractObject myImportContractObject = null;
    private static final String NAME_PATTERN = "^[a-zA-Z0-9.:/]+$";


    public interface ImportContractDialogCallback{

        void getMyImportContractObject(List<ImportContractObject>  importContractObjectList);

    }

    public ImportContractDialogFragment() {
        super(ImportContractDialogCallback.class);

    }

    public static ImportContractDialogFragment newInstance(MyImportContractObject myImportContractObject){
        Bundle bundle = new Bundle();
        bundle.putSerializable(EXTRA_IMPORT_CONTRACT_KEY, myImportContractObject);

        ImportContractDialogFragment fragment = new ImportContractDialogFragment();
        fragment.setArguments(bundle);
        return fragment;
    }



    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        importContractList = new ArrayList<>();
        extraArguments();
    }

    private void extraArguments() {
        if (getArguments() != null){
            myImportContractObject = (MyImportContractObject) getArguments().getSerializable(EXTRA_IMPORT_CONTRACT_KEY);
            if (myImportContractObject != null){
                if (myImportContractObject.getImportContractObjectList().size() > 0){
                    importContractList = new ArrayList<>(myImportContractObject.getImportContractObjectList());
                }
            }
        }

    }

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).setView(R.layout.entry_dialog_fragment_import_contracts).create();

        alertDialog.setOnShowListener(new PrivateOnShowListener(alertDialog));
        return alertDialog;
    }

   private final class PrivateOnShowListener implements DialogInterface.OnShowListener{

       private AlertDialog $dialog;

       public PrivateOnShowListener(AlertDialog alertDialog) {
           this.$dialog = alertDialog;
       }

       @Override
       public void onShow(DialogInterface dialog) {
            setupView($dialog);
       }

       private void setupView(AlertDialog dialog) {
           mListView = (ListView) dialog.findViewById(R.id.list);
           mTvShowLog = (TextView) dialog.findViewById(R.id.tm_tv_show_text);
           mSwitchScroll = (SwitchCompat) dialog.findViewById(R.id.tm_switch_dialog_contract_scroll);
           mSwitchScroll.setOnCheckedChangeListener(new PrivateOnCheckedChangeListener());
           mView = dialog.findViewById(R.id.layoutImportContracts);
           tvContract = (TextView) dialog.findViewById(R.id.tm_edt_dialog_contracts);
           tvContractName = (TextView) dialog.findViewById(R.id.tm_edt_dialog_contracts_name);
           tvIpDomain = (TextView) dialog.findViewById(R.id.tm_edt_dialog_contracts_ip_domain);
           tvAddress = (TextView) dialog.findViewById(R.id.tm_edt_dialog_contracts_address);
           btnImportContract = (Button) dialog.findViewById(R.id.tm_button_import_contract_dialog);
           btnImportFinish = (Button) dialog.findViewById(R.id.tm_button_finish_contract_dialog);
           btnImportCancel = (Button) dialog.findViewById(R.id.tm_button_cancel_contract_dialog);
           btnImportContract.setOnClickListener(new PrivateImportContractListener());
           btnImportFinish.setOnClickListener(new PrivateImportFinishListener());
           btnImportCancel.setOnClickListener(new PrivateImportCancelListener());
           contractAdapter = new ImportContractAdapter(getActivity(), importContractList);
           mListView.setAdapter(contractAdapter);
           setupDataToViews();
       }

   }

    private void setupDataToViews() {
        if (importContractList != null){
            if (importContractList.size() > 0){
                mListView.setVisibility(View.VISIBLE);
                mTvShowLog.setVisibility(View.GONE);
            }else {
                mListView.setVisibility(View.GONE);
                mTvShowLog.setVisibility(View.VISIBLE);
            }
        }

    }



    private final class PrivateOnCheckedChangeListener implements CompoundButton.OnCheckedChangeListener{

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            switch (buttonView.getId()){
                case R.id.tm_switch_dialog_contract_scroll:
                    if (isChecked){
                        mView.setVisibility(View.GONE);
                    }else{
                        mView.setVisibility(View.VISIBLE);
                    }

                    if (importContractList.size() > 0){
                        mListView.setVisibility(View.VISIBLE);
                        mTvShowLog.setVisibility(View.GONE);
                    }else {
                        mListView.setVisibility(View.GONE);
                        mTvShowLog.setVisibility(View.VISIBLE);
                    }
                    break;
            }
        }
    }


    private final class PrivateImportFinishListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            getCallback().getMyImportContractObject(importContractList);
            dismiss();
        }
    }


    private final class PrivateImportCancelListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
           dismiss();
        }
    }

    private final class PrivateImportContractListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            if(isCheckInputIfNull(tvContract.getText().toString(), tvContractName.getText().toString(), tvIpDomain.getText().toString(), tvAddress.getText().toString())){
                importContractList.add(new ImportContractObject(tvContract.getText().toString(), tvContractName.getText().toString(), tvIpDomain.getText().toString(), tvAddress.getText().toString()));

                if (importContractList.size() > 0){
                    mListView.setVisibility(View.VISIBLE);
                    mTvShowLog.setVisibility(View.GONE);
                    contractAdapter.notifyDataSetChanged();
                    clearData();
                }
            }
        }
    }

    private void clearData(){
        tvContract.setText("");
        tvContractName.setText("");
        tvIpDomain.setText("");
        tvAddress.setText("");
    }


    private boolean isCheckInputIfNull(String contract, String contractName, String ipDomain, String address){
        if (contract.equalsIgnoreCase("") && contractName.equalsIgnoreCase("") && ipDomain.equalsIgnoreCase("") && address.equalsIgnoreCase("") ){
            getTmToast().makeToast(  getString(R.string.tm_toast_input_contract_empty), Toast.LENGTH_LONG);
            return false;
        }

        if (!ipDomain.equalsIgnoreCase("")  && !checkInput(ipDomain)){
            getTmToast().makeToast(getString(R.string.tm_contract_ip_domain) + " " +getString(R.string.tm_toast_input_contract), Toast.LENGTH_LONG);
            return false;
        }

        return true;
    }

    private boolean checkInput(String content){
        Pattern pattern = Pattern.compile(NAME_PATTERN);
        Matcher matcher = pattern.matcher(content);

        return matcher.matches();
    }



}
