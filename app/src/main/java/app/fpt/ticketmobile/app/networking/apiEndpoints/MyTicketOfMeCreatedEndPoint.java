package app.fpt.ticketmobile.app.networking.apiEndpoints;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;

import java.util.HashMap;
import java.util.Map;

import app.fpt.ticketmobile.app.model.MyTicketOfUserCreatedObject;
import app.fpt.ticketmobile.app.networking.utils.GsonRequest;

/**
 * Created by Administrator on 7/1/2016.
 */
public class MyTicketOfMeCreatedEndPoint {

    private static Map<String, String> getHeader(String userEmail){
        Map<String, String>   params = new HashMap<String, String>();
        params.put("UserEmail", userEmail);
        return params;
    }

    public static GsonRequest<MyTicketOfUserCreatedObject> getMyCreatedTicket(final String userEmail, Response.Listener<MyTicketOfUserCreatedObject> listener, Response.ErrorListener errorListener){
        GsonRequest<MyTicketOfUserCreatedObject> gsonRequest = new GsonRequest<MyTicketOfUserCreatedObject>(Constants.MY_GET_TICKET_OF_USER_CREATED_END_POINT, MyTicketOfUserCreatedObject.class, listener, errorListener){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return getHeader(userEmail);
            }
        };
        return gsonRequest;
    }
}
