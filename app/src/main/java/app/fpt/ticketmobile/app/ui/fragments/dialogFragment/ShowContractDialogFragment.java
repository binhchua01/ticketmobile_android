package app.fpt.ticketmobile.app.ui.fragments.dialogFragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import app.fpt.ticketmobile.app.model.support.ImportContractObject;
import app.fpt.ticketmobile.app.model.support.MyImportContractObject;
import app.fpt.ticketmobile.app.ui.adapter.ShowContractAdapter;
import app.fpt.ticketmobile.app.ui.adapter.dialog.support.ImportContractAdapter;
import app.fpt.ticketmobile.R;

/**
 * Created by Administrator on 7/28/2016.
 */
public class ShowContractDialogFragment extends TmBaseDialogFragment<ShowContractDialogFragment.ShowContractInterface> {

    private static final String EXTRA_MY_IMPORT_CONTRACT_KEY = "extra_my_import_contract_key";

    private ImportContractAdapter mImportContractAdapter = null;
    private List<ImportContractObject> mImportContractObjectList = null;
    private RecyclerView mRecyclerViewContracts;

    public interface ShowContractInterface {

    }

    public ShowContractDialogFragment() {
        super(ShowContractInterface.class);
    }

    public static ShowContractDialogFragment newInstance(MyImportContractObject importContractObject) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(EXTRA_MY_IMPORT_CONTRACT_KEY, importContractObject);

        ShowContractDialogFragment fragment = new ShowContractDialogFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        extraArguments();
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).setTitle("Danh sách hợp đồng").setView(R.layout.entry_dialog_fragment_show_contract)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create();

        alertDialog.setOnShowListener(new PrivateOnShowListener(alertDialog));
        return alertDialog;
    }


    private final class PrivateOnShowListener implements DialogInterface.OnShowListener {

        private AlertDialog $dialog;

        public PrivateOnShowListener(AlertDialog alertDialog) {
            this.$dialog = alertDialog;
        }

        @Override
        public void onShow(DialogInterface dialog) {
            setupView($dialog);
        }

        private void setupView(AlertDialog dialog) {
            mRecyclerViewContracts = (RecyclerView) dialog.findViewById(R.id.list);
            mRecyclerViewContracts.setLayoutManager(new LinearLayoutManager(getActivity(), 1, false));
         //   mRecyclerViewContracts.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.ContainerType.VERTICAL_LIST));

            if (mImportContractObjectList != null){
                ShowContractAdapter adapter = new ShowContractAdapter(mImportContractObjectList);
                mRecyclerViewContracts.setAdapter(adapter);
            }

        }
    }

    private void extraArguments() {
        if (getArguments() != null) {
            MyImportContractObject myImportContractObject = (MyImportContractObject) getArguments().getSerializable(EXTRA_MY_IMPORT_CONTRACT_KEY);
            if (myImportContractObject != null ) {
                if (myImportContractObject.getImportContractObjectList().size() > 0) {
                    mImportContractObjectList = new ArrayList<>(myImportContractObject.getImportContractObjectList());
                }
            }
        }
    }
}
