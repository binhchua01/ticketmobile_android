package app.fpt.ticketmobile.app.ui.fragments.authenticatedFragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

import java.util.ArrayList;
import java.util.List;

import app.fpt.ticketmobile.app.model.MyTicketInventoryUnFinishObject;
import app.fpt.ticketmobile.app.networking.apiEndpoints.MyTicketInventoryUnFinishedEndPoint;
import app.fpt.ticketmobile.app.ui.adapter.MyTicketInventoryUnFinishedAdapter;
import app.fpt.ticketmobile.app.ui.lib.FragmentRefreshBasedSwipeRefreshLayoutHandler;
import app.fpt.ticketmobile.R;
import app.fpt.ticketmobile.app.ui.adapter.ticketinventoryunfinished.TicketInventoryUnFinishedHeader;
import app.fpt.ticketmobile.app.ui.fragments.TmBaseFragment;

/**
 * Created by ToanMaster on 6/23/2016.
 */
public class TicketInventoryUnFinishFragment extends TmBaseFragment<TicketInventoryUnFinishFragment.Callback> implements SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG_MY_TICKET_INVENTORY_UNFINISHED_KEY = "tag_my_ticket_inventory_unfinished_key";
    private RecyclerView mListTicketInventoryNotAssign;
    private MyTicketInventoryUnFinishedAdapter inventoryAdapter;
    List<TicketInventoryUnFinishedHeader> ticketInventoryHeaderList = null;

    public interface Callback{

    }

    public TicketInventoryUnFinishFragment() {
        super(Callback.class, Integer.valueOf(R.layout.fragment_ticket_inventory_not_assign), false, true);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        ticketInventoryHeaderList = new ArrayList<>();
    }

    @Override
    protected void onTmActivityCreated(View parent, Bundle savedInstanceState) {
        super.onTmActivityCreated(parent, savedInstanceState);
        mListTicketInventoryNotAssign = (RecyclerView) parent.findViewById(R.id.list);
        mListTicketInventoryNotAssign.setItemAnimator(new DefaultItemAnimator());
        inventoryAdapter = new MyTicketInventoryUnFinishedAdapter(getActivity(), ticketInventoryHeaderList);
        mListTicketInventoryNotAssign.setAdapter(inventoryAdapter);


        SwipeRefreshLayout swipeRefreshLayout =  (SwipeRefreshLayout) parent.findViewById(R.id.swipe_refresh_layout);
        setFragmentRefreshAnimationHandler(new FragmentRefreshBasedSwipeRefreshLayoutHandler(swipeRefreshLayout, this, Integer.valueOf(R.color.tm_swipe_refresh_color)));
        if (!isShowingRefreshAnimation()) {
            swipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    showRefreshAnimation();
                }
            }, 500);
        }

        inventoryAdapter.onRestoreInstanceState(savedInstanceState);
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        inventoryAdapter.onSaveInstanceState(outState);
    }



    @Override
    public void onRefresh() {
        autoSynced();
    }

    @Override
    protected void autoSynced() {
        super.autoSynced();
        if (!isShowingRefreshAnimation()) {
            showRefreshAnimation();
        }
        getTmApiEndPointProvider().addRequest( MyTicketInventoryUnFinishedEndPoint.getMyTicketInventory(String.valueOf(getTmSharePreferencesUtils().getUserLogin().getID()),

        new Response.Listener<MyTicketInventoryUnFinishObject>() {
            @Override
            public void onResponse(final MyTicketInventoryUnFinishObject myTicketInventoryObject) {
                if (getActivity() != null) {
                    if (myTicketInventoryObject != null && myTicketInventoryObject.getTicketInventoryNotUnFinishedHeaderList() != null) {
                        if (myTicketInventoryObject.getTicketInventoryNotUnFinishedHeaderList().size() > 0) {
                            inventoryAdapter.clear();
                            ticketInventoryHeaderList = myTicketInventoryObject.getTicketInventoryNotUnFinishedHeaderList();
                            inventoryAdapter = new MyTicketInventoryUnFinishedAdapter(getActivity(), ticketInventoryHeaderList);
                            mListTicketInventoryNotAssign.setAdapter(inventoryAdapter);
                            mListTicketInventoryNotAssign.setLayoutManager(new LinearLayoutManager(getActivity()));
                        } else {
                            setLayoutNoData(R.layout.fragment_no_data);
                        }
                    } else {
                        setLayoutNoData(R.layout.fragment_no_data);
                    }
                    stopRefreshAnimation();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onErrorListener(error);
            }
        }), TAG_MY_TICKET_INVENTORY_UNFINISHED_KEY);

    }

    @Override
    public void onStop() {
        super.onStop();
        getTmApiEndPointProvider().cancelAllRequest(TAG_MY_TICKET_INVENTORY_UNFINISHED_KEY);
    }

    private void onErrorListener(VolleyError error){
        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
            getTmToast().makeToast(getString(R.string.tm_error_connect), Toast.LENGTH_SHORT);
        } else if (error instanceof AuthFailureError) {
            Log.d( "tmt","AuthFailureError");
        } else if (error instanceof ServerError) {
            Log.d( "tmt","ServerError");
        } else if (error instanceof NetworkError) {
            Log.d( "tmt","NetworkError");
        } else if (error instanceof ParseError) {
            Log.d( "tmt","ParseError");
        }else {
            Log.d( "tmt","onErrorResponse : " + error.getMessage());
        }
        stopRefreshAnimation();
    }


    private void setLayoutNoData(Integer layoutResId) {
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewGroup rootView = (ViewGroup) getView();
        View mainView = inflater.inflate(layoutResId.intValue(), rootView, false);
        rootView.removeAllViews();
        rootView.addView(mainView);
        SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) mainView.findViewById(R.id.swipe_refresh_layout);
        setFragmentRefreshAnimationHandler(new FragmentRefreshBasedSwipeRefreshLayoutHandler(swipeRefreshLayout, this, Integer.valueOf(R.color.tm_swipe_refresh_color)));
    }
}
