package app.fpt.ticketmobile.app.networking.apiEndpoints;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;

import java.util.HashMap;
import java.util.Map;

import app.fpt.ticketmobile.app.model.MyTicketObject;
import app.fpt.ticketmobile.app.networking.utils.GsonRequest;

/**
 * Created by Administrator on 6/21/2016.
 */
public abstract class MyTicketEndPoint {


    private static  Map<String, String> getHeader(String userEmail){
        Map<String, String>   params = new HashMap<String, String>();
        params.put("UserEmail", userEmail);
        return params;
    }

    private static Map<String, String> getHeaderTicketQueueAndStatus(String userId, String queueId, String queueStatusId){
        Map<String, String>   params = new HashMap<String, String>();
        params.put("UserID", userId);
        params.put("Queue", queueId);
        params.put("TicketStatus", queueStatusId);
        return params;
    }

    public static GsonRequest<MyTicketObject> getMyTicket(final String userEmail, final Response.Listener<MyTicketObject> listener, Response.ErrorListener errorListener){
        GsonRequest<MyTicketObject> myTicketGsonRequest = new GsonRequest<MyTicketObject>(Constants.MY_GET_TICKET_END_POINT, MyTicketObject.class, listener, errorListener){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return getHeader(userEmail);
            }
        };

        return myTicketGsonRequest;
    }


    public static GsonRequest<MyTicketObject> getMyTicketQueueAndStatus(final String userId, final String queueId,final String queueStatusId, final Response.Listener listener, Response.ErrorListener errorListener){
        GsonRequest<MyTicketObject> myTicketQueueAndStatusGsonRequest = new GsonRequest<MyTicketObject>(Constants.MY_GET_TICKET_QUEUE_BY_STATUES, MyTicketObject.class, listener, errorListener){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return getHeaderTicketQueueAndStatus(userId, queueId, queueStatusId);
            }
        };

        return myTicketQueueAndStatusGsonRequest;
    }
}
