package app.fpt.ticketmobile.app.ui.adapter.dashboard;

/**
 * Created by Administrator on 7/8/2016.
 */
public class ItemDashboard {

    private String mNameDashboard;
    private int mResIdDashboard;

    public ItemDashboard(){}

    public ItemDashboard(int mResIdDashboard, String mNameDashboard) {
        this.mResIdDashboard = mResIdDashboard;
        this.mNameDashboard = mNameDashboard;
    }

    public String getNameDashboard() {
        return mNameDashboard;
    }

    public void setNameDashboard(String mNameDashboard) {
        this.mNameDashboard = mNameDashboard;
    }

    public int getResIdDashboard() {
        return mResIdDashboard;
    }

    public void setResIdDashboard(int mResIdDashboard) {
        this.mResIdDashboard = mResIdDashboard;
    }
}
