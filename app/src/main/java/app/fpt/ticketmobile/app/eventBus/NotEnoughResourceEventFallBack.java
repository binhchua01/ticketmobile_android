package app.fpt.ticketmobile.app.eventBus;

import android.widget.Toast;

import com.squareup.otto.DeadEvent;
import com.squareup.otto.Subscribe;

import app.fpt.ticketmobile.app.ui.lib.TmToast;
import app.fpt.ticketmobile.app.eventBus.notEnoughResourceEvent.BaseNotEnoughResourceEvent;

/**
 * Created by ToanMaster on 6/13/2016.
 */
public class NotEnoughResourceEventFallBack {

    private final TmToast mTmToast;
    public NotEnoughResourceEventFallBack(EventBus eventBus, TmToast tmToast){
        this.mTmToast = tmToast;
        eventBus.register(this);
    }

    @Subscribe
    public void onEvent(DeadEvent deadEvent){
        if (deadEvent.event instanceof BaseNotEnoughResourceEvent) {
            this.mTmToast.makeToast(((BaseNotEnoughResourceEvent) deadEvent.event).getStringResId(), Toast.LENGTH_LONG);
        }
    }

}
