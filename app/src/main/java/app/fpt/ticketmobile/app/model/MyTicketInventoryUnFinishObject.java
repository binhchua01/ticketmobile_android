package app.fpt.ticketmobile.app.model;

import java.util.List;

import app.fpt.ticketmobile.app.ui.adapter.ticketinventoryunfinished.TicketInventoryUnFinishedHeader;

/**
 * Created by ToanMaster on 6/23/2016.
 */
public class MyTicketInventoryUnFinishObject{


    private List<TicketInventoryUnFinishedHeader> ticketInventoryUnFinishedHeaderList;
    private List<QueueExistedTicket> Data;


    public List<QueueExistedTicket> getData() {
        return Data;
    }

    public void setData(List<QueueExistedTicket> data) {
        Data = data;
    }

    public List<TicketInventoryUnFinishedHeader> getTicketInventoryNotUnFinishedHeaderList() {
        return ticketInventoryUnFinishedHeaderList;
    }

    public void setTicketInventoryUnFinishedHeaderList(List<TicketInventoryUnFinishedHeader> ticketInventoryHeaderList) {
        this.ticketInventoryUnFinishedHeaderList = ticketInventoryHeaderList;
    }

    public class QueueExistedTicket{
        private Integer InProgress;
        private Integer New;
        private Integer NotClose;
        private Integer OverTime;
        private Integer QueueID;
        private String QueueName;


        public Integer getInProgress() {
            return InProgress;
        }

        public void setInProgress(Integer inProgress) {
            InProgress = inProgress;
        }

        public Integer getNew() {
            return New;
        }

        public void setNew(Integer aNew) {
            New = aNew;
        }

        public Integer getNotClose() {
            return NotClose;
        }

        public void setNotClose(Integer notClose) {
            NotClose = notClose;
        }

        public Integer getOverTime() {
            return OverTime;
        }

        public void setOverTime(Integer overTime) {
            OverTime = overTime;
        }

        public Integer getQueueID() {
            return QueueID;
        }

        public void setQueueID(Integer queueID) {
            QueueID = queueID;
        }

        public String getQueueName() {
            return QueueName;
        }

        public void setQueueName(String queueName) {
            QueueName = queueName;
        }
    }



}
