package app.fpt.ticketmobile.app.lib;

import java.util.regex.Pattern;

/**
 * Created by Administrator on 7/12/2016.
 */
public abstract interface Matcher<T> {
    public abstract boolean matches(T t, Pattern pattern);
}
