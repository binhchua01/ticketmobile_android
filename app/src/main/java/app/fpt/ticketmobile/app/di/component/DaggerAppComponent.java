package app.fpt.ticketmobile.app.di.component;

import android.content.Context;

import javax.inject.Provider;

import app.fpt.ticketmobile.app.di.module.AppModule;
import app.fpt.ticketmobile.app.di.module.AppModule_ProvideContextFactory;
import app.fpt.ticketmobile.app.di.module.AppModule_ProvideTbTrackerAnalyticsFactory;
import app.fpt.ticketmobile.app.di.module.AppModule_ProvideTmGraphicsFactory;
import app.fpt.ticketmobile.app.di.module.AppModule_ProvideTmUiThreadRunnerFactory;
import app.fpt.ticketmobile.app.di.module.NetworkModule;
import app.fpt.ticketmobile.app.eventBus.EventBus;
import app.fpt.ticketmobile.app.lib.TmApiEndPointProvider;
import app.fpt.ticketmobile.app.tracker.interfaces.TmTrackerAnalytics;
import app.fpt.ticketmobile.app.ui.lib.TmToast;
import dagger.internal.ScopedProvider;
import app.fpt.ticketmobile.app.db.TmSharePreferencesUtils;
import app.fpt.ticketmobile.app.di.module.AppModule_ProvideEventBusFactory;
import app.fpt.ticketmobile.app.di.module.AppModule_ProvideTmConnectivityInfoFactory;
import app.fpt.ticketmobile.app.di.module.AppModule_ProvideTmSharePreferencesUtilFactory;
import app.fpt.ticketmobile.app.di.module.AppModule_ProvideTmToastFactory;
import app.fpt.ticketmobile.app.di.module.NetworkModule_ProvideOkHttpStackFactory;
import app.fpt.ticketmobile.app.di.module.NetworkModule_ProvideTmApiEndPointProvideFactory;
import app.fpt.ticketmobile.app.lib.TmConnectivityInfo;
import app.fpt.ticketmobile.app.lib.TmGraphics;
import app.fpt.ticketmobile.app.lib.TmUiThreadRunner;
import app.fpt.ticketmobile.app.networking.utils.OkHttpStack;

/**
 * Created by ToanMaster on 6/13/2016.
 */
public class DaggerAppComponent implements AppComponent{

    private static final boolean $assertionDisabled = !DaggerAppComponent.class.desiredAssertionStatus();
    private Provider<Context> mContextProvider;
    private Provider<TmToast> mTmToastProvider;
    private Provider<TmUiThreadRunner> mTmUiThreadRunnerProvider;
    private Provider<EventBus> mEventBusProvider;
    private Provider<TmConnectivityInfo> mTmConnectivityInfoProvider;
    private Provider<OkHttpStack> mOkHttpStackProvider;
    private Provider<TmApiEndPointProvider> mTmApiEndPointProviderProvider;
    private Provider<TmSharePreferencesUtils> mTmSharePreferencesUtilsProvider;
    private Provider<TmGraphics> mTmGraphicsProvider;
    private Provider<TmTrackerAnalytics> mTmTrackerAnalyticsProvider;


    public static final class Builder{
        private AppModule appModule;
        private NetworkModule networkModule;

        private Builder(){}

        public AppComponent build() {
            if (appModule == null) {
                throw new IllegalStateException(" appModule  must be set");
            }
            if (networkModule == null) {
                networkModule = new NetworkModule();
            }
            return new DaggerAppComponent(this);
        }

        public Builder appModule(AppModule appModule){
            if (appModule == null){
                throw new NullPointerException("appModule");
            }
            this.appModule = appModule;
            return this;
        }

        public Builder networkModule(NetworkModule networkModule){
            if (networkModule == null){
                throw new NullPointerException("networkModule");
            }
            this.networkModule = networkModule;
            return this;
        }

    }

    public static Builder builder(){
        return new Builder();
    }

    private DaggerAppComponent(Builder builder){
        if ($assertionDisabled || builder != null){
            initialize(builder);
            return;
        }
        throw new AssertionError();
    }

    private void initialize(Builder builder) {
        mContextProvider = ScopedProvider.create(AppModule_ProvideContextFactory.create(builder.appModule));
        mTmToastProvider = ScopedProvider.create(AppModule_ProvideTmToastFactory.create(builder.appModule));
        mTmUiThreadRunnerProvider = ScopedProvider.create(AppModule_ProvideTmUiThreadRunnerFactory.create(builder.appModule));
        mTmConnectivityInfoProvider = ScopedProvider.create(AppModule_ProvideTmConnectivityInfoFactory.create(builder.appModule, mContextProvider));
        mEventBusProvider = ScopedProvider.create(AppModule_ProvideEventBusFactory.create(builder.appModule, mTmUiThreadRunnerProvider, mTmToastProvider));
        mOkHttpStackProvider = ScopedProvider.create(NetworkModule_ProvideOkHttpStackFactory.create(builder.networkModule));
        mTmApiEndPointProviderProvider = ScopedProvider.create(NetworkModule_ProvideTmApiEndPointProvideFactory.create(builder.networkModule, mContextProvider, mOkHttpStackProvider));
        mTmSharePreferencesUtilsProvider = ScopedProvider.create(AppModule_ProvideTmSharePreferencesUtilFactory.create(builder.appModule, mContextProvider));
        mTmGraphicsProvider = ScopedProvider.create(AppModule_ProvideTmGraphicsFactory.create(builder.appModule, mContextProvider));
        mTmTrackerAnalyticsProvider = ScopedProvider.create(AppModule_ProvideTbTrackerAnalyticsFactory.create(builder.appModule, mContextProvider));
    }


    @Override
    public EventBus getEventBus() {
        return mEventBusProvider.get();
    }

    @Override
    public TmToast getTmToast() {
        return mTmToastProvider.get();
    }

    @Override
    public TmUiThreadRunner getTmUiThreadRunner() {
        return mTmUiThreadRunnerProvider.get();
    }

    @Override
    public TmConnectivityInfo getTmConnectivityInfo() {
        return mTmConnectivityInfoProvider.get();
    }

    @Override
    public OkHttpStack getOkHttpStack() {
        return mOkHttpStackProvider.get();
    }

    @Override
    public TmApiEndPointProvider getTmApiEndPointProvider() {
        return mTmApiEndPointProviderProvider.get();
    }

    @Override
    public TmSharePreferencesUtils getTmSharePreferencesUtils() {
        return mTmSharePreferencesUtilsProvider.get();
    }

    @Override
    public TmGraphics getTmGraphics() {
        return mTmGraphicsProvider.get();
    }

    @Override
    public TmTrackerAnalytics getTmTrackerAnalytics() {
        return mTmTrackerAnalyticsProvider.get();
    }
}
