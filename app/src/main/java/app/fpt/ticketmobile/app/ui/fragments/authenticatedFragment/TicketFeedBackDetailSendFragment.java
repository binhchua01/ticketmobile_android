package app.fpt.ticketmobile.app.ui.fragments.authenticatedFragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

import app.fpt.ticketmobile.app.model.MyResponseObject;
import app.fpt.ticketmobile.app.model.MyTicketFeedbackDetailObject;
import app.fpt.ticketmobile.app.networking.apiEndpoints.MyTicketResponseEndpoint;
import app.fpt.ticketmobile.R;
import app.fpt.ticketmobile.app.lib.utils.TmUtils;
import app.fpt.ticketmobile.app.ui.fragments.TmBaseFragment;
import app.fpt.ticketmobile.app.ui.lib.FragmentRefreshBasedSwipeRefreshLayoutHandler;

/**
 * Created by Administrator on 7/5/2016.
 */
public class TicketFeedBackDetailSendFragment extends TmBaseFragment<TicketFeedBackDetailSendFragment.Callback> implements SwipeRefreshLayout.OnRefreshListener {
    private static final String EXTRA_TICKET_ID_KEY = "extra_ticket_response_id_key";
    private static final String EXTRA_FEED_BACK_DETAIL_KEY = "extra_feed_back_detail_key";
    private static final String EXTRA_BOOLEAN_FLAG_ALL_KEY = "extra_boolean_flag_all_key";

    private static final String REQUEST_TICKET_FEED_BACK_DETAIL_SEND_TAG = "request_ticket_feed_back_detail_send_tag";

    private boolean mFlagAll = false;
    private String TICKET_ID = null;
    private MyTicketFeedbackDetailObject mMyTicketFeedbackDetailObject = null;

    private TextView tvResponseTitle;
    private TextView tvResponseFromUser;
    private TextView tvResponseToUser;
    private TextView tvResponseToCC;
    private TextView tvResponseBody;
    private WebView wbResponseBody;
    private Button btnFeedbackSend;
    private Button btnFeedbackCancel;
    private View view;
    private Toolbar mToolbar;
    private SwipeRefreshLayout swipeRefreshLayout;

    private ProgressDialog mProgressDialog;


    public interface Callback {

    }

    private final class PrivateFeedbackActionListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.tm_btn_ticket_feedback_send:
                    onFeedbackSend();
                    break;
                case R.id.tm_btn_ticket_feedback_cancel:
                    getActivity().finish();
                    break;
            }

        }
    }

    public TicketFeedBackDetailSendFragment() {
        super(Callback.class, Integer.valueOf(R.layout.fragment_ticket_feedback_detail_send), false, true);
    }

    public static TicketFeedBackDetailSendFragment buildIntent(String ticketId, MyTicketFeedbackDetailObject mMyTicketFeedbackDetailObject, boolean flagAll) {
        Bundle bundle = new Bundle();
        bundle.putString(EXTRA_TICKET_ID_KEY, ticketId);
        bundle.putSerializable(EXTRA_FEED_BACK_DETAIL_KEY, mMyTicketFeedbackDetailObject);
        bundle.putBoolean(EXTRA_BOOLEAN_FLAG_ALL_KEY, flagAll);

        TicketFeedBackDetailSendFragment ticketFeedBackDetailSendFragment = new TicketFeedBackDetailSendFragment();
        ticketFeedBackDetailSendFragment.setArguments(bundle);
        return ticketFeedBackDetailSendFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        extrasArguments();
    }

    @Override
    protected void onTmActivityCreated(View parent, Bundle savedInstanceState) {
        super.onTmActivityCreated(parent, savedInstanceState);
        setupViews(parent);
    }

    @Override
    public void onStop() {
        super.onStop();
        getTmApiEndPointProvider().cancelAllRequest(REQUEST_TICKET_FEED_BACK_DETAIL_SEND_TAG);
    }

    private void setupViews(View parent) {

        mProgressDialog = new ProgressDialog(getContext());
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setCancelable(false);

        view = parent.findViewById(R.id.tm_layout_ticket_feedback_body);
        mToolbar = (Toolbar) parent.findViewById(R.id.tm_toolbar_app_bottom);
        tvResponseTitle = (TextView) parent.findViewById(R.id.tm_tv_ticket_response_title);
        tvResponseBody = (TextView) parent.findViewById(R.id.tm_tv_ticket_feedback_body);
        tvResponseFromUser = (TextView) parent.findViewById(R.id.tm_tv_ticket_response_from_user);
        tvResponseToUser = (TextView) parent.findViewById(R.id.tm_tv_ticket_response_to_user);
        tvResponseToCC = (TextView) parent.findViewById(R.id.tm_tv_ticket_response_to_cc);
        wbResponseBody = (WebView) parent.findViewById(R.id.tm_web_view_response_body);

        btnFeedbackSend = (Button) parent.findViewById(R.id.tm_btn_ticket_feedback_send);
        btnFeedbackCancel = (Button) parent.findViewById(R.id.tm_btn_ticket_feedback_cancel);
        btnFeedbackSend.setOnClickListener(new PrivateFeedbackActionListener());
        btnFeedbackCancel.setOnClickListener(new PrivateFeedbackActionListener());

        swipeRefreshLayout = (SwipeRefreshLayout) parent.findViewById(R.id.swipe_refresh_layout);
        setFragmentRefreshAnimationHandler(new FragmentRefreshBasedSwipeRefreshLayoutHandler(swipeRefreshLayout, this, Integer.valueOf(R.color.tm_swipe_refresh_color)));
        if (!isShowingRefreshAnimation()) {
            swipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    showRefreshAnimation();
                }
            }, 500);
        }
        swipeRefreshLayout.setEnabled(false);
    }

    @Override
    public void onRefresh() {

    }

    @Override
    protected void autoSynced() {
        super.autoSynced();
        if (!isShowingRefreshAnimation()) {
            showRefreshAnimation();
        }

        if (mMyTicketFeedbackDetailObject != null && TICKET_ID != null) {
            tvResponseTitle.setText(mMyTicketFeedbackDetailObject.getSubject());
            tvResponseFromUser.setText(getTmSharePreferencesUtils().getEmailUserLogin());

            wbResponseBody.loadData(TmUtils.DecodeBase64(mMyTicketFeedbackDetailObject.getBodyHTML()), "text/html; charset=UTF-8", null);
            if (mFlagAll) {
                tvResponseToUser.setText(mMyTicketFeedbackDetailObject.getToAdr());
                tvResponseToCC.setText(mMyTicketFeedbackDetailObject.getCc());
                tvResponseToCC.setEnabled(true);
            } else {
                tvResponseToUser.setText(mMyTicketFeedbackDetailObject.getFromAdr());
                tvResponseToCC.setEnabled(false);
            }

            view.setVisibility(View.VISIBLE);
            mToolbar.setVisibility(View.VISIBLE);

            stopRefreshAnimation();
        } else {
            swipeRefreshLayout.setEnabled(true);
            stopRefreshAnimation();
        }
    }


    private MyTicketFeedbackDetailObject getMyTicketFeedbackDetailObject()
    {
        MyTicketFeedbackDetailObject obj = new MyTicketFeedbackDetailObject();
        obj.setSubject(tvResponseTitle.getText().toString().isEmpty() ? "" : tvResponseTitle.getText().toString() );
        obj.setToAdr(tvResponseFromUser.getText().toString().isEmpty() ? "" : tvResponseFromUser.getText().toString() );
        obj.setFromAdr(tvResponseFromUser.getText().toString().isEmpty() ? "" : tvResponseFromUser.getText().toString() );
        obj.setCc(tvResponseToCC.getText().toString().isEmpty() ? "" : tvResponseToCC.getText().toString() );
        obj.setQueueAdr(mMyTicketFeedbackDetailObject.getQueueAdr());
        String content = tvResponseBody.getText().toString().isEmpty() ? "" : tvResponseBody.getText().toString();
        obj.setBodyHTML("<p> "+content.replace("\n", "<br />")+" </p>" + TmUtils.DecodeBase64(mMyTicketFeedbackDetailObject.getBodyHTML()));
        return obj;
    }

    private void onFeedbackSend(){
        mProgressDialog.setMessage(getString(R.string.tm_progress_dialog_feedback_send));
        mProgressDialog.show();

        getTmApiEndPointProvider().addRequest(MyTicketResponseEndpoint.MyTicketResponseUpdated(TICKET_ID, getTmSharePreferencesUtils().getUserIdLogin(),
                getTmSharePreferencesUtils().getEmailUserLogin(), getMyTicketFeedbackDetailObject(), new Response.Listener<MyResponseObject>() {
                    @Override
                    public void onResponse(MyResponseObject myResponseObject) {
                        getTmToast().makeToast(myResponseObject.getErrorDescription(), Toast.LENGTH_SHORT);
                        mProgressDialog.dismiss();
                        if (myResponseObject.getErrorCode().intValue() == 1){
                            getActivity().finish();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        onErrorListener(error);
                    }
                }), REQUEST_TICKET_FEED_BACK_DETAIL_SEND_TAG);
    }

    private void onErrorListener(VolleyError error) {
        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
            getTmToast().makeToast(getString(R.string.tm_error_connect), Toast.LENGTH_SHORT);
        } else if (error instanceof AuthFailureError) {
            Log.d("tmt", "AuthFailureError");
        } else if (error instanceof ServerError) {
            Log.d("tmt", "ServerError");
        } else if (error instanceof NetworkError) {
            Log.d("tmt", "NetworkError");
        } else if (error instanceof ParseError) {
            Log.d("tmt", "ParseError");
        } else {
            Log.d("tmt", "onErrorResponse : " + error.getMessage());
        }
        mProgressDialog.dismiss();
    }

    private void extrasArguments() {
        if (getArguments() != null) {
            TICKET_ID = getArguments().getString(EXTRA_TICKET_ID_KEY);
            mMyTicketFeedbackDetailObject = (MyTicketFeedbackDetailObject) getArguments().getSerializable(EXTRA_FEED_BACK_DETAIL_KEY);
            mFlagAll = getArguments().getBoolean(EXTRA_BOOLEAN_FLAG_ALL_KEY);
        }
    }
}
