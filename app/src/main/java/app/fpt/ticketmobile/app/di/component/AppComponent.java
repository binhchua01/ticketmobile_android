package app.fpt.ticketmobile.app.di.component;

import app.fpt.ticketmobile.app.eventBus.EventBus;
import app.fpt.ticketmobile.app.lib.TmApiEndPointProvider;
import app.fpt.ticketmobile.app.tracker.interfaces.TmTrackerAnalytics;
import app.fpt.ticketmobile.app.ui.lib.TmToast;
import app.fpt.ticketmobile.app.db.TmSharePreferencesUtils;
import app.fpt.ticketmobile.app.lib.TmConnectivityInfo;
import app.fpt.ticketmobile.app.lib.TmGraphics;
import app.fpt.ticketmobile.app.lib.TmUiThreadRunner;
import app.fpt.ticketmobile.app.networking.utils.OkHttpStack;

/**
 * Created by ToanMaster on 6/13/2016.
 *
 */
public interface AppComponent {

    EventBus getEventBus();

    TmToast getTmToast();

    TmUiThreadRunner getTmUiThreadRunner();

    TmConnectivityInfo getTmConnectivityInfo();

    OkHttpStack getOkHttpStack();

    TmApiEndPointProvider getTmApiEndPointProvider();

    TmSharePreferencesUtils getTmSharePreferencesUtils();

    TmGraphics getTmGraphics();

    TmTrackerAnalytics getTmTrackerAnalytics();


}
