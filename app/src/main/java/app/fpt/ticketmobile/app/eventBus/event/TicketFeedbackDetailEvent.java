package app.fpt.ticketmobile.app.eventBus.event;

import app.fpt.ticketmobile.app.eventBus.Event;

/**
 * Created by Administrator on 7/4/2016.
 */
public class TicketFeedbackDetailEvent implements Event{

    private String ticketResponseId;

    public String getTicketResponseId() {
        return ticketResponseId;
    }

    public void setTicketResponseId(String ticketResponseId) {
        this.ticketResponseId = ticketResponseId;
    }

    public TicketFeedbackDetailEvent(String ticketResponseId) {
        this.ticketResponseId = ticketResponseId;
    }
}
