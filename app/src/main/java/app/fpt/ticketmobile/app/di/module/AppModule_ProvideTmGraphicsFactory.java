package app.fpt.ticketmobile.app.di.module;

import android.content.Context;

import javax.inject.Provider;

import dagger.internal.Factory;
import app.fpt.ticketmobile.app.lib.TmGraphics;

/**
 * Created by Administrator on 7/13/2016.
 */
public class AppModule_ProvideTmGraphicsFactory implements Factory<TmGraphics> {
    private static final boolean $assertionDisabled = !AppModule_ProvideTmGraphicsFactory.class.desiredAssertionStatus();
    private Provider<Context> contextProvider;
    private AppModule appModule;

    public AppModule_ProvideTmGraphicsFactory(AppModule appModule, Provider<Context> contextProvider) {
        if ($assertionDisabled || appModule != null) {
            this.appModule = appModule;
            if ($assertionDisabled || contextProvider != null) {
                this.contextProvider = contextProvider;
                return;
            }
            throw new AssertionError();
        }
        throw new AssertionError();
    }

    @Override
    public TmGraphics get() {
        TmGraphics provideTmGraphics = appModule.provideTmGraphics(contextProvider.get());
        if (provideTmGraphics != null) {
            return provideTmGraphics;
        }
        throw new NullPointerException("Cannot return null from a non-@Nullable @Provides method");
    }

    public static Factory<TmGraphics> create(AppModule appModule, Provider<Context> contextProvider){
        return  new AppModule_ProvideTmGraphicsFactory(appModule, contextProvider);
    }
}
