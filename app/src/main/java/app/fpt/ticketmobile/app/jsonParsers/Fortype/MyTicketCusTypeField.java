package app.fpt.ticketmobile.app.jsonParsers.Fortype;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import app.fpt.ticketmobile.app.model.MyTicketCusTypeObject;
import app.fpt.ticketmobile.app.model.SpinnerObject;

/**
 * Created by ToanMaster on 6/29/2016.
 */
public class MyTicketCusTypeField {

    public static final class Deserializer implements JsonDeserializer<MyTicketCusTypeObject>{

        @Override
        public MyTicketCusTypeObject deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            MyTicketCusTypeObject myTicketCusTypeObject = new MyTicketCusTypeObject();
            Gson gson = new Gson();
            List<SpinnerObject> spinnerObjectList = new ArrayList<>();

            try{
                JsonObject jsObj = json.getAsJsonObject();
                JsonObject jsonObject = jsObj.getAsJsonObject("Result");
                myTicketCusTypeObject = gson.fromJson(jsonObject, MyTicketCusTypeObject.class);

                if (myTicketCusTypeObject.getData() != null && myTicketCusTypeObject.getData().size() >  0) {
                    for (int i = 0; i < myTicketCusTypeObject.getData().size(); i++) {
                        MyTicketCusTypeObject.CusTypeObject cusTypeObject = myTicketCusTypeObject.getData().get(i);
                        SpinnerObject spinnerObject = new SpinnerObject(cusTypeObject.getID(), cusTypeObject.getDescription());
                        spinnerObjectList.add(spinnerObject);
                    }
                    myTicketCusTypeObject.setSpinnerObjectList(spinnerObjectList);
                }

            }catch(Exception ex){
                Log.e("tmt", "MyTicketCusTypeField , Deserializer : " + ex.toString());
            }
            return myTicketCusTypeObject;
        }
    }
}
