package app.fpt.ticketmobile.app.jsonParsers.Fortype;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import app.fpt.ticketmobile.app.model.MyDeviceObject;

/**
 * Created by ToanMaster on 6/27/2016.
 */
public abstract class MyDeviceField {

    public static final class Deserializer implements JsonDeserializer<MyDeviceObject>{

        @Override
        public MyDeviceObject deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            Gson gson = new Gson();
            MyDeviceObject myDeviceObject = null;

            try {
                final JsonObject jsObj = json.getAsJsonObject();
                JsonObject jsonObject = jsObj.getAsJsonObject("Result");
                myDeviceObject = gson.fromJson(jsonObject, MyDeviceObject.class);
            }
            catch (Exception ex){
                Log.e("tmt", "ex : " + ex.getMessage().toString());
            }

            return myDeviceObject;

        }
    }
}
