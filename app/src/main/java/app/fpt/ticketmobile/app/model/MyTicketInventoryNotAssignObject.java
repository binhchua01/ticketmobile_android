package app.fpt.ticketmobile.app.model;

import java.util.List;

import app.fpt.ticketmobile.app.ui.adapter.ticketinventorynotassign.TicketInventoryNotAssignHeader;

/**
 * Created by ToanMaster on 7/1/2016.
 */
public class MyTicketInventoryNotAssignObject {

    private List<TicketInventoryNotAssignHeader> mInventoryNotAssignHeaderList;
    private List<TicketInventoryNotAssign> Data;

    public List<TicketInventoryNotAssign> getData() {
        return Data;
    }

    public void setData(List<TicketInventoryNotAssign> data) {
        Data = data;
    }

    public List<TicketInventoryNotAssignHeader> getInventoryNotAssignHeaderList() {
        return mInventoryNotAssignHeaderList;
    }

    public void setInventoryNotAssignHeaderList(List<TicketInventoryNotAssignHeader> mInventoryNotAssignHeaderList) {
        this.mInventoryNotAssignHeaderList = mInventoryNotAssignHeaderList;
    }

    public class TicketInventoryNotAssign{
        private String CreatedDate;
        private String EstimatedTime;
        private String ExistTime;
        private String IssueName;
        private String Level;
        private String Queue;
        private String TicketID;
        private String Title;

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String createdDate) {
            CreatedDate = createdDate;
        }

        public String getEstimatedTime() {
            return EstimatedTime;
        }

        public void setEstimatedTime(String estimatedTime) {
            EstimatedTime = estimatedTime;
        }

        public String getExistTime() {
            return ExistTime;
        }

        public void setExistTime(String existTime) {
            ExistTime = existTime;
        }

        public String getIssueName() {
            return IssueName;
        }

        public void setIssueName(String issueName) {
            IssueName = issueName;
        }

        public String getLevel() {
            return Level;
        }

        public void setLevel(String level) {
            Level = level;
        }

        public String getQueue() {
            return Queue;
        }

        public void setQueue(String queue) {
            Queue = queue;
        }

        public String getTicketID() {
            return TicketID;
        }

        public void setTicketID(String ticketID) {
            TicketID = ticketID;
        }

        public String getTitle() {
            return Title;
        }

        public void setTitle(String title) {
            Title = title;
        }
    }
}
