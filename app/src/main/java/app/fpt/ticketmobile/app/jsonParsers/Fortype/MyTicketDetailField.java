package app.fpt.ticketmobile.app.jsonParsers.Fortype;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import app.fpt.ticketmobile.app.model.MyTicketDetailObject;

/**
 * Created by ToanMaster on 6/27/2016.
 */
public abstract class MyTicketDetailField {

    public static final class Deserializer implements JsonDeserializer<MyTicketDetailObject>{

        @Override
        public MyTicketDetailObject deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            Gson gson = new GsonBuilder().create();
            MyTicketDetailObject myTicketDetailObject = null;

            try {

                final JsonObject jsObj = json.getAsJsonObject();
                JsonObject jsonObject = jsObj.getAsJsonObject("Result");
                if (jsonObject.get("Data") instanceof JsonArray) {
                    JsonObject jsonUser = (JsonObject) jsonObject.getAsJsonArray("Data").get(0);
                    myTicketDetailObject = gson.fromJson(jsonUser, MyTicketDetailObject.class);
                } else{
                    myTicketDetailObject = gson.fromJson(jsonObject, MyTicketDetailObject.class);
                }
            }
            catch (Exception ex){
                Log.e("tmt", "ex : " + ex.getMessage().toString());
            }

            return myTicketDetailObject;

        }
    }
}
