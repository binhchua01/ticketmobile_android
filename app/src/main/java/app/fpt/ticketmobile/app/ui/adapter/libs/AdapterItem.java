package app.fpt.ticketmobile.app.ui.adapter.libs;

import java.util.regex.Pattern;

import app.fpt.ticketmobile.app.lib.Matcher;

/**
 * Created by Administrator on 7/12/2016.
 */
public class AdapterItem<T> implements Matchable {

    private T mItem;
    private Class<T> mItemClass;
    private Matcher<T> mMatcher;
    private int mViewType;

    public AdapterItem(int viewType, T item, Class<T> itemClass, Matcher<T> matcher){
        this.mViewType = viewType;
        this.mItem = item;
        this.mItemClass = itemClass;
        this.mMatcher = matcher;
    }


    public T getItem() {
        return mItem;
    }

    public Class<T> getItemClass() {
        return mItemClass;
    }

    public Matcher<T> getMatcher() {
        return mMatcher;
    }

    public int getViewType() {
        return mViewType;
    }

    public void setViewType(int mViewType) {
        this.mViewType = mViewType;
    }

    @Override
    public boolean matches(Pattern pattern) {
        return mMatcher != null && mMatcher.matches(mItem, pattern);
    }
}
