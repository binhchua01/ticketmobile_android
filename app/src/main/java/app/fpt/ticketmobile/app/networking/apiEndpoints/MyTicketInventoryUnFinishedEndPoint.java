package app.fpt.ticketmobile.app.networking.apiEndpoints;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;

import java.util.HashMap;
import java.util.Map;

import app.fpt.ticketmobile.app.model.MyTicketInventoryUnFinishObject;
import app.fpt.ticketmobile.app.networking.utils.GsonRequest;

/**
 * Created by ToanMaster on 6/23/2016.
 */
public abstract class MyTicketInventoryUnFinishedEndPoint {


    private static  Map<String, String> getHeader(String userId){
        Map<String, String>   params = new HashMap<String, String>();
        params.put("UserID", userId);
        return params;
    }

    public static GsonRequest<MyTicketInventoryUnFinishObject> getMyTicketInventory(final String userId, Response.Listener listener, Response.ErrorListener errorListener){
        GsonRequest<MyTicketInventoryUnFinishObject> gsonRequest = new GsonRequest<MyTicketInventoryUnFinishObject>(Constants.MY_GET_TICKET_INVENTORY_UNFINISHED_END_POINT, MyTicketInventoryUnFinishObject.class, listener, errorListener){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Log.d("tmt", "user id : " + userId);
                return getHeader(userId);
            }
        };
        return gsonRequest;
    }
}
