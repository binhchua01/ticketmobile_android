package app.fpt.ticketmobile.app.lib;

import com.android.volley.Request;
import com.android.volley.RequestQueue;

/**
 * Created by ToanMaster on 6/14/2016.
 */
public interface TmApiEndPointProvider {

    RequestQueue getVolleyRequestQueue();

    <T> void addRequest(Request<T> request);

    <T> void addRequest(Request<T> request, String tag);

    void cancelAllRequest(String tag);

}
