package app.fpt.ticketmobile.app.jsonParsers.Fortype;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import app.fpt.ticketmobile.app.model.MyTicketInventoryNotAssignObject;
import app.fpt.ticketmobile.app.ui.adapter.ticketinventorynotassign.TicketInventoryNotAssignChild;
import app.fpt.ticketmobile.app.ui.adapter.ticketinventorynotassign.TicketInventoryNotAssignHeader;

/**
 * Created by Administrator on 7/1/2016.
 */
public class MyTicketInventoryNotAssignField {

    public static final class Deserializer implements JsonDeserializer<MyTicketInventoryNotAssignObject>{

        @Override
        public MyTicketInventoryNotAssignObject deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            Gson gson = new Gson();
            MyTicketInventoryNotAssignObject myTicketInventoryNotAssignObject = null;
            List<TicketInventoryNotAssignHeader> mTicketInventoryNotAssignHeaderList = new ArrayList<>();

            try{

                JsonObject jsObj = json.getAsJsonObject();
                JsonObject jsResult = jsObj.getAsJsonObject("Result");
                myTicketInventoryNotAssignObject = gson.fromJson(jsResult, MyTicketInventoryNotAssignObject.class);

                if (myTicketInventoryNotAssignObject != null && myTicketInventoryNotAssignObject.getData().size() > 0) {
                    for (int i = 0; i < myTicketInventoryNotAssignObject.getData().size(); i++) {
                        boolean mCheck = true;
                        MyTicketInventoryNotAssignObject.TicketInventoryNotAssign ticketInventoryNotAssign = myTicketInventoryNotAssignObject.getData().get(i);
                        TicketInventoryNotAssignChild ticketInventoryNotAssignChild = new TicketInventoryNotAssignChild(ticketInventoryNotAssign);
                        if (mTicketInventoryNotAssignHeaderList.size() > 0) {
                            for (int j = 0; j < mTicketInventoryNotAssignHeaderList.size(); j++) {
                                if (mTicketInventoryNotAssignHeaderList.get(j).getQueueName().contains(ticketInventoryNotAssign.getQueue())) {
                                    mCheck = false;
                                    mTicketInventoryNotAssignHeaderList.get(j).getTicketInventoryNotAssignChildList().add(ticketInventoryNotAssignChild);
                                    break;
                                }
                            }

                            if (mCheck) {
                                List<TicketInventoryNotAssignChild> mTicketInventoryNotAssignChildList = new ArrayList<>();
                                mTicketInventoryNotAssignChildList.add(ticketInventoryNotAssignChild);
                                mTicketInventoryNotAssignHeaderList.add(new TicketInventoryNotAssignHeader(ticketInventoryNotAssign.getQueue(), mTicketInventoryNotAssignChildList));
                            }
                        } else {
                            List<TicketInventoryNotAssignChild> mTicketInventoryNotAssignChildList = new ArrayList<>();
                            mTicketInventoryNotAssignChildList.add(ticketInventoryNotAssignChild);
                            mTicketInventoryNotAssignHeaderList.add(new TicketInventoryNotAssignHeader(ticketInventoryNotAssign.getQueue(), mTicketInventoryNotAssignChildList));
                        }
                    }

                    myTicketInventoryNotAssignObject.setInventoryNotAssignHeaderList(mTicketInventoryNotAssignHeaderList);
                }

            }catch(Exception e){
                Log.e("tmt", "MyTicketInventoryNotAssignField : Deserializer " + e.toString());
            }
            return myTicketInventoryNotAssignObject;
        }
    }
}
