package app.fpt.ticketmobile.app.model;

import java.util.List;

import app.fpt.ticketmobile.app.ui.adapter.ticketofusercreated.TicketOfUserCreatedHeader;

/**
 * Created by Administrator on 7/1/2016.
 */
public class MyTicketOfUserCreatedObject {

    private List<TicketOfUserCreatedHeader> mTicketOfUserCreatedHeaderList;
    private List<TicketOfUserCreated> Data;

    public List<TicketOfUserCreated> getData() {
        return Data;
    }

    public void setData(List<TicketOfUserCreated> data) {
        Data = data;
    }

    public List<TicketOfUserCreatedHeader> getTicketOfMeCreatedHeaderList() {
        return mTicketOfUserCreatedHeaderList;
    }

    public void setTicketOfMeCreatedHeaderList(List<TicketOfUserCreatedHeader> mTicketOfUserCreatedHeaderList) {
        this.mTicketOfUserCreatedHeaderList = mTicketOfUserCreatedHeaderList;
    }

    public class TicketOfUserCreated{
        private String CreatedDate;
        private String EstimatedTime;
        private String Level;
        private String Queue;
        private String Staff;
        private String TicketID;
        private String TicketStatus;
        private String TimeWorked;
        private String Title;
        private String UpdateDate;

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String createdDate) {
            CreatedDate = createdDate;
        }

        public String getEstimatedTime() {
            return EstimatedTime;
        }

        public void setEstimatedTime(String estimatedTime) {
            EstimatedTime = estimatedTime;
        }

        public String getLevel() {
            return Level;
        }

        public void setLevel(String level) {
            Level = level;
        }

        public String getQueue() {
            return Queue;
        }

        public void setQueue(String queue) {
            Queue = queue;
        }

        public String getStaff() {
            return Staff;
        }

        public void setStaff(String staff) {
            Staff = staff;
        }

        public String getTicketID() {
            return TicketID;
        }

        public void setTicketID(String ticketID) {
            TicketID = ticketID;
        }

        public String getTicketStatus() {
            return TicketStatus;
        }

        public void setTicketStatus(String ticketStatus) {
            TicketStatus = ticketStatus;
        }

        public String getTimeWorked() {
            return TimeWorked;
        }

        public void setTimeWorked(String timeWorked) {
            TimeWorked = timeWorked;
        }

        public String getTitle() {
            return Title;
        }

        public void setTitle(String title) {
            Title = title;
        }

        public String getUpdateDate() {
            return UpdateDate;
        }

        public void setUpdateDate(String updateDate) {
            UpdateDate = updateDate;
        }
    }
}
