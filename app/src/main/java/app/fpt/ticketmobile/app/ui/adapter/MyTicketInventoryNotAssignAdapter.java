package app.fpt.ticketmobile.app.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapter;
import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;

import java.util.List;

import app.fpt.ticketmobile.app.ui.adapter.ticketinventorynotassign.TicketInventoryNotAssignChild;
import app.fpt.ticketmobile.app.ui.adapter.ticketinventorynotassign.TicketInventoryNotAssignHeaderViewHolder;
import app.fpt.ticketmobile.R;
import app.fpt.ticketmobile.app.ui.adapter.ticketinventorynotassign.TicketInventoryNotAssignChildViewHolder;
import app.fpt.ticketmobile.app.ui.adapter.ticketinventorynotassign.TicketInventoryNotAssignHeader;

/**
 * Created by Administrator on 7/1/2016.
 */
public class MyTicketInventoryNotAssignAdapter extends ExpandableRecyclerAdapter<TicketInventoryNotAssignHeaderViewHolder, TicketInventoryNotAssignChildViewHolder>{

    private LayoutInflater mInflater;
    private List<? extends ParentListItem> parentListItems;

    public MyTicketInventoryNotAssignAdapter(Context context, @NonNull List<? extends ParentListItem> parentItemList) {
        super(parentItemList);
        parentListItems = parentItemList;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public TicketInventoryNotAssignHeaderViewHolder onCreateParentViewHolder(ViewGroup parentViewGroup) {
        View headerView = mInflater.inflate(R.layout.entry_ticket_inventory_not_assign_header, parentViewGroup, false);
        return new TicketInventoryNotAssignHeaderViewHolder(headerView);
    }

    @Override
    public TicketInventoryNotAssignChildViewHolder onCreateChildViewHolder(ViewGroup childViewGroup) {
        View childView = mInflater.inflate(R.layout.entry_ticket_inventory_not_assign_child, childViewGroup, false);
        return new TicketInventoryNotAssignChildViewHolder(childView);
    }

    @Override
    public void onBindParentViewHolder(TicketInventoryNotAssignHeaderViewHolder parentViewHolder, int position, ParentListItem parentListItem) {
        TicketInventoryNotAssignHeader inventoryHeader = (TicketInventoryNotAssignHeader) parentListItem;
        parentViewHolder.bind(inventoryHeader);
    }

    @Override
    public void onBindChildViewHolder(TicketInventoryNotAssignChildViewHolder childViewHolder, int position, Object childListItem) {
        TicketInventoryNotAssignChild inventoryChild = (TicketInventoryNotAssignChild) childListItem;
        childViewHolder.bind(inventoryChild);
    }

    public void clear() {
        parentListItems.clear();
        notifyDataSetChanged();
    }

    // Add a list of items
    public void addAll(List<? extends  ParentListItem> list) {
        parentListItems = list;
        notifyDataSetChanged();
    }
}
