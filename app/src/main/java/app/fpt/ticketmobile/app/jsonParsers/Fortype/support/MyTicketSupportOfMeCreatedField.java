package app.fpt.ticketmobile.app.jsonParsers.Fortype.support;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import app.fpt.ticketmobile.app.model.support.MyTicketSupportOfUserCreatedObject;
import app.fpt.ticketmobile.app.ui.adapter.support.TicketSupportOfMeCreatedChild;
import app.fpt.ticketmobile.app.ui.adapter.support.TicketSupportOfMeCreatedHeader;


/**
 * Created by Administrator on 7/25/2016.
 */
public class MyTicketSupportOfMeCreatedField {

    public static final class Deserializer implements JsonDeserializer<MyTicketSupportOfUserCreatedObject> {

        @Override
        public MyTicketSupportOfUserCreatedObject deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

            Gson gson = new Gson();
            MyTicketSupportOfUserCreatedObject myTicketOfUserCreatedObject = null;
            List<TicketSupportOfMeCreatedHeader> mTicketOfUserCreatedHeaderList = new ArrayList<>();

            try{

                JsonObject jsObj = json.getAsJsonObject();
                JsonObject jsResult = jsObj.getAsJsonObject("Result");
                myTicketOfUserCreatedObject = gson.fromJson(jsResult, MyTicketSupportOfUserCreatedObject.class);
                if (myTicketOfUserCreatedObject != null && myTicketOfUserCreatedObject.getData().size() > 0) {
                    for (int i = 0; i < myTicketOfUserCreatedObject.getData().size(); i++) {
                        boolean mCheck = true;
                        MyTicketSupportOfUserCreatedObject.TicketSupportOfUserCreated ticketOfUserCreated = myTicketOfUserCreatedObject.getData().get(i);
                        TicketSupportOfMeCreatedChild ticketOfUserCreatedChild = new TicketSupportOfMeCreatedChild(ticketOfUserCreated);
                        if (mTicketOfUserCreatedHeaderList.size() > 0) {
                            for (int j = 0; j < mTicketOfUserCreatedHeaderList.size(); j++) {
                                if (mTicketOfUserCreatedHeaderList.get(j).getQueueName().contains(ticketOfUserCreated.getQueue())) {
                                    mCheck = false;
                                    mTicketOfUserCreatedHeaderList.get(j).getTicketOfUserCreatedChildList().add(ticketOfUserCreatedChild);
                                    break;
                                }
                            }

                            if (mCheck) {
                                List<TicketSupportOfMeCreatedChild> mTicketSupportOfMeCreatedChildList = new ArrayList<>();
                                mTicketSupportOfMeCreatedChildList.add(ticketOfUserCreatedChild);
                                mTicketOfUserCreatedHeaderList.add(new TicketSupportOfMeCreatedHeader(ticketOfUserCreated.getQueue(), mTicketSupportOfMeCreatedChildList));
                            }
                        } else {
                            List<TicketSupportOfMeCreatedChild> mTicketSupportOfMeCreatedChildList = new ArrayList<>();
                            mTicketSupportOfMeCreatedChildList.add(ticketOfUserCreatedChild);
                            mTicketOfUserCreatedHeaderList.add(new TicketSupportOfMeCreatedHeader(ticketOfUserCreated.getQueue(), mTicketSupportOfMeCreatedChildList));
                        }
                    }

                    myTicketOfUserCreatedObject.setTicketSupportOfUserCreatedHeaderList(mTicketOfUserCreatedHeaderList);
                }

            }catch(Exception e){
                Log.e("tmt", "MyTicketInventoryNotAssignField : Deserializer " + e.toString());
            }
            return myTicketOfUserCreatedObject;
        }
    }
}
