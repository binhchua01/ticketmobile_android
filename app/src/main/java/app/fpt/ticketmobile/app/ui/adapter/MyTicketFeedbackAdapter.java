package app.fpt.ticketmobile.app.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import app.fpt.ticketmobile.app.model.MyTicketFeedbackObject;
import app.fpt.ticketmobile.R;
import app.fpt.ticketmobile.app.eventBus.EventBus;
import app.fpt.ticketmobile.app.eventBus.event.TicketFeedbackDetailEvent;

/**
 * Created by Administrator on 7/4/2016.
 */
public class MyTicketFeedbackAdapter extends RecyclerView.Adapter<MyTicketFeedbackAdapter.MyTicketFeedbackViewHolder> {

    private List<MyTicketFeedbackObject.FeedbackObject> mFeedbackObjectList = null;
    private EventBus eventBus;

    public MyTicketFeedbackAdapter(EventBus eventBus , List<MyTicketFeedbackObject.FeedbackObject> mFeedbackObjectList){
        this.eventBus = eventBus;
        this.mFeedbackObjectList = mFeedbackObjectList;
        eventBus.register(this);
    }

    private class PrivateFeedbackOnListener implements View.OnClickListener{
        private MyTicketFeedbackObject.FeedbackObject mFeedbackObject;

        public PrivateFeedbackOnListener(MyTicketFeedbackObject.FeedbackObject feedbackObject){
            this.mFeedbackObject = feedbackObject;
        }

        @Override
        public void onClick(View v) {
           // v.getContext().startActivity(TicketFeedBackDetailActivity.buildIntent(v.getContext(), String.valueOf(mFeedbackObject.getID().intValue())));
            eventBus.post(new TicketFeedbackDetailEvent(String.valueOf(mFeedbackObject.getID().intValue())));
        }
    }

    public class MyTicketFeedbackViewHolder extends RecyclerView.ViewHolder {

        private TextView tvTicketResponseTitle;
        private TextView tvTicketResponseUser;
        private TextView tvTicketResponseTime;
        private View view;

        public MyTicketFeedbackViewHolder(View parent) {
            super(parent);
            view = parent;
            tvTicketResponseTitle = (TextView) parent.findViewById(R.id.tm_tv_ticket_response_code);
            tvTicketResponseUser = (TextView) parent.findViewById(R.id.tm_tv_ticket_response_user);
            tvTicketResponseTime = (TextView) parent.findViewById(R.id.tm_tv_ticket_response_time);

        }

        public void blind(MyTicketFeedbackObject.FeedbackObject feedbackObject) {
            view.setOnClickListener(new PrivateFeedbackOnListener(feedbackObject));
            tvTicketResponseTitle.setText(feedbackObject.getSubject());
            tvTicketResponseUser.setText(feedbackObject.getFromAdr());
            tvTicketResponseTime.setText(feedbackObject.getCreatedDate());
        }
    }

    @Override
    public MyTicketFeedbackViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.entry_ticket_feedback, parent, false);
        return new MyTicketFeedbackViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyTicketFeedbackViewHolder holder, int position) {
        MyTicketFeedbackObject.FeedbackObject feedbackObject = mFeedbackObjectList.get(position);
        holder.blind(feedbackObject);
    }

    @Override
    public int getItemCount() {
        return mFeedbackObjectList.size();
    }


}
