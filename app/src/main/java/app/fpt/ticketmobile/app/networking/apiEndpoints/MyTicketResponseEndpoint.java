package app.fpt.ticketmobile.app.networking.apiEndpoints;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;

import java.util.HashMap;
import java.util.Map;

import app.fpt.ticketmobile.app.model.MyResponseObject;
import app.fpt.ticketmobile.app.model.MyTicketFeedbackDetailObject;
import app.fpt.ticketmobile.app.model.MyTicketFeedbackObject;
import app.fpt.ticketmobile.app.networking.utils.GsonRequest;

/**
 * Created by Administrator on 7/4/2016.
 */
public class MyTicketResponseEndpoint {


    private static Map<String, String> getHeaderResponseList(String ticketId){
        Map<String, String>   params = new HashMap<String, String>();
        params.put("TicketID", ticketId);
        return params;
    }

    private static Map<String, String> getHeaderResponseDetail(String ticketResponseId){
        Map<String, String>   params = new HashMap<String, String>();
        params.put("ID", ticketResponseId);
        return params;
    }

    private static Map<String, String> getHeaderResponseSend(String ticketId, String updatedBy, String updatedEmail, MyTicketFeedbackDetailObject ticketFeedbackDetailObject){
        Map<String, String>   params = new HashMap<String, String>();
        params.put("Subject", ticketFeedbackDetailObject.getSubject());
        params.put("FromAdr", ticketFeedbackDetailObject.getFromAdr());
        params.put("ToAdr", ticketFeedbackDetailObject.getToAdr());
        params.put("Cc", ticketFeedbackDetailObject.getCc());
        params.put("QueueAdr", ticketFeedbackDetailObject.getQueueAdr());
        params.put("BodyHTML", ticketFeedbackDetailObject.getBodyHTML());

        params.put("UpdatedBy", updatedBy);
        params.put("UpdatedEmail", updatedEmail);
        params.put("TicketID", ticketId);

        return params;
    }

    public static GsonRequest<MyTicketFeedbackObject> getMyTicketFeedbackList(final String ticketId, Response.Listener<MyTicketFeedbackObject> listener, Response.ErrorListener errorListener){
        GsonRequest<MyTicketFeedbackObject> gsonRequest = new GsonRequest<MyTicketFeedbackObject>(Constants.MY_GET_RESPONSE_INFO, MyTicketFeedbackObject.class, listener, errorListener){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return getHeaderResponseList(ticketId);
            }
        };

        return gsonRequest;
    }


    public static GsonRequest<MyTicketFeedbackDetailObject> getMyTicketFeedbackDetailList(final String ticketResponseId, Response.Listener<MyTicketFeedbackDetailObject> listener, Response.ErrorListener errorListener){
        GsonRequest<MyTicketFeedbackDetailObject> gsonRequest = new GsonRequest<MyTicketFeedbackDetailObject>(Constants.MY_GET_RESPONSE_DETAIL, MyTicketFeedbackDetailObject.class, listener, errorListener){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Log.d("tmt", "ticketResponseId : " + ticketResponseId);
                return getHeaderResponseDetail(ticketResponseId);
            }
        };

        return gsonRequest;
    }

    public static GsonRequest<MyResponseObject> MyTicketResponseUpdated(final String ticketId, final String updatedBy, final String updatedEmail,
                                                                        final MyTicketFeedbackDetailObject ticketFeedbackDetailObject, Response.Listener<MyResponseObject> listener, Response.ErrorListener errorListener){
        GsonRequest<MyResponseObject> request = new GsonRequest<MyResponseObject>(Constants.MY_UPDATE_RESPONSE_SEND, MyResponseObject.class, listener, errorListener){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return getHeaderResponseSend(ticketId, updatedBy, updatedEmail, ticketFeedbackDetailObject);
            }
        };

        return request;
    }




}
