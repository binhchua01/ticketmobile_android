package app.fpt.ticketmobile.app.ui.adapter.libs;

import app.fpt.ticketmobile.app.ui.lib.TicketPrimaryFunction;

/**
 * Created by Administrator on 7/12/2016.
 */
public class GroupTicketPrimaryFunctionInfo {
    private final TicketPrimaryFunction mTicketPrimaryFunction;
    private final String mTicketPrimaryFunctionTitle;

    public GroupTicketPrimaryFunctionInfo(TicketPrimaryFunction mTicketPrimaryFunction, String mTicketPrimaryFunctionTitle) {
        this.mTicketPrimaryFunction = mTicketPrimaryFunction;
        this.mTicketPrimaryFunctionTitle = mTicketPrimaryFunctionTitle;
    }

    public TicketPrimaryFunction getTicketPrimaryFunction() {
        return mTicketPrimaryFunction;
    }

    public String getTicketPrimaryFunctionTitle() {
        return mTicketPrimaryFunctionTitle;
    }
}
