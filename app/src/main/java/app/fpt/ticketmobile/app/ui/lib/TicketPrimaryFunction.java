package app.fpt.ticketmobile.app.ui.lib;

/**
 * Created by Administrator on 7/12/2016.
 */
public enum TicketPrimaryFunction {
    TICKET_OF_USER,
    TICKET_OF_USER_CREATE,
    TICKET_INVENTORY_NOT_ASSIGN,
    TICKET_INVENTORY_UNFINISHED
}
