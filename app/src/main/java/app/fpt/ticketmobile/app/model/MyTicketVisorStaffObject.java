package app.fpt.ticketmobile.app.model;

import java.util.List;

/**
 * Created by Administrator on 6/29/2016.
 */
public class MyTicketVisorStaffObject {

    private List<SpinnerObject> mSpinnerObjectList;
    private List<VisorStaffObject> Data;

    public List<VisorStaffObject> getData() {
        return Data;
    }

    public void setData(List<VisorStaffObject> data) {
        Data = data;
    }

    public List<SpinnerObject> getSpinnerObjectList() {
        return mSpinnerObjectList;
    }

    public void setSpinnerObjectList(List<SpinnerObject> mSpinnerObjectList) {
        this.mSpinnerObjectList = mSpinnerObjectList;
    }

    public class VisorStaffObject{
        private String Email;
        private String Name;

        public String getEmail() {
            return Email;
        }

        public void setEmail(String email) {
            Email = email;
        }

        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }
    }
}
