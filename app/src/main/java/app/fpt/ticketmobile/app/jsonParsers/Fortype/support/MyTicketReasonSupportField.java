package app.fpt.ticketmobile.app.jsonParsers.Fortype.support;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import app.fpt.ticketmobile.app.model.SpinnerObject;
import app.fpt.ticketmobile.app.model.support.MyTicketReasonSupportObject;

/**
 * Created by Administrator on 7/7/2016.
 */
public class MyTicketReasonSupportField {

    public static final class Deserializer implements JsonDeserializer<MyTicketReasonSupportObject>{

        @Override
        public MyTicketReasonSupportObject deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            Gson gson = new Gson();
            MyTicketReasonSupportObject ticketReasonSupportObject = null;
            List<SpinnerObject> spinnerObjectList = new ArrayList<>();

            try{
                JsonObject jsResult = json.getAsJsonObject();
                if (jsResult.has("Result")){
                    jsResult = jsResult.getAsJsonObject("Result");
                    if (jsResult.has("Data")){
                        if (jsResult.get("Data").isJsonArray()) {
                            ticketReasonSupportObject = gson.fromJson(jsResult, MyTicketReasonSupportObject.class);

                            if (ticketReasonSupportObject.getReasonList().size() > 0){
                                for (int i = 0; i < ticketReasonSupportObject.getReasonList().size(); i++){
                                    MyTicketReasonSupportObject.ReasonObject reasonObject = ticketReasonSupportObject.getReasonList().get(i);
                                    SpinnerObject spinnerObject = new SpinnerObject(String.valueOf(reasonObject.getID().intValue()), reasonObject.getDescription());
                                    spinnerObjectList.add(spinnerObject);
                                }
                                ticketReasonSupportObject.setSpinnerObjectList(spinnerObjectList);
                            }
                        }
                    }
                }

            }catch (Exception e){
                Log.e("tmt", "MyTicketBusinessAreaField : Deserializer : " + e.toString());
            }

            return ticketReasonSupportObject;
        }
    }
}
