package app.fpt.ticketmobile.app.ui.activities.authenticatedActivities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

//import com.roughike.bottombar.BottomBar;

import app.fpt.ticketmobile.app.eventBus.event.TicketDetailFeedBackEvent;
import app.fpt.ticketmobile.app.eventBus.event.TicketDetailSaveEvent;
import app.fpt.ticketmobile.app.ui.activities.TmBaseActivity;
import app.fpt.ticketmobile.app.ui.fragments.authenticatedFragment.TicketDetailFragment;
import app.fpt.ticketmobile.R;

/**
 * Created by ToanMaster on 6/24/2016.
 */
public class TicketDetailActivity extends TmBaseActivity implements View.OnClickListener {

    private static final String EXTRA_FRAGMENT_TICKET_DETAIL_TAG = "extra_fragment_ticket_detail_tag";
    private static final String EXTRA_TICKET_CODE_INTENT_KEY = "extra_ticket_code_intent_key";

//    private BottomBar bottomBar = null;

    public TicketDetailActivity() {
        super(Integer.valueOf(R.layout.activity_ticket_detail));
    }

    public static Intent buildIntent(Context context, String ticketID ){
        Intent intent = new Intent(context, TicketDetailActivity.class);
        intent.putExtra(EXTRA_TICKET_CODE_INTENT_KEY, ticketID);
        return intent;
    }



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        bottomBar = BottomBar.attach(this, savedInstanceState);
//        bottomBar.setItemsFromMenu(R.menu.bottom_bar_menu, new OnMenuTabSelectedListener() {
//            @Override
//            public void onMenuItemSelected(@IdRes int menuItemId) {
//                switch (menuItemId){
//                    case R.id.tm_ticket_save:
//                        getEventBus().post(new TicketDetailSaveEvent());
//                        break;
//
//                    case R.id.tm_ticket_feedback:
//                        getEventBus().postOnUiThread(new TicketDetailFeedBackEvent());
//                        break;
//                }
//            }
//        });
//        bottomBar.setActiveTabColor(ContextCompat.getColor(this, R.color.tm_green_color));
//        bottomBar.mapColorForTab(0, ContextCompat.getColor(this, R.color.colorAccent));
//        bottomBar.mapColorForTab(1, ContextCompat.getColor(this, R.color.tm_green_color));
//        bottomBar.useDarkTheme(true);

     //   bottomBar.setBackgroundColor(getResources().getColor(R.color.tm_green_color));

        setupToolbarBottomViews();
        setupIfFragmentIsNeed();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
//        bottomBar.onSaveInstanceState(outState);
    }

    private void setupToolbarBottomViews(){
        Toolbar toolbarBottom = (Toolbar) findViewById(R.id.tm_toolbar_app_bottom);
        Button btnSaveTicketDetail = (Button) toolbarBottom.findViewById(R.id.tm_ticket_save);
        Button btnFeedbackTicketDetail = (Button) toolbarBottom.findViewById(R.id.tm_ticket_feedback);
        btnSaveTicketDetail.setOnClickListener(this);
        btnFeedbackTicketDetail.setOnClickListener(this);
        //check allow updata
        if(getTmSharePreferencesUtils().getUserLogin().getUpdate() != 1)
        {
            btnSaveTicketDetail.setEnabled(false);
        }
    }

    private void setupIfFragmentIsNeed() {
        String ticketId = null;
        if (getIntent() != null){
            ticketId = getIntent().getStringExtra(EXTRA_TICKET_CODE_INTENT_KEY);
        }
        TicketDetailFragment ticketDetailFragment = (TicketDetailFragment) getSupportFragmentManager().findFragmentByTag(EXTRA_FRAGMENT_TICKET_DETAIL_TAG);
        if (ticketDetailFragment == null && ticketId != null){
            ticketDetailFragment = TicketDetailFragment.getInstances(ticketId);
        }
        getSupportFragmentManager().beginTransaction().replace(R.id.content_fragment, ticketDetailFragment, EXTRA_FRAGMENT_TICKET_DETAIL_TAG).commit();

    }

    @Override
    protected void setupToolbar(Toolbar mToolbar) {
        super.setupToolbar(mToolbar);
        ((TextView) mToolbar.findViewById(R.id.main_toolbar_title)).setText(getString(R.string.tm_ticket_detail));;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tm_ticket_save:
                getEventBus().post(new TicketDetailSaveEvent());
                break;

            case R.id.tm_ticket_feedback:
                getEventBus().post(new TicketDetailFeedBackEvent());
                break;
        }
    }


    //    @Produce
//    public TicketDetailSaveEvent onPostSaveTicketDetail(){
//        return new TicketDetailSaveEvent();
//    }

}
