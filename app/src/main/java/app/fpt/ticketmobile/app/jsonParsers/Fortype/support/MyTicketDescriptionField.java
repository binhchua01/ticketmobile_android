package app.fpt.ticketmobile.app.jsonParsers.Fortype.support;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import app.fpt.ticketmobile.app.model.SpinnerObject;
import app.fpt.ticketmobile.app.model.support.MyTicketDescriptionObject;

/**
 * Created by Administrator on 7/7/2016.
 */
public class MyTicketDescriptionField {

    public static final class Deserializer implements JsonDeserializer<MyTicketDescriptionObject>{

        @Override
        public MyTicketDescriptionObject deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            Gson gson = new Gson();
            MyTicketDescriptionObject ticketDescriptionObject = null;
            List<SpinnerObject> spinnerObjectList = new ArrayList<>();

            try{
                JsonObject jsResult = json.getAsJsonObject();
                if (jsResult.has("Result")){
                    jsResult = jsResult.getAsJsonObject("Result");
                    if (jsResult.has("Data")){
                        if (jsResult.get("Data").isJsonArray()) {
                            ticketDescriptionObject = gson.fromJson(jsResult, MyTicketDescriptionObject.class);

                            if (ticketDescriptionObject.getDescriptionList().size() > 0){
                                for (int i = 0; i < ticketDescriptionObject.getDescriptionList().size(); i++){
                                    MyTicketDescriptionObject.DescriptionObject descriptionObject = ticketDescriptionObject.getDescriptionList().get(i);
                                    SpinnerObject spinnerObject = new SpinnerObject(String.valueOf(descriptionObject.getID().intValue()), descriptionObject.getDescription());
                                    spinnerObjectList.add(spinnerObject);
                                }
                                ticketDescriptionObject.setSpinnerObjectList(spinnerObjectList);
                            }
                        }
                    }
                }

            }catch (Exception e){
                Log.e("tmt", "MyTicketBusinessAreaField : Deserializer : " + e.toString());
            }

            return ticketDescriptionObject;
        }
    }

}
