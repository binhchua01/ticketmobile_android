package app.fpt.ticketmobile.app.eventBus.event;

import app.fpt.ticketmobile.app.ui.lib.TicketSupport;
import app.fpt.ticketmobile.app.eventBus.Event;

/**
 * Created by Administrator on 7/28/2016.
 */
public class DashbroadChangeItemBackground implements Event {
    private TicketSupport mTicketSupport;
    private String mTitle;

    public TicketSupport getTicketSupport() {
        return mTicketSupport;
    }

    public void setTicketSupport(TicketSupport mTicketSupport) {
        this.mTicketSupport = mTicketSupport;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public DashbroadChangeItemBackground(TicketSupport mTicketSupport, String mTitle) {
        this.mTicketSupport = mTicketSupport;
        this.mTitle = mTitle;
    }
}
