package app.fpt.ticketmobile.app.jsonParsers.Fortype;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

import app.fpt.ticketmobile.app.model.User;

/**
 * Created by ToanMaster on 6/15/2016.
 *
 */
public abstract class UserField {

    public static final class Deserializer implements JsonDeserializer<User> {

        @Override
        public User deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            User user = new User();
            Gson gson = new GsonBuilder().create();
            try {

                final JsonObject jsObj = json.getAsJsonObject();
                JsonObject jsonObject = jsObj.getAsJsonObject("Result");
                if (jsonObject.get("Data") instanceof JsonArray) {
                    JsonObject jsonUser = (JsonObject) jsonObject.getAsJsonArray("Data").get(0);
                    user = gson.fromJson(jsonUser, User.class);
                } else{
                    user = gson.fromJson(jsonObject, User.class);
                }
            }
            catch (Exception ex){
                Log.e("tmt", "ex : " + ex.getMessage().toString());
            }
            return user;
        }
    }


    public static final class Serializer implements JsonSerializer<User>{

        @Override
        public JsonElement serialize(User user, Type typeOfSrc, JsonSerializationContext context) {
            return null;
        }
    }
}
