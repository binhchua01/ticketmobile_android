package app.fpt.ticketmobile.app.jsonParsers.Fortype;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import app.fpt.ticketmobile.app.model.MyTicketReasonDetailObject;
import app.fpt.ticketmobile.app.model.SpinnerObject;

/**
 * Created by Administrator on 6/28/2016.
 */
public class MyTicketReasonDetailField {

    public static final class Deserializer implements JsonDeserializer<MyTicketReasonDetailObject> {

        @Override
        public MyTicketReasonDetailObject deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            Gson gson = new Gson();
            MyTicketReasonDetailObject myTicketReasonDetailObject = new MyTicketReasonDetailObject();
            List<SpinnerObject> spinnerObjectList = new ArrayList<>();

            try{
                JsonObject jsObj = json.getAsJsonObject();
                JsonObject jsonObject = jsObj.getAsJsonObject("Result");

                myTicketReasonDetailObject = gson.fromJson(jsonObject, MyTicketReasonDetailObject.class);

                if (myTicketReasonDetailObject.getData() != null && myTicketReasonDetailObject.getData().size() > 0) {
                    for (int i = 0; i < myTicketReasonDetailObject.getData().size(); i++) {
                        MyTicketReasonDetailObject.ReasonDetailObject reasonDetailObject = myTicketReasonDetailObject.getData().get(i);
                        SpinnerObject spinnerObject = new SpinnerObject(reasonDetailObject.getReasonDetaiID(), reasonDetailObject.getReasonDetaiName());
                        spinnerObjectList.add(spinnerObject);
                    }

                    myTicketReasonDetailObject.setSpinnerObjectList(spinnerObjectList);
                }

            }catch (Exception e){
                Log.e("tmt", "MyTicketReasonDetailField : " + e.toString());
            }

            return myTicketReasonDetailObject;
        }
    }

}
