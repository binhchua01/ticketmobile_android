package app.fpt.ticketmobile.app.model.support;

import java.util.List;

import app.fpt.ticketmobile.app.model.SpinnerObject;

/**
 * Created by Administrator on 7/22/2016.
 */
public class MyOperateStaffSupportObject {

    private List<OperateStaffSupportObject> Data;
    private List<SpinnerObject> mSpinnerObjectList;

    public List<OperateStaffSupportObject> getOperateStaffSupportList() {
        return Data;
    }

    public void setOperateStaffSupportList (List<OperateStaffSupportObject> data) {
        Data = data;
    }

    public List<SpinnerObject> getSpinnerObjectList() {
        return mSpinnerObjectList;
    }

    public void setSpinnerObjectList(List<SpinnerObject> mSpinnerObjectList) {
        this.mSpinnerObjectList = mSpinnerObjectList;
    }

    public class OperateStaffSupportObject {
        private String Email;
        private Integer ID;
        private String Name;

        public String getEmail() {
            return Email;
        }

        public void setEmail(String email) {
            Email = email;
        }

        public Integer getID() {
            return ID;
        }

        public void setID(Integer ID) {
            this.ID = ID;
        }

        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }
    }
}
