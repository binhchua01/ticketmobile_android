package app.fpt.ticketmobile.app.model;

import java.util.List;

/**
 * Created by Administrator on 6/28/2016.
 */
public class MyTicketReasonObject {

    private List<SpinnerObject> mSpinnerObjectList;
    private List<ReasonObject> Data;

    public List<ReasonObject> getData() {
        return Data;
    }

    public List<SpinnerObject> getSpinnerObjectList() {
        return mSpinnerObjectList;
    }

    public void setSpinnerObjectList(List<SpinnerObject> mSpinnerObjectList) {
        this.mSpinnerObjectList = mSpinnerObjectList;
    }

    public class ReasonObject{
        private String ReasonID;
        private String ReasonName;

        public String getReasonID() {
            return ReasonID;
        }

        public void setReasonID(String reasonID) {
            ReasonID = reasonID;
        }

        public String getReasonName() {
            return ReasonName;
        }

        public void setReasonName(String reasonName) {
            ReasonName = reasonName;
        }
    }
}
