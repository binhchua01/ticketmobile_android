package app.fpt.ticketmobile.app.di.module.internal;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.StandardExceptionParser;
import com.google.android.gms.analytics.Tracker;

import android.content.Context;

import app.fpt.ticketmobile.app.tracker.AnalyticsTrackers;
import app.fpt.ticketmobile.app.tracker.interfaces.TmTrackerAnalytics;

public class TmTrackerAnalyticsImpl implements TmTrackerAnalytics {

    private Context mContext;

    public TmTrackerAnalyticsImpl(Context context){
        this.mContext = context;
    }

    @Override
    public void trackScreenView(String screenName) {
        Tracker t = getGoogleAnalyticsTracker();
        t.setScreenName(screenName);
        t.send(new HitBuilders.ScreenViewBuilder().build());
        GoogleAnalytics.getInstance(mContext).dispatchLocalHits();

    }

    @Override
    public void trackEvent(String category, String action, String label) {
        Tracker t = getGoogleAnalyticsTracker();
        t.send(new HitBuilders.EventBuilder().setCategory(category).setAction(action).setLabel(label).build());

    }

    @Override
    public void trackException(Exception e) {
        if (e != null) {
            Tracker t = getGoogleAnalyticsTracker();

            t.send(new HitBuilders.ExceptionBuilder().setDescription(
                    new StandardExceptionParser(mContext, null).getDescription(Thread.currentThread().getName(), e)).setFatal(false).build());
        }

    }


    public synchronized Tracker getGoogleAnalyticsTracker() {
        AnalyticsTrackers analyticsTrackers = AnalyticsTrackers.getInstance();
        return analyticsTrackers.get(AnalyticsTrackers.Target.APP);
    }

}