package app.fpt.ticketmobile.app.model.support;

import java.util.List;

import app.fpt.ticketmobile.app.model.SpinnerObject;

/**
 * Created by Administrator on 7/7/2016.
 */
public class MyTicketBusinessAreaObject {

    private List<TicketBusinessAreaObject> Data;
    private List<SpinnerObject> mSpinnerObjectList;

    public List<TicketBusinessAreaObject> getBusinessAreaList() {
        return Data;
    }

    public void setBusinessAreaList(List<TicketBusinessAreaObject> data) {
        Data = data;
    }

    public List<SpinnerObject> getSpinnerObjectList() {
        return mSpinnerObjectList;
    }

    public void setSpinnerObjectList(List<SpinnerObject> mSpinnerObjectList) {
        this.mSpinnerObjectList = mSpinnerObjectList;
    }

    public class TicketBusinessAreaObject{
        private Integer LocationID;
        private String Description;

        public String getDescription() {
            return Description;
        }

        public void setDescription(String description) {
            Description = description;
        }

        public Integer getLocationID() {
            return LocationID;
        }

        public void setLocationID(Integer locationID) {
            LocationID = locationID;
        }
    }
}
