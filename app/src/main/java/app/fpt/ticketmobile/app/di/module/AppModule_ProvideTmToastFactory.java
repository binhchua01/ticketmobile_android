package app.fpt.ticketmobile.app.di.module;

import app.fpt.ticketmobile.app.ui.lib.TmToast;
import dagger.internal.Factory;

/**
 * Created by ToanMaster on 6/14/2016.
 */
public class AppModule_ProvideTmToastFactory implements Factory<TmToast> {

    private static final boolean $assertionDisabled = !AppModule_ProvideTmToastFactory.class.desiredAssertionStatus();
    private AppModule appModule;

    public AppModule_ProvideTmToastFactory(AppModule appModule){

        if ($assertionDisabled || appModule != null){
            this.appModule = appModule;
            return;
        }
        throw new AssertionError();
    }


    @Override
    public TmToast get() {
        TmToast provideTmToast = appModule.provideTmToast();
        if (provideTmToast != null) {
            return provideTmToast;
        }
        throw new NullPointerException("Cannot return null from a non-@Nullable @Provides method");
    }

    public static Factory<TmToast> create(AppModule appModule){
        return new AppModule_ProvideTmToastFactory(appModule);
    }
}

