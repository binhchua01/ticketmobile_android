package app.fpt.ticketmobile.app.jsonParsers.Fortype;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import app.fpt.ticketmobile.app.model.MyTicketReasonObject;
import app.fpt.ticketmobile.app.model.SpinnerObject;

/**
 * Created by Administrator on 6/28/2016.
 */
public class MyTicketReasonField {

    public static final class Deserializer implements JsonDeserializer<MyTicketReasonObject> {

        @Override
        public MyTicketReasonObject deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            Gson gson = new Gson();
            MyTicketReasonObject ticketReasonObject = new MyTicketReasonObject();
            List<SpinnerObject> spinnerObjectList = new ArrayList<>();

            try{
                JsonObject jsObj = json.getAsJsonObject();
                JsonObject jsonObject = jsObj.getAsJsonObject("Result");

                ticketReasonObject = gson.fromJson(jsonObject, MyTicketReasonObject.class);

                if (ticketReasonObject.getData() != null && ticketReasonObject.getData().size() > 0) {
                    for (int i = 0; i < ticketReasonObject.getData().size(); i++) {
                        MyTicketReasonObject.ReasonObject reasonObject = ticketReasonObject.getData().get(i);
                        SpinnerObject spinnerObject = new SpinnerObject(reasonObject.getReasonID(), reasonObject.getReasonName());
                        spinnerObjectList.add(spinnerObject);
                    }
                    ticketReasonObject.setSpinnerObjectList(spinnerObjectList);
                }

            }catch (Exception e){
                Log.e("tmt", "MyTicketEffectField : " + e.toString());
            }

            return ticketReasonObject;
        }
    }
}
