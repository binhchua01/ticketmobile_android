package app.fpt.ticketmobile.app.jsonParsers.Fortype.support;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import app.fpt.ticketmobile.app.model.SpinnerObject;
import app.fpt.ticketmobile.app.model.support.MyTicketCustomTypeObject;

/**
 * Created by Administrator on 7/7/2016.
 */
public class MyTicketCustomTypeField {

    public static final class Deserializer implements JsonDeserializer<MyTicketCustomTypeObject>{

        @Override
        public MyTicketCustomTypeObject deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            Gson gson = new Gson();
            MyTicketCustomTypeObject ticketCustomTypeObject = null;
            List<SpinnerObject> spinnerObjectList = new ArrayList<>();

            try{
                JsonObject jsResult = json.getAsJsonObject();
                if (jsResult.has("Result")){
                    jsResult = jsResult.getAsJsonObject("Result");
                    if (jsResult.has("Data")){
                        if (jsResult.get("Data").isJsonArray()) {
                            ticketCustomTypeObject = gson.fromJson(jsResult, MyTicketCustomTypeObject.class);

                            if (ticketCustomTypeObject.getCustomTypeList().size() > 0){
                                for (int i = 0; i < ticketCustomTypeObject.getCustomTypeList().size(); i++){
                                    MyTicketCustomTypeObject.CustomTypeObject customTypeObject = ticketCustomTypeObject.getCustomTypeList().get(i);
                                    SpinnerObject spinnerObject = new SpinnerObject(String.valueOf(customTypeObject.getID().intValue()), customTypeObject.getName());
                                    spinnerObjectList.add(spinnerObject);
                                }
                                ticketCustomTypeObject.setSpinnerObjectList(spinnerObjectList);
                            }
                        }
                    }
                }

            }catch (Exception e){
                Log.e("tmt", "MyTicketCustomTypeField : Deserializer : " + e.toString());
            }

            return ticketCustomTypeObject;
        }
    }
}
