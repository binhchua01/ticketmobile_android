package app.fpt.ticketmobile.app.di.module.internal;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.net.ConnectivityManagerCompat;

import app.fpt.ticketmobile.app.lib.TmConnectivityInfo;

/**
 * Created by ToanMaster on 6/14/2016.
 */
public class TmConnectivityInfoImpl implements TmConnectivityInfo {

    private Context mContext;

    public TmConnectivityInfoImpl(Context context){
        this.mContext = context;
    }


    @Override
    public boolean isInterfaceConnected() {
        NetworkInfo networkInfo = ((ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    @Override
    public boolean isActiveNetworkMetered() {
        return ConnectivityManagerCompat.isActiveNetworkMetered((ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE));
    }


}
