package app.fpt.ticketmobile.app.ui.adapter.support;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ViewHolder.ParentViewHolder;

import app.fpt.ticketmobile.R;

/**
 * Created by Administrator on 7/25/2016.
 */
public class TicketSupportOfMeCreatedHeaderViewHolder extends ParentViewHolder {

    private static final float INITIAL_POSITION = 0.0f;
    private static final float ROTATED_POSITION = 180f;

    private final ImageView mExpandToggle;
    private TextView mTicketInventoryHeaderTittle;

    public TicketSupportOfMeCreatedHeaderViewHolder(View parentView) {
        super(parentView);
        mExpandToggle = (ImageView) parentView.findViewById(R.id.tm_expand_toggle);
        mTicketInventoryHeaderTittle = (TextView) parentView.findViewById(R.id.tm_header_tittle);
    }

    public void bind(TicketSupportOfMeCreatedHeader ticketSupportOfMeCreatedHeader){
        mTicketInventoryHeaderTittle.setText(ticketSupportOfMeCreatedHeader.getQueueName() +  " (" + ticketSupportOfMeCreatedHeader.getTicketOfUserCreatedChildList().size()+")");
    }

    @Override
    public void setExpanded(boolean expanded) {
        super.setExpanded(expanded);
    }

    @Override
    public void onExpansionToggled(boolean expanded) {
        super.onExpansionToggled(expanded);
    }
}

