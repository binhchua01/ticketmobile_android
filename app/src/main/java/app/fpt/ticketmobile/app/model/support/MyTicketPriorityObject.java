package app.fpt.ticketmobile.app.model.support;

import java.util.List;

/**
 * Created by Administrator on 7/7/2016.
 */
public class MyTicketPriorityObject {

    private List<PriorityObject> Data;

    public List<PriorityObject> getPriorityList() {
        return Data;
    }

    public void setPriorityList(List<PriorityObject> data) {
        Data = data;
    }

    public class PriorityObject{
        private Integer PriorityValue;

        public Integer getPriorityValue() {
            return PriorityValue;
        }

        public void setPriorityValue(Integer priorityValue) {
            PriorityValue = priorityValue;
        }

    }


}
