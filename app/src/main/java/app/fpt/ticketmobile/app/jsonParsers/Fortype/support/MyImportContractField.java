package app.fpt.ticketmobile.app.jsonParsers.Fortype.support;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import app.fpt.ticketmobile.app.model.support.MyImportContractObject;

/**
 * Created by Administrator on 7/25/2016.
 */
public class MyImportContractField {

    public static final class Deserializer implements JsonDeserializer<MyImportContractObject>{

        @Override
        public MyImportContractObject deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            Gson gson = new Gson();
            MyImportContractObject myImportContractObject = new MyImportContractObject();

            try{
                JsonObject jsObj = json.getAsJsonObject();
                JsonObject jsonObject = jsObj.getAsJsonObject("Result");
                myImportContractObject = gson.fromJson(jsonObject, MyImportContractObject.class);

            }catch (Exception e){
                Log.e("tmt", "MyVisorStaffSupportField : " + e.toString());
            }

            return myImportContractObject;
        }
    }
}
