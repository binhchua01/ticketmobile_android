package app.fpt.ticketmobile.app.networking.libs;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.github.mrengineer13.snackbar.SnackBar;

import app.fpt.ticketmobile.R;

/**
 * Created by Administrator on 6/28/2016.
 */
public class TmUtilsVolleyErrorListener{

    public static void onErrorListener(Context context, View root, VolleyError error){
        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
            SnackBar snackBar = new SnackBar.Builder(context, root)
                    .withMessage("This library is awesome!") // OR
                    .withActionMessage("UNDO") // OR
                    .withTextColorId(R.color.tm_snack_action_color)
                    .withBackgroundColorId(R.color.tm_snack_background_color)
                    .withDuration(SnackBar.LONG_SNACK)
                    .show();

        } else if (error instanceof AuthFailureError) {
            Log.d("tmt", "AuthFailureError");
        } else if (error instanceof ServerError) {
            Log.d("tmt", "ServerError");
        } else if (error instanceof NetworkError) {
            Log.d("tmt", "NetworkError");
        } else if (error instanceof ParseError) {
            Log.d("tmt", "ParseError");
        } else {
            Log.d("tmt", "onErrorResponse : " + error.getMessage());
        }

    }


}
