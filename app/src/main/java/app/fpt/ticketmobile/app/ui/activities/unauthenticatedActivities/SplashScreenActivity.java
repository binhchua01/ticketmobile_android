package app.fpt.ticketmobile.app.ui.activities.unauthenticatedActivities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;

import app.fpt.ticketmobile.app.db.TmSharePreferencesUtils;
import app.fpt.ticketmobile.app.model.IAMDeviceTokenModel;
import app.fpt.ticketmobile.app.networking.apiEndpoints.Constants;
import app.fpt.ticketmobile.app.networking.apiEndpoints.GetAIMTokenEndPoint;
import app.fpt.ticketmobile.app.networking.utils.GsonGetRequest;
import app.fpt.ticketmobile.app.ui.activities.BaseActivity;
import app.fpt.ticketmobile.app.ui.activities.authenticatedActivities.HomeActivity;
import app.fpt.ticketmobile.R;
import app.fpt.ticketmobile.app.model.User;
import app.fpt.ticketmobile.app.model.checkversion.CheckVersionObject;
import app.fpt.ticketmobile.app.networking.apiEndpoints.CheckApkVersionEndpoint;

/**
 * Created by ToanMaster on 6/15/2016.
 */
public class SplashScreenActivity extends BaseActivity {

    private static final String EXTRA_CHECK_VERSION_TAG = "extra_check_version_tag";

    private static final int LOGIN_REQUEST_CODE = 101;
    private static final int NO_ANIMATION = 0;
    private Intent mUseIntentIfAuthenticatedFromCache;

    private RequestQueue mRequestQueue;
    private StringRequest mStringRequest;
    private String url = "https://sapi.fpt.vn/token/GenerateToken";

    public SplashScreenActivity() {
        super(Integer.valueOf(R.layout.activity_splash_screen));
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mRequestQueue = Volley.newRequestQueue(this);
        if (getTmConnectivityInfo().isInterfaceConnected()) {
            getCheckVersion();
            getKongToken();
//            login(false);
        } else {
            show_Message("Không nhận được phản hồi từ Server!");
        }

//       login(false);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null && requestCode == LOGIN_REQUEST_CODE && resultCode == RESULT_OK) {
            login(true);
        }else {
            finish();
        }
    }

    private void getKongToken() {
        mStringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                getTmSharePreferencesUtils().saveKongToken(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("TAG", "Error :" + error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                return getHeaderKongToken();
            }
        };
        mRequestQueue.add(mStringRequest);
    }

    private static Map<String, String> getHeaderKongToken(){
        Map<String, String> params = new HashMap<>();
        params.put("Authorization", "Basic dGlja2V0OkZ0ZWxpc2NAMTIz");
        params.put("Content-Type", "application/x-www-form-urlencoded");
        return params;
    }

    private void getCheckVersion() {
        getTmApiEndPointProvider().addRequest(CheckApkVersionEndpoint.getCheckVersion(getString(R.string.platform), new Response.Listener<CheckVersionObject>() {
            @Override
            public void onResponse(CheckVersionObject checkVersionObject) {
                String version_now = getResources().getString(R.string.VersionToCheck);
                String releaseDate_now = getResources().getString(R.string.ReleaseDateToCheck);
                if (version_now.equalsIgnoreCase(checkVersionObject.getCheckVersion().getVersion()) && releaseDate_now.equalsIgnoreCase(checkVersionObject.getCheckVersion().getReleaseDate())) {
                    login(false);
                } else {
                    show_inform(checkVersionObject.getCheckVersion().getLink());
                    //test
//                   login(false);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onErrorListener(error);
            }
        }), EXTRA_CHECK_VERSION_TAG);

    }

    private void login(boolean authTokenAcquiredFromServer) {
        User user = getTmSharePreferencesUtils().getUserLogin();
        if (user.getEmail() == "") {
            startActivityForResult(AuthActivity.buildIntent(SplashScreenActivity.this), LOGIN_REQUEST_CODE);
            return;
        }
        startAppropriateActivityOnLogin(authTokenAcquiredFromServer);
    }

    private void startAppropriateActivityOnLogin(boolean authTokenAcquiredFromServer) {
        Intent intent = HomeActivity.buildIntent(SplashScreenActivity.this);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        overridePendingTransition(NO_ANIMATION, NO_ANIMATION);
        finish();
    }

    public void show_Message(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Thông báo");
        builder.setMessage(msg);
        builder.setCancelable(false);

        builder.setPositiveButton("Thoát",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }

                });

        AlertDialog alert = builder.create();
        alert.getWindow().setGravity(Gravity.TOP);
        alert.show();
    }

    private void onErrorListener(VolleyError error) {
        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
            getTmToast().makeToast(getString(R.string.tm_error_connect), Toast.LENGTH_SHORT);
        } else if (error instanceof AuthFailureError) {
            Log.d("tmt", "AuthFailureError");
        } else if (error instanceof ServerError) {
            Log.d("tmt", "ServerError");
        } else if (error instanceof NetworkError) {
            Log.d("tmt", "NetworkError");
        } else if (error instanceof ParseError) {
            Log.d("tmt", "ParseError");
        } else {
            Log.d("tmt", "onErrorResponse : " + error.getMessage());
        }
        show_Message(error.getMessage().toString());
    }

    public void show_inform(final String linkDownLoad) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.XacNhan));
        builder.setMessage(getResources().getString(R.string.ThongBaoCoPhienBanMoi));
        builder.setPositiveButton("Cập nhật", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                new DownloadAPKFromURL().execute(linkDownLoad);
            }

        });
        builder.setCancelable(false);
        builder.setNegativeButton("Thoát", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });

        AlertDialog alert = builder.create();
        alert.getWindow().setGravity(Gravity.TOP);
        alert.show();
    }

    public class DownloadAPKFromURL extends AsyncTask<String, String, String> {
        // "/sdcard/download/";
        String path_file = Environment.getExternalStorageDirectory()
                + "/download/";
        String file_name = "abc.apk";
        String linkdownload = "";
        String temp;
        ProgressDialog pDialog;
        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            pDialog = new ProgressDialog(SplashScreenActivity.this);
            pDialog.setTitle(getResources().getString(
                    R.string.CapNhapPhienBanMoi));
            pDialog.setMessage(getResources().getString(R.string.DangDownload));
            pDialog.setIndeterminate(false);
            pDialog.setMax(100);
            pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            int count;
            int progress = 0;
            try {
                URL url = new URL(params[0]);
                URLConnection conection = url.openConnection();
                conection.connect();
                // Check folder
                File folder = new File(
                        Environment.getExternalStorageDirectory() + "/download");
                if (!folder.exists()) {
                    folder.mkdir();
                }
                // Get file name
                file_name = url.getFile().substring(
                        url.getFile().lastIndexOf('/') + 1,
                        url.getFile().length());
                // Check exists file
                File file = new File(path_file, file_name);
                if (file.exists()) {
                    file.delete();
                }
                try {
                    // getting file length
                    int lenghtOfFile = conection.getContentLength();
                    // input stream to read file - with 8k buffer
                    InputStream input = new BufferedInputStream(
                            url.openStream(), 8192);
                    // Output stream to write file
                    OutputStream output = new FileOutputStream(path_file
                            + file_name);
                    byte data[] = new byte[1024];
                    long total = 0;
                    while ((count = input.read(data)) != -1) {
                        total += count;
                        // publishing the progress....
                        // After this onProgressUpdate will be called
                        progress = (int) ((total * 100) / lenghtOfFile);
                        publishProgress("" + progress);
                        // writing data to file
                        output.write(data, 0, count);
                    }
                    // flushing output
                    output.flush();
                    // closing streams
                    output.close();
                    input.close();

                } catch (IOException ex) {
                    ex.printStackTrace();
                    temp = ex.getMessage().toString() + "ioconnect";
                    return "DISCONNET_INTERNET";

                }

            } catch (Exception e) {
                temp = e.getMessage().toString() + "exception all";
                return "DISCONNET_INTERNET";
            }
            temp = "ok";
            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);
            pDialog.setProgress(Integer.parseInt(values[0]));
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);

            // hide dialog
            pDialog.dismiss();

            if (result != null && result.equals("DISCONNET_INTERNET")) {
                Toast.makeText(getApplicationContext(),
                        getResources().getString(R.string.KiemTraKetNoi),
                        Toast.LENGTH_LONG).show();
                finish();

                return;
            }

            // Install app
            try {

                finish();

                // cai dat file apk moi download
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(
                        Uri.fromFile(new File(path_file + file_name)),
                        "application/vnd.android.package-archive");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                Toast.makeText(getApplicationContext(),
                        getResources().getString(R.string.DownloadThanhCong),
                        Toast.LENGTH_LONG).show();

            } catch (Exception ex) {
                Toast.makeText(getApplicationContext(),
                        getResources().getString(R.string.ErrorCapNhat),
                        Toast.LENGTH_LONG).show();
                finish();
            }

        }

    }
}
