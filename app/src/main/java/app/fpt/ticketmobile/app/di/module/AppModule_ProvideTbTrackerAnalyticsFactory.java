package app.fpt.ticketmobile.app.di.module;

import javax.inject.Provider;

import android.content.Context;

import app.fpt.ticketmobile.app.tracker.interfaces.TmTrackerAnalytics;
import dagger.internal.Factory;

public class AppModule_ProvideTbTrackerAnalyticsFactory implements Factory<TmTrackerAnalytics>{
	 private static final boolean $assertionDisabled = !AppModule_ProvideTbTrackerAnalyticsFactory.class.desiredAssertionStatus();
	 private AppModule appModule;
	 private Provider<Context> providerContext;
	 
	 
	 public AppModule_ProvideTbTrackerAnalyticsFactory(AppModule appModule, Provider<Context> providerContext){
		 if ($assertionDisabled || appModule != null){
			 this.appModule = appModule;
			if ($assertionDisabled || providerContext != null){
				this.providerContext = providerContext;
				return;
			}
			throw new AssertionError();
		 }
		 throw new AssertionError();
	 }
	 
	 
	 
	@Override
	public TmTrackerAnalytics get() {
		TmTrackerAnalytics provideTbTrackerAnalytics = appModule.provideTbTrackerAnalytics(providerContext.get());
		
		if (provideTbTrackerAnalytics != null){
			return provideTbTrackerAnalytics;
		}
		throw new NullPointerException("Return null from  a @Nullable @provide method.");
	}
	
	
	public static Factory<TmTrackerAnalytics> create(AppModule appModule, Provider<Context> providerContext){
		return new AppModule_ProvideTbTrackerAnalyticsFactory(appModule, providerContext);
	}
	 

}
