package app.fpt.ticketmobile.app.lib;

/**
 * Created by Administrator on 7/12/2016.
 */
public interface Provider<T> {
    T provider();
}
