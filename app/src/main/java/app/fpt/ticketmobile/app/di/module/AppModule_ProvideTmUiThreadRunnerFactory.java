package app.fpt.ticketmobile.app.di.module;

import dagger.internal.Factory;
import app.fpt.ticketmobile.app.lib.TmUiThreadRunner;

/**
 * Created by ToanMaster on 6/14/2016.
 */
public class AppModule_ProvideTmUiThreadRunnerFactory implements Factory<TmUiThreadRunner> {
    private static final boolean $assertionDisabled = !AppModule_ProvideTmUiThreadRunnerFactory.class.desiredAssertionStatus();
    private AppModule appModule;

    public AppModule_ProvideTmUiThreadRunnerFactory(AppModule appModule){
        if ($assertionDisabled || appModule != null){
            this.appModule = appModule;
            return;
        }
        throw new AssertionError();
    }

    @Override
    public TmUiThreadRunner get() {
        TmUiThreadRunner provideTmUiThreadRunner = appModule.provideTmUiThreadHandler();
        if (provideTmUiThreadRunner != null) {
            return provideTmUiThreadRunner;
        }
        throw new NullPointerException("Cannot return null from a non-@Nullable @Provides method");
    }

    public static Factory<TmUiThreadRunner> create(AppModule appModule){
        return new AppModule_ProvideTmUiThreadRunnerFactory(appModule);
    }
}
