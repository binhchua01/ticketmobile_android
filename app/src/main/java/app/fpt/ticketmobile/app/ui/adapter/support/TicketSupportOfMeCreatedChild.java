package app.fpt.ticketmobile.app.ui.adapter.support;

import app.fpt.ticketmobile.app.model.support.MyTicketSupportOfUserCreatedObject;

/**
 * Created by Administrator on 7/25/2016.
 */
public class TicketSupportOfMeCreatedChild {

    private MyTicketSupportOfUserCreatedObject.TicketSupportOfUserCreated ticketSupportOfUserCreated;

    public MyTicketSupportOfUserCreatedObject.TicketSupportOfUserCreated getTicketSupportOfUserCreated() {
        return ticketSupportOfUserCreated;
    }

    public void setTicketSupportOfUserCreated(MyTicketSupportOfUserCreatedObject.TicketSupportOfUserCreated ticketSupportOfUserCreated) {
        this.ticketSupportOfUserCreated = ticketSupportOfUserCreated;
    }

    public TicketSupportOfMeCreatedChild(MyTicketSupportOfUserCreatedObject.TicketSupportOfUserCreated ticketSupportOfUserCreated) {
        this.ticketSupportOfUserCreated = ticketSupportOfUserCreated;
    }
}
