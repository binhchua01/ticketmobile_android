package app.fpt.ticketmobile.app.ui.fragments.dialogFragment;

import android.app.Activity;
import android.support.v4.app.DialogFragment;

import app.fpt.ticketmobile.app.lib.TmApiEndPointProvider;
import app.fpt.ticketmobile.app.ui.lib.TmToast;
import app.fpt.ticketmobile.app.core.TmApplication;
import app.fpt.ticketmobile.app.db.TmSharePreferencesUtils;
import app.fpt.ticketmobile.app.di.component.AppComponent;
import app.fpt.ticketmobile.app.lib.TmConnectivityInfo;

/**
 * Created by Administrator on 7/20/2016.
 */
public abstract class TmBaseDialogFragment<C> extends DialogFragment {

    private final Class<C> mCallbackInterface;

    protected TmBaseDialogFragment(Class<C> callbackInterface){
        this.mCallbackInterface = callbackInterface;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

    }

    protected final C getCallback(){
        if (getActivity() != null) {
            return (C) getActivity();
        }
        return null;
    }

    protected TmApiEndPointProvider getTmApiEndPointProvider(){
        checkIfNeedIsNull();
        return getAppComponent().getTmApiEndPointProvider();
    }

    protected TmConnectivityInfo getTmConnectivityInfo(){
        checkIfNeedIsNull();
        return getAppComponent().getTmConnectivityInfo();
    }

    protected TmSharePreferencesUtils getTmSharePreferencesUtils(){
        checkIfNeedIsNull();
        return  getAppComponent().getTmSharePreferencesUtils();
    }

    protected TmToast getTmToast(){
        checkIfNeedIsNull();
        return  getAppComponent().getTmToast();
    }

    private AppComponent getAppComponent(){
        AppComponent appComponent = ((TmApplication) getActivity().getApplication()).getAppComponent();
        return appComponent;
    }

    private void checkIfNeedIsNull(){
        if (getAppComponent() == null){
            throw new RuntimeException("AppComponent is null");
        }
    }


}
