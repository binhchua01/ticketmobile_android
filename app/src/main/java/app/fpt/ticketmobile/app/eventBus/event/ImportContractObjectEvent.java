package app.fpt.ticketmobile.app.eventBus.event;

import java.util.List;

import app.fpt.ticketmobile.app.eventBus.Event;
import app.fpt.ticketmobile.app.model.support.ImportContractObject;

/**
 * Created by Administrator on 7/25/2016.
 */
public class ImportContractObjectEvent implements Event {
    List<ImportContractObject> mImportContractObjectList;

    public List<ImportContractObject> getImportContractObjectList() {
        return mImportContractObjectList;
    }

    public void setImportContractObjectList(List<ImportContractObject> mImportContractObjectList) {
        this.mImportContractObjectList = mImportContractObjectList;
    }

    public ImportContractObjectEvent(List<ImportContractObject> mImportContractObjectList) {
        this.mImportContractObjectList = mImportContractObjectList;
    }
}
