package app.fpt.ticketmobile.app.model;

import java.io.Serializable;

/**
 * Created by Administrator on 7/4/2016.
 */
public class MyTicketFeedbackDetailObject implements Serializable {

    private String Subject;
    private String FromAdr;
    private String ToAdr;
    private String Cc;
    private String CreatedDate;
    private String BodyHTML;
    private String QueueAdr;

    public String getBodyHTML() {
        return BodyHTML;
    }

    public void setBodyHTML(String bodyHTML) {
        BodyHTML = bodyHTML;
    }

    public String getCc() {
        return Cc;
    }

    public void setCc(String cc) {
        Cc = cc;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }

    public String getFromAdr() {
        return FromAdr;
    }

    public void setFromAdr(String fromAdr) {
        FromAdr = fromAdr;
    }

    public String getQueueAdr() {
        return QueueAdr;
    }

    public void setQueueAdr(String queueAdr) {
        QueueAdr = queueAdr;
    }

    public String getSubject() {
        return Subject;
    }

    public void setSubject(String subject) {
        Subject = subject;
    }

    public String getToAdr() {
        return ToAdr;
    }

    public void setToAdr(String toAdr) {
        ToAdr = toAdr;
    }
}
