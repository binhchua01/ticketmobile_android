package app.fpt.ticketmobile.app.ui.activities.authenticatedActivities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import app.fpt.ticketmobile.app.ui.activities.TmBaseActivity;
import app.fpt.ticketmobile.R;
import app.fpt.ticketmobile.app.ui.fragments.authenticatedFragment.TicketFeedBackFragment;

/**
 * Created by Administrator on 6/30/2016.
 */
public class TicketFeedBackActivity extends TmBaseActivity {

    private static final String EXTRA_TICKET_ID_KEY = "extra_ticket_id_key";
    private static final String FRAGMENT_FEED_BACK_TAG = "fragment_feed_back_tag";

    public TicketFeedBackActivity() {
        super(Integer.valueOf(R.layout.activity_ticket_feedback));
    }

    public static Intent buildIntent(Context context, String ticketId){
        Intent intent = new Intent(context, TicketFeedBackActivity.class);
        intent.putExtra(EXTRA_TICKET_ID_KEY, ticketId);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        extrasIntent();
    }

    private void extrasIntent(){

        Intent intent = getIntent();
        if (intent != null){
            String ticketId = intent.getStringExtra(EXTRA_TICKET_ID_KEY);
            setupIsFragmentIfNeed(ticketId);
        }
    }

    private void setupIsFragmentIfNeed(String ticketId) {

        TicketFeedBackFragment ticketFeedBackFragment = (TicketFeedBackFragment) getSupportFragmentManager().findFragmentByTag(FRAGMENT_FEED_BACK_TAG);
        if (ticketFeedBackFragment == null){
            ticketFeedBackFragment = TicketFeedBackFragment.getInstances(ticketId);
        }
        getSupportFragmentManager().beginTransaction().replace(R.id.content_fragment, ticketFeedBackFragment, FRAGMENT_FEED_BACK_TAG).commit();
    }

    @Override
    protected void setupToolbar(Toolbar mToolbar) {
        super.setupToolbar(mToolbar);
        ((TextView) mToolbar.findViewById(R.id.main_toolbar_title)).setText(getString(R.string.tm_ticket_response));;
    }
}
