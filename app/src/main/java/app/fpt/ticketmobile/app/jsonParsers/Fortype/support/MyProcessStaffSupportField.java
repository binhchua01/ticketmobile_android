package app.fpt.ticketmobile.app.jsonParsers.Fortype.support;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import app.fpt.ticketmobile.app.model.SpinnerObject;
import app.fpt.ticketmobile.app.model.support.MyProcessStaffSupportObject;

/**
 * Created by Administrator on 7/22/2016.
 */
public class MyProcessStaffSupportField {

    public static final class Deserializer implements JsonDeserializer<MyProcessStaffSupportObject>{

        @Override
        public MyProcessStaffSupportObject deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            Gson gson = new Gson();
            MyProcessStaffSupportObject myProcessStaffSupportObject = new MyProcessStaffSupportObject();
            List<SpinnerObject> spinnerObjectList = new ArrayList<>();

            try{
                JsonObject jsObj = json.getAsJsonObject();
                JsonObject jsonObject = jsObj.getAsJsonObject("Result");

                myProcessStaffSupportObject = gson.fromJson(jsonObject, MyProcessStaffSupportObject.class);

                if (myProcessStaffSupportObject.getProcessStaffSupportList() != null && myProcessStaffSupportObject.getProcessStaffSupportList().size() > 0) {
                    for (int i = 0; i < myProcessStaffSupportObject.getProcessStaffSupportList().size(); i++) {
                        MyProcessStaffSupportObject.ProcessStaffSupportObject processStaffSupportObject = myProcessStaffSupportObject.getProcessStaffSupportList().get(i);
                        SpinnerObject spinnerObject = new SpinnerObject(String.valueOf(processStaffSupportObject.getID()), processStaffSupportObject.getName(), String.valueOf(processStaffSupportObject.getEmail()));
                        spinnerObjectList.add(spinnerObject);
                    }
                    myProcessStaffSupportObject.setSpinnerObjectList(spinnerObjectList);
                }

            }catch (Exception e){
                Log.e("tmt", "MyVisorStaffSupportField : " + e.toString());
            }

            return myProcessStaffSupportObject;
        }
    }
}
