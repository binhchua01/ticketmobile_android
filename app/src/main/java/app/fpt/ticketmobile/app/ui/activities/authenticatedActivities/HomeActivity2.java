package app.fpt.ticketmobile.app.ui.activities.authenticatedActivities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import app.fpt.ticketmobile.R;
import app.fpt.ticketmobile.app.ui.activities.TmBaseActivity;

/**
 * Created by ToanMaster on 6/15/2016.
 */
public class HomeActivity2 extends TmBaseActivity implements View.OnClickListener{

    public HomeActivity2() {
        super(Integer.valueOf(R.layout.activity_home_2));
    }

    public static Intent buildIntent(Context context){
        return new Intent(context, HomeActivity2.class);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupViews();
    }

    private void setupViews() {
        findViewById(R.id.tm_action_ticket_inventory_not_assign).setOnClickListener(this);
        findViewById(R.id.tm_action_ticket_of_user).setOnClickListener(this);
        findViewById(R.id.tm_action_ticket_of_user_create).setOnClickListener(this);
        findViewById(R.id.tm_action_ticket_inventory_unfinished).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tm_action_ticket_of_user:
                startActivity(TicketOfUserActivity.buildIntent(this));
                break;
            case R.id.tm_action_ticket_of_user_create:
                startActivity(TicketOfUserCreatedActivity.buildIntent(this));
                break;
            case R.id.tm_action_ticket_inventory_not_assign:
                startActivity(TicketInventoryNotAssignActivity.buildIntent(this));
                break;
            case R.id.tm_action_ticket_inventory_unfinished:
                startActivity(TicketInventoryUnFinishActivity.buildIntent(this));
                break;
        }
    }
}
