package app.fpt.ticketmobile.app.jsonParsers.Fortype;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import app.fpt.ticketmobile.app.model.MyTicketVisorStaffObject;
import app.fpt.ticketmobile.app.model.SpinnerObject;

/**
 * Created by Administrator on 6/29/2016.
 */
public class MyTicketVisorStaffField {

    public static final class Deserializer implements JsonDeserializer<MyTicketVisorStaffObject>{

        @Override
        public MyTicketVisorStaffObject deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            Gson gson = new Gson();
            MyTicketVisorStaffObject myTicketVisorStaffObject = new MyTicketVisorStaffObject();
            List<SpinnerObject> spinnerObjectList = new ArrayList<>();

            try{
                JsonObject jsObj = json.getAsJsonObject();
                JsonObject jsonObject = jsObj.getAsJsonObject("Result");
                myTicketVisorStaffObject = gson.fromJson(jsonObject, MyTicketVisorStaffObject.class);

                if (myTicketVisorStaffObject.getData() != null && myTicketVisorStaffObject.getData().size() > 0){
                    for (int i = 0; i < myTicketVisorStaffObject.getData().size(); i++){
                        MyTicketVisorStaffObject.VisorStaffObject visorStaffObject = myTicketVisorStaffObject.getData().get(i);
                        SpinnerObject spinnerObject = new SpinnerObject(visorStaffObject.getEmail(), visorStaffObject.getName());
                        spinnerObjectList.add(spinnerObject);
                    }
                    myTicketVisorStaffObject.setSpinnerObjectList(spinnerObjectList);
                }

            }catch(Exception e){
                Log.e("tmt", "MyTicketProcessStaffField : Deserializer " + e.toString());
            }
            return myTicketVisorStaffObject;
        }
    }
}
