package app.fpt.ticketmobile.app.model.support;

/**
 * Created by Administrator on 7/21/2016.
 */
public class MyTicketSupportObject {

    String Kind;
    String TicketStatus;
    String Title;
    String Description;
    String Priority;
    String CricLev;
    String EstimatedTime;
    String FoundStaff;
    String FoundDivision;
    String FoundPhone;
    String FoundIP;
    String RemindStatus;
    String HasWarning;
    String OperateStaff;
    String VisorStaff;
    String ProcessStaff;
    String ProcessPhone;
    String ProcessIP;
    String Queue;
    String ParentID;
    String LocationID;
    String EffBranch;
    String ServiceType;
    String CusType;
    String IssueID;
    String ReasonGroup;
    String ProcessStep;
    String ProcessName;
    String IssueName;
    String LocationName;
    String BranchName;
    String ServiceName;
    String CusName;
    String FoundDivName;
    String FoundName;
    String Options;

    public String getBranchName() {
        return BranchName;
    }

    public void setBranchName(String branchName) {
        BranchName = branchName;
    }

    public String getCricLev() {
        return CricLev;
    }

    public void setCricLev(String cricLev) {
        CricLev = cricLev;
    }

    public String getCusName() {
        return CusName;
    }

    public void setCusName(String cusName) {
        this.CusName = cusName;
    }

    public String getCusType() {
        return CusType;
    }

    public void setCusType(String cusType) {
        CusType = cusType;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getEffBranch() {
        return EffBranch;
    }

    public void setEffBranch(String effBranch) {
        EffBranch = effBranch;
    }

    public String getEstimatedTime() {
        return EstimatedTime;
    }

    public void setEstimatedTime(String estimatedTime) {
        EstimatedTime = estimatedTime;
    }

    public String getFoundDivision() {
        return FoundDivision;
    }

    public void setFoundDivision(String foundDivision) {
        FoundDivision = foundDivision;
    }

    public String getFoundDivName() {
        return FoundDivName;
    }

    public void setFoundDivName(String foundDivName) {
        FoundDivName = foundDivName;
    }

    public String getFoundIP() {
        return FoundIP;
    }

    public void setFoundIP(String foundIP) {
        FoundIP = foundIP;
    }

    public String getFoundName() {
        return FoundName;
    }

    public void setFoundName(String foundName) {
        FoundName = foundName;
    }

    public String getFoundPhone() {
        return FoundPhone;
    }

    public void setFoundPhone(String foundPhone) {
        FoundPhone = foundPhone;
    }

    public String getFoundStaff() {
        return FoundStaff;
    }

    public void setFoundStaff(String foundStaff) {
        FoundStaff = foundStaff;
    }

    public String getHasWarning() {
        return HasWarning;
    }

    public void setHasWarning(String hasWarning) {
        HasWarning = hasWarning;
    }

    public String getIssueID() {
        return IssueID;
    }

    public void setIssueID(String issueID) {
        IssueID = issueID;
    }

    public String getIssueName() {
        return IssueName;
    }

    public void setIssueName(String issueName) {
        IssueName = issueName;
    }

    public String getKind() {
        return Kind;
    }

    public void setKind(String kind) {
        Kind = kind;
    }

    public String getLocationID() {
        return LocationID;
    }

    public void setLocationID(String locationID) {
        LocationID = locationID;
    }

    public String getLocationName() {
        return LocationName;
    }

    public void setLocationName(String locationName) {
        LocationName = locationName;
    }

    public String getOperateStaff() {
        return OperateStaff;
    }

    public void setOperateStaff(String operateStaff) {
        OperateStaff = operateStaff;
    }

    public String getOptions() {
        return Options;
    }

    public void setOptions(String options) {
        Options = options;
    }

    public String getParentID() {
        return ParentID;
    }

    public void setParentID(String parentID) {
        ParentID = parentID;
    }

    public String getPriority() {
        return Priority;
    }

    public void setPriority(String priority) {
        Priority = priority;
    }

    public String getProcessIP() {
        return ProcessIP;
    }

    public void setProcessIP(String processIP) {
        ProcessIP = processIP;
    }

    public String getProcessName() {
        return ProcessName;
    }

    public void setProcessName(String processName) {
        ProcessName = processName;
    }

    public String getProcessPhone() {
        return ProcessPhone;
    }

    public void setProcessPhone(String processPhone) {
        ProcessPhone = processPhone;
    }

    public String getProcessStaff() {
        return ProcessStaff;
    }

    public void setProcessStaff(String processStaff) {
        ProcessStaff = processStaff;
    }

    public String getProcessStep() {
        return ProcessStep;
    }

    public void setProcessStep(String processStep) {
        ProcessStep = processStep;
    }

    public String getQueue() {
        return Queue;
    }

    public void setQueue(String queue) {
        Queue = queue;
    }

    public String getReasonGroup() {
        return ReasonGroup;
    }

    public void setReasonGroup(String reasonGroup) {
        ReasonGroup = reasonGroup;
    }

    public String getRemindStatus() {
        return RemindStatus;
    }

    public void setRemindStatus(String remindStatus) {
        RemindStatus = remindStatus;
    }

    public String getServiceName() {
        return ServiceName;
    }

    public void setServiceName(String serviceName) {
        ServiceName = serviceName;
    }

    public String getServiceType() {
        return ServiceType;
    }

    public void setServiceType(String serviceType) {
        ServiceType = serviceType;
    }

    public String getTicketStatus() {
        return TicketStatus;
    }

    public void setTicketStatus(String ticketStatus) {
        TicketStatus = ticketStatus;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getVisorStaff() {
        return VisorStaff;
    }

    public void setVisorStaff(String visorStaff) {
        VisorStaff = visorStaff;
    }


    @Override
    public String toString() {
        return " Kind " + getKind()
                + " TicketStatus " + getTicketStatus()
                + " Title " + getTitle()
                + " Description " + getDescription()
                + " Priority " + getPriority()
                + " CricLev " + getCricLev()
                + " EstimatedTime " + getEstimatedTime()
                + " FoundStaff " + getFoundStaff()
                + " FoundDivision " + getFoundDivision()
                + " FoundPhone " + getFoundPhone()
                + " FoundIP " + getFoundIP()
                + " RemindStatus " + getRemindStatus()
                + " HasWarning " + getHasWarning()
                + " OperateStaff " + getOperateStaff()
                + " VisorStaff " + getVisorStaff()
                + " ProcessStaff " + getProcessStaff()
                + " ProcessPhone " + getProcessPhone()
                + " ProcessIP " + getProcessIP()
                + " Queue " + getQueue()
                + " ParentID " + getParentID()
                + " LocationID " + getLocationID()
                + " EffBranch " + getEffBranch()
                + " ServiceType " + getServiceType()
                + " CusType " + getCusType()
                + " IssueID " + getIssueID()
                + " ReasonGroup " + getReasonGroup()
                + " ProcessStep " + getProcessStep()
                + " ProcessName " + getProcessName()
                + " IssueName " + getIssueName()
                + " LocationName " + getLocationName()
                + " BranchName " + getBranchName()
                + " ServiceName " + getServiceName()
                + " CusName " + getCusName()
                + " FoundDivName " + getFoundDivName()
                + " FoundName " + getFoundName()
                + " Options " + getOptions();

    }
}
