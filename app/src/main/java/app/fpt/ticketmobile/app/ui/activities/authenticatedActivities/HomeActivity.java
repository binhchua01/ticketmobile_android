package app.fpt.ticketmobile.app.ui.activities.authenticatedActivities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.squareup.otto.Subscribe;

import java.util.List;

import app.fpt.ticketmobile.app.eventBus.event.DashboardTicketPrimaryFunctionEvent;
import app.fpt.ticketmobile.app.eventBus.event.DashboardTicketSupportEvent;
import app.fpt.ticketmobile.app.eventBus.event.DashbroadChangeItemBackground;
import app.fpt.ticketmobile.app.eventBus.event.ImportContractObjectEvent;
import app.fpt.ticketmobile.app.gcm.GCMClientManager;
import app.fpt.ticketmobile.app.model.MyResponseObject;
import app.fpt.ticketmobile.app.ui.activities.BaseActivity;
import app.fpt.ticketmobile.app.ui.fragments.authenticatedFragment.TicketHomeDashboardFragment;
import app.fpt.ticketmobile.app.ui.fragments.authenticatedFragment.TicketSupportCreatedFragment;
import app.fpt.ticketmobile.app.ui.lib.TicketSupport;
import app.fpt.ticketmobile.app.ui.lib.utils.SupportActionBarUtils;
import app.fpt.ticketmobile.widget.views.utils.KeyboardUtils;
import app.fpt.ticketmobile.R;
import app.fpt.ticketmobile.app.eventBus.event.DashBroadChangeMenu;
import app.fpt.ticketmobile.app.eventBus.event.DashboardAccountEvent;
import app.fpt.ticketmobile.app.eventBus.event.DashbroadTicketSettingEvent;
import app.fpt.ticketmobile.app.lib.utils.TmUtils;
import app.fpt.ticketmobile.app.model.support.ImportContractObject;
import app.fpt.ticketmobile.app.networking.apiEndpoints.PushNotificationEndPoint;
import app.fpt.ticketmobile.app.ui.activities.unauthenticatedActivities.SplashScreenActivity;
import app.fpt.ticketmobile.app.ui.fragments.authenticatedFragment.TicketInventoryNotAssignFragment;
import app.fpt.ticketmobile.app.ui.fragments.authenticatedFragment.TicketInventoryUnFinishFragment;
import app.fpt.ticketmobile.app.ui.fragments.authenticatedFragment.TicketOfUserCreatedFragment;
import app.fpt.ticketmobile.app.ui.fragments.authenticatedFragment.TicketOfUserFragment;
import app.fpt.ticketmobile.app.ui.fragments.authenticatedFragment.TicketSupportOfMeCreatedFragment;
import app.fpt.ticketmobile.app.ui.fragments.dialogFragment.ImportContractDialogFragment;
import app.fpt.ticketmobile.app.ui.lib.TicketPrimaryFunction;
import app.fpt.ticketmobile.app.ui.lib.TicketSettingInfo;

/**
 * Created by ToanMaster on 7/7/2016.
 */
public class HomeActivity extends BaseActivity implements ImportContractDialogFragment.ImportContractDialogCallback {

    private static final String FRAGMENT_NAVIGATION_TAG = "fragment_navigation_tag";
    private static final String FRAGMENT_TICKET_OF_USER_TAG = "fragment_ticket_of_user_tag";
    private static final String FRAGMENT_TICKET_OF_USER_CREATED_TAG = "fragment_ticket_of_user_created_tag";
    private static final String FRAGMENT_TICKET_INVENTORY_NOT_ASSIGN_TAG = "fragment_ticket_inventory_not_assign_tag";
    private static final String FRAGMENT_TICKET_INVENTORY_UNFINISHED_TAG = "fragment_ticket_inventory_unfinished_tag";
    private static final String FRAGMENT_TICKET_SUPPORT_OF_ME_TAG = "fragment_ticket_support_of_me_tag";
    private static final String FRAGMENT_TICKET_SUPPORT_CREATED_TAG = "fragment_ticket_support_created_tag";
    private static final TicketPrimaryFunction DEFAULT_GROUP_TICKET_PRIMARY_FUNCTION = TicketPrimaryFunction.TICKET_INVENTORY_NOT_ASSIGN;

    private static final String STATE_PRESERVING_GROUP_TICKET_PRIMARY_FUNCTION_KEY = "state_preserving_group_ticket_primary_function_key";
    private static final String STATE_PRESERVING_GROUP_TICKET_SUPPORT_KEY = "state_preserving_group_ticket_support_key";
    private static final String STATE_PRESERVING_GROUP_TICKET_SETTING_KEY = "state_preserving_group_ticket_setting_key";
    private static final String STATE_PRESERVING_TITLE_KEY = "state_preserving_title_key";


    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private boolean mActivityRecreated;
    private TicketPrimaryFunction mSelectedGroupTicketPrimaryFunction;
    private TicketSupport mSelectedGroupTicketSupport;
    private TicketSettingInfo mSelectedTicketSetting;

    private String mTicketTittle = null;
    private TextView customAppBarTitleView;

    private Boolean exit = false;


    public HomeActivity() {
        super(Integer.valueOf(R.layout.activity_home));
        mActivityRecreated = false;
        mSelectedGroupTicketPrimaryFunction = null;
        mSelectedGroupTicketSupport = null;
        mSelectedTicketSetting = null;
    }

    public static Intent buildIntent(Context context) {
        return new Intent(context, HomeActivity.class);
    }

    @Override
    protected void setupToolbar(Toolbar mToolbar) {
        super.setupToolbar(mToolbar);

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupDefaultSelection();
        extractSavedInstanceStateData(savedInstanceState);
        setupNavigation();
        setupActionBar();
        setupDrawerLayout();
        setupActionBarToggle();
        startAction();
        setupGcm();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(STATE_PRESERVING_TITLE_KEY, mTicketTittle);
        outState.putSerializable(STATE_PRESERVING_GROUP_TICKET_PRIMARY_FUNCTION_KEY, mSelectedGroupTicketPrimaryFunction);
        outState.putSerializable(STATE_PRESERVING_GROUP_TICKET_SUPPORT_KEY, mSelectedGroupTicketSupport);
        outState.putSerializable(STATE_PRESERVING_GROUP_TICKET_SETTING_KEY, mSelectedTicketSetting);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return mDrawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        this.mDrawerToggle.onConfigurationChanged(newConfig);
    }


    private void extractSavedInstanceStateData(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            mTicketTittle = savedInstanceState.getString(STATE_PRESERVING_TITLE_KEY);
            TicketPrimaryFunction primaryFunction = (TicketPrimaryFunction) savedInstanceState.getSerializable(STATE_PRESERVING_GROUP_TICKET_PRIMARY_FUNCTION_KEY);
            if (primaryFunction != null) {
                mSelectedGroupTicketPrimaryFunction = primaryFunction;
            }
            TicketSupport ticketSupport = (TicketSupport) savedInstanceState.getSerializable(STATE_PRESERVING_GROUP_TICKET_SUPPORT_KEY);
            if (ticketSupport != null) {
                mSelectedGroupTicketSupport = ticketSupport;
            }

            TicketSettingInfo ticketSettingInfo = (TicketSettingInfo) savedInstanceState.getSerializable(STATE_PRESERVING_GROUP_TICKET_SETTING_KEY);
            if (ticketSettingInfo != null){
                mSelectedTicketSetting = ticketSettingInfo;
            }
        }
    }

    private void setupDefaultSelection() {
        mSelectedGroupTicketPrimaryFunction = DEFAULT_GROUP_TICKET_PRIMARY_FUNCTION;
        mSelectedGroupTicketSupport = null;
        mSelectedTicketSetting = null;
        mTicketTittle = getString(R.string.tm_ticket_not_yet_assigned);

    }

    private void setupActionBarToggle() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
        this.mDrawerToggle = new PrivateActionBarDrawerToggle();
        this.mDrawerLayout.addDrawerListener(this.mDrawerToggle);
    }


    private void setupNavigation() {
        TicketHomeDashboardFragment ticketHomeDashboardFragment = (TicketHomeDashboardFragment) getSupportFragmentManager().findFragmentByTag(FRAGMENT_NAVIGATION_TAG);
        if (ticketHomeDashboardFragment == null) {
            ticketHomeDashboardFragment = new TicketHomeDashboardFragment();
        }

        getSupportFragmentManager().beginTransaction().replace(R.id.tm_navigation_frame, ticketHomeDashboardFragment, FRAGMENT_NAVIGATION_TAG).commit();
    }


    private final void setupActionBar() {
        if (getSupportActionBar() != null && SupportActionBarUtils.disableTitleAndSetCustomView(this, R.layout.custom_app_bar_title_and_subtitle_center_aligned)) {
            ActionBar supportActionBar = getSupportActionBar();
            if (supportActionBar != null) {
                View customView = supportActionBar.getCustomView();
                this.customAppBarTitleView = (TextView) customView.findViewById(R.id.title);
            }
        }
    }

    private void setupDrawerLayout() {
        this.mDrawerLayout = (DrawerLayout) findViewById(R.id.tm_draw_layout);
    }

    private void startAction() {
        if (!this.mActivityRecreated) {
            onSelectionMade(this.mSelectedGroupTicketPrimaryFunction, this.mSelectedGroupTicketSupport, false);
            this.mDrawerLayout.openDrawer(GravityCompat.START);
        }
        setAppropriateTitle(mTicketTittle);
    }

    private void onSelectionMade(TicketPrimaryFunction mSelectedGroupTicketPrimaryFunction, TicketSupport mSelectedGroupTicketSupport, boolean userInteractionWasWithGroup) {
        if (mSelectedGroupTicketPrimaryFunction != null) {
            switch (PrivateSelectionGroupTicketPrimaryFunction.$SelectionGroupTicketPrimaryFunction[mSelectedGroupTicketPrimaryFunction.ordinal()]) {
                case 1:
                    setupTicketOfUserFragmentIfNeed();
                    break;
                case 2:
                    setupTicketOfUserCreatedFragmentIfNeed();
                    break;
                case 3:
                    setupTicketInventoryNotAssignFragmentIfNeeded();
                    break;
                case 4:
                    setupTicketInventoryUnfinishedFragmentIfNeed();
                    break;
            }

        } else {

            switch (PrivateSelectionGroupTicketSupport.$SelectionGroupTicketSupport[mSelectedGroupTicketSupport.ordinal()]) {
                case 1:
                    setupTicketSupportOfMeFragmentIfNeed();
                    break;
                case 2:
                    setupTicketSupportCreateFragmentIfNeed();
                    break;
            }

        }

        if (userInteractionWasWithGroup) {
            this.mDrawerLayout.closeDrawer((int) GravityCompat.START);
        }
    }

    private void setAppropriateTitle(String title) {
        customAppBarTitleView.setText(title);
    }

    @Override
    public void getMyImportContractObject(List<ImportContractObject> importContractObjectList) {
        getEventBus().post(new ImportContractObjectEvent(importContractObjectList));
    }


    private class PrivateActionBarDrawerToggle extends ActionBarDrawerToggle {
        public PrivateActionBarDrawerToggle() {
            super(HomeActivity.this, mDrawerLayout, R.string.tm_cd_drawer_open_description, R.string.tm_cd_drawer_close_description);
        }

        @Override
        public void onDrawerOpened(View drawerView) {
            super.onDrawerOpened(drawerView);
            KeyboardUtils.hideSoftInput(HomeActivity.this.mDrawerLayout);
            invalidateOptionsMenu();
        }

        @Override
        public void onDrawerClosed(View drawerView) {
            super.onDrawerClosed(drawerView);
            invalidateOptionsMenu();
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);

    }

    static class PrivateSelectionGroupTicketPrimaryFunction {
        static final int[] $SelectionGroupTicketPrimaryFunction = new int[TicketPrimaryFunction.values().length];

        static {
            try {
                $SelectionGroupTicketPrimaryFunction[TicketPrimaryFunction.TICKET_OF_USER.ordinal()] = 1;
                $SelectionGroupTicketPrimaryFunction[TicketPrimaryFunction.TICKET_OF_USER_CREATE.ordinal()] = 2;
                $SelectionGroupTicketPrimaryFunction[TicketPrimaryFunction.TICKET_INVENTORY_NOT_ASSIGN.ordinal()] = 3;
                $SelectionGroupTicketPrimaryFunction[TicketPrimaryFunction.TICKET_INVENTORY_UNFINISHED.ordinal()] = 4;
            } catch (NoSuchFieldError noSuchFieldError) {
                Log.e("tmt", " HomeActivity : PrivateSelectionGroupTicketPrimaryFunction : " + noSuchFieldError.toString());
            }
        }
    }

    static class PrivateSelectionGroupTicketSupport {
        static final int[] $SelectionGroupTicketSupport = new int[TicketSupport.values().length];

        static {
            try {
                $SelectionGroupTicketSupport[TicketSupport.TICKET_SUPPORT_OF_ME.ordinal()] = 1;
                $SelectionGroupTicketSupport[TicketSupport.TICKET_SUPPORT_CREATE.ordinal()] = 2;
            } catch (NoSuchFieldError noSuchFieldError) {
                Log.e("tmt", " HomeActivity : PrivateSelectionGroupTicketSupport : " + noSuchFieldError.toString());
            }
        }
    }

    static class PrivateSelectionGroupTicketSetting {
        static final int[] $SelectionGroupTicketSetting = new int[TicketSettingInfo.values().length];

        static {
            try {
                $SelectionGroupTicketSetting[TicketSettingInfo.TICKET_SUPPORT_VERSION.ordinal()] = 1;
                $SelectionGroupTicketSetting[TicketSettingInfo.TICKET_SUPPORT_EXIT.ordinal()] = 2;

            } catch (NoSuchFieldError noSuchFieldError) {
                Log.e("tmt", " HomeActivity : PrivateSelectionGroupTicketSetting : " + noSuchFieldError.toString());
            }
        }
    }


    private Fragment getFragmentByTag(String tag) {
        return getSupportFragmentManager().findFragmentByTag(tag);
    }

    private void setupTicketOfUserFragmentIfNeed() {
        TicketOfUserFragment ticketOfUserFragment = (TicketOfUserFragment) getFragmentByTag(FRAGMENT_TICKET_OF_USER_TAG);
        if (ticketOfUserFragment == null) {
            ticketOfUserFragment = new TicketOfUserFragment();
        }
        getSupportFragmentManager().beginTransaction().replace(R.id.content_fragment, ticketOfUserFragment, FRAGMENT_TICKET_OF_USER_TAG).commit();
    }


    private void setupTicketOfUserCreatedFragmentIfNeed() {
        TicketOfUserCreatedFragment ticketOfUserCreatedFragment = (TicketOfUserCreatedFragment) getFragmentByTag(FRAGMENT_TICKET_OF_USER_CREATED_TAG);
        if (ticketOfUserCreatedFragment == null) {
            ticketOfUserCreatedFragment = new TicketOfUserCreatedFragment();
        }
        getSupportFragmentManager().beginTransaction().replace(R.id.content_fragment, ticketOfUserCreatedFragment, FRAGMENT_TICKET_OF_USER_CREATED_TAG).commit();
    }

    private void setupTicketInventoryNotAssignFragmentIfNeeded() {
        TicketInventoryNotAssignFragment ticketInventoryNotAssignFragment = (TicketInventoryNotAssignFragment) getFragmentByTag(FRAGMENT_TICKET_INVENTORY_NOT_ASSIGN_TAG);
        if (ticketInventoryNotAssignFragment == null) {
            ticketInventoryNotAssignFragment = new TicketInventoryNotAssignFragment();
        }
        getSupportFragmentManager().beginTransaction().replace(R.id.content_fragment, ticketInventoryNotAssignFragment, FRAGMENT_TICKET_INVENTORY_NOT_ASSIGN_TAG).commit();
    }

    private void setupTicketInventoryUnfinishedFragmentIfNeed() {
        TicketInventoryUnFinishFragment ticketInventoryUnFinishFragment = (TicketInventoryUnFinishFragment) getFragmentByTag(FRAGMENT_TICKET_INVENTORY_UNFINISHED_TAG);
        if (ticketInventoryUnFinishFragment == null) {
            ticketInventoryUnFinishFragment = new TicketInventoryUnFinishFragment();
        }
        getSupportFragmentManager().beginTransaction().replace(R.id.content_fragment, ticketInventoryUnFinishFragment, FRAGMENT_TICKET_INVENTORY_UNFINISHED_TAG).commit();
    }


    private void setupTicketSupportOfMeFragmentIfNeed() {
        TicketSupportOfMeCreatedFragment supportOfMeFragment = (TicketSupportOfMeCreatedFragment) getFragmentByTag(FRAGMENT_TICKET_SUPPORT_OF_ME_TAG);
        if (supportOfMeFragment == null) {
            supportOfMeFragment = new TicketSupportOfMeCreatedFragment();
        }
        getSupportFragmentManager().beginTransaction().replace(R.id.content_fragment, supportOfMeFragment, FRAGMENT_TICKET_SUPPORT_OF_ME_TAG).commit();
    }

    private void setupTicketSupportCreateFragmentIfNeed() {
        TicketSupportCreatedFragment supportCreatedFragment = (TicketSupportCreatedFragment) getFragmentByTag(FRAGMENT_TICKET_SUPPORT_CREATED_TAG);
        if (supportCreatedFragment == null) {
            supportCreatedFragment = new TicketSupportCreatedFragment();
        }
        getSupportFragmentManager().beginTransaction().replace(R.id.content_fragment, supportCreatedFragment, FRAGMENT_TICKET_SUPPORT_CREATED_TAG).commit();
    }


    @Subscribe
    public void onTicketPrimaryFunctionEvent(DashboardTicketPrimaryFunctionEvent ticketPrimaryFunctionEvent) {
        mSelectedGroupTicketSupport = null;
        mSelectedTicketSetting = null;
        mSelectedGroupTicketPrimaryFunction = ticketPrimaryFunctionEvent.getTicketPrimaryFunction();
        mTicketTittle = ticketPrimaryFunctionEvent.getTitle();
        onSelectionMade(mSelectedGroupTicketPrimaryFunction, mSelectedGroupTicketSupport, true);
        setAppropriateTitle(mTicketTittle);
    }


    @Subscribe
    public void onTicketSupportEvent(DashboardTicketSupportEvent ticketSupportEvent) {
        mSelectedGroupTicketPrimaryFunction = null;
        mSelectedTicketSetting = null;
        mSelectedGroupTicketSupport = ticketSupportEvent.getTicketSupport();
        mTicketTittle = ticketSupportEvent.getTitle();
        onSelectionMade(mSelectedGroupTicketPrimaryFunction, mSelectedGroupTicketSupport, true);
        setAppropriateTitle(mTicketTittle);


    }

    @Subscribe
    public void onTicketSettingEvent(DashbroadTicketSettingEvent ticketSettingEvent) {
        mSelectedGroupTicketPrimaryFunction = null;
        mSelectedGroupTicketSupport = null;
        mSelectedTicketSetting = ticketSettingEvent.getTicketSettingInfo();
        switch (PrivateSelectionGroupTicketSetting.$SelectionGroupTicketSetting[mSelectedTicketSetting.ordinal()]) {
            case 1:
                getTmToast().makeToast("Version", Toast.LENGTH_LONG);
                if (true) {
                    this.mDrawerLayout.closeDrawer((int) GravityCompat.START);
                }
                break;

            case 2:
                AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this).setTitle(getString(R.string.tm_menu_exit)).setPositiveButton("Đồng ý", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getTmSharePreferencesUtils().clear();
                        Intent intent = new Intent(HomeActivity.this, SplashScreenActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    }
                }).setNegativeButton("Hủy", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                builder.create().show();
                if (true) {
                    this.mDrawerLayout.closeDrawer((int) GravityCompat.START);
                }
                break;
        }


    }


    @Subscribe
    public void onTicketSupportChangeBackGround(DashBroadChangeMenu broadChangeMenu) {
        getEventBus().post(new DashbroadChangeItemBackground(broadChangeMenu.getTicketSupport(), broadChangeMenu.getTitle()));
    }

    @Subscribe
    public void onAccountEvent(DashboardAccountEvent dashboardAccountEvent) {
    }

    @Override
    public void onBackPressed() {
        if (exit) {
            finish();
        } else {
            Toast.makeText(this, "Nhấn phím back thêm lần nữa sẽ thoát ra.",
                    Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 3 * 1000);
        }
    }


    private void setupGcm(){
        GCMClientManager pushClientManager = new GCMClientManager(this, getString(R.string.gcm_defaultSenderId));
        pushClientManager.registerIfNeeded(new GCMClientManager.RegistrationCompletedHandler() {
            @Override
            public void onSuccess(String registrationId, boolean isNewRegistration) {
                Log.d("tmt", "registrationId home : " + registrationId);
                getRegisterDeviceToken(registrationId);
            }

            @Override
            public void onFailure(String ex) {
                super.onFailure(ex);
            }
        });
    }

    private void getRegisterDeviceToken(String deviceTokenGcm){

        String key = "f23f7217E5a617294A1adBe756b180fa3F564e5fdc5e7f1fbfde6a5f5182599E2e01e16b";
        String email = getTmSharePreferencesUtils().getEmailUserLogin();
        String platform = "android";
        String signature = TmUtils.MD5(deviceTokenGcm + key + email + key + platform);

        getTmApiEndPointProvider().addRequest(PushNotificationEndPoint.registerDeviceToken(email, deviceTokenGcm, platform, signature, new Response.Listener<MyResponseObject>() {
            @Override
            public void onResponse(MyResponseObject response) {
                Log.d("tmt", " push noti : " + response.getErrorDescription());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onErrorListener(error);
            }
        }));

    }

    private void onErrorListener(VolleyError error) {
        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
            getTmToast().makeToast(getString(R.string.tm_error_connect), Toast.LENGTH_SHORT);
        } else if (error instanceof AuthFailureError) {
            Log.d("tmt", "AuthFailureError");
        } else if (error instanceof ServerError) {
            Log.d("tmt", "ServerError");
        } else if (error instanceof NetworkError) {
            Log.d("tmt", "NetworkError");
        } else if (error instanceof ParseError) {
            Log.d("tmt", "ParseError");
        } else {
            Log.d("tmt", "onErrorResponse : " + error.getMessage());
        }

    }
}
