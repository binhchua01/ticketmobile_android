package app.fpt.ticketmobile.app.ui.adapter.libs.adapterDelegates;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import app.fpt.ticketmobile.app.lib.TmGraphics;
import app.fpt.ticketmobile.app.ui.adapter.libs.utils.ViewUtils;
import app.fpt.ticketmobile.app.ui.lib.TicketSupport;
import app.fpt.ticketmobile.widget.views.CircularImageView;
import app.fpt.ticketmobile.R;
import app.fpt.ticketmobile.app.lib.Provider;
import app.fpt.ticketmobile.app.ui.adapter.libs.GroupTicketSupportInfo;

/**
 * Created by Administrator on 7/12/2016.
 */
public class DashGroupTicketSupportAdapter extends TmAdapterDelegates<GroupTicketSupportInfo, DashGroupTicketSupportAdapter.DashboardGroupTicketSupportAdapter>{

    private final TicketSupportCallback mCallback;
    private final TmGraphics mTmGraphics;
    private Provider<TicketSupport> mSelectedTicketSupportProvider = null;

    public interface TicketSupportCallback{
        void onTicketSupportSelected(TicketSupport ticketSupport, String title);
    }


    public DashGroupTicketSupportAdapter(int itemViewType, TmGraphics tmGraphics, Provider<TicketSupport> selectedPrimaryFunctionProvider, TicketSupportCallback callback) {
        super(itemViewType);
        this.mTmGraphics = tmGraphics;
        this.mSelectedTicketSupportProvider = selectedPrimaryFunctionProvider;
        this.mCallback = callback;
    }

    @Override
    public void onBindViewHolder(GroupTicketSupportInfo groupTicketSupportInfo, DashboardGroupTicketSupportAdapter holder) {
        holder.bind(groupTicketSupportInfo.getTicketSupport(), groupTicketSupportInfo.getTicketSupportTitle());
    }

    @Override
    public DashboardGroupTicketSupportAdapter onCreateViewHolder(ViewGroup parent) {
        return new DashboardGroupTicketSupportAdapter(ViewUtils.inflate(R.layout.entry_dashboard_ticket_support, parent, false));
    }


    public  final class DashboardGroupTicketSupportAdapter extends RecyclerView.ViewHolder implements View.OnClickListener{

        private final TextView mTicketSupportTitle;
        private final CircularImageView mTicketSupportImage;
        private TicketSupport mTicketSupport;
        private String mTitle;


        public DashboardGroupTicketSupportAdapter(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            mTicketSupportTitle = (TextView) itemView.findViewById(R.id.group_ticket_primary_function_title);
            mTicketSupportImage = (CircularImageView) itemView.findViewById(R.id.group_ticket_primary_function_image);
        }

        public void bind(TicketSupport ticketSupport, String ticketSupportTitle) {
            this.mTitle = ticketSupportTitle;
            this.mTicketSupport = ticketSupport;
            if (mSelectedTicketSupportProvider.provider() != null) {
                boolean isSelectedGroup = ((TicketSupport) mSelectedTicketSupportProvider.provider()).equals(ticketSupport);
                this.itemView.setActivated(isSelectedGroup);
            }else {
                this.itemView.setActivated(false);
            }
             this.mTicketSupportImage.setImageBitmap(mTmGraphics.getGroupTicketSupportBitmap(ticketSupport));
            this.mTicketSupportTitle.setText(ticketSupportTitle);
        }

        @Override
        public void onClick(View v) {
            v.setActivated(true);
            mCallback.onTicketSupportSelected(mTicketSupport, mTitle);
        }
    }
}
