package app.fpt.ticketmobile.app.jsonParsers.Fortype;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import app.fpt.ticketmobile.app.model.MyTicketInventoryUnFinishObject;
import app.fpt.ticketmobile.app.ui.adapter.ticketinventoryunfinished.TicketInventoryUnFinishedChild;
import app.fpt.ticketmobile.app.ui.adapter.ticketinventoryunfinished.TicketInventoryUnFinishedHeader;

/**
 * Created by Administrator on 6/23/2016.
 */
public abstract class MyTicketInventoryUnFinishedField {

    public static class Deserializer implements JsonDeserializer<MyTicketInventoryUnFinishObject>{

        @Override
        public MyTicketInventoryUnFinishObject deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            MyTicketInventoryUnFinishObject ticketInventoryObject = new MyTicketInventoryUnFinishObject();
            List<TicketInventoryUnFinishedHeader> ticketInventoryHeaderList = new ArrayList<>();
            Gson gson = new Gson();

            try {
                final JsonObject jsObj = json.getAsJsonObject();
                JsonObject jsonObject = jsObj.getAsJsonObject("Result");
                JsonArray jsonArray = jsonObject.getAsJsonArray("Data");
                for (int i = 0; i < jsonArray.size(); i++){

                    MyTicketInventoryUnFinishObject.QueueExistedTicket queueExistedTicket = gson.fromJson( jsonArray.get(i), MyTicketInventoryUnFinishObject.QueueExistedTicket.class);

                    List<TicketInventoryUnFinishedChild> ticketInventoryChildren = new ArrayList<>();

                    TicketInventoryUnFinishedChild inventoryChildNew = new TicketInventoryUnFinishedChild(queueExistedTicket, Integer.valueOf(-1), "Chưa xử lý", queueExistedTicket.getNew());
                    TicketInventoryUnFinishedChild inventoryChildInProgress = new TicketInventoryUnFinishedChild(queueExistedTicket, Integer.valueOf(0), "Đang xử lý", queueExistedTicket.getInProgress());
                    TicketInventoryUnFinishedChild inventoryChildNotClose = new TicketInventoryUnFinishedChild(queueExistedTicket, Integer.valueOf(3), "Đã xử lý ", queueExistedTicket.getNotClose());
                    TicketInventoryUnFinishedChild inventoryChildOverTime = new TicketInventoryUnFinishedChild(queueExistedTicket, Integer.valueOf(4), "Quá hạn", queueExistedTicket.getOverTime());

                    ticketInventoryChildren.add(inventoryChildNew);
                    ticketInventoryChildren.add(inventoryChildInProgress);
                    ticketInventoryChildren.add(inventoryChildNotClose);
                    ticketInventoryChildren.add(inventoryChildOverTime);

                    TicketInventoryUnFinishedHeader inventoryHeader = new TicketInventoryUnFinishedHeader(queueExistedTicket.getQueueName(), queueExistedTicket.getQueueID(), ticketInventoryChildren);
                    ticketInventoryHeaderList.add(inventoryHeader);
                }

                ticketInventoryObject.setTicketInventoryUnFinishedHeaderList(ticketInventoryHeaderList);

            }
            catch (Exception ex){
                Log.e("tmt", "ex : " + ex.getMessage().toString());
            }

            return ticketInventoryObject;
        }
    }
}
