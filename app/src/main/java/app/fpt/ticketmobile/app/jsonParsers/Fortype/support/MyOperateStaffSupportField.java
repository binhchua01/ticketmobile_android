package app.fpt.ticketmobile.app.jsonParsers.Fortype.support;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import app.fpt.ticketmobile.app.model.SpinnerObject;
import app.fpt.ticketmobile.app.model.support.MyOperateStaffSupportObject;

/**
 * Created by Administrator on 7/22/2016.
 */
public class MyOperateStaffSupportField {

    public static class Deserializer implements JsonDeserializer<MyOperateStaffSupportObject>{

        @Override
        public MyOperateStaffSupportObject deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            Gson gson = new Gson();
            MyOperateStaffSupportObject myOperateStaffSupportObject = new MyOperateStaffSupportObject();
            List<SpinnerObject> spinnerObjectList = new ArrayList<>();

            try{
                JsonObject jsObj = json.getAsJsonObject();
                JsonObject jsonObject = jsObj.getAsJsonObject("Result");

                myOperateStaffSupportObject = gson.fromJson(jsonObject, MyOperateStaffSupportObject.class);

                if (myOperateStaffSupportObject.getOperateStaffSupportList() != null && myOperateStaffSupportObject.getOperateStaffSupportList().size() > 0) {
                    for (int i = 0; i < myOperateStaffSupportObject.getOperateStaffSupportList().size(); i++) {
                        MyOperateStaffSupportObject.OperateStaffSupportObject operateStaffSupportObject = myOperateStaffSupportObject.getOperateStaffSupportList().get(i);
                        SpinnerObject spinnerObject = new SpinnerObject(String.valueOf(operateStaffSupportObject.getID()), operateStaffSupportObject.getName(), String.valueOf(operateStaffSupportObject.getEmail()));
                        spinnerObjectList.add(spinnerObject);
                    }
                    myOperateStaffSupportObject.setSpinnerObjectList(spinnerObjectList);
                }

            }catch (Exception e){
                Log.e("tmt", "MyVisorStaffSupportField : " + e.toString());
            }

            return myOperateStaffSupportObject;
        }
    }
}
