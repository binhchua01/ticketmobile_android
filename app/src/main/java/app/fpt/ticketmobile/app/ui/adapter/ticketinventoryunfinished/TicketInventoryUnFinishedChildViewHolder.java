package app.fpt.ticketmobile.app.ui.adapter.ticketinventoryunfinished;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ViewHolder.ChildViewHolder;

import app.fpt.ticketmobile.app.ui.activities.authenticatedActivities.TicketQueueAndStatusActivity;
import app.fpt.ticketmobile.R;

/**
 * Created by Administrator on 6/23/2016.
 */
public class TicketInventoryUnFinishedChildViewHolder extends ChildViewHolder {

    private TextView tvTicketInventoryTitle;
    private TextView tvTicketInventoryValue;
    private ImageView imgTicketInventoryState;
    private View childView = null;

    public TicketInventoryUnFinishedChildViewHolder(View childView) {
        super(childView);
        this.childView = childView;
        tvTicketInventoryTitle = (TextView) childView.findViewById(R.id.tm_child_ticket_inventory_title);
        tvTicketInventoryValue = (TextView) childView.findViewById(R.id.tm_child_ticket_inventory_value);
        imgTicketInventoryState = (ImageView) childView.findViewById(R.id.tm_child_ticket_inventory_state);
    }


    private final class PrivateTicketInventoryListener implements View.OnClickListener{
        private TicketInventoryUnFinishedChild ticketInventoryChild;
        private Context context;

        public PrivateTicketInventoryListener(Context context, TicketInventoryUnFinishedChild ticketInventoryChild){
            this.ticketInventoryChild = ticketInventoryChild;
            this.context = context;
        }

        @Override
        public void onClick(View v) {
           context.startActivity(TicketQueueAndStatusActivity.buildIntent(context, String.valueOf(ticketInventoryChild.getQueueExistedTicket().getQueueID()),
                   ticketInventoryChild.getQueueExistedTicket().getQueueName(), ticketInventoryChild.getTicketInventoryTitle(), String.valueOf(ticketInventoryChild.getTicketInventoryStatusId())));
        }
    }

    public void bind(TicketInventoryUnFinishedChild ticketInventoryChild){
        childView.setOnClickListener(new PrivateTicketInventoryListener(childView.getContext() ,ticketInventoryChild));
        tvTicketInventoryTitle.setText(ticketInventoryChild.getTicketInventoryTitle());
        tvTicketInventoryValue.setText(String.valueOf(ticketInventoryChild.getTicketInventoryValue().intValue()));
        switch (ticketInventoryChild.getTicketInventoryStatusId().intValue()){
            case -1:
                imgTicketInventoryState.setImageResource(R.drawable.ic_starts_green_color);
                tvTicketInventoryTitle.setTextColor(childView.getContext().getResources().getColor(android.R.color.black));
                break;
            case 0:
                imgTicketInventoryState.setImageResource(R.drawable.ic_starts_orange_color);
                tvTicketInventoryTitle.setTextColor(childView.getContext().getResources().getColor(android.R.color.darker_gray));
                break;
            case 3:
                imgTicketInventoryState.setImageResource(R.drawable.ic_starts_skyblue_color);
                tvTicketInventoryTitle.setTextColor(childView.getContext().getResources().getColor(android.R.color.darker_gray));
                break;
            case 4:
                imgTicketInventoryState.setImageResource(R.drawable.ic_starts_green_light_color);
                tvTicketInventoryTitle.setTextColor(childView.getContext().getResources().getColor(android.R.color.darker_gray));
                break;
        }
    }
}
