package app.fpt.ticketmobile.app.ui.adapter.libs.adapterDelegates;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import app.fpt.ticketmobile.app.model.support.ImportContractObject;

/**
 * Created by Administrator on 7/25/2016.
 */
public class ImportContractAdapterDelegates extends TmAdapterDelegates<ImportContractObject,  ImportContractAdapterDelegates.ImportContractViewHolder> {



    public class ImportContractViewHolder extends RecyclerView.ViewHolder {
        private ImportContractObject importContractObject;
//        private final TextView mTvContract;
//        private final TextView mTvContractName;
//        private final TextView mTvDomainIp;
//        private final TextView mTvAddress;


        public ImportContractViewHolder(View itemView) {
            super(itemView);
        }
    }

    public ImportContractAdapterDelegates(int itemViewType) {
        super(itemViewType);
    }

    @Override
    public void onBindViewHolder(ImportContractObject importContractObject, ImportContractViewHolder importContractViewHolder) {

    }

    @Override
    public ImportContractViewHolder onCreateViewHolder(ViewGroup viewGroup) {
        return null;
    }


}
