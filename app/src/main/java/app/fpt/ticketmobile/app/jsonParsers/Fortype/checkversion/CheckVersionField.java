package app.fpt.ticketmobile.app.jsonParsers.Fortype.checkversion;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import app.fpt.ticketmobile.app.model.checkversion.CheckVersionObject;

/**
 * Created by Administrator on 7/29/2016.
 */
public class CheckVersionField {

    public static final class Deserializer implements JsonDeserializer<CheckVersionObject>{

        @Override
        public CheckVersionObject deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            Gson gson = new Gson();
            CheckVersionObject checkVersionObject = null;

            try {
                final JsonObject jsObj = json.getAsJsonObject();
                JsonObject jsonObject = jsObj.getAsJsonObject("Result");
                checkVersionObject = gson.fromJson(jsonObject, CheckVersionObject.class);
            }
            catch (Exception ex){
                Log.e("tmt", "CheckVersionField : " + ex.getMessage().toString());
            }

            return checkVersionObject;

        }
    }
}
