package app.fpt.ticketmobile.app.ui.adapter.ticketinventorynotassign;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ViewHolder.ChildViewHolder;

import app.fpt.ticketmobile.R;
import app.fpt.ticketmobile.app.ui.activities.authenticatedActivities.TicketDetailActivity;

/**
 * Created by ToanMaster on 7/1/2016.
 */
public class TicketInventoryNotAssignChildViewHolder extends ChildViewHolder {

    private TextView tvTicketInventoryNotAssignNumberCode;
    private TextView tvTicketInventoryNotAssignTitle;
    private TextView tvTicketInventoryNotAssignUnit;
    private TextView tvTicketInventoryNotAssignCreatedTime;
    private TextView tvTicketInventoryNotAssignEstimatedTime;
    private View childView = null;

    public TicketInventoryNotAssignChildViewHolder(View childView) {
        super(childView);
        this.childView = childView;
        tvTicketInventoryNotAssignNumberCode = (TextView) childView.findViewById(R.id.tm_tv_ticket_number_code);
        tvTicketInventoryNotAssignTitle = (TextView) childView.findViewById(R.id.tm_tv_ticket_title);
        tvTicketInventoryNotAssignUnit = (TextView) childView.findViewById(R.id.tm_tv_ticket_unit);
        tvTicketInventoryNotAssignCreatedTime = (TextView) childView.findViewById(R.id.tm_tv_ticket_created_time);
        tvTicketInventoryNotAssignEstimatedTime = (TextView) childView.findViewById(R.id.tm_tv_ticket_estimated_time);
    }


    private final class PrivateTicketInventoryListener implements View.OnClickListener{
        private TicketInventoryNotAssignChild ticketInventoryNotAssignChild;
        private Context context;

        public PrivateTicketInventoryListener(Context context, TicketInventoryNotAssignChild ticketInventoryNotAssignChild){
            this.ticketInventoryNotAssignChild = ticketInventoryNotAssignChild;
            this.context = context;
        }

        @Override
        public void onClick(View v) {

         //   if (TmApplication.getmTMAPPLICATION().getAppComponent().getTmConnectivityInfo().isInterfaceConnected()) {
                context.startActivity(TicketDetailActivity.buildIntent(context, String.valueOf(ticketInventoryNotAssignChild.getTicketInventoryNotAssign().getTicketID())));
           // }else {
           //     TmApplication.getmTMAPPLICATION().getAppComponent().getTmToast().makeToast("Không có mạng...", 2500);
           // }
        }
    }

    public void bind(TicketInventoryNotAssignChild ticketInventoryNotAssignChild){
        childView.setOnClickListener(new PrivateTicketInventoryListener(childView.getContext() ,ticketInventoryNotAssignChild));
        tvTicketInventoryNotAssignNumberCode.setText(ticketInventoryNotAssignChild.getTicketInventoryNotAssign().getTicketID());
        tvTicketInventoryNotAssignTitle.setText(ticketInventoryNotAssignChild.getTicketInventoryNotAssign().getTitle());
        tvTicketInventoryNotAssignUnit.setText(ticketInventoryNotAssignChild.getTicketInventoryNotAssign().getIssueName());
        tvTicketInventoryNotAssignCreatedTime.setText(ticketInventoryNotAssignChild.getTicketInventoryNotAssign().getCreatedDate());
        tvTicketInventoryNotAssignEstimatedTime.setText(ticketInventoryNotAssignChild.getTicketInventoryNotAssign().getExistTime());
    }
}
