package app.fpt.ticketmobile.app.networking.apiEndpoints;

/**
 * Created by ToanMaster on 6/23/2016.
 */
public class Constants {
//      private static String ROOT = "http://app.ticket.fpt.net/application/index/";
    //private static String API = "http://api.ticket.fpt.net/application/index/";
    private static String ROOT = "http://api.ticket.fpt.net/application/index/";

    public static final String SING_IN_ENDPOINT =  ROOT + "login";
    public static final String MY_GET_TICKET_END_POINT = ROOT + "myTicket";
    public static final String MY_GET_TICKET_OF_USER_CREATED_END_POINT = ROOT + "myCreate";
    public static final String MY_GET_TICKET_INVENTORY_NOT_ASSIGN_END_POINT = ROOT + "newTicket";
    public static final String MY_GET_TICKET_INVENTORY_UNFINISHED_END_POINT = ROOT + "queueExistedTicket";

    public static final String MY_GET_TICKET_QUEUE_BY_STATUES = ROOT + "ticketByStatus";
    public static final String MY_GET_TICKET_DETAIL = ROOT + "ticketInfo";
    public static final String MY_GET_TICKET_DEVICE = ROOT + "ticketDevice";

    public static final String MY_GET_ISSUE_LIST = ROOT + "issueList";
    public static final String MY_GET_EFFECT_LIST = ROOT + "effectList";
    public static final String MY_GET_REASON_LIST = ROOT + "reasonList";
    public static final String MY_GET_REASON_DETAIL_LIST = ROOT + "ReasonDetailList";
    public static final String MY_GET_SERVICE_TYPE_LIST = ROOT + "serviceList";
    public static final String MY_GET_CUS_TYPE_LIST = ROOT + "cusTypeList";
    public static final String MY_GET_QUEUE_LIST =  ROOT + "queueList";
    public static final String MY_GET_STATUS_LIST = ROOT + "ticketStatusList";
    public static final String MY_GET_OPERATE_STAFF_LIST = ROOT + "operateStaff";
    public static final String MY_GET_VISOR_STAFF_LIST = ROOT + "visorStaff";
    public static final String MY_GET_PROCESS_STAFF_LIST = ROOT + "processStaff";

    public static final String MY_GET_ESTIMATED_DATE = ROOT + "EstimatedDate";
    public static final String MY_GET_CUS_QTY = ROOT + "GetCusQty";
    public static final String MY_GET_PHONE_STAFF =  ROOT + "phoneStaff";

    public static final String MY_GET_RESPONSE_INFO = ROOT + "reponseInfo";
    public static final String MY_GET_RESPONSE_DETAIL = ROOT + "responseDetail";

    public static final String MY_UPDATE_TICKET_DEVICE = ROOT + "ticketDeviceUpdate";
    public static final String MY_UPDATE_TICKET_DETAIL = ROOT + "ticketUpdate";
    public static final String MY_UPDATE_RESPONSE_SEND = ROOT + "responseSend";

    // api create support
    public static final String MY_GET_SUPPORT_BUSINESS_AREA_LIST = ROOT + "GetBusinessArea";
    public static final String MY_GET_SUPPORT_BRANCH_LOCATION_LIST = ROOT + "GetBranchLocation";
    public static final String MY_GET_SUPPORT_SERVICE_TYPE_LIST = ROOT + "GetServiceType";
    public static final String MY_GET_SUPPORT_CUSTOM_TYPE_LIST =  ROOT + "GetCustomerType";
    public static final String MY_GET_SUPPORT_DESCRIPTION_LIST = ROOT + "GetInput";
    public static final String MY_GET_SUPPORT_REASON_LIST = ROOT + "GetRFO";
    // api QueueList  ProcessStaff PhoneStaff OperateStaff VisorStaff TicketStatusList
    public static final String MY_GET_SUPPORT_CREATED_TICKET_LIST = ROOT + "MyCreateSupport";
    public static final String MY_GET_SUPPORT_PRIORITY_LIST = ROOT + "GetPriority";
    public static final String MY_GET_SUPPORT_QUEUE_LIST = ROOT + "QueueList";
    public static final String MY_GET_SUPPORT_OPERATE_STAFF = ROOT + "OperateStaff";
    public static final String MY_GET_SUPPORT_VISOR_STAFF = ROOT + "VisorStaff";
    public static final String MY_GET_SUPPORT_HT_ESTIMATED_TIME = ROOT + "HTEstimatedTime";
    public static final String MY_GET_SUPPORT_CUSTOMER_LIST = ROOT + "CustomerList";

    public static final String MY_UPDATED_CREATED_TICKET_SUPPORT  = ROOT + "CreateSupport";

    public static final String MY_TICKET_CHECK_VERSION = "http://api.ticket.fpt.net/application/check-version";
    public static final String MY_TICKET_REGISTER_PUSH_NOTIFICATION = ROOT + "register";

    public static final String KONG_TOKEN_URL = "http://sapi.fpt.vn/token/GenerateToken";

    public static final String LOGIN_IAM_WCF = "https://sapi.fpt.vn/ticketapi-staging/mobile/api/AuthenMobile/LoginWithIamWcf";
    public static final String URL_STAGING_DEVICE_TOKEN = "https://sapi.fpt.vn/ticketapi-staging/mobile/api/AuthenMobile/GetIamConfig?ClientID=mobiticket2.0_portal";
}
