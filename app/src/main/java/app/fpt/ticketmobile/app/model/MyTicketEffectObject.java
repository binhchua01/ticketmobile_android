package app.fpt.ticketmobile.app.model;

import java.util.List;

/**
 * Created by Administrator on 6/28/2016.
 */
public class MyTicketEffectObject {

    private List<EffectObject> Data;
    private List<SpinnerObject> mSpinnerObjectList;

    public List<EffectObject> getData() {
        return Data;
    }


    public List<SpinnerObject> getSpinnerObjectList() {
        return mSpinnerObjectList;
    }

    public void setSpinnerObjectList(List<SpinnerObject> mSpinnerObjectList) {
        this.mSpinnerObjectList = mSpinnerObjectList;
    }

    public class EffectObject{
        private Integer EffectID;
        private String EffectName;
        private Integer IsEffect;

        public Integer getEffectID() {
            return EffectID;
        }

        public void setEffectID(Integer effectID) {
            EffectID = effectID;
        }

        public String getEffectName() {
            return EffectName;
        }

        public void setEffectName(String effectName) {
            EffectName = effectName;
        }

        public Integer getIsEffect() {
            return IsEffect;
        }

        public void setIsEffect(Integer isEffect) {
            IsEffect = isEffect;
        }
    }
}
