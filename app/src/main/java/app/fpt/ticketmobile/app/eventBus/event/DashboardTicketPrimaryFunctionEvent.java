package app.fpt.ticketmobile.app.eventBus.event;

import app.fpt.ticketmobile.app.eventBus.Event;
import app.fpt.ticketmobile.app.ui.lib.TicketPrimaryFunction;

/**
 * Created by Administrator on 7/13/2016.
 */
public class DashboardTicketPrimaryFunctionEvent implements Event{

    private TicketPrimaryFunction mTicketPrimaryFunction;
    private String mTitle;

    public TicketPrimaryFunction getTicketPrimaryFunction() {
        return mTicketPrimaryFunction;
    }

    public void setTicketPrimaryFunction(TicketPrimaryFunction mTicketPrimaryFunction) {
        this.mTicketPrimaryFunction = mTicketPrimaryFunction;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public DashboardTicketPrimaryFunctionEvent(TicketPrimaryFunction mTicketPrimaryFunction, String mTitle) {
        this.mTicketPrimaryFunction = mTicketPrimaryFunction;
        this.mTitle = mTitle;
    }
}
