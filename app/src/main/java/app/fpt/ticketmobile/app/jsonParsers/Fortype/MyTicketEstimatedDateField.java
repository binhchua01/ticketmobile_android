package app.fpt.ticketmobile.app.jsonParsers.Fortype;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import app.fpt.ticketmobile.app.model.MyTicketEstimatedDateObject;

/**
 * Created by Administrator on 7/5/2016.
 */
public class MyTicketEstimatedDateField {

    public static final class Deserializer implements JsonDeserializer<MyTicketEstimatedDateObject>{

        @Override
        public MyTicketEstimatedDateObject deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            Gson gson = new Gson();
            MyTicketEstimatedDateObject myTicketEstimatedDateObject = null;

            try{
                JsonObject jsonObject = json.getAsJsonObject();
                JsonObject jsResult = jsonObject.getAsJsonObject("Result");

                JsonArray jsArray = jsResult.getAsJsonArray("Data");
                myTicketEstimatedDateObject = gson.fromJson(jsArray.get(0), MyTicketEstimatedDateObject.class);

            }catch(Exception e){
                Log.e("tmt", " MyTicketEstimatedDateField : Deserializer " + e.toString());
            }
            return myTicketEstimatedDateObject;
        }
    }
}
