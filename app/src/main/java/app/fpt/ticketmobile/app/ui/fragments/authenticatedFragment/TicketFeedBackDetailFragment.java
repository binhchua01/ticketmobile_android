package app.fpt.ticketmobile.app.ui.fragments.authenticatedFragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.squareup.otto.Subscribe;

import app.fpt.ticketmobile.app.eventBus.event.TicketFeedbackReplyAllEvent;
import app.fpt.ticketmobile.R;
import app.fpt.ticketmobile.app.eventBus.event.TicketFeedbackReplyEvent;
import app.fpt.ticketmobile.app.eventBus.event.TicketShowViewEvent;
import app.fpt.ticketmobile.app.lib.utils.TmUtils;
import app.fpt.ticketmobile.app.model.MyTicketFeedbackDetailObject;
import app.fpt.ticketmobile.app.networking.apiEndpoints.MyTicketResponseEndpoint;
import app.fpt.ticketmobile.app.ui.activities.authenticatedActivities.TicketFeedBackDetailSendActivity;
import app.fpt.ticketmobile.app.ui.fragments.TmBaseFragment;
import app.fpt.ticketmobile.app.ui.lib.FragmentRefreshBasedSwipeRefreshLayoutHandler;

/**
 * Created by Administrator on 7/4/2016.
 */
public class TicketFeedBackDetailFragment extends TmBaseFragment<TicketFeedBackDetailFragment.Callback> implements SwipeRefreshLayout.OnRefreshListener {

    private static final String EXTRA_TICKET_RESPONSE_ID_KEY = "extra_ticket_response_id_key";
    private static final String EXTRA_TICKET_ID_KEY = "extra_ticket_id_key";

    private static final String EXTRA_REQUEST_TICKET_FEED_BACK_DETAIL_TAG = "extra_request_ticket_feed_back_detail_tag";

    private String TICKET_ID = null;
    private String TICKET_RESPONSE_ID = null;

    private TextView tvResponseTitle;
    private TextView tvResponseTime;
    private TextView tvResponseFromUser;
    private TextView tvResponseToUser;
    private TextView tvResponseToCC;
    private WebView wbResponseBody;
    private View view;
    private  SwipeRefreshLayout swipeRefreshLayout;

    public interface Callback{

    }

    public TicketFeedBackDetailFragment() {
        super(Callback.class, Integer.valueOf(R.layout.fragment_ticket_feedback_detail), false, true);
    }

    public static TicketFeedBackDetailFragment buildIntent(String ticketId, String ticketResponseId){
        Bundle bundle = new Bundle();
        bundle.putString(EXTRA_TICKET_ID_KEY, ticketId);
        bundle.putString(EXTRA_TICKET_RESPONSE_ID_KEY, ticketResponseId);

        TicketFeedBackDetailFragment ticketFeedBackDetailFragment = new TicketFeedBackDetailFragment();
        ticketFeedBackDetailFragment.setArguments(bundle);
        return ticketFeedBackDetailFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        extrasArguments();
    }

    @Override
    protected void onTmActivityCreated(View parent, Bundle savedInstanceState) {
        super.onTmActivityCreated(parent, savedInstanceState);
        setupViews(parent);
    }

    private void setupViews(View parent) {
        view = parent.findViewById(R.id.tm_layout_ticket_feedback_body);
        tvResponseTitle = (TextView) parent.findViewById(R.id.tm_tv_ticket_response_title);
        tvResponseTime = (TextView) parent.findViewById(R.id.tm_tv_ticket_response_time);
        tvResponseFromUser = (TextView) parent.findViewById(R.id.tm_tv_ticket_response_from_user);
        tvResponseToUser = (TextView) parent.findViewById(R.id.tm_tv_ticket_response_to_user);
        tvResponseToCC = (TextView) parent.findViewById(R.id.tm_tv_ticket_response_to_cc);
        wbResponseBody = (WebView) parent.findViewById(R.id.tm_web_view_response_body);

        swipeRefreshLayout = (SwipeRefreshLayout) parent.findViewById(R.id.swipe_refresh_layout);
        setFragmentRefreshAnimationHandler(new FragmentRefreshBasedSwipeRefreshLayoutHandler(swipeRefreshLayout, this, Integer.valueOf(R.color.tm_swipe_refresh_color)));
        if (!isShowingRefreshAnimation()) {
            swipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    showRefreshAnimation();
                }
            }, 500);
        }
        swipeRefreshLayout.setEnabled(false);
    }


    private void extrasArguments(){
        if (getArguments() != null){
            TICKET_ID = getArguments().getString(EXTRA_TICKET_ID_KEY);
            TICKET_RESPONSE_ID = getArguments().getString(EXTRA_TICKET_RESPONSE_ID_KEY);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        getTmApiEndPointProvider().cancelAllRequest(EXTRA_REQUEST_TICKET_FEED_BACK_DETAIL_TAG);
    }




    @Override
    public void onRefresh() {
        autoSynced();
    }




    @Override
    protected void autoSynced() {
        super.autoSynced();
        if (!isShowingRefreshAnimation()) {
            showRefreshAnimation();
        }

        if (TICKET_RESPONSE_ID != null) {
            getTmApiEndPointProvider().addRequest(MyTicketResponseEndpoint.getMyTicketFeedbackDetailList(TICKET_RESPONSE_ID, new Response.Listener<MyTicketFeedbackDetailObject>() {
                @Override
                public void onResponse(MyTicketFeedbackDetailObject ticketFeedbackDetailObject) {
                    if (getActivity() != null) {
                        if (ticketFeedbackDetailObject != null) {
                            tvResponseTitle.setText(ticketFeedbackDetailObject.getSubject());
                            tvResponseTime.setText(ticketFeedbackDetailObject.getCreatedDate());
                            tvResponseFromUser.setText(ticketFeedbackDetailObject.getFromAdr());
                            tvResponseToUser.setText(ticketFeedbackDetailObject.getToAdr());
                            tvResponseToCC.setText(ticketFeedbackDetailObject.getCc());
                            wbResponseBody.loadData(TmUtils.DecodeBase64(ticketFeedbackDetailObject.getBodyHTML()), "text/html; charset=UTF-8", null);
                            view.setVisibility(View.VISIBLE);
                            stopRefreshAnimation();
                            getEventBus().post(new TicketShowViewEvent(ticketFeedbackDetailObject));
                        } else {
                            swipeRefreshLayout.setEnabled(true);
                        }

                        stopRefreshAnimation();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    swipeRefreshLayout.setFocusable(true);
                    swipeRefreshLayout.setEnabled(true);
                    onErrorListener(error);
                }
            }), EXTRA_REQUEST_TICKET_FEED_BACK_DETAIL_TAG);

        }else {
            swipeRefreshLayout.setFocusable(true);
            swipeRefreshLayout.setEnabled(true);
            stopRefreshAnimation();
        }
    }

    private void onErrorListener(VolleyError error) {
        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
            getTmToast().makeToast(getString(R.string.tm_error_connect), Toast.LENGTH_SHORT);
        } else if (error instanceof AuthFailureError) {
            Log.d("tmt", "AuthFailureError");
        } else if (error instanceof ServerError) {
            Log.d("tmt", "ServerError");
        } else if (error instanceof NetworkError) {
            Log.d("tmt", "NetworkError");
        } else if (error instanceof ParseError) {
            Log.d("tmt", "ParseError");
        } else {
            Log.d("tmt", "onErrorResponse : " + error.getMessage());
        }
        stopRefreshAnimation();
    }

    @Subscribe
    public void onFeedbackReplyEvent(TicketFeedbackReplyEvent ticketFeedbackReplyEvent){
        if (ticketFeedbackReplyEvent.getMyTicketFeedbackDetailObject() != null) {
            getActivity().startActivity(TicketFeedBackDetailSendActivity.buildIntent(getActivity(), TICKET_ID, ticketFeedbackReplyEvent.getMyTicketFeedbackDetailObject(), false));
        }
    }

    @Subscribe
    public void onFeedbackReplyAllEvent(TicketFeedbackReplyAllEvent ticketFeedbackReplyAllEvent){
        if (ticketFeedbackReplyAllEvent.getMyTicketFeedbackDetailObject() != null) {
            getActivity().startActivity(TicketFeedBackDetailSendActivity.buildIntent(getActivity(), TICKET_ID, ticketFeedbackReplyAllEvent.getMyTicketFeedbackDetailObject(), true));
        }
    }
}
