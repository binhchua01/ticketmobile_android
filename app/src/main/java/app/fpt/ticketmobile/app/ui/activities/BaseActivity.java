package app.fpt.ticketmobile.app.ui.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.balysv.materialmenu.MaterialMenuDrawable;
import com.balysv.materialmenu.extras.toolbar.MaterialMenuIconToolbar;
import com.google.android.gms.analytics.GoogleAnalytics;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Random;

import app.fpt.ticketmobile.app.eventBus.EventBus;
import app.fpt.ticketmobile.app.tracker.interfaces.TmTrackerAnalytics;
import app.fpt.ticketmobile.app.ui.lib.TmToast;
import app.fpt.ticketmobile.R;
import app.fpt.ticketmobile.app.core.TmApplication;
import app.fpt.ticketmobile.app.db.TmSharePreferencesUtils;
import app.fpt.ticketmobile.app.di.component.AppComponent;
import app.fpt.ticketmobile.app.lib.TmApiEndPointProvider;
import app.fpt.ticketmobile.app.lib.TmConnectivityInfo;

/**
 * Created by ToanMaster on 6/13/2016.
 */
public abstract class BaseActivity extends AppCompatActivity  {

    private Integer mLayoutResId;
    private Toolbar mToolbar;
    private final Collection<Object> eventBusListeners;
    private boolean isEventBusRegistrationDone;
    private boolean isTmActivityResumed;
    private MaterialMenuIconToolbar materialMenu;
    private int actionBarMenuState;


    protected BaseActivity(Integer layoutResId){
        this.mLayoutResId = layoutResId;
        this.mToolbar = null;
        this.eventBusListeners = new HashSet();
        addEventBusListeners(this);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (mLayoutResId != null){
            setContentView(mLayoutResId.intValue());
            mToolbar = (Toolbar) findViewById(R.id.tm_toolbar_app);
            if (mToolbar != null){
                setSupportActionBar(mToolbar);
                getSupportActionBar().setDisplayShowHomeEnabled(false);
                getSupportActionBar().setDisplayShowCustomEnabled(true);
                getSupportActionBar().setDisplayShowTitleEnabled(false);
//                mToolbar.setNavigationIcon(R.drawable.ic_arrow_back);
//                mToolbar.setContentInsetsAbsolute(0, 0);
//                mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        finish();
//                    }
//                });
                setupToolbar(mToolbar);


//                mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
//                    @Override public void onClick(View v) {
//                        // random state
//                        actionBarMenuState = generateState(actionBarMenuState);
//                        materialMenu.animateState(MaterialMenuDrawable.IconState.ARROW);
//                    }
//                });
//
//                materialMenu = new MaterialMenuIconToolbar(this, Color.WHITE, Stroke.THIN) {
//                    @Override public int getToolbarViewId() {
//                        return R.id.tm_toolbar_app;
//                    }
//                };

            }

            getTmTrackerAnalytics().trackScreenView(this.getClass().getSimpleName().toString());
            getTmTrackerAnalytics().trackEvent("TrackInit", this.getClass().getSimpleName(), "init " + this.getClass().getSimpleName().toString());
            Log.d("tmt", "activity name : " + this.getClass().getSimpleName());
        }

    }

    public static int generateState(int previous) {
        int generated = new Random().nextInt(4);
        return generated != previous ? generated : generateState(previous);
    }

    public static MaterialMenuDrawable.IconState intToState(int state) {
        switch (state) {
            case 0:
                return MaterialMenuDrawable.IconState.BURGER;
            case 1:
                return MaterialMenuDrawable.IconState.ARROW;
            case 2:
                return MaterialMenuDrawable.IconState.X;
            case 3:
                return MaterialMenuDrawable.IconState.CHECK;
        }
        throw new IllegalArgumentException("Must be a number [0,3)");
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if (materialMenu != null) {
            materialMenu.syncState(savedInstanceState);
        }
    }

    @Override protected void onSaveInstanceState(Bundle outState) {
        if (materialMenu != null) {
            materialMenu.onSaveInstanceState(outState);
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onStart() {
        super.onStart();
        GoogleAnalytics.getInstance(getApplicationContext()).reportActivityStart(this);


    }

    @Override
    public void onResume() {
        super.onResume();
        isTmActivityResumed = true;
        performEventBusRegistration();
    }

    @Override
    public void onPause() {
        isTmActivityResumed = false;
        performEventBusUnregistration();
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(getApplicationContext()).reportActivityStop(this);
    }

    protected void setupToolbar(Toolbar mToolbar){

    }


    public Toolbar getToolbar() {
        return mToolbar;
    }

    protected final void addEventBusListeners(Object... listeners) {
        Collections.addAll(this.eventBusListeners, Arrays.copyOf(listeners, listeners.length));
        if (this.isEventBusRegistrationDone) {
            for (Object listener : listeners) {
                getEventBus().register(listener);
            }
        }
    }

    protected final void removeEventBusListeners(Object... listeners) {
        for (Object listener : listeners) {
            getEventBus().unregister(listener);
            this.eventBusListeners.remove(listener);
        }
    }

    private final void performEventBusRegistration() {
        for (Object listener : this.eventBusListeners) {
            getEventBus().register(listener);
        }
        this.isEventBusRegistrationDone = true;
    }

    private final void performEventBusUnregistration() {
        for (Object listener : this.eventBusListeners) {
            getEventBus().unregister(listener);
        }
        this.isEventBusRegistrationDone = false;
    }

    protected AppComponent getAppComponent(){
        AppComponent mAppComponent = ((TmApplication) getApplication()).getAppComponent();
        return mAppComponent;
    }

    protected EventBus getEventBus(){
        checkIfNeedIsNull();
        return getAppComponent().getEventBus();
    }

    protected TmToast getTmToast(){
        checkIfNeedIsNull();
        return getAppComponent().getTmToast();
    }

    protected TmApiEndPointProvider getTmApiEndPointProvider(){
        checkIfNeedIsNull();
        return getAppComponent().getTmApiEndPointProvider();
    }

    protected TmConnectivityInfo getTmConnectivityInfo(){
        checkIfNeedIsNull();
        return getAppComponent().getTmConnectivityInfo();
    }

    protected TmSharePreferencesUtils getTmSharePreferencesUtils(){
        checkIfNeedIsNull();
        return getAppComponent().getTmSharePreferencesUtils();
    }

    protected final TmTrackerAnalytics getTmTrackerAnalytics(){
        checkIfNeedIsNull();
        return getAppComponent().getTmTrackerAnalytics();
    }

    private void checkIfNeedIsNull(){
        if (getAppComponent() == null){
            throw new RuntimeException("AppComponent is null !!!");
        }
    }
}
