package app.fpt.ticketmobile.app.model.support;

import java.util.List;

import app.fpt.ticketmobile.app.model.SpinnerObject;

/**
 * Created by Administrator on 7/7/2016.
 */
public class MyTicketCustomTypeObject {

    private List<CustomTypeObject> Data;
    private List<SpinnerObject> mSpinnerObjectList;

    public List<CustomTypeObject> getCustomTypeList() {
        return Data;
    }

    public void setCustomTypeList(List<CustomTypeObject> data) {
        Data = data;
    }

    public List<SpinnerObject> getSpinnerObjectList() {
        return mSpinnerObjectList;
    }

    public void setSpinnerObjectList(List<SpinnerObject> mSpinnerObjectList) {
        this.mSpinnerObjectList = mSpinnerObjectList;
    }

    public class CustomTypeObject{
        private Integer ID;
        private String Name;

        public Integer getID() {
            return ID;
        }

        public void setID(Integer ID) {
            this.ID = ID;
        }

        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }
    }
}
