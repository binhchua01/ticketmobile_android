package app.fpt.ticketmobile.app.ui.adapter.ticketinventoryunfinished;

import app.fpt.ticketmobile.app.model.MyTicketInventoryUnFinishObject;

/**
 * Created by ToanMaster on 6/23/2016.
 */
public class TicketInventoryUnFinishedChild {
    private MyTicketInventoryUnFinishObject.QueueExistedTicket mQueueExistedTicket ;
    private Integer mTicketInventoryStatusId;
    private String mTicketInventoryTitle;
    private Integer mTicketInventoryValue;

    public Integer getTicketInventoryStatusId() {
        return mTicketInventoryStatusId;
    }

    public void setTicketInventoryStatusId(Integer ticketInventoryStatusId) {
        this.mTicketInventoryStatusId = ticketInventoryStatusId;
    }

    public String getTicketInventoryTitle() {
        return mTicketInventoryTitle;
    }

    public void setTicketInventoryTitle(String ticketInventoryTitle) {
        this.mTicketInventoryTitle = ticketInventoryTitle;
    }

    public Integer getTicketInventoryValue() {
        return mTicketInventoryValue;
    }

    public void setTicketInventoryValue(Integer ticketInventoryValue) {
        this.mTicketInventoryValue = ticketInventoryValue;
    }

    public MyTicketInventoryUnFinishObject.QueueExistedTicket getQueueExistedTicket() {
        return mQueueExistedTicket;
    }

    public void setQueueExistedTicket(MyTicketInventoryUnFinishObject.QueueExistedTicket mQueueExistedTicket) {
        this.mQueueExistedTicket = mQueueExistedTicket;
    }

    public TicketInventoryUnFinishedChild(MyTicketInventoryUnFinishObject.QueueExistedTicket queueExistedTicket, Integer mTicketInventoryStatusId, String mTicketInventoryTitle, Integer mTicketInventoryValue) {
        this.mQueueExistedTicket = queueExistedTicket;
        this.mTicketInventoryStatusId = mTicketInventoryStatusId;
        this.mTicketInventoryTitle = mTicketInventoryTitle;
        this.mTicketInventoryValue = mTicketInventoryValue;
    }
}
