package app.fpt.ticketmobile.app.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import app.fpt.ticketmobile.R;
import app.fpt.ticketmobile.app.ui.adapter.dashboard.Header;
import app.fpt.ticketmobile.app.ui.adapter.dashboard.ItemDashboard;

/**
 * Created by Administrator on 7/8/2016.
 */
public class DashboardAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;

    private Header header;
    private List<ItemDashboard> itemDashboardList;

    public DashboardAdapter(Header header, List<ItemDashboard> itemDashboardList) {
        this.header = header;
        this.itemDashboardList = itemDashboardList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.entry_navigation_header, parent, false);
            return new HeaderViewHolder(v);
        } else if (viewType == TYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.entry_navigation_dashboard_item, parent, false);
            return new ItemDashboardViewHolder(v);
        }
        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof HeaderViewHolder) {
            HeaderViewHolder headerViewHolder = (HeaderViewHolder) holder;
            headerViewHolder.blind(header);
        } else if (holder instanceof ItemDashboardViewHolder) {
            ItemDashboard itemDashboard = itemDashboardList.get(position - 1);
            ItemDashboardViewHolder dashboardViewHolder = (ItemDashboardViewHolder) holder;
            dashboardViewHolder.bind(itemDashboard);
        }
    }

    @Override
    public int getItemCount() {
        return itemDashboardList.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position)) {
            return TYPE_HEADER;
        }
        return TYPE_ITEM;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    public final class ItemDashboardViewHolder extends RecyclerView.ViewHolder {

        private TextView tvTicketDashboardTitle;
        private ImageView imgTicketDashboardImage;
        private TextView tvTicketDashboardCount;

        public ItemDashboardViewHolder(View parent) {
            super(parent);
            tvTicketDashboardTitle = (TextView) parent.findViewById(R.id.tm_tv_ticket_title);
            imgTicketDashboardImage = (ImageView) parent.findViewById(R.id.tm_img_ticket_image);
            tvTicketDashboardCount = (TextView) parent.findViewById(R.id.tm_tv_ticket_count);
        }

        public void bind(ItemDashboard itemDashboard){
            tvTicketDashboardTitle.setText(itemDashboard.getNameDashboard());
            imgTicketDashboardImage.setBackgroundResource(itemDashboard.getResIdDashboard());
            tvTicketDashboardCount.setVisibility(View.GONE);

        }
    }

    public final class HeaderViewHolder extends RecyclerView.ViewHolder {

        private TextView tvUserName;
        private TextView tvUserEmail;

        public HeaderViewHolder(View parent) {
            super(parent);
            tvUserName = (TextView) parent.findViewById(R.id.tm_tv_user_name);
            tvUserEmail = (TextView) parent.findViewById(R.id.tm_tv_user_email);
        }

        public void blind(Header header){
            tvUserName.setText(header.getUserName());
            tvUserEmail.setText(header.getUserEmail());
        }

    }

}
