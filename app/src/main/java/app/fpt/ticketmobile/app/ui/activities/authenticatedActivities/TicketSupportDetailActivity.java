package app.fpt.ticketmobile.app.ui.activities.authenticatedActivities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import java.util.List;

import app.fpt.ticketmobile.R;
import app.fpt.ticketmobile.app.eventBus.event.ImportContractObjectEvent;
import app.fpt.ticketmobile.app.model.support.ImportContractObject;
import app.fpt.ticketmobile.app.ui.activities.TmBaseActivity;
import app.fpt.ticketmobile.app.ui.fragments.authenticatedFragment.TicketSupportDetailFragment;
import app.fpt.ticketmobile.app.ui.fragments.dialogFragment.ImportContractDialogFragment;

/**
 * Created by Administrator on 7/25/2016.
 */
public class TicketSupportDetailActivity extends TmBaseActivity  implements ImportContractDialogFragment.ImportContractDialogCallback {

    private static final String EXTRA_TICKET_CODE_INTENT_KEY = "extra_ticket_code_intent_key";
    private static final String FRAGMENT_TICKET_SUPPORT_DETAIL_TAG = "fragment_feed_back_tag";

    public TicketSupportDetailActivity() {
        super(R.layout.activity_ticket_support_detail);
    }


    public static Intent buildIntent(Context context, String ticketId){
        Intent intent = new Intent(context, TicketSupportDetailActivity.class);
        intent.putExtra(EXTRA_TICKET_CODE_INTENT_KEY, ticketId);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        extraIntent();
    }

    private void extraIntent() {
        Intent intent = getIntent();
        if (intent != null){
            String ticketId = intent.getStringExtra(EXTRA_TICKET_CODE_INTENT_KEY);
            setupIsFragmentIfNeed(ticketId);
        }
    }

    private void setupIsFragmentIfNeed(String ticketId) {

        TicketSupportDetailFragment ticketFeedBackFragment = (TicketSupportDetailFragment) getSupportFragmentManager().findFragmentByTag(FRAGMENT_TICKET_SUPPORT_DETAIL_TAG);
        if (ticketFeedBackFragment == null){
            ticketFeedBackFragment = TicketSupportDetailFragment.getInstances(ticketId);
        }
        getSupportFragmentManager().beginTransaction().replace(R.id.content_fragment, ticketFeedBackFragment, FRAGMENT_TICKET_SUPPORT_DETAIL_TAG).commit();
    }


    @Override
    protected void setupToolbar(Toolbar mToolbar) {
        super.setupToolbar(mToolbar);
        ((TextView) mToolbar.findViewById(R.id.main_toolbar_title)).setText(getString(R.string.tm_ticket_detail));
    }

    @Override
    public void getMyImportContractObject(List<ImportContractObject> importContractObjectList) {
        getEventBus().post(new ImportContractObjectEvent(importContractObjectList));
    }
}
