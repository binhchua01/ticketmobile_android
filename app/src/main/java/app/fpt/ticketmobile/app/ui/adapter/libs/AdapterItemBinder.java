package app.fpt.ticketmobile.app.ui.adapter.libs;

/**
 * Created by Administrator on 7/12/2016.
 */
public class AdapterItemBinder<AIT extends AdapterItem<? extends ADT>, ADT, VH> implements AdapterDelegatesManager.Binder<AIT, ADT, VH>{

    @Override
    public int getViewType(AIT ait) {
        return ait.getViewType();
    }

    @Override
    public void onBindViewHolder(AIT ait, VH vh, AdapterDelegate<ADT, VH> adapterDelegate) {
        adapterDelegate.onBindViewHolder(ait.getItem(), vh);
    }
}
