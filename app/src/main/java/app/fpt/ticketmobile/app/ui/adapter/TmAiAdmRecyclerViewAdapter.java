package app.fpt.ticketmobile.app.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import app.fpt.ticketmobile.R;
import app.fpt.ticketmobile.app.ui.adapter.libs.AdapterDelegatesManager;
import app.fpt.ticketmobile.app.ui.adapter.libs.AdapterItemBinder;
import app.fpt.ticketmobile.app.ui.adapter.libs.AdapterDelegate;
import app.fpt.ticketmobile.app.ui.adapter.libs.AdapterItem;

/**
 * Created by ToanMaster on 7/12/2016.
 */
public abstract class TmAiAdmRecyclerViewAdapter<AI extends AdapterItem<? extends ADT>, ADT, VH extends RecyclerView.ViewHolder> extends TmRecycleViewAdapter<VH> {

    private AdapterDelegatesManager<AI, ADT, VH> mAdapterDelagatesManager = new AdapterDelegatesManager(new AdapterItemBinder());
    private Interceptor<AI> mInterceptor;

    public abstract int onGetItemCount();
    public abstract AI onGetAdapterItem(int position);


    public TmAiAdmRecyclerViewAdapter(Context context, Callback callback) {
        super(context, callback);
        if (callback != null){
            registerAdapterDataObserver(new PrivateDataObserver(callback));
        }
    }


    public final void addDelegate(AdapterDelegate<? extends ADT, ? extends VH> delegate){
        mAdapterDelagatesManager.addDelegate(delegate);
    }

    public void setInterceptor(Interceptor<AI> interceptor){
        mInterceptor = interceptor;
        notifyDataSetChanged();
    }

    public AI getAdapterItem(int position){
        if (mInterceptor != null){
          return  mInterceptor.getAdapterItem(position);
        }
        return onGetAdapterItem(position);
    }


    @Override
    public VH onCreateViewHolder(ViewGroup parent, int viewType) {

        return mAdapterDelagatesManager.onCreateViewHolder(parent, viewType);
    }

    @Override
    public void onBindViewHolder(VH holder, int position) {
        mAdapterDelagatesManager.onBindViewHolder(getAdapterItem(position), holder);
    }

    @Override
    public int getItemCount() {
        return mInterceptor != null ? mInterceptor.getItemCount() : onGetItemCount();
    }

    @Override
    public int getItemViewType(int position) {
        return getAdapterItem(position).getViewType();
    }


    public static abstract class Interceptor<AI extends AdapterItem<?>>{
        private final TmAiAdmRecyclerViewAdapter<AI, ?, ?> mAdapter;

        public abstract AI getAdapterItem(int i);

        public abstract int getItemCount();

        public Interceptor(TmAiAdmRecyclerViewAdapter<AI, ?, ?> adapter){
            this.mAdapter = adapter;
        }

        public int getItemCountFromAdapter(){
            return mAdapter.onGetItemCount();
        }

        public AI getAdapterItemFromAdapter(int position){
            return mAdapter.onGetAdapterItem(position);
        }
    }
}
