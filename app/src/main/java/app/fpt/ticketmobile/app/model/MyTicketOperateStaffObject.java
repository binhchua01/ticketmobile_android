package app.fpt.ticketmobile.app.model;

import java.util.List;

/**
 * Created by Administrator on 6/29/2016.
 */
public class MyTicketOperateStaffObject {

    private List<SpinnerObject> mSpinnerObjectList;
    private List<OperateStaffObject> Data;

    public List<OperateStaffObject> getData() {
        return Data;
    }

    public void setData(List<OperateStaffObject> data) {
        Data = data;
    }

    public List<SpinnerObject> getSpinnerObjectList() {
        return mSpinnerObjectList;
    }

    public void setSpinnerObjectList(List<SpinnerObject> mSpinnerObjectList) {
        this.mSpinnerObjectList = mSpinnerObjectList;
    }

    public class OperateStaffObject{
        private String Email;
        private String Name;

        public String getEmail() {
            return Email;
        }

        public void setEmail(String email) {
            Email = email;
        }

        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }
    }
}
