package app.fpt.ticketmobile.app.jsonParsers.Fortype.support;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import app.fpt.ticketmobile.app.model.SpinnerObject;
import app.fpt.ticketmobile.app.model.support.MyVisorStaffSupportObject;

/**
 * Created by Administrator on 7/22/2016.
 */
public class MyVisorStaffSupportField {

    public static class Deserializer implements JsonDeserializer<MyVisorStaffSupportObject>{

        @Override
        public MyVisorStaffSupportObject deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            Gson gson = new Gson();
            MyVisorStaffSupportObject myVisorStaffSupportObject = new MyVisorStaffSupportObject();
            List<SpinnerObject> spinnerObjectList = new ArrayList<>();

            try{
                JsonObject jsObj = json.getAsJsonObject();
                JsonObject jsonObject = jsObj.getAsJsonObject("Result");

                myVisorStaffSupportObject = gson.fromJson(jsonObject, MyVisorStaffSupportObject.class);

                if (myVisorStaffSupportObject.getVisorStaffSupportList() != null && myVisorStaffSupportObject.getVisorStaffSupportList().size() > 0) {
                    for (int i = 0; i < myVisorStaffSupportObject.getVisorStaffSupportList().size(); i++) {
                        MyVisorStaffSupportObject.VisorStaffSupportObject visorStaffSupportObject = myVisorStaffSupportObject.getVisorStaffSupportList().get(i);
                        SpinnerObject spinnerObject = new SpinnerObject(String.valueOf(visorStaffSupportObject.getID()), visorStaffSupportObject.getName(), String.valueOf(visorStaffSupportObject.getEmail()));
                        spinnerObjectList.add(spinnerObject);
                    }
                    myVisorStaffSupportObject.setSpinnerObjectList(spinnerObjectList);
                }

            }catch (Exception e){
                Log.e("tmt", "MyVisorStaffSupportField : " + e.toString());
            }

            return myVisorStaffSupportObject;
        }
    }
}
