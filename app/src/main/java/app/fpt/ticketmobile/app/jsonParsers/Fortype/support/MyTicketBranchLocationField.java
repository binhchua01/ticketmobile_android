package app.fpt.ticketmobile.app.jsonParsers.Fortype.support;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import app.fpt.ticketmobile.app.model.SpinnerObject;
import app.fpt.ticketmobile.app.model.support.MyTicketBranchLocationObject;

/**
 * Created by Administrator on 7/7/2016.
 */
public class MyTicketBranchLocationField {

    public static final class Deserializer implements JsonDeserializer<MyTicketBranchLocationObject>{

        @Override
        public MyTicketBranchLocationObject deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

            Gson gson = new Gson();
            MyTicketBranchLocationObject ticketBranchLocationObject = null;
            List<SpinnerObject> spinnerObjectList = new ArrayList<>();

            try{
                JsonObject jsResult = json.getAsJsonObject();
                if (jsResult.has("Result")){
                    jsResult = jsResult.getAsJsonObject("Result");
                    if (jsResult.has("Data")){
                        if (jsResult.get("Data").isJsonArray()) {
                            ticketBranchLocationObject = gson.fromJson(jsResult, MyTicketBranchLocationObject.class);

                            if (ticketBranchLocationObject.getBranchLocationList().size() > 0){
                                for (int i = 0; i < ticketBranchLocationObject.getBranchLocationList().size(); i++){
                                    MyTicketBranchLocationObject.BranchLocationObject branchLocationObject = ticketBranchLocationObject.getBranchLocationList().get(i);
                                    SpinnerObject spinnerObject = new SpinnerObject(String.valueOf(branchLocationObject.getBranchID().intValue()), branchLocationObject.getBranchName());
                                    spinnerObjectList.add(spinnerObject);
                                }
                                ticketBranchLocationObject.setSpinnerObjectList(spinnerObjectList);
                            }
                        }
                    }
                }

            }catch (Exception e){
                Log.e("tmt", "MyTicketBusinessAreaField : Deserializer : " + e.toString());
            }

            return ticketBranchLocationObject;
        }
    }
}
