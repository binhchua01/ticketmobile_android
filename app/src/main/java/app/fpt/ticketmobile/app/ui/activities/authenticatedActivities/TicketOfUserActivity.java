package app.fpt.ticketmobile.app.ui.activities.authenticatedActivities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import app.fpt.ticketmobile.app.ui.activities.TmBaseActivity;
import app.fpt.ticketmobile.R;
import app.fpt.ticketmobile.app.ui.fragments.authenticatedFragment.TicketOfUserFragment;

/**
 * Created by ToanMaster on 6/21/2016.
 */
public class TicketOfUserActivity extends TmBaseActivity {

    private static final String EXTRA_FRAGMENT_TICKET_OF_USER_KEY = "extra_fragment_ticket_of_user_key";


    public TicketOfUserActivity() {
        super(Integer.valueOf(R.layout.activity_ticket_of_user));
    }

    public static Intent buildIntent(Context context){
        Intent intent = new Intent(context, TicketOfUserActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupIfFragmentIsNeeded();
    }

    @Override
    protected void onStart() {
        super.onStart();

    }



    private void setupIfFragmentIsNeeded() {

        TicketOfUserFragment fragmentTicketOfMe = (TicketOfUserFragment) getSupportFragmentManager().findFragmentByTag(EXTRA_FRAGMENT_TICKET_OF_USER_KEY);
        if (fragmentTicketOfMe == null){
            fragmentTicketOfMe = new TicketOfUserFragment();
        }
        getSupportFragmentManager().beginTransaction().replace(R.id.content_fragment, fragmentTicketOfMe, EXTRA_FRAGMENT_TICKET_OF_USER_KEY).commit();
    }

    @Override
    protected void setupToolbar(Toolbar mToolbar) {
        super.setupToolbar(mToolbar);
        ((TextView) mToolbar.findViewById(R.id.main_toolbar_title)).setText(getString(R.string.tm_ticket_of_me));
    }
}
