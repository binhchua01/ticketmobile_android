package app.fpt.ticketmobile.app.ui.activities;

import android.support.v7.widget.Toolbar;
import android.view.View;

import app.fpt.ticketmobile.R;

/**
 * Created by Administrator on 7/14/2016.
 */
public class TmBaseActivity extends BaseActivity {
    protected TmBaseActivity(Integer layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void setupToolbar(Toolbar mToolbar) {
        super.setupToolbar(mToolbar);

        mToolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        mToolbar.setContentInsetsAbsolute(0, 0);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
