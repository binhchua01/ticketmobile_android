package app.fpt.ticketmobile.app.ui.activities.authenticatedActivities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import app.fpt.ticketmobile.app.model.MyTicketFeedbackDetailObject;
import app.fpt.ticketmobile.app.ui.activities.TmBaseActivity;
import app.fpt.ticketmobile.app.ui.fragments.authenticatedFragment.TicketFeedBackDetailSendFragment;
import app.fpt.ticketmobile.R;

/**
 * Created by Administrator on 7/5/2016.
 */
public class TicketFeedBackDetailSendActivity extends TmBaseActivity {

    private static final String EXTRA_TICKET_ID_KEY = "extra_ticket_response_id_key";
    private static final String EXTRA_FEED_BACK_DETAIL_KEY = "extra_feed_back_detail_key";
    private static final String EXTRA_BOOLEAN_FLAG_ALL_KEY = "extra_boolean_flag_all_key";
    private static final String FRAGMENT_FEEDBACK_DETAIL_SEND_TAG = "fragment_feedback_detail_send_tag";

    public TicketFeedBackDetailSendActivity() {
        super(Integer.valueOf(R.layout.activity_ticket_feedback_send));
    }

    public static Intent buildIntent(Context context, String ticketId, MyTicketFeedbackDetailObject ticketFeedbackDetailObject, boolean flagAll) {
        Intent intent = new Intent(context, TicketFeedBackDetailSendActivity.class);
        intent.putExtra(EXTRA_TICKET_ID_KEY, ticketId);
        intent.putExtra(EXTRA_FEED_BACK_DETAIL_KEY, ticketFeedbackDetailObject);
        intent.putExtra(EXTRA_BOOLEAN_FLAG_ALL_KEY, flagAll);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        extrasIntent();
    }

    private void extrasIntent() {
        if (getIntent() != null){
            String TICKET_ID = getIntent().getStringExtra(EXTRA_TICKET_ID_KEY);
            MyTicketFeedbackDetailObject feedbackDetailObject = (MyTicketFeedbackDetailObject) getIntent().getSerializableExtra(EXTRA_FEED_BACK_DETAIL_KEY);
            boolean flagAll = getIntent().getBooleanExtra(EXTRA_BOOLEAN_FLAG_ALL_KEY, true);
            setupIsFragmentIfNeed(TICKET_ID, feedbackDetailObject, flagAll);
        }
    }

    private void setupIsFragmentIfNeed(String ticketId, MyTicketFeedbackDetailObject feedbackDetailObject, boolean flagAll) {
        TicketFeedBackDetailSendFragment ticketFeedBackDetailSendFragment = (TicketFeedBackDetailSendFragment) getSupportFragmentManager().findFragmentByTag(FRAGMENT_FEEDBACK_DETAIL_SEND_TAG);
        if (ticketFeedBackDetailSendFragment == null){
            ticketFeedBackDetailSendFragment = TicketFeedBackDetailSendFragment.buildIntent(ticketId, feedbackDetailObject, flagAll);
        }

        getSupportFragmentManager().beginTransaction().replace(R.id.content_fragment, ticketFeedBackDetailSendFragment, FRAGMENT_FEEDBACK_DETAIL_SEND_TAG).commit();
    }


    @Override
    protected void setupToolbar(Toolbar mToolbar) {
        super.setupToolbar(mToolbar);
        ((TextView) mToolbar.findViewById(R.id.main_toolbar_title)).setText(getString(R.string.tm_ticket_feedback_send));
    }
}
