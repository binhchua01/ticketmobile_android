package app.fpt.ticketmobile.app.model.support;

import java.util.List;

/**
 * Created by Administrator on 7/7/2016.
 */
public class MyTicketCreatedObject {

    private List<TicketCreatedObject> Data;

    public List<TicketCreatedObject> getTicketCreatedList() {
        return Data;
    }

    public void setTicketCreatedList(List<TicketCreatedObject> data) {
        Data = data;
    }

    public class TicketCreatedObject {
        private String TicketID;
        private String Title;
        private String Queue;
        private String TicketStatus;
        private String Priority;
        private String UpdateDate;
        private String Staff;
        private String CreatedDate;
        private String EstimatedTime;
        private String TimeWorked;

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String createdDate) {
            CreatedDate = createdDate;
        }

        public String getEstimatedTime() {
            return EstimatedTime;
        }

        public void setEstimatedTime(String estimatedTime) {
            EstimatedTime = estimatedTime;
        }

        public String getPriority() {
            return Priority;
        }

        public void setPriority(String priority) {
            Priority = priority;
        }

        public String getQueue() {
            return Queue;
        }

        public void setQueue(String queue) {
            Queue = queue;
        }

        public String getStaff() {
            return Staff;
        }

        public void setStaff(String staff) {
            Staff = staff;
        }

        public String getTicketID() {
            return TicketID;
        }

        public void setTicketID(String ticketID) {
            TicketID = ticketID;
        }

        public String getTicketStatus() {
            return TicketStatus;
        }

        public void setTicketStatus(String ticketStatus) {
            TicketStatus = ticketStatus;
        }

        public String getTimeWorked() {
            return TimeWorked;
        }

        public void setTimeWorked(String timeWorked) {
            TimeWorked = timeWorked;
        }

        public String getTitle() {
            return Title;
        }

        public void setTitle(String title) {
            Title = title;
        }

        public String getUpdateDate() {
            return UpdateDate;
        }

        public void setUpdateDate(String updateDate) {
            UpdateDate = updateDate;
        }
    }
}
