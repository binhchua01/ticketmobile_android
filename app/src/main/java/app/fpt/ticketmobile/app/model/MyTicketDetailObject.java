package app.fpt.ticketmobile.app.model;

/**
 * Created by Administrator on 6/27/2016.
 */
public class MyTicketDetailObject {

    private String Branch;
    private String BranchID;
    private String CreatedDate;
    private Integer CricLev;
    private Integer CusQty;
    private String CusType;
    private String Description;
    private Integer DeviceType;
    private String DeviceTypeName;
    private Integer Effect;
    private String ExpectedDate;
    private String FoundIPPhone;
    private String FoundMobile;
    private String FoundStaff;
    private String IssueDate;
    private String IssueGroup;
    private Integer IssueID;
    private String Level;
    private String Location;
    private String LocationID;
    private String OperateStaff;
    private Integer PayTVQty;
    private String ProcessIPPhone;
    private String ProcessMobile;
    private String ProcessStaff;
    private Integer Queue;
    private Integer Reason;
    private Integer ReasonDetail;
    private String RequiredDate;
    private String ServiceType;
    private Integer TicketStatus;
    private String VisorStaff;

    public String getBranch() {
        return Branch;
    }

    public void setBranch(String branch) {
        Branch = branch;
    }

    public String getBranchID() {
        return BranchID;
    }

    public void setBranchID(String branchID) {
        BranchID = branchID;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }

    public Integer getCricLev() {
        return CricLev;
    }

    public void setCricLev(Integer cricLev) {
        CricLev = cricLev;
    }

    public Integer getCusQty() {
        return CusQty;
    }

    public void setCusQty(Integer cusQty) {
        CusQty = cusQty;
    }

    public String getCusType() {
        return CusType;
    }

    public void setCusType(String cusType) {
        CusType = cusType;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public Integer getDeviceType() {
        return DeviceType;
    }

    public void setDeviceType(Integer deviceType) {
        DeviceType = deviceType;
    }

    public String getDeviceTypeName() {
        return DeviceTypeName;
    }

    public void setDeviceTypeName(String deviceTypeName) {
        DeviceTypeName = deviceTypeName;
    }

    public Integer getEffect() {
        return Effect;
    }

    public void setEffect(Integer effect) {
        Effect = effect;
    }

    public String getExpectedDate() {
        return ExpectedDate;
    }

    public void setExpectedDate(String expectedDate) {
        ExpectedDate = expectedDate;
    }

    public String getFoundIPPhone() {
        return FoundIPPhone;
    }

    public void setFoundIPPhone(String foundIPPhone) {
        FoundIPPhone = foundIPPhone;
    }

    public String getFoundMobile() {
        return FoundMobile;
    }

    public void setFoundMobile(String foundMobile) {
        FoundMobile = foundMobile;
    }

    public String getFoundStaff() {
        return FoundStaff;
    }

    public void setFoundStaff(String foundStaff) {
        FoundStaff = foundStaff;
    }

    public String getIssueDate() {
        return IssueDate;
    }

    public void setIssueDate(String issueDate) {
        IssueDate = issueDate;
    }

    public String getIssueGroup() {
        return IssueGroup;
    }

    public void setIssueGroup(String issueGroup) {
        IssueGroup = issueGroup;
    }

    public Integer getIssueID() {
        return IssueID;
    }

    public void setIssueID(Integer issueID) {
        IssueID = issueID;
    }

    public String getLevel() {
        return Level;
    }

    public void setLevel(String level) {
        Level = level;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        Location = location;
    }

    public String getLocationID() {
        return LocationID;
    }

    public void setLocationID(String locationID) {
        LocationID = locationID;
    }

    public String getOperateStaff() {
        return OperateStaff;
    }

    public void setOperateStaff(String operateStaff) {
        OperateStaff = operateStaff;
    }

    public Integer getPayTVQty() {
        return PayTVQty;
    }

    public void setPayTVQty(Integer payTVQty) {
        PayTVQty = payTVQty;
    }

    public String getProcessIPPhone() {
        return ProcessIPPhone;
    }

    public void setProcessIPPhone(String processIPPhone) {
        ProcessIPPhone = processIPPhone;
    }

    public String getProcessMobile() {
        return ProcessMobile;
    }

    public void setProcessMobile(String processMobile) {
        ProcessMobile = processMobile;
    }

    public String getProcessStaff() {
        return ProcessStaff;
    }

    public void setProcessStaff(String processStaff) {
        ProcessStaff = processStaff;
    }

    public Integer getQueue() {
        return Queue;
    }

    public void setQueue(Integer queue) {
        Queue = queue;
    }

    public Integer getReason() {
        return Reason;
    }

    public void setReason(Integer reason) {
        Reason = reason;
    }

    public Integer getReasonDetail() {
        return ReasonDetail;
    }

    public void setReasonDetail(Integer reasonDetail) {
        ReasonDetail = reasonDetail;
    }

    public String getRequiredDate() {
        return RequiredDate;
    }

    public void setRequiredDate(String requiredDate) {
        RequiredDate = requiredDate;
    }

    public String getServiceType() {
        return ServiceType;
    }

    public void setServiceType(String serviceType) {
        ServiceType = serviceType;
    }

    public Integer getTicketStatus() {
        return TicketStatus;
    }

    public void setTicketStatus(Integer ticketStatus) {
        TicketStatus = ticketStatus;
    }

    public String getVisorStaff() {
        return VisorStaff;
    }

    public void setVisorStaff(String visorStaff) {
        VisorStaff = visorStaff;
    }
}
