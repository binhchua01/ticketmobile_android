package app.fpt.ticketmobile.app.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import app.fpt.ticketmobile.app.model.MyTicketObject;
import app.fpt.ticketmobile.R;
import app.fpt.ticketmobile.app.ui.activities.authenticatedActivities.TicketDetailActivity;

/**
 * Created by ToanMaster on 6/24/2016.
 */
public class MyTicketAdapter extends RecyclerView.Adapter<MyTicketAdapter.MyTicketHolder>{

    private List<MyTicketObject.MyTicket> mMyTicketObjectList;

    private static class PrivateItemClickListener implements View.OnClickListener{

        private Context context;
        private MyTicketObject.MyTicket myTicket;

        public PrivateItemClickListener(Context context, MyTicketObject.MyTicket myTicket){
            this.context = context;
            this.myTicket = myTicket;
        }

        @Override
        public void onClick(View v) {
            context.startActivity(TicketDetailActivity.buildIntent(context, myTicket.getTicketID()));
        }
    }

    public final class MyTicketHolder extends RecyclerView.ViewHolder{
        private View parentView = null;
        private TextView mTicketNumberCode;
        private TextView mTicketTittle;
        private TextView mTicketUnit;
        private TextView mTicketCreatedTime;
        private TextView mTicketFinishedTime;
        private TextView mTicketEstimatedTime;

        public MyTicketHolder(View parent) {
            super(parent);
            parentView = parent;
            mTicketNumberCode = (TextView) parent.findViewById(R.id.tm_tv_ticket_number_code);
            mTicketTittle = (TextView) parent.findViewById(R.id.tm_tv_ticket_title);
            mTicketUnit = (TextView) parent.findViewById(R.id.tm_tv_ticket_unit);
            mTicketCreatedTime = (TextView) parent.findViewById(R.id.tm_tv_ticket_created_time);
            mTicketFinishedTime = (TextView) parent.findViewById(R.id.tm_tv_ticket_finished_time);
            mTicketEstimatedTime = (TextView) parent.findViewById(R.id.tm_tv_ticket_estimated_time);
        }

        public void onBind(MyTicketObject.MyTicket myTicket){
            // on click item
            parentView.setOnClickListener(new PrivateItemClickListener(parentView.getContext(), myTicket));
            // set data to itemView
            mTicketNumberCode.setText(myTicket.getTicketID());
            mTicketTittle.setText(myTicket.getTitle());
            mTicketUnit.setText(myTicket.getIssueName());
            mTicketCreatedTime.setText(myTicket.getCreatedDate());
            mTicketFinishedTime.setText(myTicket.getEstimatedTime());
            mTicketEstimatedTime.setText(myTicket.getExistTime());
        }
    }

    public MyTicketAdapter(List<MyTicketObject.MyTicket> myTicketObjectList){
        this.mMyTicketObjectList = myTicketObjectList;
    }

    @Override
    public MyTicketHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.entry_ticket_queue_and_status_detail, parent, false);
        return new MyTicketHolder(view);
    }

    @Override
    public void onBindViewHolder(MyTicketHolder holder, int position) {
        MyTicketObject.MyTicket myTicket = mMyTicketObjectList.get(position);
        holder.onBind(myTicket);
    }

    @Override
    public int getItemCount() {
        return mMyTicketObjectList.size();
    }

}
