package app.fpt.ticketmobile.app.model;

import java.util.List;

/**
 * Created by Administrator on 6/21/2016.
 */
public class MyTicketObject {

    private List<MyTicket> Data;


    public List<MyTicket> getMyTicketList() {
        return Data;
    }

    public void setMyTicketList(List<MyTicket> myTicketList) {
        this.Data = myTicketList;
    }

    public class MyTicket {


        private String TicketID;
        private String Title;
        private String IssueName;
        private String ExistTime;
        private String CreatedDate;
        private String TimeWorked;
        private String EstimatedTime;
        private String Queue;
        private String ProcessStaff;
        private String Level;
        private String TicketStatus;
        private String UpdatedDate;


        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String createdDate) {
            CreatedDate = createdDate;
        }

        public String getEstimatedTime() {
            return EstimatedTime;
        }

        public void setEstimatedTime(String estimatedTime) {
            EstimatedTime = estimatedTime;
        }

        public String getExistTime() {
            return ExistTime;
        }

        public void setExistTime(String existTime) {
            ExistTime = existTime;
        }

        public String getIssueName() {
            return IssueName;
        }

        public void setIssueName(String issueName) {
            IssueName = issueName;
        }

        public String getTicketID() {
            return TicketID;
        }

        public void setTicketID(String ticketID) {
            TicketID = ticketID;
        }

        public String getTimeWorked() {
            return TimeWorked;
        }

        public void setTimeWorked(String timeWorked) {
            TimeWorked = timeWorked;
        }

        public String getTitle() {
            return Title;
        }

        public void setTitle(String title) {
            Title = title;
        }
    }


}
