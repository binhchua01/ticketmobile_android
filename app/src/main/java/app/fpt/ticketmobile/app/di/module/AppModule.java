package app.fpt.ticketmobile.app.di.module;

import android.app.Application;
import android.content.Context;
import android.os.Handler;


import app.fpt.ticketmobile.app.core.TmApplication;
import app.fpt.ticketmobile.app.db.TmSharePreferencesUtils;
import app.fpt.ticketmobile.app.di.module.internal.TmConnectivityInfoImpl;
import app.fpt.ticketmobile.app.di.module.internal.TmTrackerAnalyticsImpl;
import app.fpt.ticketmobile.app.di.module.internal.TmUiThreadRunnerImpl;
import app.fpt.ticketmobile.app.eventBus.EventBus;
import app.fpt.ticketmobile.app.lib.TmConnectivityInfo;
import app.fpt.ticketmobile.app.lib.TmGraphics;
import app.fpt.ticketmobile.app.lib.TmUiThreadRunner;
import app.fpt.ticketmobile.app.tracker.interfaces.TmTrackerAnalytics;
import app.fpt.ticketmobile.app.ui.lib.TmToast;

/**
 * Created by ToanMaster on 6/13/2016.
 */
public class AppModule {

    private final Handler mHandlerUiThread;
    private final TmApplication mTmApplication;

    public AppModule(TmApplication tmApplication, Handler handlerUiThread){
        this.mTmApplication = tmApplication;
        this.mHandlerUiThread = handlerUiThread;
    }

    public Application provideApplication(){
        return mTmApplication;
    }

    public TmApplication provideTmApplication(){
        return mTmApplication;
    }

    public Context provideContext(){
        return mTmApplication;
    }

    public TmUiThreadRunner provideTmUiThreadHandler(){
        return new TmUiThreadRunnerImpl(mHandlerUiThread);
    }

    public TmToast provideTmToast(){
        return new TmToast(mTmApplication);
    }

    public EventBus provideEventBus(TmUiThreadRunner tmUiThreadRunner, TmToast tmToast){
        return EventBus.create(tmUiThreadRunner, tmToast);
    }

    public TmConnectivityInfo provideTmConnectivityInfo(Context context){
        return new TmConnectivityInfoImpl(context);
    }

    public TmSharePreferencesUtils provideTmSharePreferencesUtil(Context context){
        return TmSharePreferencesUtils.getInstance(context);
    }

    public TmGraphics provideTmGraphics(Context context){
        return new TmGraphics(context);
    }

    public TmTrackerAnalytics provideTbTrackerAnalytics(Context context){
        return new TmTrackerAnalyticsImpl(context);
    }





}
