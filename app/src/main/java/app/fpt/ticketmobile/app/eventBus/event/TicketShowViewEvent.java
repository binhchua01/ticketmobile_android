package app.fpt.ticketmobile.app.eventBus.event;

import app.fpt.ticketmobile.app.eventBus.Event;
import app.fpt.ticketmobile.app.model.MyTicketFeedbackDetailObject;

/**
 * Created by Administrator on 7/4/2016.
 */
public class TicketShowViewEvent implements Event {

    private MyTicketFeedbackDetailObject myTicketFeedbackDetailObject;

    public MyTicketFeedbackDetailObject getMyTicketFeedbackDetailObject() {
        return myTicketFeedbackDetailObject;
    }

    public void setMyTicketFeedbackDetailObject(MyTicketFeedbackDetailObject myTicketFeedbackDetailObject) {
        this.myTicketFeedbackDetailObject = myTicketFeedbackDetailObject;
    }

    public TicketShowViewEvent(MyTicketFeedbackDetailObject myTicketFeedbackDetailObject) {
        this.myTicketFeedbackDetailObject = myTicketFeedbackDetailObject;
    }
}
