package app.fpt.ticketmobile.app.eventBus;

import android.util.Log;

import com.squareup.otto.Bus;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import app.fpt.ticketmobile.app.ui.lib.TmToast;
import app.fpt.ticketmobile.app.lib.TmUiThreadRunner;

/**
 * Created by ToanMaster on 6/13/2016.
 */
public class EventBus {

    private final Bus mBus;
    private final TmToast mTmToast;
    private final TmUiThreadRunner mTmUiThreadRunner;
    private final List<Object> mRegisterObject;


    private class PrivatePostUiThread implements Runnable{

        private Event $event;
        public PrivatePostUiThread(Event event){
            this.$event = event;
        }

        @Override
        public void run() {
            post($event);
        }
    }


    private class PrivateOnRegister implements  Runnable{
        private Object $object;
        public PrivateOnRegister(Object object){
            this.$object = object;
        }

        @Override
        public void run() {
            register($object);
        }
    }

    private class PrivateOnUnregister implements Runnable{
        private Object $object;
        public PrivateOnUnregister(Object object){
            this.$object = object;
        }

        @Override
        public void run() {
            unregister($object);
        }
    }


    public static EventBus create(TmUiThreadRunner tmUiThreadRunner, TmToast tmToast){
        EventBus eventBus = new EventBus(tmUiThreadRunner, tmToast);
        eventBus.init();

        return eventBus;
    }

    private EventBus(TmUiThreadRunner tmUiThreadRunner, TmToast tmToast){
        this.mBus = new Bus();
        this.mTmToast = tmToast;
        this.mRegisterObject = new ArrayList<>();
        this.mTmUiThreadRunner = tmUiThreadRunner;
    }

    private void init(){
        NotEnoughResourceEventFallBack notEnoughResourceEventFallBack = new NotEnoughResourceEventFallBack(this, mTmToast);
    }

    public void post(Event event){
        mBus.post(event);
    }

    public void register(Object object){
        mBus.register(object);
        mRegisterObject.add(object);
    }

    public void unregister(Object object){
        boolean unregistered = false;
        Iterator<Object> iterator = this.mRegisterObject.iterator();
        while (iterator.hasNext()) {
            if (iterator.next() == object) {
                this.mBus.unregister(object);
                iterator.remove();
                unregistered = true;
                break;
            }
        }
        if (!unregistered) {
            Log.e( "tmt" , "You didn't register at all: " + object.toString());
        }
    }

    public void postOnUiThread(Event event){
        mTmUiThreadRunner.runOnUiThread(new PrivatePostUiThread(event));
    }

    public void registerOnUiThread(Object object){
        mTmUiThreadRunner.runOnUiThread(new PrivateOnRegister(object));
    }

    public void unregisterOnUiThread(Object object){
        mTmUiThreadRunner.runOnUiThread(new PrivateOnUnregister(object));
    }

}
