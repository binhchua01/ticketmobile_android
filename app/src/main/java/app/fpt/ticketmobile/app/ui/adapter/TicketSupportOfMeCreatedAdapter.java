package app.fpt.ticketmobile.app.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapter;
import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;

import java.util.List;

import app.fpt.ticketmobile.app.ui.adapter.support.TicketSupportOfMeCreatedChild;
import app.fpt.ticketmobile.app.ui.adapter.support.TicketSupportOfMeCreatedHeader;
import app.fpt.ticketmobile.R;
import app.fpt.ticketmobile.app.ui.adapter.support.TicketSupportOfMeCreatedChildViewHolder;
import app.fpt.ticketmobile.app.ui.adapter.support.TicketSupportOfMeCreatedHeaderViewHolder;

/**
 * Created by Administrator on 7/14/2016.
 */
public class TicketSupportOfMeCreatedAdapter extends ExpandableRecyclerAdapter<TicketSupportOfMeCreatedHeaderViewHolder, TicketSupportOfMeCreatedChildViewHolder> {
    private LayoutInflater mInflater;
    private List<? extends ParentListItem> parentListItems;


    public TicketSupportOfMeCreatedAdapter(Context context, @NonNull List<? extends ParentListItem> parentItemList) {
        super(parentItemList);
        parentListItems = parentItemList;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public TicketSupportOfMeCreatedHeaderViewHolder onCreateParentViewHolder(ViewGroup parentViewGroup) {
        View headerView = mInflater.inflate(R.layout.entry_ticket_of_me_created_header, parentViewGroup, false);
        return new TicketSupportOfMeCreatedHeaderViewHolder(headerView);
    }

    @Override
    public TicketSupportOfMeCreatedChildViewHolder onCreateChildViewHolder(ViewGroup childViewGroup) {
        View childView = mInflater.inflate(R.layout.entry_ticket_of_me_created_support_child, childViewGroup, false);
        return new TicketSupportOfMeCreatedChildViewHolder(childView);
    }

    @Override
    public void onBindParentViewHolder(TicketSupportOfMeCreatedHeaderViewHolder parentViewHolder, int position, ParentListItem parentListItem) {
        TicketSupportOfMeCreatedHeader ticketOfUserCreatedHeader = (TicketSupportOfMeCreatedHeader) parentListItem;
        parentViewHolder.bind(ticketOfUserCreatedHeader);
    }

    @Override
    public void onBindChildViewHolder(TicketSupportOfMeCreatedChildViewHolder childViewHolder, int position, Object childListItem) {
        TicketSupportOfMeCreatedChild ticketOfUserCreatedChild = (TicketSupportOfMeCreatedChild) childListItem;

        childViewHolder.bind(ticketOfUserCreatedChild);
    }

    public void clear() {
        parentListItems.clear();
        notifyDataSetChanged();
    }

    // Add a list of items
    public void addAll(List<? extends  ParentListItem> list) {
        parentListItems = list;
        notifyDataSetChanged();
    }
}
