package app.fpt.ticketmobile.app.networking.apiEndpoints;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;


import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import app.fpt.ticketmobile.app.model.MyResponseObject;
import app.fpt.ticketmobile.app.model.MyTicketStatusObject;
import app.fpt.ticketmobile.app.model.MyDeviceObject;
import app.fpt.ticketmobile.app.model.MyTicketCusTypeObject;
import app.fpt.ticketmobile.app.model.MyTicketDetailObject;
import app.fpt.ticketmobile.app.model.MyTicketEffectObject;
import app.fpt.ticketmobile.app.model.MyTicketEstimatedDateObject;
import app.fpt.ticketmobile.app.model.MyTicketIssueObject;
import app.fpt.ticketmobile.app.model.MyTicketOperateStaffObject;
import app.fpt.ticketmobile.app.model.MyTicketProcessStaffObject;
import app.fpt.ticketmobile.app.model.MyTicketQueueObject;
import app.fpt.ticketmobile.app.model.MyTicketReasonDetailObject;
import app.fpt.ticketmobile.app.model.MyTicketReasonObject;
import app.fpt.ticketmobile.app.model.MyTicketServiceTypeObject;
import app.fpt.ticketmobile.app.model.MyTicketVisorStaffObject;
import app.fpt.ticketmobile.app.networking.utils.GsonRequest;

/**
 * Created by ToanMaster on 6/27/2016.
 */
public abstract class MyTicketDetailEndPoint {


    private static Map<String, String> getHeader(String ticketId){
        Map<String, String>   params = new HashMap<String, String>();
        params.put("TicketID", ticketId);
        return params;
    }

    private static Map<String, String> getHeaderIssueList(String deviceType){
        Map<String, String>   params = new HashMap<String, String>();
        params.put("DeviceType", deviceType);
        return params;
    }

    private static Map<String, String> getHeaderEffectList(String issueId){
        Map<String, String>   params = new HashMap<String, String>();
        params.put("IssueID", issueId);
        return params;
    }

    private static Map<String, String> getHeaderReasonList(String issueId){
        Map<String, String>   params = new HashMap<String, String>();
        params.put("IssueID", issueId);
        return params;
    }

    private static Map<String, String> getHeaderReasonDetailList(String reasonId){
        Map<String, String>   params = new HashMap<String, String>();
        params.put("ReasonID", reasonId);
        return params;
    }


    private static Map<String, String> getHeaderServiceTypeList(){
        Map<String, String>   params = new HashMap<String, String>();
        return params;
    }

    private static Map<String, String> getHeaderCusTypeList(String serviceType){
        Map<String, String> params = new HashMap<>();
        params.put("ServiceType", serviceType);
        return params;
    }


    private static Map<String, String> getHeaderStatusList(String userId, String ticketStatusId){
        Map<String, String> params = new HashMap<>();
        params.put("UserID", userId);
        params.put("CurrStatus", ticketStatusId);
        return params;
    }

    private static Map<String, String> getHeaderOperateStaffList(String queue, String locationId, String branchId, String cricLev){
        Map<String, String> params = new HashMap<>();
        params.put("Queue", queue);
        params.put("LocationID", locationId);
        params.put("BranchID", branchId);
        params.put("CricLev", cricLev);
        return params;
    }

    private static Map<String, String> getHeaderVisorStaffList(String queue, String locationId, String branchId){
        Map<String, String> params = new HashMap<>();
        params.put("Queue", queue);
        params.put("LocationID", locationId);
        params.put("BranchID", branchId);
        return params;
    }

    private static Map<String, String> getHeaderProcessStaffList(String queue, String locationId, String branchId){

        Map<String, String> params = new HashMap<>();
        params.put("Queue", queue);
        params.put("LocationID", locationId);
        params.put("BranchID", branchId);
        return params;
    }


    private static Map<String, String> getHeaderUpdateTicketDevice(String deviceName, String status, String ticketId, String userId, String effect, String cusQty, String keyCode){
        Map<String, String> params = new HashMap<>();
        params.put("DeviceName", deviceName);
        params.put("Status", status);
        params.put("TicketID", ticketId);
        params.put("UserID", userId);
        params.put("Effect", effect);
        params.put("CusQty", cusQty);
        params.put("KeyCode", keyCode);
        return params;
    }

    private static Map<String, String> getHeaderUpdatedTicketDetail(String userId, String userEmail, String tickerId, String queueName, String keycode, MyTicketDetailObject ticketDetailObject){
        Map<String, String> params = new HashMap<>();
        params.put("TicketID", tickerId);
        params.put("LocationID", ticketDetailObject.getLocationID());
        params.put("BranchID", ticketDetailObject.getBranchID());
        params.put("Issue", String.valueOf(ticketDetailObject.getIssueID()));

        params.put("Reason", String.valueOf(ticketDetailObject.getReason()));
        params.put("ServiceType", ticketDetailObject.getServiceType());
        params.put("CusType", ticketDetailObject.getCusType());

        params.put("Effect", String.valueOf(ticketDetailObject.getEffect()));
        params.put("Level", ticketDetailObject.getLevel());
        params.put("Description", ticketDetailObject.getDescription());
        params.put("TicketStatus", String.valueOf(ticketDetailObject.getTicketStatus()));
        params.put("Queue", String.valueOf(ticketDetailObject.getQueue()));

        params.put("QueueName", queueName);
        params.put("OperateStaff", ticketDetailObject.getOperateStaff());
        params.put("VisorStaff", ticketDetailObject.getVisorStaff());
        params.put("ProcessStaff", ticketDetailObject.getProcessStaff());
        params.put("ProcessMobile", ticketDetailObject.getProcessMobile());
        params.put("ProcessIPPhone", ticketDetailObject.getProcessIPPhone());

        params.put("CricLev", String.valueOf(ticketDetailObject.getCricLev()));
        params.put("CusQty", String.valueOf(ticketDetailObject.getCusQty()));
        params.put("PayTVQty", String.valueOf(ticketDetailObject.getPayTVQty()));

        params.put("UpdatedBy", userId);
        params.put("UpdatedEmail", userEmail);
        params.put("IssueDate", ticketDetailObject.getIssueDate());
        params.put("RequiredDate", ticketDetailObject.getRequiredDate());
        params.put("ExpectedDate", ticketDetailObject.getExpectedDate());
        params.put("ReasonDetail", String.valueOf(ticketDetailObject.getReasonDetail()));
        params.put("KeyCode", keycode);
        return params;
    }


    private static Map<String, String> getHeaderEstimatedDate(String createdDate, String cricLev, String reasonDetailID){
        Map<String, String> params = new HashMap<>();
        params.put("CreatedDate", createdDate);
        params.put("CricLev", cricLev);
        params.put("ReasonDetailID", reasonDetailID);
        return params;
    }


    private static Map<String, String> getHeaderCusQty(String type, String device){
        Map<String, String> params = new HashMap<>();
        params.put("Type", type);
        params.put("Device", device);
        return params;
    }

    private static Map<String, String> getHeaderProcessStaff(String staffEmail){
        Map<String, String> params = new HashMap<>();
        params.put("StaffEmail", staffEmail);
        return params;
    }


    public static GsonRequest<MyTicketDetailObject> getTicketDetail(final String ticketId, Response.Listener<MyTicketDetailObject> listener, Response.ErrorListener errorListener){
        GsonRequest<MyTicketDetailObject> gsonRequest = new GsonRequest<MyTicketDetailObject>(Constants.MY_GET_TICKET_DETAIL, MyTicketDetailObject.class, listener, errorListener){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return getHeader(ticketId);
            }
        };

        return gsonRequest;
    }

    public static GsonRequest<MyDeviceObject> getMyDeviceList(final String ticketId, Response.Listener<MyDeviceObject> listener, Response.ErrorListener errorListener){
        GsonRequest<MyDeviceObject> gsonRequest = new GsonRequest<MyDeviceObject>(Constants.MY_GET_TICKET_DEVICE, MyDeviceObject.class, listener, errorListener){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return getHeader(ticketId);
            }
        };

        return gsonRequest;
    }


    public static GsonRequest<MyTicketIssueObject> getMyIssueList(final String deviceType, Response.Listener<MyTicketIssueObject> listener, Response.ErrorListener errorListener){
        GsonRequest<MyTicketIssueObject> gsonRequest = new GsonRequest<MyTicketIssueObject>(Constants.MY_GET_ISSUE_LIST, MyTicketIssueObject.class, listener, errorListener){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return getHeaderIssueList(deviceType);
            }

        };

        return gsonRequest;
    }

    public static GsonRequest<MyTicketEffectObject> getMyTicketEffectList(final String issueId, Response.Listener<MyTicketEffectObject> listener, Response.ErrorListener errorListener){
        GsonRequest<MyTicketEffectObject> gsonRequest = new GsonRequest<MyTicketEffectObject>(Constants.MY_GET_EFFECT_LIST, MyTicketEffectObject.class, listener, errorListener){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return getHeaderEffectList(issueId);
            }
        };

        return gsonRequest;
    }

    public static GsonRequest<MyTicketReasonObject> getMyTicketReasonList(final String issueId, Response.Listener<MyTicketReasonObject> listener, Response.ErrorListener errorListener){
        GsonRequest<MyTicketReasonObject> gsonRequest = new GsonRequest<MyTicketReasonObject>(Constants.MY_GET_REASON_LIST, MyTicketReasonObject.class, listener, errorListener){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return getHeaderReasonList(issueId);
            }
        };

        return gsonRequest;
    }

    public static GsonRequest<MyTicketReasonDetailObject> getMyTicketReasonDetailList(final String reasonId, Response.Listener<MyTicketReasonDetailObject> listener, Response.ErrorListener errorListener){
        GsonRequest<MyTicketReasonDetailObject> gsonRequest = new GsonRequest<MyTicketReasonDetailObject>(Constants.MY_GET_REASON_DETAIL_LIST, MyTicketReasonDetailObject.class, listener, errorListener){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Log.d("tmt", "reasonId : " + reasonId);
                return getHeaderReasonDetailList(reasonId);
            }
        };

        return gsonRequest;
    }

    public static GsonRequest<MyTicketServiceTypeObject> getMyTicketServiceTypeList(Response.Listener<MyTicketServiceTypeObject> listener, Response.ErrorListener errorListener){
        GsonRequest<MyTicketServiceTypeObject> gsonRequest = new GsonRequest<MyTicketServiceTypeObject>(Constants.MY_GET_SERVICE_TYPE_LIST, MyTicketServiceTypeObject.class, listener, errorListener){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return getHeaderServiceTypeList();
            }
        };
        return gsonRequest;
    }


    public static GsonRequest<MyTicketCusTypeObject> getMyTicketCusTypeList(final String serviceType, Response.Listener<MyTicketCusTypeObject> listener, Response.ErrorListener errorListener){
        GsonRequest<MyTicketCusTypeObject> gsonRequest = new GsonRequest<MyTicketCusTypeObject>(Constants.MY_GET_CUS_TYPE_LIST, MyTicketCusTypeObject.class, listener, errorListener){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return getHeaderCusTypeList(serviceType);
            }
        };

        return gsonRequest;
    }

    public static GsonRequest<MyTicketQueueObject> getMyTicketQueueList(Response.Listener<MyTicketQueueObject> listener, Response.ErrorListener errorListener){
        GsonRequest<MyTicketQueueObject> gsonRequest = new GsonRequest<>(Constants.MY_GET_QUEUE_LIST, MyTicketQueueObject.class, listener, errorListener);
        return gsonRequest;
    }


    public static GsonRequest<MyTicketStatusObject>  getMyTicketStatusList(final String userId, final String ticketStatusId, Response.Listener<MyTicketStatusObject> listener, Response.ErrorListener errorListener){
        GsonRequest<MyTicketStatusObject> request = new GsonRequest<MyTicketStatusObject>(Constants.MY_GET_STATUS_LIST, MyTicketStatusObject.class, listener, errorListener){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return getHeaderStatusList(userId, ticketStatusId);
            }
        };
        return request;
    }

    public static GsonRequest<MyTicketOperateStaffObject> getMyTicketOperateStaffList(final String queue, final String locationId, final String branchId, final String cricLev, Response.Listener<MyTicketOperateStaffObject> listener, Response.ErrorListener errorListener){
        GsonRequest<MyTicketOperateStaffObject> request = new GsonRequest<MyTicketOperateStaffObject>(Constants.MY_GET_OPERATE_STAFF_LIST, MyTicketOperateStaffObject.class, listener, errorListener){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return getHeaderOperateStaffList(queue, locationId, branchId, cricLev);
            }
        };

        return request;
    }

    public static GsonRequest<MyTicketVisorStaffObject> getMyTicketVisorStaffList(final String queue, final String locationId, final String branchId, Response.Listener<MyTicketVisorStaffObject> listener, Response.ErrorListener errorListener){
        GsonRequest<MyTicketVisorStaffObject> request = new GsonRequest<MyTicketVisorStaffObject>(Constants.MY_GET_VISOR_STAFF_LIST, MyTicketVisorStaffObject.class, listener, errorListener){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return getHeaderVisorStaffList(queue, locationId, branchId);
            }
        };

        return request;
    }

    public static GsonRequest<MyTicketProcessStaffObject> getMyTicketProcessStaffList( final String queue, final String locationId, final String branchId, Response.Listener<MyTicketProcessStaffObject> listener, Response.ErrorListener errorListener){
        GsonRequest<MyTicketProcessStaffObject> request = new GsonRequest<MyTicketProcessStaffObject>(Constants.MY_GET_PROCESS_STAFF_LIST, MyTicketProcessStaffObject.class, listener, errorListener){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return getHeaderProcessStaffList(queue, locationId, branchId);
            }
        };

        return request;
    }


    public static GsonRequest<MyTicketEstimatedDateObject> getMyTicketEstimatedDate(final String createdDate, final String cricLev, final String reasonDetailID, Response.Listener<MyTicketEstimatedDateObject> listener, Response.ErrorListener errorListener) {
        GsonRequest<MyTicketEstimatedDateObject> gsonRequest = new GsonRequest<MyTicketEstimatedDateObject>(Constants.MY_GET_ESTIMATED_DATE, MyTicketEstimatedDateObject.class, listener, errorListener){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return getHeaderEstimatedDate(createdDate, cricLev, reasonDetailID);
            }
        };
        return gsonRequest;
    }


    public static StringRequest getMyTicketCusQty(final String type, final String device, Response.Listener<String> listener, Response.ErrorListener errorListener){
        StringRequest jsonObjectRequest = new StringRequest( Request.Method.POST, Constants.MY_GET_CUS_QTY, listener, errorListener){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return getHeaderCusQty(type, device);
            }
        };

        return jsonObjectRequest;
    }


    public static StringRequest getMyTicketProcessStaff(final String staffEmail, Response.Listener<String> listener, Response.ErrorListener errorListener){
        StringRequest jsonObjectRequest = new StringRequest( Request.Method.POST, Constants.MY_GET_PHONE_STAFF, listener, errorListener){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return getHeaderProcessStaff(staffEmail);
            }
        };

        return jsonObjectRequest;
    }




    // update ticket detail

    public static GsonRequest<MyResponseObject> MyTicketDeviceUpdateData(final String deviceName, final String status, final String ticketId, final String userId,
                                                                         final String effect, final String cusQty, final String keyCode, Response.Listener<MyResponseObject> listener, Response.ErrorListener errorListener){
        GsonRequest<MyResponseObject> request = new GsonRequest<MyResponseObject>(Constants.MY_UPDATE_TICKET_DEVICE, MyResponseObject.class, listener, errorListener){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return getHeaderUpdateTicketDevice(deviceName, status, ticketId, userId, effect, cusQty, keyCode);
            }
        };

        return request;
    }


    public static GsonRequest<MyResponseObject> MyTicketDetailUpdated(final String userId, final String userEmail, final String tickerId, final String queueName,
                                                      final String keycode, final MyTicketDetailObject ticketDetailObject , Response.Listener<MyResponseObject> listener, Response.ErrorListener errorListener){
        GsonRequest<MyResponseObject> request = new GsonRequest<MyResponseObject>(Constants.MY_UPDATE_TICKET_DETAIL, MyResponseObject.class, listener, errorListener){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return getHeaderUpdatedTicketDetail(userId, userEmail, tickerId, queueName, keycode, ticketDetailObject);
            }

            @Override
            protected Response<MyResponseObject> parseNetworkResponse(NetworkResponse response) {
                if (response.headers == null)
                {
                    // cant just set a new empty map because the member is final.
                    response = new NetworkResponse(
                            response.statusCode,
                            response.data,
                            Collections.<String, String>emptyMap(), // this is the important line, set an empty but non-null map.
                            response.notModified,
                            response.networkTimeMs);


                }

                return super.parseNetworkResponse(response);
            }
        };


        return request;
    }




}
