package app.fpt.ticketmobile.app.jsonParsers.Fortype;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

import app.fpt.ticketmobile.app.model.User;
import app.fpt.ticketmobile.app.model.UserIam;

/**
 * Created by NhanNH26.
 *
 */
public abstract class UserIAMField {

    public static final class Deserializer implements JsonDeserializer<UserIam> {

        @Override
        public UserIam deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            UserIam user = new UserIam();
            Gson gson = new GsonBuilder().create();
            try {

                final JsonObject jsObj = json.getAsJsonObject();
                JsonObject jsonObject = jsObj.getAsJsonObject("data");
                user = gson.fromJson(jsonObject, UserIam.class);
            }
            catch (Exception ex){
                Log.e("tmt", "ex : " + ex.getMessage());
            }
            return user;
        }
    }


    public static final class Serializer implements JsonSerializer<UserIam>{

        @Override
        public JsonElement serialize(UserIam user, Type typeOfSrc, JsonSerializationContext context) {
            return null;
        }
    }
}
