package app.fpt.ticketmobile.app.model;

import java.util.List;

/**
 * Created by Administrator on 6/27/2016.
 */
public class MyDeviceObject {

    private List<DeviceObject> Data;

    public List<DeviceObject> getData() {
        return Data;
    }

    public void setData(List<DeviceObject> data) {
        Data = data;
    }

    public class DeviceObject {
        private String DeviceName;
        private Integer ErrorTimes;
        private String Status;
        private String UpdatedBy;
        private String UpdatedDate;

        public String getDeviceName() {
            return DeviceName;
        }

        public void setDeviceName(String deviceName) {
            DeviceName = deviceName;
        }

        public Integer getErrorTimes() {
            return ErrorTimes;
        }

        public void setErrorTimes(Integer errorTimes) {
            ErrorTimes = errorTimes;
        }

        public String getStatus() {
            return Status;
        }

        public void setStatus(String status) {
            Status = status;
        }

        public String getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(String updatedBy) {
            UpdatedBy = updatedBy;
        }

        public String getUpdatedDate() {
            return UpdatedDate;
        }

        public void setUpdatedDate(String updatedDate) {
            UpdatedDate = updatedDate;
        }
    }
}
