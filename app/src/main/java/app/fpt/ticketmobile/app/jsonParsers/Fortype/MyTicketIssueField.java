package app.fpt.ticketmobile.app.jsonParsers.Fortype;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import app.fpt.ticketmobile.app.model.MyTicketIssueObject;
import app.fpt.ticketmobile.app.model.SpinnerObject;

/**
 * Created by ToanMaster on 6/28/2016.
 */
public class MyTicketIssueField {

    public static class Deserializer implements JsonDeserializer<MyTicketIssueObject>{

        @Override
        public MyTicketIssueObject deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            Gson gson = new Gson();
            MyTicketIssueObject myTicketIssueObject = null;
            List<SpinnerObject> spinnerObjectList = new ArrayList<>();
            try {
                final JsonObject jsObj = json.getAsJsonObject();
                JsonObject jsonObject = jsObj.getAsJsonObject("Result");
                myTicketIssueObject = gson.fromJson(jsonObject, MyTicketIssueObject.class);

                if (myTicketIssueObject.getData() != null && myTicketIssueObject.getData().size() > 0) {
                    for (int i = 0; i < myTicketIssueObject.getData().size(); i++) {
                        MyTicketIssueObject.TicketIssueObject ticketIssueObject = myTicketIssueObject.getData().get(i);

                        SpinnerObject spinnerObject = new SpinnerObject(ticketIssueObject.getIssueID(), ticketIssueObject.getIssueName());
                        spinnerObjectList.add(spinnerObject);
                    }
                    myTicketIssueObject.setSpinnerObjectList(spinnerObjectList);
                }

            }catch(Exception e){
                Log.e("tmt", e.getMessage());
            }

            return myTicketIssueObject;
        }
    }
}
