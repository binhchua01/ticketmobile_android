package app.fpt.ticketmobile.app.ui.activities.authenticatedActivities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import app.fpt.ticketmobile.app.ui.activities.TmBaseActivity;
import app.fpt.ticketmobile.R;
import app.fpt.ticketmobile.app.ui.fragments.authenticatedFragment.TicketInventoryUnFinishFragment;

/**
 * Created by ToanMaster on 6/23/2016.
 */
public class TicketInventoryUnFinishActivity extends TmBaseActivity {
    private static final String EXTRA_FRAGMENT_TICKET_INVENTORY_UNFINISHED_KEY = "extra_fragment_ticket_inventory_unfinished_key";

    public TicketInventoryUnFinishActivity() {
        super(Integer.valueOf(R.layout.activity_ticket_inventory));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupIfFragmentIsNeeded();
    }

    public static Intent buildIntent(Context context){
        Intent intent = new Intent(context, TicketInventoryUnFinishActivity.class);
        return intent;
    }


    private void setupIfFragmentIsNeeded() {

        TicketInventoryUnFinishFragment fragmentTicketInventory = (TicketInventoryUnFinishFragment) getSupportFragmentManager().findFragmentByTag(EXTRA_FRAGMENT_TICKET_INVENTORY_UNFINISHED_KEY);
        if (fragmentTicketInventory == null){
            fragmentTicketInventory = new TicketInventoryUnFinishFragment();
        }
        getSupportFragmentManager().beginTransaction().replace(R.id.content_fragment, fragmentTicketInventory, EXTRA_FRAGMENT_TICKET_INVENTORY_UNFINISHED_KEY).commit();
    }

    @Override
    protected void setupToolbar(Toolbar mToolbar) {
        super.setupToolbar(mToolbar);
        ((TextView) mToolbar.findViewById(R.id.main_toolbar_title)).setText(getString(R.string.tm_ticket_backoff_unfinished));
    }
}
