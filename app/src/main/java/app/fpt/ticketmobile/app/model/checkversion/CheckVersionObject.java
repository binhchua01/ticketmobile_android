package app.fpt.ticketmobile.app.model.checkversion;

/**
 * Created by Administrator on 7/29/2016.
 */
public class CheckVersionObject {

    private CheckVersion Data;

    public CheckVersion getCheckVersion() {
        return Data;
    }

    public void setCheckVersion(CheckVersion data) {
        Data = data;
    }

    public class CheckVersion{
        private String Version;
        private String Link;
        private String ReleaseDate;
        private String Flag;

        public String getFlag() {
            return Flag;
        }

        public void setFlag(String flag) {
            Flag = flag;
        }

        public String getLink() {
            return Link;
        }

        public void setLink(String link) {
            Link = link;
        }

        public String getReleaseDate() {
            return ReleaseDate;
        }

        public void setReleaseDate(String releaseDate) {
            ReleaseDate = releaseDate;
        }

        public String getVersion() {
            return Version;
        }

        public void setVersion(String version) {
            Version = version;
        }
    }
}
