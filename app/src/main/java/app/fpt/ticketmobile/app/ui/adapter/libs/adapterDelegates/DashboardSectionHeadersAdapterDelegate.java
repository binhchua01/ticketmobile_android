package app.fpt.ticketmobile.app.ui.adapter.libs.adapterDelegates;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import app.fpt.ticketmobile.app.ui.adapter.libs.utils.ViewUtils;
import app.fpt.ticketmobile.R;

/**
 * Created by Administrator on 7/12/2016.
 */
public class DashboardSectionHeadersAdapterDelegate extends TmAdapterDelegates<String, DashboardSectionHeadersAdapterDelegate.DashboardSectionHeaderViewHolder> {

    public class DashboardSectionHeaderViewHolder extends RecyclerView.ViewHolder {
        private final TextView mSectionHeaderTitleView;

        public DashboardSectionHeaderViewHolder(View itemView) {
            super(itemView);
            this.mSectionHeaderTitleView = (TextView) itemView.findViewById(R.id.section_header_title);
        }

        public void bind(String title) {
            this.mSectionHeaderTitleView.setText(title);
        }
    }

    public DashboardSectionHeadersAdapterDelegate(int itemViewType) {
        super(itemViewType);
    }

    @Override
    public DashboardSectionHeaderViewHolder onCreateViewHolder(ViewGroup parent) {
        return new DashboardSectionHeaderViewHolder(ViewUtils.inflate(R.layout.entry_dashboard_section_header, parent, false));
    }

    @Override
    public void onBindViewHolder(String item, DashboardSectionHeaderViewHolder holder) {
        holder.bind(item);
    }
}
