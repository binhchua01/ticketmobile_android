package app.fpt.ticketmobile.app.jsonParsers.Fortype.support;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import app.fpt.ticketmobile.app.model.SpinnerObject;
import app.fpt.ticketmobile.app.model.support.MyTicketSupportServiceTypeObject;

/**
 * Created by Administrator on 7/7/2016.
 */
public class MyTicketSupportServiceTypeField {

    public static final class Deserializer implements JsonDeserializer<MyTicketSupportServiceTypeObject>{

        @Override
        public MyTicketSupportServiceTypeObject deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            Gson gson = new Gson();
            MyTicketSupportServiceTypeObject ticketSupportServiceTypeObject = null;
            List<SpinnerObject> spinnerObjectList = new ArrayList<>();

            try{
                JsonObject jsResult = json.getAsJsonObject();
                if (jsResult.has("Result")){
                    jsResult = jsResult.getAsJsonObject("Result");
                    if (jsResult.has("Data")){
                        if (jsResult.get("Data").isJsonArray()) {
                            ticketSupportServiceTypeObject = gson.fromJson(jsResult, MyTicketSupportServiceTypeObject.class);

                            if (ticketSupportServiceTypeObject.getServiceTypeList().size() > 0){
                                for (int i = 0; i < ticketSupportServiceTypeObject.getServiceTypeList().size(); i++){
                                    MyTicketSupportServiceTypeObject.ServiceTypeObject serviceTypeObject = ticketSupportServiceTypeObject.getServiceTypeList().get(i);
                                    SpinnerObject spinnerObject = new SpinnerObject(String.valueOf(serviceTypeObject.getID().intValue()), serviceTypeObject.getName());
                                    spinnerObjectList.add(spinnerObject);
                                }
                                ticketSupportServiceTypeObject.setSpinnerObjectList(spinnerObjectList);
                            }
                        }
                    }
                }

            }catch (Exception e){
                Log.e("tmt", "MyTicketSupportServiceTypeField : Deserializer : " + e.toString());
            }

            return ticketSupportServiceTypeObject;
        }
    }
}
