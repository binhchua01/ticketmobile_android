package app.fpt.ticketmobile.app.model;

import java.util.List;

/**
 * Created by Administrator on 6/28/2016.
 */
public class MyTicketServiceTypeObject {

    private List<ServiceTypeObject> Data;
    private List<SpinnerObject> mSpinnerObjectList;


    public List<ServiceTypeObject> getData() {
        return Data;
    }

    public void setData(List<ServiceTypeObject> data) {
        Data = data;
    }

    public List<SpinnerObject> getSpinnerObjectList() {
        return mSpinnerObjectList;
    }

    public void setSpinnerObjectList(List<SpinnerObject> mSpinnerObjectList) {
        this.mSpinnerObjectList = mSpinnerObjectList;
    }

    public class ServiceTypeObject{
        private Integer ID;
        private String Name;

        public Integer getID() {
            return ID;
        }

        public void setID(Integer ID) {
            this.ID = ID;
        }

        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }
    }
}
