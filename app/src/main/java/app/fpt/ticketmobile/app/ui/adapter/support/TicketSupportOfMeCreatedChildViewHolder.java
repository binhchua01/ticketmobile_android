package app.fpt.ticketmobile.app.ui.adapter.support;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ViewHolder.ChildViewHolder;

import app.fpt.ticketmobile.app.ui.activities.authenticatedActivities.TicketSupportDetailActivity;
import app.fpt.ticketmobile.R;

/**
 * Created by Administrator on 7/25/2016.
 */
public class TicketSupportOfMeCreatedChildViewHolder extends ChildViewHolder {

    private TextView tvTicketInventoryNotAssignNumberCode;
    private TextView tvTicketInventoryNotAssignTitle;
    private TextView tvTicketInventoryNotAssignUnit;
    private TextView tvTicketInventoryNotAssignCreatedTime;
    private TextView tvTicketInventoryNotAssignEstimatedTime;
    private View childView = null;

    public TicketSupportOfMeCreatedChildViewHolder(View childView) {
        super(childView);
        this.childView = childView;
        tvTicketInventoryNotAssignNumberCode = (TextView) childView.findViewById(R.id.tm_tv_ticket_number_code);
        tvTicketInventoryNotAssignTitle = (TextView) childView.findViewById(R.id.tm_tv_ticket_title);
        tvTicketInventoryNotAssignUnit = (TextView) childView.findViewById(R.id.tm_tv_ticket_unit);
        tvTicketInventoryNotAssignCreatedTime = (TextView) childView.findViewById(R.id.tm_tv_ticket_created_time);
        tvTicketInventoryNotAssignEstimatedTime = (TextView) childView.findViewById(R.id.tm_tv_ticket_estimated_time);
    }


    private final class PrivateTicketInventoryListener implements View.OnClickListener{
        private TicketSupportOfMeCreatedChild ticketOfUserCreatedChild;
        private Context context;

        public PrivateTicketInventoryListener(Context context, TicketSupportOfMeCreatedChild ticketOfUserCreatedChild){
            this.ticketOfUserCreatedChild = ticketOfUserCreatedChild;
            this.context = context;
        }

        @Override
        public void onClick(View v) {
            context.startActivity(TicketSupportDetailActivity.buildIntent(context, ticketOfUserCreatedChild.getTicketSupportOfUserCreated().getTicketID()));
        }
    }

    public void bind(TicketSupportOfMeCreatedChild ticketOfUserCreatedChild){
        childView.setOnClickListener(new PrivateTicketInventoryListener(childView.getContext() , ticketOfUserCreatedChild));
        tvTicketInventoryNotAssignNumberCode.setText(ticketOfUserCreatedChild.getTicketSupportOfUserCreated().getTicketID());
        tvTicketInventoryNotAssignTitle.setText(ticketOfUserCreatedChild.getTicketSupportOfUserCreated().getTitle());
        tvTicketInventoryNotAssignUnit.setText(ticketOfUserCreatedChild.getTicketSupportOfUserCreated().getTicketStatus());
        tvTicketInventoryNotAssignCreatedTime.setText(ticketOfUserCreatedChild.getTicketSupportOfUserCreated().getCreatedDate());
        tvTicketInventoryNotAssignEstimatedTime.setText(ticketOfUserCreatedChild.getTicketSupportOfUserCreated().getUpdateDate());
    }
}