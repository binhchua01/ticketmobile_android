package app.fpt.ticketmobile.app.jsonParsers.Fortype;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import app.fpt.ticketmobile.app.model.MyTicketFeedbackObject;

/**
 * Created by Administrator on 7/4/2016.
 */
public class MyTicketFeedbackField {

    public static final class Deserializer implements JsonDeserializer<MyTicketFeedbackObject>{

        @Override
        public MyTicketFeedbackObject deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

            Gson gson = new Gson();
            MyTicketFeedbackObject feedbackObject = null;

            try{
                JsonObject jsObj = json.getAsJsonObject();
                JsonObject jsonObject = jsObj.getAsJsonObject("Result");
                feedbackObject = gson.fromJson(jsonObject, MyTicketFeedbackObject.class);

            }catch(Exception e){
                Log.e("tmt", "MyTicketFeedbackField : Deserializer : " + e.toString());
            }

            return feedbackObject;
        }
    }
}
