package app.fpt.ticketmobile.app.jsonParsers.Fortype;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import app.fpt.ticketmobile.app.model.MyResponseObject;

/**
 * Created by Administrator on 6/30/2016.
 */
public class MyResponseJsonField  {

    public static final class Deserializer implements JsonDeserializer<MyResponseObject>{

        @Override
        public MyResponseObject deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            Gson gson = new Gson();
            MyResponseObject myResponseObject = null;

            try{
                JsonObject jsObj = json.getAsJsonObject();
                myResponseObject = gson.fromJson(jsObj.getAsJsonObject("Result"), MyResponseObject.class);

            }catch(Exception e){
                e.printStackTrace();
            }

            return myResponseObject;
        }
    }
}
