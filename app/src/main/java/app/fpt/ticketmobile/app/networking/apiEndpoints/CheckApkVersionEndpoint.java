package app.fpt.ticketmobile.app.networking.apiEndpoints;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;

import java.util.HashMap;
import java.util.Map;

import app.fpt.ticketmobile.app.model.checkversion.CheckVersionObject;
import app.fpt.ticketmobile.app.networking.utils.GsonRequest;

/**
 * Created by Administrator on 7/29/2016.
 */
public class CheckApkVersionEndpoint {

    public static GsonRequest<CheckVersionObject> getCheckVersion(final String platform, Response.Listener<CheckVersionObject> listener, Response.ErrorListener errorListener){
        GsonRequest<CheckVersionObject> request = new GsonRequest<CheckVersionObject>(Constants.MY_TICKET_CHECK_VERSION, CheckVersionObject.class, listener, errorListener){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return getParamCheckVersion(platform);
            }
        };

        return request;
    }

    private static Map<String, String> getParamCheckVersion(String platform){
        Map<String, String> params = new HashMap<>();
        params.put("Platform", platform);
        return params;
    }
}
