package app.fpt.ticketmobile.app.ui.activities.unauthenticatedActivities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import app.fpt.ticketmobile.app.ui.activities.BaseActivity;
import app.fpt.ticketmobile.app.ui.fragments.unauthenticatedFragment.FragmentLogin;
import app.fpt.ticketmobile.R;

/**
 * Created by ToanMaster on 6/15/2016.
 */
public class AuthActivity extends BaseActivity {

    private static final String EXTRA_FRAGMENT_LOGIN_KEY = "extra_fragment_login_key";

    public AuthActivity() {
        super(R.layout.activity_auth);
    }

    public static Intent buildIntent(Context context){
        Intent intent = new Intent(context, AuthActivity.class);
        return intent;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupFragmentIfNeed();
    }

    private void setupFragmentIfNeed() {
        FragmentLogin fragmentLogin = (FragmentLogin) getSupportFragmentManager().findFragmentByTag(EXTRA_FRAGMENT_LOGIN_KEY);
        if (fragmentLogin == null){
            fragmentLogin = new FragmentLogin();
        }
        getSupportFragmentManager().beginTransaction().replace(R.id.content_fragment, fragmentLogin, EXTRA_FRAGMENT_LOGIN_KEY).commit();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        //android.os.Process.killProcess(android.os.Process.myPid());
      //  finish();
      //  System.exit(0);
        Intent intent = new Intent();
        setResult(Activity.RESULT_OK, intent);
        finish();

    }


}
