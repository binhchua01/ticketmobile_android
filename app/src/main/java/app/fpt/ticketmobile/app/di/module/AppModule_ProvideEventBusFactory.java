package app.fpt.ticketmobile.app.di.module;

import javax.inject.Provider;

import app.fpt.ticketmobile.app.eventBus.EventBus;
import app.fpt.ticketmobile.app.lib.TmUiThreadRunner;
import app.fpt.ticketmobile.app.ui.lib.TmToast;
import dagger.internal.Factory;

/**
 * Created by ToanMaster on 6/14/2016.
 */
public class AppModule_ProvideEventBusFactory implements Factory<EventBus> {

    private static final boolean $assertionDisabled = !AppModule_ProvideEventBusFactory.class.desiredAssertionStatus();
    private AppModule appModule;
    private Provider<TmUiThreadRunner> threadRunnerProvider;
    private Provider<TmToast> toastProvider;

    public AppModule_ProvideEventBusFactory(AppModule appModule, Provider<TmUiThreadRunner> threadRunnerProvider, Provider<TmToast> toastProvider){
        if ($assertionDisabled || appModule != null){
            this.appModule = appModule;
            if ($assertionDisabled || threadRunnerProvider != null){
                this.threadRunnerProvider = threadRunnerProvider;
                if ($assertionDisabled || toastProvider != null){
                    this.toastProvider = toastProvider;;
                    return;
                }
                throw new AssertionError();
            }
            throw new AssertionError();
        }
        throw new AssertionError();
    }

    @Override
    public EventBus get() {
        EventBus provideEventBus = appModule.provideEventBus(threadRunnerProvider.get(), toastProvider.get());
        if (provideEventBus != null){
            return provideEventBus;
        }
        throw new NullPointerException("Cannot return null from a non-@Nullable @Provides method");
    }

    public static Factory<EventBus> create(AppModule appModule, Provider<TmUiThreadRunner> threadRunnerProvider, Provider<TmToast> toastProvider){
        return new AppModule_ProvideEventBusFactory(appModule, threadRunnerProvider, toastProvider);
    }
}
