package app.fpt.ticketmobile.app.ui.fragments.authenticatedFragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

import java.util.ArrayList;
import java.util.List;

import app.fpt.ticketmobile.app.model.MyTicketObject;
import app.fpt.ticketmobile.app.networking.apiEndpoints.MyTicketEndPoint;
import app.fpt.ticketmobile.app.ui.adapter.MyTicketAdapter;
import app.fpt.ticketmobile.app.ui.fragments.TmBaseFragment;
import app.fpt.ticketmobile.app.ui.lib.FragmentRefreshBasedSwipeRefreshLayoutHandler;
import app.fpt.ticketmobile.R;

/**
 * Created by ToanMaster on 6/21/2016.
 */
public class TicketOfUserFragment extends TmBaseFragment<TicketOfUserFragment.Callback> implements SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG_MY_TICKET_KEY = "tag_my_ticket_key";

    private RecyclerView mListTicketQueueAnStatus;
    private MyTicketAdapter myTicketAdapter;
    private List<MyTicketObject.MyTicket> myTicketObjectList;
    private View mTicketContentLayout;
    private TextView mTicketOfMeTitleContent;

    public interface Callback {

    }

    public TicketOfUserFragment() {
        super(Callback.class, Integer.valueOf(R.layout.fragment_ticket_of_user), false, true);
    }


    @Override
    protected void onTmActivityCreated(View parent, Bundle savedInstanceState) {
        super.onTmActivityCreated(parent, savedInstanceState);
        setupSwipeRefreshLayoutView(parent);

    }


    private void setupSwipeRefreshLayoutView(View parent) {
        mTicketContentLayout = parent.findViewById(R.id.tm_layout_ticket_content);
        mTicketOfMeTitleContent = (TextView) parent.findViewById(R.id.tm_tv_ticket_number_code);
        SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) parent.findViewById(R.id.swipe_refresh_layout);
        setFragmentRefreshAnimationHandler(new FragmentRefreshBasedSwipeRefreshLayoutHandler(swipeRefreshLayout, this, Integer.valueOf(R.color.tm_swipe_refresh_color)));
        mListTicketQueueAnStatus = (RecyclerView) parent.findViewById(R.id.list);
        myTicketObjectList = new ArrayList<>();
        myTicketAdapter = new MyTicketAdapter(myTicketObjectList);
        mListTicketQueueAnStatus.setAdapter(myTicketAdapter);
        mListTicketQueueAnStatus.setLayoutManager(new LinearLayoutManager(parent.getContext()));

        if (!isShowingRefreshAnimation()) {
            swipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    showRefreshAnimation();
                }
            });
        }
    }

    @Override
    public void onRefresh() {
        autoSynced();
    }


    @Override
    protected void autoSynced() {
        super.autoSynced();
        if (!isShowingRefreshAnimation()) {
            showRefreshAnimation();
        }

        getTmApiEndPointProvider().addRequest(MyTicketEndPoint.getMyTicket(getTmSharePreferencesUtils().getEmailUserLogin(), new Response.Listener<MyTicketObject>() {
            @Override
            public void onResponse(MyTicketObject myTicketObject) {
                if (getActivity() != null) {
                    if (myTicketObject != null && myTicketObject.getMyTicketList() != null) {
                        if (myTicketObject.getMyTicketList().size() > 0) {
                            myTicketObjectList.clear();
                            myTicketObjectList = new ArrayList<>(myTicketObject.getMyTicketList());

                            myTicketAdapter = new MyTicketAdapter(myTicketObjectList);
                            mListTicketQueueAnStatus.setAdapter(myTicketAdapter);
                            mListTicketQueueAnStatus.setLayoutManager(new LinearLayoutManager(getActivity()));
                            mTicketContentLayout.setVisibility(View.VISIBLE);
                            mTicketOfMeTitleContent.setText(String.valueOf(myTicketObjectList.size()));
                        } else {
                            setLayoutNoData(R.layout.fragment_no_data);
                            mTicketContentLayout.setVisibility(View.GONE);
                        }
                    } else {
                        setLayoutNoData(R.layout.fragment_no_data);
                        mTicketContentLayout.setVisibility(View.GONE);
                    }
                    stopRefreshAnimation();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onErrorListener(error);

            }
        }), TAG_MY_TICKET_KEY);
    }

    @Override
    public void onStop() {
        super.onStop();
        getTmApiEndPointProvider().cancelAllRequest(TAG_MY_TICKET_KEY);
    }

    private void onErrorListener(VolleyError error) {
        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
            getTmToast().makeToast(getString(R.string.tm_error_connect), Toast.LENGTH_SHORT);
        } else if (error instanceof AuthFailureError) {
            Log.d("tmt", "AuthFailureError");
        } else if (error instanceof ServerError) {
            Log.d("tmt", "ServerError");
        } else if (error instanceof NetworkError) {
            Log.d("tmt", "NetworkError");
        } else if (error instanceof ParseError) {
            Log.d("tmt", "ParseError");
        } else {
            Log.d("tmt", "onErrorResponse : " + error.getMessage());
        }
        stopRefreshAnimation();
    }

    private void setLayoutNoData(Integer layoutResId) {
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewGroup rootView = (ViewGroup) getView();
        View mainView = inflater.inflate(layoutResId.intValue(), rootView, false);
        rootView.removeAllViews();
        rootView.addView(mainView);
        SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) mainView.findViewById(R.id.swipe_refresh_layout);
        setFragmentRefreshAnimationHandler(new FragmentRefreshBasedSwipeRefreshLayoutHandler(swipeRefreshLayout, this, Integer.valueOf(R.color.tm_swipe_refresh_color)));

    }
}
