package app.fpt.ticketmobile.app.di.module.internal;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import app.fpt.ticketmobile.app.lib.TmApiEndPointProvider;
import app.fpt.ticketmobile.app.networking.utils.OkHttpStack;

/**
 * Created by ToanMaster on 6/14/2016.
 */
public class TmApiEndPointProvideImpl implements TmApiEndPointProvider {

    private RequestQueue mRequestQueue;
    private Map<Class, Request> mEndpointMap;

    public TmApiEndPointProvideImpl(Context context, OkHttpStack okHttpStack){
        mEndpointMap = new ConcurrentHashMap<>();
        mRequestQueue = Volley.newRequestQueue(context  , okHttpStack);
    }


    @Override
    public RequestQueue getVolleyRequestQueue() {
        return mRequestQueue;
    }

    @Override
    public <T> void addRequest(Request<T> request) {
        mRequestQueue.add(request);
    }

    @Override
    public <T> void addRequest(Request<T> request, String tag) {
        request.setTag(tag);
        mRequestQueue.add(request);
    }

    @Override
    public void cancelAllRequest(String tag) {
        mRequestQueue.cancelAll(tag);
    }


}
