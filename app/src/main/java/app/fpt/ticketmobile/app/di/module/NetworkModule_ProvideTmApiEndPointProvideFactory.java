package app.fpt.ticketmobile.app.di.module;

import android.content.Context;

import javax.inject.Provider;

import app.fpt.ticketmobile.app.lib.TmApiEndPointProvider;
import dagger.internal.Factory;
import app.fpt.ticketmobile.app.networking.utils.OkHttpStack;

/**
 * Created by ToanMaster on 6/14/2016.
 */
public class NetworkModule_ProvideTmApiEndPointProvideFactory implements Factory<TmApiEndPointProvider> {

    private static final boolean $assertionDisabled = !NetworkModule_ProvideTmApiEndPointProvideFactory.class.desiredAssertionStatus();
    private NetworkModule networkModule;
    private Provider<Context> contextProvider;
    private Provider<OkHttpStack> okHttpStackProvider;

    public NetworkModule_ProvideTmApiEndPointProvideFactory(NetworkModule networkModule, Provider<Context> contextProvider, Provider<OkHttpStack> okHttpStackProvider) {
        if ($assertionDisabled || networkModule != null) {
            this.contextProvider = contextProvider;
            if ($assertionDisabled || networkModule != null) {
                this.networkModule = networkModule;
                if ($assertionDisabled || okHttpStackProvider != null) {
                    this.okHttpStackProvider = okHttpStackProvider;
                    return;
                }
                throw new AssertionError();
            }
            throw new AssertionError();
        }
        throw new AssertionError();
    }

    @Override
    public TmApiEndPointProvider get() {
        TmApiEndPointProvider provideTmApiEndPointProvider = networkModule.provideTmApiEndPointProvider(contextProvider.get(), okHttpStackProvider.get());
        if (provideTmApiEndPointProvider != null) {
            return provideTmApiEndPointProvider;
        }
        throw new NullPointerException("Cannot return null from a non-@Nullable @Provides method");
    }

    public static Factory<TmApiEndPointProvider> create(NetworkModule networkModule, Provider<Context> contextProvider, Provider<OkHttpStack> okHttpStackProvider){
        return new NetworkModule_ProvideTmApiEndPointProvideFactory(networkModule, contextProvider, okHttpStackProvider);
    }
}
