package app.fpt.ticketmobile.app.networking.apiEndpoints;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.util.HashMap;
import java.util.Map;

import app.fpt.ticketmobile.app.model.UserIam;
import app.fpt.ticketmobile.app.model.checkversion.CheckVersionObject;
import app.fpt.ticketmobile.app.networking.utils.GsonRequest;

/**
 * Created by Administrator on 7/29/2016.
 */
public class LoginWithIamWcf {
    public static GsonRequest<UserIam> getIamWcf(final String code, final String clientSecret, final String kong,  Response.Listener<UserIam> listener, Response.ErrorListener errorListener){
        GsonRequest<UserIam> request = new GsonRequest<UserIam>(Constants.LOGIN_IAM_WCF, UserIam.class, listener, errorListener){
            @Override
            protected Map<String, String> getParams() {
                return getParam(code, clientSecret);
            }
            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                return super.parseNetworkError(volleyError);
            }
            @Override
            public Map<String, String> getHeaders() {
                return getHeaderIAM(kong);
            }
        };
        return request;
    }

    private static Map<String, String> getHeaderIAM(String token) {
        Map<String, String> params = new HashMap<>();
        params.put("Authorization", "Bearer " + token.replace('"', ' '));
        return params;
    }
    private static Map<String, String> getParam(String code, String clientSecret){
        Map<String, String> params = new HashMap<>();
        params.put("token", code);
        params.put("clientSecret", clientSecret);
        params.put("otp", "0");
        return params;
    }
}
