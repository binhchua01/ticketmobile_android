package app.fpt.ticketmobile.app.ui.adapter.libs.adapterDelegates;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import app.fpt.ticketmobile.app.ui.adapter.libs.utils.ViewUtils;
import app.fpt.ticketmobile.R;
import app.fpt.ticketmobile.app.model.User;
import app.fpt.ticketmobile.widget.views.CircularImageView;

/**
 * Created by Administrator on 7/13/2016.
 */
public class DashboardAccountAdapter extends TmAdapterDelegates<User, DashboardAccountAdapter.DashboardAccountViewHolder> {

    private AccountCallback mAccountCallback;

    public interface AccountCallback{
        void onAccountSelected(User user);
    }

    public DashboardAccountAdapter(int itemViewType, AccountCallback accountCallback) {
        super(itemViewType);
        this.mAccountCallback = accountCallback;
    }

    @Override
    public void onBindViewHolder(User user, DashboardAccountViewHolder holder) {
        holder.bind(user);
    }

    @Override
    public DashboardAccountViewHolder onCreateViewHolder(ViewGroup parent) {
        return new DashboardAccountViewHolder(ViewUtils.inflate(R.layout.entry_navigation_header, parent, false));
    }



    public class DashboardAccountViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private CircularImageView imgAccount;
        private TextView tvAccountName;
        private TextView tvAccountEmail;
        private User user;

        public DashboardAccountViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            imgAccount = (CircularImageView) itemView.findViewById(R.id.tm_img_user_account);
            tvAccountName = (TextView) itemView.findViewById(R.id.tm_tv_user_name);
        }

        public void bind(User user){
            this.user = user;
            tvAccountName.setText(user.getName());
        }

        @Override
        public void onClick(View v) {
            mAccountCallback.onAccountSelected(user);
        }
    }
}
