package app.fpt.ticketmobile.app.ui.adapter.libs.utils;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Administrator on 7/12/2016.
 */
public class ViewUtils {

    private ViewUtils() {
    }

    public static View inflate(int layoutResId, ViewGroup root, boolean attachToRoot) {
        return LayoutInflater.from(root.getContext()).inflate(layoutResId, root, false);
        // return ((LayoutInflater) root.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(layoutResId, root, attachToRoot);
    }

    public static void setVisibility(ViewGroup parent, Integer exceptId, int toVisibility) {
        int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View child = parent.getChildAt(i);
            if (exceptId == null || child.getId() != exceptId.intValue()) {
                child.setVisibility(toVisibility);
            }
        }
    }




}