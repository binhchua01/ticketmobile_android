package app.fpt.ticketmobile.app.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapter;
import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;


import java.util.List;

import app.fpt.ticketmobile.R;
import app.fpt.ticketmobile.app.ui.adapter.ticketinventoryunfinished.TicketInventoryUnFinishedChild;
import app.fpt.ticketmobile.app.ui.adapter.ticketinventoryunfinished.TicketInventoryUnFinishedChildViewHolder;
import app.fpt.ticketmobile.app.ui.adapter.ticketinventoryunfinished.TicketInventoryUnFinishedHeader;
import app.fpt.ticketmobile.app.ui.adapter.ticketinventoryunfinished.TicketInventoryUnFinishedHeaderViewHolder;

/**
 * Created by Administrator on 6/23/2016.
 */
public class MyTicketInventoryUnFinishedAdapter extends ExpandableRecyclerAdapter<TicketInventoryUnFinishedHeaderViewHolder, TicketInventoryUnFinishedChildViewHolder> {

    private LayoutInflater mInflater;
    private List<? extends ParentListItem> parentListItems;
    /**
     * Primary constructor. Sets up {@link #mParentItemList} and {@link #mItemList}.
     * <p/>
     * Changes to {@link #mParentItemList} should be made through add/remove methods in
     * {@link ExpandableRecyclerAdapter}
     *
     * @param parentItemList List of all {@link ParentListItem} objects to be
     *                       displayed in the RecyclerView that this
     *                       adapter is linked to
     */
    public MyTicketInventoryUnFinishedAdapter(Context context, @NonNull List<? extends ParentListItem> parentItemList) {
        super(parentItemList);
        parentListItems = parentItemList;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public TicketInventoryUnFinishedHeaderViewHolder onCreateParentViewHolder(ViewGroup parentViewGroup) {
        View headerView = mInflater.inflate(R.layout.entry_ticket_inventory_unfinished_header, parentViewGroup, false);
        return new TicketInventoryUnFinishedHeaderViewHolder(headerView);

    }

    @Override
    public TicketInventoryUnFinishedChildViewHolder onCreateChildViewHolder(ViewGroup childViewGroup) {
        View childView = mInflater.inflate(R.layout.entry_ticket_inventory_unfinished_child, childViewGroup, false);
        return new TicketInventoryUnFinishedChildViewHolder(childView);
    }

    @Override
    public void onBindParentViewHolder(TicketInventoryUnFinishedHeaderViewHolder parentViewHolder, int position, ParentListItem parentListItem) {
        TicketInventoryUnFinishedHeader inventoryHeader = (TicketInventoryUnFinishedHeader) parentListItem;
        parentViewHolder.bind(inventoryHeader);

    }

    @Override
    public void onBindChildViewHolder(TicketInventoryUnFinishedChildViewHolder childViewHolder, int position, Object childListItem) {
        TicketInventoryUnFinishedChild inventoryChild = (TicketInventoryUnFinishedChild) childListItem;
        childViewHolder.bind(inventoryChild);
    }

    public void clear() {
        parentListItems.clear();
        notifyDataSetChanged();
    }

    // Add a list of items
    public void addAll(List<? extends  ParentListItem> list) {
        parentListItems = list;
        notifyDataSetChanged();
    }
}
