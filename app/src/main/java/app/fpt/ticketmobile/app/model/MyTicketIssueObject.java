package app.fpt.ticketmobile.app.model;

import java.util.List;

/**
 * Created by Administrator on 6/28/2016.
 */
public class MyTicketIssueObject {

    private List<TicketIssueObject> Data;

    private List<SpinnerObject> mSpinnerObjectList;

    public List<TicketIssueObject> getData() {
        return Data;
   }

    public void setData(List<TicketIssueObject> data) {
        Data = data;
    }

    public List<SpinnerObject> getSpinnerObjectList() {
        return mSpinnerObjectList;
    }

    public void setSpinnerObjectList(List<SpinnerObject> mSpinnerObjectList) {
        this.mSpinnerObjectList = mSpinnerObjectList;
    }

    public class TicketIssueObject{
        private String IssueID;
        private String IssueName;

        public String getIssueID() {
            return IssueID;
        }

        public void setIssueID(String issueID) {
            IssueID = issueID;
        }

        public String getIssueName() {
            return IssueName;
        }

        public void setIssueName(String issueName) {
            IssueName = issueName;
        }
    }
}
