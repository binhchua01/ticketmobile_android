package app.fpt.ticketmobile.app.model;

import java.util.List;

/**
 * Created by Administrator on 6/28/2016.
 */
public class MyTicketReasonDetailObject {

    private List<ReasonDetailObject> Data;
    private List<SpinnerObject> mSpinnerObjectList;

    public List<ReasonDetailObject> getData() {
        return Data;
    }

    public void setData(List<ReasonDetailObject> data) {
        Data = data;
    }

    public List<SpinnerObject> getSpinnerObjectList() {
        return mSpinnerObjectList;
    }

    public void setSpinnerObjectList(List<SpinnerObject> mSpinnerObjectList) {
        this.mSpinnerObjectList = mSpinnerObjectList;
    }

    public class ReasonDetailObject{
        private String ReasonDetaiID;
        private String ReasonDetaiName;

        public String getReasonDetaiID() {
            return ReasonDetaiID;
        }

        public void setReasonDetaiID(String reasonDetaiID) {
            ReasonDetaiID = reasonDetaiID;
        }

        public String getReasonDetaiName() {
            return ReasonDetaiName;
        }

        public void setReasonDetaiName(String reasonDetaiName) {
            ReasonDetaiName = reasonDetaiName;
        }
    }
}
