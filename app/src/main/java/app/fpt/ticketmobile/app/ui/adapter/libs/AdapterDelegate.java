package app.fpt.ticketmobile.app.ui.adapter.libs;

import android.view.ViewGroup;

/**
 * Created by Administrator on 7/12/2016.
 */
public abstract interface AdapterDelegate<T, VH> {

    public abstract int getItemViewType();

    public abstract void onBindViewHolder(T  t, VH vh);

    public abstract VH onCreateViewHolder(ViewGroup viewGroup);
}
