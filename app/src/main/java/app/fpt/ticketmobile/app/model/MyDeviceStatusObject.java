package app.fpt.ticketmobile.app.model;

/**
 * Created by ToanMaster on 6/27/2016.
 */
public class MyDeviceStatusObject {

    private String StatusKey;
    private String StatusValue;

    public String getStatusKey() {
        return StatusKey.isEmpty() ? "" : StatusKey;
    }

    public void setStatusKey(String statusKey) {
        StatusKey = statusKey;
    }

    public String getStatusValue() {
        return StatusValue.isEmpty() ? "" : StatusValue;
    }

    public void setStatusValue(String statusValue) {
        StatusValue = statusValue;
    }


    public MyDeviceStatusObject(){}

    public MyDeviceStatusObject(String statusKey, String statusValue) {
        StatusKey = statusKey;
        StatusValue = statusValue;
    }


    @Override
    public String toString() {
        return StatusValue;
    }
}
