package app.fpt.ticketmobile.app.ui.lib;

import android.graphics.Color;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.widget.Spinner;

import app.fpt.ticketmobile.R;

/**
 * Created by ToanMaster on 6/15/2016.
 */
public class CredentialsValidator {

    public static boolean EmailValidationResult(String email){
        return email != null && !email.isEmpty() &&
                email.replaceAll("[^@]", "").length() == 1
                && !email.contains(".@");
    }


    public static boolean PasswordValidateResult(String password){
        return password != null && !password.isEmpty() && password.length() > 3;
    }
    public static void setColorPromptSpinner(Spinner spinnerInput, String inputPrompt) {
        Spannable  color_prompt = new SpannableString(inputPrompt);
        color_prompt.setSpan(new ForegroundColorSpan(Color.rgb(0,204,255)), 0,  color_prompt.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spinnerInput.setPrompt(color_prompt);
    }
}
