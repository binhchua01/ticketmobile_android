package app.fpt.ticketmobile.app.di.module;

import android.content.Context;

import javax.inject.Provider;

import app.fpt.ticketmobile.app.lib.TmConnectivityInfo;
import dagger.internal.Factory;

/**
 * Created by ToanMaster on 6/14/2016.
 */
public class AppModule_ProvideTmConnectivityInfoFactory implements Factory<TmConnectivityInfo> {

    private static final boolean $assertionDisabled = !AppModule_ProvideTmConnectivityInfoFactory.class.desiredAssertionStatus();
    private AppModule appModule;
    private Provider<Context> contextProvider;

    public AppModule_ProvideTmConnectivityInfoFactory(AppModule appModule, Provider<Context> contextProvider){
        if ($assertionDisabled || appModule != null){
            this.appModule = appModule;
            if ($assertionDisabled || contextProvider != null){
                this.contextProvider = contextProvider;
                return;
            }
        }
        throw new AssertionError();
    }


    @Override
    public TmConnectivityInfo get() {
        TmConnectivityInfo provideTmConnectivityInfo = appModule.provideTmConnectivityInfo(contextProvider.get());
        if (provideTmConnectivityInfo != null) {
            return provideTmConnectivityInfo;
        }
        throw new NullPointerException("Cannot return null from a non-@Nullable @Provides method");
    }

    public static Factory<TmConnectivityInfo> create(AppModule appModule, Provider<Context> contextProvider){
        return new AppModule_ProvideTmConnectivityInfoFactory(appModule, contextProvider);
    }
}
