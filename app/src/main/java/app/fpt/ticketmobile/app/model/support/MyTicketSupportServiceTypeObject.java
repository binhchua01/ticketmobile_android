package app.fpt.ticketmobile.app.model.support;

import java.util.List;

import app.fpt.ticketmobile.app.model.SpinnerObject;

/**
 * Created by Administrator on 7/7/2016.
 */
public class MyTicketSupportServiceTypeObject {

    private List<ServiceTypeObject> Data;
    private List<SpinnerObject> mSpinnerObjectList;


    public List<ServiceTypeObject> getServiceTypeList() {
        return Data;
    }

    public void setServiceTypeList(List<ServiceTypeObject> data) {
        Data = data;
    }

    public List<SpinnerObject> getSpinnerObjectList() {
        return mSpinnerObjectList;
    }

    public void setSpinnerObjectList(List<SpinnerObject> mSpinnerObjectList) {
        this.mSpinnerObjectList = mSpinnerObjectList;
    }

    public class ServiceTypeObject{
        private Integer ID;
        private String Name;

        public Integer getID() {
            return ID;
        }

        public void setID(Integer ID) {
            this.ID = ID;
        }

        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }
    }


}
