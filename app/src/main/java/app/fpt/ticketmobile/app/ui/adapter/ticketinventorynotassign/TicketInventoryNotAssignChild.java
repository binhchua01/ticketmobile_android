package app.fpt.ticketmobile.app.ui.adapter.ticketinventorynotassign;

import app.fpt.ticketmobile.app.model.MyTicketInventoryNotAssignObject;

/**
 * Created by ToanMaster on 7/1/2016.
 */
public class TicketInventoryNotAssignChild {

    private MyTicketInventoryNotAssignObject.TicketInventoryNotAssign ticketInventoryNotAssign;

    public MyTicketInventoryNotAssignObject.TicketInventoryNotAssign getTicketInventoryNotAssign() {
        return ticketInventoryNotAssign;
    }

    public void setTicketInventoryNotAssign(MyTicketInventoryNotAssignObject.TicketInventoryNotAssign ticketInventoryNotAssign) {
        this.ticketInventoryNotAssign = ticketInventoryNotAssign;
    }

    public TicketInventoryNotAssignChild(MyTicketInventoryNotAssignObject.TicketInventoryNotAssign ticketInventoryNotAssign) {
        this.ticketInventoryNotAssign = ticketInventoryNotAssign;
    }
}
