package app.fpt.ticketmobile.app.ui.activities.authenticatedActivities;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import app.fpt.ticketmobile.app.ui.activities.TmBaseActivity;
import app.fpt.ticketmobile.R;
import app.fpt.ticketmobile.app.ui.fragments.authenticatedFragment.TicketQueueAndStatusFragment;

/**
 * Created by ToanMaster on 6/24/2016.
 */
public class TicketQueueAndStatusActivity extends TmBaseActivity {

    private static final String FRAGMENT_TICKET_QUEUE_AND_STATUS_TAG = "fragment_ticket_queue_and_status_tag";
    private static final String EXTRA_INTENT_QUEUE_ID_KEY = "extra_intent_queue_id_key";
    private static final String EXTRA_INTENT_QUEUE_NAME_KEY = "extra_intent_queue_name_key";
    private static final String EXTRA_INTENT_QUEUE_STATUS_TITLE_KEY = "extra_intent_queue_status_title_key";
    private static final String EXTRA_INTENT_QUEUE_STATUS_ID_KEY = "extra_intent_queue_status_id_key";


    public TicketQueueAndStatusActivity() {
        super(Integer.valueOf(R.layout.activity_ticket_queue_and_status_activity));
    }

    public static Intent buildIntent(Context context, String queueId, String queueName, String statusTitle, String statusId){
        Intent intent = new Intent(context, TicketQueueAndStatusActivity.class);
        intent.putExtra(EXTRA_INTENT_QUEUE_ID_KEY, queueId);
        intent.putExtra(EXTRA_INTENT_QUEUE_NAME_KEY, queueName);
        intent.putExtra(EXTRA_INTENT_QUEUE_STATUS_TITLE_KEY, statusTitle);
        intent.putExtra(EXTRA_INTENT_QUEUE_STATUS_ID_KEY, statusId);
        return intent;
    }


    @Override
    protected void setupToolbar(Toolbar mToolbar) {
        super.setupToolbar(mToolbar);
        Intent intent = getIntent();
        if (intent != null){
            String mQueueId = intent.getStringExtra(EXTRA_INTENT_QUEUE_ID_KEY);
            String mQueueName = intent.getStringExtra(EXTRA_INTENT_QUEUE_NAME_KEY);
            String mQueueStatusTitle = intent.getStringExtra(EXTRA_INTENT_QUEUE_STATUS_TITLE_KEY);
            String mQueueStatusId = intent.getStringExtra(EXTRA_INTENT_QUEUE_STATUS_ID_KEY);
            if (mQueueName != null) {
                ((TextView) mToolbar.findViewById(R.id.main_toolbar_title)).setText("Queue : " + mQueueName);
            }
            setupFragmentIfNeeds(mQueueId, mQueueStatusId, mQueueStatusTitle);
        }

    }

    private void setupFragmentIfNeeds(String mQueueId, String mQueueStatusId, String mQueueStatusTitle) {
        TicketQueueAndStatusFragment ticketQueueAndStatusFragment =  (TicketQueueAndStatusFragment) getSupportFragmentManager().findFragmentByTag(FRAGMENT_TICKET_QUEUE_AND_STATUS_TAG);
        if (ticketQueueAndStatusFragment == null){
            ticketQueueAndStatusFragment = TicketQueueAndStatusFragment.getInstances(mQueueId, mQueueStatusId, mQueueStatusTitle);
        }
        getSupportFragmentManager().beginTransaction().replace(R.id.content_fragment, ticketQueueAndStatusFragment, FRAGMENT_TICKET_QUEUE_AND_STATUS_TAG).commit();
    }
}
