package app.fpt.ticketmobile.app.ui.adapter.ticketinventorynotassign;

import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Administrator on 7/1/2016.
 */
public class TicketInventoryNotAssignHeader implements ParentListItem, Serializable {

    private String QueueName;
    private List<TicketInventoryNotAssignChild> mTicketInventoryNotAssignChildList;

    public TicketInventoryNotAssignHeader(){}

    public TicketInventoryNotAssignHeader(String queueName, List<TicketInventoryNotAssignChild> mTicketInventoryNotAssignChildList) {
        QueueName = queueName;
        this.mTicketInventoryNotAssignChildList = mTicketInventoryNotAssignChildList;
    }


    public String getQueueName() {
        return QueueName;
    }

    public void setQueueName(String queueName) {
        QueueName = queueName;
    }

    public List<TicketInventoryNotAssignChild> getTicketInventoryNotAssignChildList() {
        return mTicketInventoryNotAssignChildList;
    }

    public void setTicketInventoryNotAssignChildList(List<TicketInventoryNotAssignChild> mTicketInventoryNotAssignChildList) {
        this.mTicketInventoryNotAssignChildList = mTicketInventoryNotAssignChildList;
    }

    @Override
    public List<?> getChildItemList() {
        return mTicketInventoryNotAssignChildList;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }
}
