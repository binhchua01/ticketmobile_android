package app.fpt.ticketmobile.app.jsonParsers.Fortype;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import app.fpt.ticketmobile.app.model.MyTicketStatusObject;
import app.fpt.ticketmobile.app.model.SpinnerObject;

/**
 * Created by Administrator on 6/29/2016.
 */
public class MyTicketStatusField {

    public static final class Deserializer implements JsonDeserializer<MyTicketStatusObject>{

        @Override
        public MyTicketStatusObject deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            Gson gson = new Gson();
            MyTicketStatusObject myTicketStatusObject = new MyTicketStatusObject();
            List<SpinnerObject> spinnerObjectList = new ArrayList<>();

            try{
                JsonObject jsObj = json.getAsJsonObject();
                JsonObject jsonObject = jsObj.getAsJsonObject("Result");
                myTicketStatusObject = gson.fromJson(jsonObject, MyTicketStatusObject.class);

                if (myTicketStatusObject.getData() != null && myTicketStatusObject.getData().size() > 0){
                    for (int i = 0; i < myTicketStatusObject.getData().size(); i++){
                        MyTicketStatusObject.StatusObject statusObject = myTicketStatusObject.getData().get(i);
                        SpinnerObject spinnerObject = new SpinnerObject(String.valueOf(statusObject.getValue()), statusObject.getDescription());
                        spinnerObjectList.add(spinnerObject);
                    }
                    myTicketStatusObject.setSpinnerObjectList(spinnerObjectList);
                }


            }catch(Exception exception){
                Log.e("tmt", "MyTicketStatusField : Deserializer " + exception.toString());
            }
            return myTicketStatusObject;
        }
    }
}
