package app.fpt.ticketmobile.app.model;

import java.util.List;

/**
 * Created by Administrator on 6/29/2016.
 */
public class MyTicketQueueObject {

    private List<QueueObject> Data;
    private List<SpinnerObject> mSpinnerObjectList;

    public List<QueueObject> getData() {
        return Data;
    }

    public void setData(List<QueueObject> data) {
        Data = data;
    }

    public List<SpinnerObject> getSpinnerObjectList() {
        return mSpinnerObjectList;
    }

    public void setSpinnerObjectList(List<SpinnerObject> mSpinnerObjectList) {
        this.mSpinnerObjectList = mSpinnerObjectList;
    }

    public class QueueObject{
        private String ID;
        private String Name;

        public String getID() {
            return ID;
        }

        public void setID(String ID) {
            this.ID = ID;
        }

        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }
    }
}
