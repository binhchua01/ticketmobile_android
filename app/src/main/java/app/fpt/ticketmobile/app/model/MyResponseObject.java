package app.fpt.ticketmobile.app.model;

/**
 * Created by ToanMaster on 6/30/2016.
 */
public class MyResponseObject {

    private Integer ErrorCode;
    private String Data;
    private String ErrorDescription;

    public String getData() {
        return Data;
    }

    public void setData(String data) {
        Data = data;
    }

    public Integer getErrorCode() {
        return ErrorCode;
    }

    public void setErrorCode(Integer errorCode) {
        ErrorCode = errorCode;
    }

    public String getErrorDescription() {
        return ErrorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        ErrorDescription = errorDescription;
    }
}
