package app.fpt.ticketmobile.app.ui.lib;

/**
 * Created by ToanMaster on 6/15/2016.
 */
public interface FragmentRefreshAnimationHandler {

    boolean isFragmentShowingRefreshAnimation();

    void showRefreshAnimationInFragment();

    void stopRefreshAnimationInFragment();
}
