package app.fpt.ticketmobile.app.jsonParsers.Fortype;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import app.fpt.ticketmobile.app.model.MyTicketObject;

/**
 * Created by ToanMaster on 6/21/2016.
 */
public abstract class MyTicketField {

    public static final class Deserializer implements JsonDeserializer<MyTicketObject> {

        @Override
        public MyTicketObject deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

            MyTicketObject myTicketObject = null;
            Gson gson = new GsonBuilder().create();
            try {

                final JsonObject jsObj = json.getAsJsonObject();
                JsonObject jsonObject = jsObj.getAsJsonObject("Result");
                myTicketObject = gson.fromJson(jsonObject, MyTicketObject.class);
            }
            catch (Exception ex){
                Log.e("tmt", "ex : " + ex.getMessage().toString());
            }
            return myTicketObject;
        }
    }

}


