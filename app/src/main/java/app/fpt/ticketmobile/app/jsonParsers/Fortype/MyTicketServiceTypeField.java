package app.fpt.ticketmobile.app.jsonParsers.Fortype;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import app.fpt.ticketmobile.app.model.MyTicketServiceTypeObject;
import app.fpt.ticketmobile.app.model.SpinnerObject;

/**
 * Created by Administrator on 6/28/2016.
 */
public class MyTicketServiceTypeField {

    public static final class Deserializer implements JsonDeserializer<MyTicketServiceTypeObject>{

        @Override
        public MyTicketServiceTypeObject deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            Gson gson = new Gson();
            MyTicketServiceTypeObject ticketServiceTypeObject = new MyTicketServiceTypeObject();
            List<SpinnerObject> spinnerObjectList = new ArrayList<>();

            try{
                JsonObject jsObj = json.getAsJsonObject();
                JsonObject jsonObject = jsObj.getAsJsonObject("Result");

                ticketServiceTypeObject = gson.fromJson(jsonObject, MyTicketServiceTypeObject.class);

                if (ticketServiceTypeObject.getData() != null && ticketServiceTypeObject.getData().size() > 0) {
                    for (int i = 0; i < ticketServiceTypeObject.getData().size(); i++) {
                        MyTicketServiceTypeObject.ServiceTypeObject serviceTypeObject = ticketServiceTypeObject.getData().get(i);
                        SpinnerObject spinnerObject = new SpinnerObject(String.valueOf(serviceTypeObject.getID()), serviceTypeObject.getName());
                        spinnerObjectList.add(spinnerObject);
                    }
                    ticketServiceTypeObject.setSpinnerObjectList(spinnerObjectList);
                }

            }catch (Exception e){
                Log.e("tmt", "MyTicketReasonDetailField : " + e.toString());
            }

            return ticketServiceTypeObject;
        }
    }
}
