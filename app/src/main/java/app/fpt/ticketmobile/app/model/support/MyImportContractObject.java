package app.fpt.ticketmobile.app.model.support;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Administrator on 7/25/2016.
 */
public class MyImportContractObject implements Serializable{

    private List<ImportContractObject> Data;

    public List<ImportContractObject> getImportContractObjectList() {
        return Data;
    }

    public void setImportContractObjectList(List<ImportContractObject> mImportContractObjectList) {
        this.Data = mImportContractObjectList;
    }

    public MyImportContractObject(){}

    public MyImportContractObject(List<ImportContractObject> mImportContractObjectList) {
        this.Data = mImportContractObjectList;
    }
}
