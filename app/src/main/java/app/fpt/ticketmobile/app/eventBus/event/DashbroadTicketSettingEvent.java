package app.fpt.ticketmobile.app.eventBus.event;

import app.fpt.ticketmobile.app.eventBus.Event;
import app.fpt.ticketmobile.app.ui.lib.TicketSettingInfo;

/**
 * Created by Administrator on 7/28/2016.
 */
public class DashbroadTicketSettingEvent implements Event {
    private TicketSettingInfo mTicketSetting;
    private String mTitle;

    public TicketSettingInfo getTicketSettingInfo() {
        return mTicketSetting;
    }

    public void setTicketSettingInfo(TicketSettingInfo mTicketSetting) {
        this.mTicketSetting = mTicketSetting;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public DashbroadTicketSettingEvent(TicketSettingInfo mTicketSetting, String mTitle) {
        this.mTicketSetting = mTicketSetting;
        this.mTitle = mTitle;
    }
}
