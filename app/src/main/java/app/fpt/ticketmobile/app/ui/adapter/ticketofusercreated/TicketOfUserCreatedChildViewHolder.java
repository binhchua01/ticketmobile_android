package app.fpt.ticketmobile.app.ui.adapter.ticketofusercreated;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ViewHolder.ChildViewHolder;

import app.fpt.ticketmobile.R;
import app.fpt.ticketmobile.app.ui.activities.authenticatedActivities.TicketDetailActivity;

/**
 * Created by Administrator on 7/1/2016.
 */
public class TicketOfUserCreatedChildViewHolder extends ChildViewHolder {

    private TextView tvTicketInventoryNotAssignNumberCode;
    private TextView tvTicketInventoryNotAssignTitle;
    private TextView tvTicketInventoryNotAssignUnit;
    private TextView tvTicketInventoryNotAssignCreatedTime;
    private TextView tvTicketInventoryNotAssignEstimatedTime;
    private View childView = null;

    public TicketOfUserCreatedChildViewHolder(View childView) {
        super(childView);
        this.childView = childView;
        tvTicketInventoryNotAssignNumberCode = (TextView) childView.findViewById(R.id.tm_tv_ticket_number_code);
        tvTicketInventoryNotAssignTitle = (TextView) childView.findViewById(R.id.tm_tv_ticket_title);
        tvTicketInventoryNotAssignUnit = (TextView) childView.findViewById(R.id.tm_tv_ticket_unit);
        tvTicketInventoryNotAssignCreatedTime = (TextView) childView.findViewById(R.id.tm_tv_ticket_created_time);
        tvTicketInventoryNotAssignEstimatedTime = (TextView) childView.findViewById(R.id.tm_tv_ticket_estimated_time);
    }


    private final class PrivateTicketInventoryListener implements View.OnClickListener{
        private TicketOfUserCreatedChild ticketOfUserCreatedChild;
        private Context context;

        public PrivateTicketInventoryListener(Context context, TicketOfUserCreatedChild ticketOfUserCreatedChild){
            this.ticketOfUserCreatedChild = ticketOfUserCreatedChild;
            this.context = context;
        }

        @Override
        public void onClick(View v) {
            context.startActivity(TicketDetailActivity.buildIntent(context, String.valueOf(ticketOfUserCreatedChild.getTicketOfUserCreated().getTicketID())));
        }
    }

    public void bind(TicketOfUserCreatedChild ticketOfUserCreatedChild){
        childView.setOnClickListener(new PrivateTicketInventoryListener(childView.getContext() , ticketOfUserCreatedChild));
        tvTicketInventoryNotAssignNumberCode.setText(ticketOfUserCreatedChild.getTicketOfUserCreated().getTicketID());
        tvTicketInventoryNotAssignTitle.setText(ticketOfUserCreatedChild.getTicketOfUserCreated().getTitle());
        tvTicketInventoryNotAssignUnit.setText(ticketOfUserCreatedChild.getTicketOfUserCreated().getTicketStatus());
        tvTicketInventoryNotAssignCreatedTime.setText(ticketOfUserCreatedChild.getTicketOfUserCreated().getCreatedDate());
        tvTicketInventoryNotAssignEstimatedTime.setText(ticketOfUserCreatedChild.getTicketOfUserCreated().getEstimatedTime());
    }
}
