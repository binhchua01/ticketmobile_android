package app.fpt.ticketmobile.app.ui.adapter.ticketinventoryunfinished;

import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ToanMaster on 6/23/2016.
 */
public class TicketInventoryUnFinishedHeader implements ParentListItem, Serializable{

    private Integer QueueID ;
    private String QueueName;
    private List<TicketInventoryUnFinishedChild> mTicketInventoryChildList;


    public TicketInventoryUnFinishedHeader(String queueName, Integer queueID, List<TicketInventoryUnFinishedChild> mTicketInventoryChildList) {
        QueueName = queueName;
        QueueID = queueID;
        this.mTicketInventoryChildList = mTicketInventoryChildList;
    }

    public Integer getQueueID() {
        return QueueID;
    }

    public void setQueueID(Integer queueID) {
        QueueID = queueID;
    }

    public String getQueueName() {
        return QueueName;
    }

    public void setQueueName(String queueName) {
        QueueName = queueName;
    }

    public List<TicketInventoryUnFinishedChild> getTicketInventoryChildList() {
        return mTicketInventoryChildList;
    }

    public void setTicketInventoryChildList(List<TicketInventoryUnFinishedChild> mTicketInventoryChildList) {
        this.mTicketInventoryChildList = mTicketInventoryChildList;
    }


    @Override
    public List<?> getChildItemList() {
        return mTicketInventoryChildList;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }
}
