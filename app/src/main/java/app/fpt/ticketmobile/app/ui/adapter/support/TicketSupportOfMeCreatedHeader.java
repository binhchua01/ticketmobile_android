package app.fpt.ticketmobile.app.ui.adapter.support;

import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Administrator on 7/25/2016.
 */
public class TicketSupportOfMeCreatedHeader implements ParentListItem, Serializable {

    private String QueueName;
    private List<TicketSupportOfMeCreatedChild> ticketOfUserCreatedChildList;

    public TicketSupportOfMeCreatedHeader(){}

    public TicketSupportOfMeCreatedHeader(String queueName, List<TicketSupportOfMeCreatedChild> ticketOfUserCreatedChildList) {
        QueueName = queueName;
        this.ticketOfUserCreatedChildList = ticketOfUserCreatedChildList;
    }


    public String getQueueName() {
        return QueueName;
    }

    public void setQueueName(String queueName) {
        QueueName = queueName;
    }

    public List<TicketSupportOfMeCreatedChild> getTicketOfUserCreatedChildList() {
        return ticketOfUserCreatedChildList;
    }

    public void setTicketOfUserCreatedChildList(List<TicketSupportOfMeCreatedChild> ticketSupportOfMeCreatedChildList) {
        this.ticketOfUserCreatedChildList = ticketOfUserCreatedChildList;
    }

    @Override
    public List<?> getChildItemList() {
        return ticketOfUserCreatedChildList;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }

}
