package app.fpt.ticketmobile.app.di.module.internal;

import android.os.Handler;
import android.os.Looper;


import app.fpt.ticketmobile.app.lib.TmUiThreadRunner;

/**
 * Created by ToanMaster on 6/13/2016.
 */
public class TmUiThreadRunnerImpl implements TmUiThreadRunner {

    private final Handler mUiThreadHandler;

    public TmUiThreadRunnerImpl(Handler uiThreadRunner){
        this.mUiThreadHandler = uiThreadRunner;
    }

    @Override
    public void runOnUiThread(Runnable runnable) {
        if (Looper.getMainLooper() == Looper.myLooper()){
            runnable.run();
            return;
        }
        mUiThreadHandler.post(runnable);
    }

    @Override
    public void runOnUiThread(Runnable runnable, long delay) {
        if (delay <= 0L){
            runOnUiThread(runnable);
            return;
        }
        mUiThreadHandler.postDelayed(runnable, delay);
    }
}
