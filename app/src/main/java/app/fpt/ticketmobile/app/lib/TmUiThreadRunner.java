package app.fpt.ticketmobile.app.lib;

/**
 * Created by Administrator on 6/13/2016.
 */
public interface TmUiThreadRunner {

    void runOnUiThread(Runnable runnable);

    void runOnUiThread(Runnable runnable, long delay);
}
