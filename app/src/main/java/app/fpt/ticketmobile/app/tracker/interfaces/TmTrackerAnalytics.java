package app.fpt.ticketmobile.app.tracker.interfaces;

/**
 * Created by tmt on 8/26/2016.
 */
public interface TmTrackerAnalytics {

    void trackScreenView(String screenView);

    void trackEvent(String category, String action, String label);

    void trackException(Exception e);
}
