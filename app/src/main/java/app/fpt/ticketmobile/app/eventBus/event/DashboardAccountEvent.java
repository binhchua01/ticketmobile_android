package app.fpt.ticketmobile.app.eventBus.event;

import app.fpt.ticketmobile.app.eventBus.Event;
import app.fpt.ticketmobile.app.model.User;

/**
 * Created by Administrator on 7/13/2016.
 */
public class DashboardAccountEvent implements Event {

    private User mUser;

    public User getUser() {
        return mUser;
    }

    public void setUser(User mUser) {
        this.mUser = mUser;
    }

    public DashboardAccountEvent(User mUser) {
        this.mUser = mUser;
    }
}
