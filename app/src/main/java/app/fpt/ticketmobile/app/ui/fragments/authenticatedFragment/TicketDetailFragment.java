package app.fpt.ticketmobile.app.ui.fragments.authenticatedFragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.squareup.otto.Subscribe;

import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import app.fpt.ticketmobile.app.eventBus.event.TicketDetailFeedBackEvent;
import app.fpt.ticketmobile.app.eventBus.event.TicketDetailSaveEvent;
import app.fpt.ticketmobile.app.model.MyResponseObject;
import app.fpt.ticketmobile.app.model.MyTicketCusTypeObject;
import app.fpt.ticketmobile.app.model.MyTicketDetailObject;
import app.fpt.ticketmobile.app.model.MyTicketIssueObject;
import app.fpt.ticketmobile.app.model.MyTicketReasonDetailObject;
import app.fpt.ticketmobile.app.model.MyTicketReasonObject;
import app.fpt.ticketmobile.app.model.MyTicketServiceTypeObject;
import app.fpt.ticketmobile.app.model.MyTicketStatusObject;
import app.fpt.ticketmobile.app.model.SpinnerObject;
import app.fpt.ticketmobile.app.networking.apiEndpoints.MyTicketDetailEndPoint;
import app.fpt.ticketmobile.app.ui.adapter.MyDeviceAdapter;
import app.fpt.ticketmobile.R;
import app.fpt.ticketmobile.app.lib.utils.TmUtils;
import app.fpt.ticketmobile.app.model.MyDeviceObject;
import app.fpt.ticketmobile.app.model.MyTicketEffectObject;
import app.fpt.ticketmobile.app.model.MyTicketEstimatedDateObject;
import app.fpt.ticketmobile.app.model.MyTicketOperateStaffObject;
import app.fpt.ticketmobile.app.model.MyTicketProcessStaffObject;
import app.fpt.ticketmobile.app.model.MyTicketQueueObject;
import app.fpt.ticketmobile.app.model.MyTicketVisorStaffObject;
import app.fpt.ticketmobile.app.ui.activities.authenticatedActivities.TicketFeedBackActivity;
import app.fpt.ticketmobile.app.ui.fragments.TmBaseFragment;
import app.fpt.ticketmobile.app.ui.lib.CredentialsValidator;
import app.fpt.ticketmobile.app.ui.lib.FragmentRefreshBasedSwipeRefreshLayoutHandler;
import app.fpt.ticketmobile.widget.views.MultiSelectionObjectSpinner;
import app.fpt.ticketmobile.widget.views.libs.SpinnerCallbackInterface;

/**
 * Created by ToanMaster on 6/24/2016.
 */
public class TicketDetailFragment extends TmBaseFragment<TicketDetailFragment.Callback> implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    private static final String EXTRA_REQUEST_TICKET_DETAIL_TAG = "extra_request_ticket_detail_tag";
    private static final String EXTRA_REQUEST_TICKET_DEVICE_TAG = "extra_request_ticket_device_tag";
    private static final String EXTRA_REQUEST_TICKET_ISSUE_TAG = "extra_request_ticket_issue_tag";
    private static final String EXTRA_REQUEST_TICKET_REASON_TAG = "extra_request_ticket_reason_tag";
    private static final String EXTRA_REQUEST_TICKET_REASON_DETAIL_TAG = "extra_request_ticket_reason_detail_tag";
    private static final String EXTRA_REQUEST_TICKET_SERVICE_TYPE_TAG = "extra_request_ticket_service_type_tag";
    private static final String EXTRA_REQUEST_TICKET_CUS_TYPE_TAG = "extra_request_ticket_cus_type_tag";
    private static final String EXTRA_REQUEST_TICKET_EFFECT_TAG = "extra_request_ticket_effect_tag";
    private static final String EXTRA_REQUEST_TICKET_QUEUE_TAG = "extra_request_ticket_queue_tag";
    private static final String EXTRA_REQUEST_TICKET_STATUS_TAG = "extra_request_ticket_status_tag";
    private static final String EXTRA_REQUEST_TICKET_OPERATE_STAFF_TAG = "extra_request_ticket_operate_staff_tag";
    private static final String EXTRA_REQUEST_TICKET_VISOR_STAFF_TAG = "extra_request_ticket_visor_staff_tag";
    private static final String EXTRA_REQUEST_TICKET_PROCESS_STAFF_TAG = "extra_request_ticket_process_staff_tag";

    private static final String EXTRA_UPDATE_TICKET_DEVICE_TAG = "extra_update_ticket_device";
    private static final String EXTRA_UPDATE_TICKET_DETAIL_TAG = "extra_update_ticket_detail_tag";

    private static final String EXTRA_TICKET_CODE_INTENT_KEY = "extra_ticket_code_intent_key";
    private static final String PRIVATE_KEY_CODE = "AiEcSi@@@2015";
    private static final String EXTRA_TICKET_USER_ASSIGN_KEY = "1";

    private static int CURRENT_DESCRIPTION_ISSUE_SPINNER_SELECTION = 0;
    private static int CURRENT_DESCRIPTION_REASON_SPINNER_SELECTION = 0;
    private static int CURRENT_DESCRIPTION_REASON_DETAIL_SPINNER_SELECTION = 0;
    private static int CURRENT_DESCRIPTION_EFFECT_SPINNER_SELECTION = 0;
    private static int CURRENT_EMPLOYEE_QUEUE_SPINNER_SELECTION = 0;
    private static int CURRENT_EMPLOYEE_PROCESS_STAFF_SPINNER_SELECTION = 0;
    private static int CURRENT_EMPLOYEE_STATUS_SPINNER_SELECTION = 0;

    private String TICKET_ID = null;

    private TextView tvTicketNumber;
    private TextView tvRegionLocation, tvRegionAgent, tvRegionGroupElement, tvRegionElement, tvRegionDeviceCount, tvRegionDescription;
    private RecyclerView lvRegionDevice;
    private Spinner mSpinnerIssueDescription, mSpinnerReason, mSpinnerReasonDetail,
            mSpinnerDescriptionEffect;
    private MultiSelectionObjectSpinner mSpinnerServiceType, mSpinnerCusType;
    private TextView tvDescriptionCusQty, tvDescriptionLevel, tvDescriptionPlayTvQty, tvDescriptionCritical;
    private TextView tvEmployeeFoundName, tvEmployeeFoundMobile, tvEmployeeFoundIpPhone, tvEmployeeCreatedTicket,
            tvEmployeeRequiredDateTicket, tvEmployeeProcessPhone, tvEmployeeProcessIpPhone;
    private Spinner mSpinnerEmployeeStatus, mSpinnerEmployeeQueue, mSpinnerEmployeeOperateStaff,
            mSpinnerEmployeeVisorStaff, mSpinnerEmployeeProcessStaff;
    private TextView tvEmployeeIssueDate, tvEmployeeFinishedExpectDate;
    private ProgressDialog mProgressDialog;

    private MyDeviceAdapter myDeviceAdapter = null;
    private MyDeviceObject mMyDeviceObject = null;
    private boolean mLoadProcessStaff = false;

    private List<SpinnerObject> mSpinnerProcessStaffList = null;

    private static MyTicketDetailObject MY_TICKET_DETAIL_OBJECT = null;
    private static StateTicket MY_TICKET_STATE = StateTicket.NEW;

    private SpinnerObject mDescriptionObject = null;
    private SpinnerObject mReasonObject = null;
    private SpinnerObject mReasonDetailObject = null;
    private SpinnerObject mServiceTypeObject = null;
    private SpinnerObject mCusTypeObject = null;
    private SpinnerObject mEffectObject = null;
    private SpinnerObject mStatusObject = null;
    private SpinnerObject mQueueObject = null;
    private SpinnerObject mOperateStaffObject = null;
    private SpinnerObject mVisorStaffObject = null;
    private SpinnerObject mProcessStaffObject = null;

    private final List<SpinnerObject> mListSpinner = new ArrayList<>();
    private List<SpinnerObject> mDescriptionList = new ArrayList<>();
    private List<SpinnerObject> mReasonList = new ArrayList<>();
    private List<SpinnerObject> mReasonDetailList = new ArrayList<>();
    private List<SpinnerObject> mServiceTypeList = new ArrayList<>();
    private List<SpinnerObject> mCusTypeList = new ArrayList<>();
    private List<SpinnerObject> mEffectList = new ArrayList<>();
    private List<SpinnerObject> mStatusTypeList = new ArrayList<>();
    private List<SpinnerObject> mQueueList = new ArrayList<>();
    private List<SpinnerObject> mOperateStaffList = new ArrayList<>();
    private List<SpinnerObject> mVisorStaffList = new ArrayList<>();
    private List<SpinnerObject> mProcessStaffList = new ArrayList<>();
    // tao chon tg ngay thang nam
    private List<String> lDays = new ArrayList<String>();
    private List<String> lMoths = new ArrayList<String>();
    private List<String> lYears = new ArrayList<String>();
    private Spinner sDays, sMoths, sYears;
    private ArrayAdapter<String> daysAdapter, mothsAdapter, yearsAdapter;

    public enum StateTicket {
        NEW,
        INPROGRESS
    }

    public interface Callback {

    }

    public TicketDetailFragment() {
        super(Callback.class, Integer.valueOf(R.layout.fragment_ticket_detail), false, true);
    }

    public static TicketDetailFragment getInstances(String ticketId) {
        Bundle bundle = new Bundle();
        bundle.putString(EXTRA_TICKET_CODE_INTENT_KEY, ticketId);
        TicketDetailFragment ticketDetailFragment = new TicketDetailFragment();
        ticketDetailFragment.setArguments(bundle);
        return ticketDetailFragment;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mMyDeviceObject = new MyDeviceObject();

        mDescriptionObject = new SpinnerObject("-100", getString(R.string.tm_choose_description));
        mReasonObject = new SpinnerObject("-100", getString(R.string.tm_choose_reason));
        mReasonDetailObject = new SpinnerObject("-100", getString(R.string.tm_choose_reason_detail));
        mServiceTypeObject = new SpinnerObject("-100", getString(R.string.tm_choose_service_type));
        mCusTypeObject = new SpinnerObject("-100", getString(R.string.tm_choose_cus_type));
        mEffectObject = new SpinnerObject("-100", getString(R.string.tm_choose_effect));

        // because default key "Description":"Inprogress","Value":0}
        mStatusObject = new SpinnerObject("-100", getString(R.string.tm_choose_status));

        mQueueObject = new SpinnerObject("-100", getString(R.string.tm_choose_queue));
        mOperateStaffObject = new SpinnerObject("-100", getString(R.string.tm_choose_operate_staff));
        mVisorStaffObject = new SpinnerObject("-100", getString(R.string.tm_choose_visor_staff));
        mProcessStaffObject = new SpinnerObject("-100", getString(R.string.tm_choose_process_staff));

        mDescriptionList.add(0, mDescriptionObject);
        mReasonList.add(0, mReasonObject);
        mReasonDetailList.add(0, mReasonDetailObject);
        mServiceTypeList.add(0, mServiceTypeObject);
        mCusTypeList.add(0, mCusTypeObject);
        mEffectList.add(0, mEffectObject);
        mStatusTypeList.add(0, mStatusObject);
        mQueueList.add(0, mQueueObject);
        mOperateStaffList.add(0, mOperateStaffObject);
        mVisorStaffList.add(0, mVisorStaffObject);
        mProcessStaffList.add(0, mProcessStaffObject);
        extraArguments();
    }


    @Override
    protected void onTmActivityCreated(View parent, Bundle savedInstanceState) {
        super.onTmActivityCreated(parent, savedInstanceState);
        setupViews(parent);
        if (parent != null && getActivity() != null) {
            TmUtils.setupHideKeyBoardParent(parent, getActivity());
        }
    }


    @Override
    public void onStop() {
        getTmApiEndPointProvider().cancelAllRequest(EXTRA_REQUEST_TICKET_DETAIL_TAG);
        getTmApiEndPointProvider().cancelAllRequest(EXTRA_REQUEST_TICKET_DEVICE_TAG);
        getTmApiEndPointProvider().cancelAllRequest(EXTRA_REQUEST_TICKET_ISSUE_TAG);
        getTmApiEndPointProvider().cancelAllRequest(EXTRA_REQUEST_TICKET_REASON_TAG);
        getTmApiEndPointProvider().cancelAllRequest(EXTRA_REQUEST_TICKET_REASON_DETAIL_TAG);
        getTmApiEndPointProvider().cancelAllRequest(EXTRA_REQUEST_TICKET_SERVICE_TYPE_TAG);
        getTmApiEndPointProvider().cancelAllRequest(EXTRA_REQUEST_TICKET_CUS_TYPE_TAG);
        getTmApiEndPointProvider().cancelAllRequest(EXTRA_REQUEST_TICKET_EFFECT_TAG);
        getTmApiEndPointProvider().cancelAllRequest(EXTRA_REQUEST_TICKET_QUEUE_TAG);
        getTmApiEndPointProvider().cancelAllRequest(EXTRA_REQUEST_TICKET_STATUS_TAG);
        getTmApiEndPointProvider().cancelAllRequest(EXTRA_REQUEST_TICKET_OPERATE_STAFF_TAG);
        getTmApiEndPointProvider().cancelAllRequest(EXTRA_REQUEST_TICKET_VISOR_STAFF_TAG);
        getTmApiEndPointProvider().cancelAllRequest(EXTRA_REQUEST_TICKET_PROCESS_STAFF_TAG);
        getTmApiEndPointProvider().cancelAllRequest(EXTRA_UPDATE_TICKET_DETAIL_TAG);

        super.onStop();
    }

    private void setupViews(View parent) {
        mProgressDialog = new ProgressDialog(getContext());
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setCancelable(false);

        SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) parent.findViewById(R.id.swipe_refresh_layout);
        setFragmentRefreshAnimationHandler(new FragmentRefreshBasedSwipeRefreshLayoutHandler(swipeRefreshLayout, this, Integer.valueOf(R.color.tm_swipe_refresh_color)));
        // swipeRefreshLayout.setEnabled(false);

        tvTicketNumber = (TextView) parent.findViewById(R.id.tm_tv_ticket_detail_no_number);
        tvRegionLocation = (TextView) parent.findViewById(R.id.tm_tv_ticket_detail_region_location);
        tvRegionAgent = (TextView) parent.findViewById(R.id.tm_tv_ticket_detail_region_agent);
        tvRegionGroupElement = (TextView) parent.findViewById(R.id.tm_tv_ticket_detail_region_group_element);
        tvRegionElement = (TextView) parent.findViewById(R.id.tm_tv_ticket_detail_region_element);
        tvRegionDeviceCount = (TextView) parent.findViewById(R.id.tm_tv_ticket_detail_region_device_count);
        tvRegionDescription = (TextView) parent.findViewById(R.id.tm_tv_region_description);
        lvRegionDevice = (RecyclerView) parent.findViewById(R.id.tm_lv_ticket_detail_region_device);

        mSpinnerIssueDescription = (Spinner) parent.findViewById(R.id.tm_spinner_ticket_detail_description_issue);
        CredentialsValidator.setColorPromptSpinner(mSpinnerIssueDescription, getResources().getString(R.string.tm_ticket_detail_description_title));
        mSpinnerReason = (Spinner) parent.findViewById(R.id.tm_spinner_ticket_detail_description_reason);
        CredentialsValidator.setColorPromptSpinner(mSpinnerReason, getResources().getString(R.string.tm_ticket_detail_description_reason_spinner));
        mSpinnerReasonDetail = (Spinner) parent.findViewById(R.id.tm_spinner_ticket_detail_description_reason_detail);
        CredentialsValidator.setColorPromptSpinner(mSpinnerReasonDetail, getResources().getString(R.string.tm_ticket_detail_description_reason_detail_spinner));
        mSpinnerServiceType = (MultiSelectionObjectSpinner) parent.findViewById(R.id.tm_spinner_ticket_detail_description_service_type);
        mSpinnerCusType = (MultiSelectionObjectSpinner) parent.findViewById(R.id.tm_spinner_ticket_detail_description_cus_type);
        mSpinnerDescriptionEffect = (Spinner) parent.findViewById(R.id.tm_spinner_ticket_detail_description_circle_of_influence);
        CredentialsValidator.setColorPromptSpinner(mSpinnerDescriptionEffect, getResources().getString(R.string.tm_ticket_detail_description_circle_of_influence_spinner));
        tvDescriptionCusQty = (TextView) parent.findViewById(R.id.tm_tv_ticket_detail_description_count_cusQty);
        tvDescriptionLevel = (TextView) parent.findViewById(R.id.tm_tv_ticket_detail_description_level);
        tvDescriptionPlayTvQty = (TextView) parent.findViewById(R.id.tm_tv_ticket_detail_description_count_payTVQty);
        tvDescriptionCritical = (TextView) parent.findViewById(R.id.tm_tv_ticket_detail_description_critical);

        tvEmployeeFoundName = (TextView) parent.findViewById(R.id.tm_tv_ticket_detail_employee_found_name);
        tvEmployeeFoundMobile = (TextView) parent.findViewById(R.id.tm_tv_ticket_detail_employee_found_phone);
        tvEmployeeFoundIpPhone = (TextView) parent.findViewById(R.id.tm_tv_ticket_detail_employee_found_ip_phone);
        tvEmployeeCreatedTicket = (TextView) parent.findViewById(R.id.tm_tv_ticket_detail_employee_created_ticket_date);
        tvEmployeeRequiredDateTicket = (TextView) parent.findViewById(R.id.tm_tv_ticket_detail_employee_finished_ticket_date);
        tvEmployeeProcessPhone = (TextView) parent.findViewById(R.id.tm_tv_ticket_detail_employee_process_phone);
        tvEmployeeProcessIpPhone = (TextView) parent.findViewById(R.id.tm_tv_ticket_detail_employee_process_ip_phone);

        tvEmployeeIssueDate = (TextView) parent.findViewById(R.id.tm_tv_ticket_detail_employee_issue_date);
        tvEmployeeFinishedExpectDate = (TextView) parent.findViewById(R.id.tm_tv_ticket_detail_employee_finished_expect_date);
        mSpinnerEmployeeStatus = (Spinner) parent.findViewById(R.id.tm_spinner_ticket_detail_employee_status);
        CredentialsValidator.setColorPromptSpinner(mSpinnerEmployeeStatus, getResources().getString(R.string.tm_ticket_status));
        mSpinnerEmployeeQueue = (Spinner) parent.findViewById(R.id.tm_spinner_ticket_detail_employee_queue);
        CredentialsValidator.setColorPromptSpinner(mSpinnerEmployeeQueue, getResources().getString(R.string.tm_ticket_queue));
        mSpinnerEmployeeOperateStaff = (Spinner) parent.findViewById(R.id.tm_spinner_ticket_detail_employee_operate_staff);
        CredentialsValidator.setColorPromptSpinner(mSpinnerEmployeeOperateStaff, getResources().getString(R.string.tm_ticket_operate_staff));
        mSpinnerEmployeeVisorStaff = (Spinner) parent.findViewById(R.id.tm_spinner_ticket_detail_employee_visor_staff);
        CredentialsValidator.setColorPromptSpinner(mSpinnerEmployeeVisorStaff, getResources().getString(R.string.tm_ticket_visor_staff));
        mSpinnerEmployeeProcessStaff = (Spinner) parent.findViewById(R.id.tm_spinner_ticket_detail_employee_process_staff);
        CredentialsValidator.setColorPromptSpinner(mSpinnerEmployeeVisorStaff, getResources().getString(R.string.tm_ticket_process_staff));
        tvEmployeeIssueDate.setOnClickListener(this);
        tvEmployeeFinishedExpectDate.setOnClickListener(this);

        mSpinnerIssueDescription.setOnItemSelectedListener(new onSpinnerSelectedListener());
        mSpinnerReason.setOnItemSelectedListener(new onSpinnerSelectedListener());
        mSpinnerReasonDetail.setOnItemSelectedListener(new onSpinnerSelectedListener());
        mSpinnerDescriptionEffect.setOnItemSelectedListener(new onSpinnerSelectedListener());
        mSpinnerEmployeeQueue.setOnItemSelectedListener(new onSpinnerSelectedListener());
        mSpinnerEmployeeStatus.setOnItemSelectedListener(new onSpinnerSelectedListener());
        mSpinnerEmployeeProcessStaff.setOnItemSelectedListener(new onSpinnerSelectedListener());

        loadDataForListDaysMothsYears();
        setupLoadDataLocal();

        if (!isShowingRefreshAnimation()) {
            swipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    showRefreshAnimation();
                }
            });
        }

    }

    private void setupLoadDataLocal() {
//        mProgressDialog.setMessage("Loading ...");
//        mProgressDialog.show();

        tvTicketNumber.setText(TICKET_ID);
        loadDataLocalSpinnerIssueDescriptionType();
        loadDataLocalSpinnerReasonType();
        loadDataLocalSpinnerReasonDetailType();
        loadDataLocalServiceType();
        loadDataLocalSpinnerCusType();
        loadDataLocalSpinnerEffect();
        loadDataLocalSpinnerStatusType();
        loadDataLocalSpinnerQueue();
        loadDataLocalSpinnerOperateStaff();
        loadDataLocalSpinnerVisorStaff();
        loadDataLocalSpinnerProcessStaff();
    }

    private void loadDataLocalSpinnerIssueDescriptionType() {
        mDescriptionList.clear();
        mDescriptionList.add(0, mDescriptionObject);
        TmUtils.setSpinnerAdapter(getActivity(), mSpinnerIssueDescription, mDescriptionList);
    }

    private void loadDataLocalSpinnerReasonType() {
        mReasonList.clear();
        mReasonList.add(0, mReasonObject);
        TmUtils.setSpinnerAdapter(getActivity(), mSpinnerReason, mReasonList);
    }

    private void loadDataLocalSpinnerReasonDetailType() {
        mReasonDetailList.clear();
        mReasonDetailList.add(0, mReasonDetailObject);
        TmUtils.setSpinnerAdapter(getActivity(), mSpinnerReasonDetail, mReasonDetailList);
    }

    private void loadDataLocalServiceType() {
        mSpinnerServiceType.setSpinnerMultiAdapter(mServiceTypeObject, mListSpinner, false);
    }

    private void loadDataLocalSpinnerCusType() {
        mSpinnerCusType.setSpinnerMultiAdapter(mCusTypeObject, mListSpinner, false);
    }

    private void loadDataLocalSpinnerEffect() {

    }

    private void loadDataLocalSpinnerStatusType() {
        mStatusTypeList.clear();
        mStatusTypeList.add(0, mStatusObject);
        TmUtils.setSpinnerAdapter(getActivity(), mSpinnerEmployeeStatus, mStatusTypeList);
    }

    private void loadDataLocalSpinnerQueue() {
        mQueueList.clear();
        mQueueList.add(0, mQueueObject);
        TmUtils.setSpinnerAdapter(getActivity(), mSpinnerEmployeeQueue, mQueueList);
    }

    private void loadDataLocalSpinnerOperateStaff() {
        mOperateStaffList.clear();
        mOperateStaffList.add(0, mOperateStaffObject);
        TmUtils.setSpinnerAdapter(getActivity(), mSpinnerEmployeeOperateStaff, mOperateStaffList);
    }

    private void loadDataLocalSpinnerVisorStaff() {
        mVisorStaffList.clear();
        mVisorStaffList.add(0, mVisorStaffObject);
        TmUtils.setSpinnerAdapter(getActivity(), mSpinnerEmployeeVisorStaff, mVisorStaffList);
    }

    private void loadDataLocalSpinnerProcessStaff() {
        mProcessStaffList.clear();
        mProcessStaffList.add(0, mVisorStaffObject);
        TmUtils.setSpinnerAdapter(getActivity(), mSpinnerEmployeeProcessStaff, mProcessStaffList);
    }

    // spinner ngay thang nam
    private void referenceSpinnersDaysMonthsYears(View view) {
        sDays = (Spinner) view.findViewById(R.id.sp_day);
        sMoths = (Spinner) view.findViewById(R.id.sp_moth);
        sYears = (Spinner) view.findViewById(R.id.sp_year);
        sDays.setOnItemSelectedListener(new onSpinnerTimeListener());
        sMoths.setOnItemSelectedListener(new onSpinnerTimeListener());
        sYears.setOnItemSelectedListener(new onSpinnerTimeListener());
    }

    private void loadDataForListDaysMothsYears() {
        for (int i = 1; i <= 31; i++) {
            lDays.add(String.valueOf(i));
        }
        for (int i = 1; i <= 12; i++) {
            lMoths.add("Tháng " + String.valueOf(i));
        }
        for (int i = 1900; i < 2100; i++) {
            lYears.add(String.valueOf(i));
        }
    }

    //load du lieu ngay thang nam
    private void loadDataLocalSpinnerDaysMothsYears() {
        daysAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, lDays);
        daysAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sDays.setAdapter(daysAdapter);
        mothsAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, lMoths);
        mothsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sMoths.setAdapter(mothsAdapter);
        yearsAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, lYears);
        yearsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sYears.setAdapter(yearsAdapter);
        Calendar now = Calendar.getInstance();
        int year = now.get(Calendar.YEAR);
        sYears.setSelection(year - Integer.valueOf(lYears.get(0)));
        int month = now.get(Calendar.MONTH) + 1;
        sMoths.setSelection(month - 1);
        int day = now.get(Calendar.DAY_OF_MONTH);
        sDays.setSelection(day - 1);
    }

    private int checkDayBigerMoth(String month, String year) {
        Calendar cal = new GregorianCalendar(Integer.valueOf(year), Integer.valueOf(month), 0);
        Date date = cal.getTime();
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String value = sdf.format(date);
        int max = Integer.valueOf(value.substring(value.length() - 2, value.length()));
        return max;

    }

    private void setupLoadDataToView() {
        if (TICKET_ID != null) {
            getMyDeviceList();
            getTicketDetail();
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // getMyDeviceList()
    private void getMyDeviceList() {
        getTmApiEndPointProvider().addRequest(MyTicketDetailEndPoint.getMyDeviceList(TICKET_ID, new Response.Listener<MyDeviceObject>() {
            @Override
            public void onResponse(MyDeviceObject myDeviceObject) {
                if (getActivity() != null) {
                    if (myDeviceObject != null && myDeviceObject.getData().size() > 0) {
                        mMyDeviceObject.setData(myDeviceObject.getData());
                        myDeviceAdapter = new MyDeviceAdapter(myDeviceObject.getData(), new DeviceSpinnerCallbackInterface());
                        tvRegionDeviceCount.setText(String.valueOf(myDeviceObject.getData().size()));
                        lvRegionDevice.setAdapter(myDeviceAdapter);
                        lvRegionDevice.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
                        lvRegionDevice.setLayoutManager(new LinearLayoutManager(getActivity()));
                    } else {
                        lvRegionDevice.setVisibility(View.GONE);
                        tvRegionDeviceCount.setText("0");
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onErrorListener(error);
            }
        }), EXTRA_REQUEST_TICKET_DEVICE_TAG);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // getTicketDetail()
    private void getTicketDetail() {
        getTmApiEndPointProvider().addRequest(MyTicketDetailEndPoint.getTicketDetail(TICKET_ID, new Response.Listener<MyTicketDetailObject>() {
            @Override
            public void onResponse(MyTicketDetailObject ticketDetailObject) {
                if (getActivity() != null) {
                    if (ticketDetailObject != null) {
                        MY_TICKET_DETAIL_OBJECT = ticketDetailObject;
                        tvRegionAgent.setText(ticketDetailObject.getBranch());
                        tvRegionLocation.setText(ticketDetailObject.getLocation());
                        tvRegionGroupElement.setText(ticketDetailObject.getIssueGroup());
                        tvRegionElement.setText(String.valueOf(ticketDetailObject.getDeviceTypeName()));
                        tvDescriptionLevel.setText(ticketDetailObject.getLevel());
                        tvDescriptionCritical.setText(String.valueOf(ticketDetailObject.getCricLev()));
                        tvDescriptionCusQty.setText(String.valueOf(ticketDetailObject.getCusQty()));
                        tvDescriptionPlayTvQty.setText(String.valueOf(ticketDetailObject.getPayTVQty()));
                        tvRegionDescription.setText(ticketDetailObject.getDescription());
                        tvEmployeeFoundName.setText(ticketDetailObject.getFoundStaff());
                        tvEmployeeFoundMobile.setText(ticketDetailObject.getFoundMobile());
                        tvEmployeeFoundIpPhone.setText(ticketDetailObject.getFoundIPPhone());
                        tvEmployeeCreatedTicket.setText(ticketDetailObject.getCreatedDate());
                        tvEmployeeRequiredDateTicket.setText(ticketDetailObject.getRequiredDate());
                        tvEmployeeIssueDate.setText(ticketDetailObject.getIssueDate());
                        tvEmployeeFinishedExpectDate.setText(ticketDetailObject.getExpectedDate());
                        tvEmployeeProcessPhone.setText(ticketDetailObject.getProcessMobile());
                        tvEmployeeProcessIpPhone.setText(ticketDetailObject.getProcessIPPhone());

                        // setup Data to Spinner
                        setupDataSpinner(ticketDetailObject);
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onErrorListener(error);
            }
        }), EXTRA_REQUEST_TICKET_DETAIL_TAG);
    }

    private void setupDataSpinner(MyTicketDetailObject ticketDetailObject) {
        // set onItemSelected
        if (MY_TICKET_DETAIL_OBJECT.getTicketStatus().intValue() == 2) {
            MY_TICKET_STATE = StateTicket.INPROGRESS;
        }

        getMyIssueList(String.valueOf(MY_TICKET_DETAIL_OBJECT.getDeviceType()), true);
        getMyTicketReasonList(String.valueOf(MY_TICKET_DETAIL_OBJECT.getIssueID()), true);
        getMyTicketEffectList(String.valueOf(MY_TICKET_DETAIL_OBJECT.getIssueID()));
        getMyTicketServiceTypeList();
        getMyTicketCusTypeList(MY_TICKET_DETAIL_OBJECT.getServiceType(), true);
        getMyTicketQueueList(true);
        getMyTicketStatusList(String.valueOf(getTmSharePreferencesUtils().getUserLogin().getID()), String.valueOf(MY_TICKET_DETAIL_OBJECT.getTicketStatus()), true);
        getMyTicketOperateStaffList(String.valueOf(MY_TICKET_DETAIL_OBJECT.getQueue()), MY_TICKET_DETAIL_OBJECT.getLocationID(), MY_TICKET_DETAIL_OBJECT.getBranchID(), String.valueOf(MY_TICKET_DETAIL_OBJECT.getCricLev()), true);
        getMyTicketVisorStaffList(String.valueOf(MY_TICKET_DETAIL_OBJECT.getQueue()), MY_TICKET_DETAIL_OBJECT.getLocationID(), MY_TICKET_DETAIL_OBJECT.getBranchID(), true);
        getMyTicketProcessStaffList(String.valueOf(MY_TICKET_DETAIL_OBJECT.getQueue()), MY_TICKET_DETAIL_OBJECT.getLocationID(), MY_TICKET_DETAIL_OBJECT.getBranchID(), true);

        //Role view
        RoleView();

    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // getMyIssueList(final MyTicketDetailObject ticketDetailObject, String deviceType, final boolean isLoad)
    private void getMyIssueList(String deviceType, final boolean isLoad) {
        getTmApiEndPointProvider().addRequest(MyTicketDetailEndPoint.getMyIssueList(deviceType, new Response.Listener<MyTicketIssueObject>() {
            @Override
            public void onResponse(MyTicketIssueObject ticketIssueObject) {
                if (ticketIssueObject != null && ticketIssueObject.getSpinnerObjectList() != null) {
                    if (ticketIssueObject.getSpinnerObjectList().size() > 0) {
                        mDescriptionList.clear();
                        mDescriptionList = new ArrayList<>(ticketIssueObject.getSpinnerObjectList());
                        mDescriptionList.add(0, mDescriptionObject);

                        if (isLoad == true) {
                            TmUtils.setSpinnerAdapter(getActivity(), mSpinnerIssueDescription, mDescriptionList, String.valueOf(MY_TICKET_DETAIL_OBJECT.getIssueID()));
                        } else {
                            TmUtils.setSpinnerAdapter(getActivity(), mSpinnerIssueDescription, mDescriptionList);
                        }
                        CURRENT_DESCRIPTION_ISSUE_SPINNER_SELECTION = mSpinnerIssueDescription.getSelectedItemPosition();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onErrorListener(error);
            }
        }), EXTRA_REQUEST_TICKET_ISSUE_TAG);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // getMyTicketReasonList( String issueId, final boolean isLoad)
    private void getMyTicketReasonList(String issueId, final boolean isLoad) {
        getTmApiEndPointProvider().addRequest(MyTicketDetailEndPoint.getMyTicketReasonList(issueId, new Response.Listener<MyTicketReasonObject>() {
            @Override
            public void onResponse(MyTicketReasonObject myTicketReasonObject) {
                if (getActivity() != null) {
                    if (myTicketReasonObject != null && myTicketReasonObject.getSpinnerObjectList() != null) {
                        if (myTicketReasonObject.getSpinnerObjectList().size() > 0) {
                            mSpinnerReason.setEnabled(true);
                            mReasonList.clear();
                            mReasonList = new ArrayList<>(myTicketReasonObject.getSpinnerObjectList());
                            mReasonList.add(0, mReasonObject);
                            String keyReason = MY_TICKET_DETAIL_OBJECT != null ? String.valueOf(MY_TICKET_DETAIL_OBJECT.getReason()) : "";
                            if (isLoad == true) {
                                TmUtils.setSpinnerAdapter(getActivity(), mSpinnerReason, mReasonList, keyReason);
                                getMyTicketReasonDetailList(String.valueOf(MY_TICKET_DETAIL_OBJECT.getReason()), true);
                            } else {
                                TmUtils.setSpinnerAdapter(getActivity(), mSpinnerReason, mReasonList);
                            }
                            CURRENT_DESCRIPTION_REASON_SPINNER_SELECTION = mSpinnerReason.getSelectedItemPosition();
                        }
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onErrorListener(error);
            }
        }), EXTRA_REQUEST_TICKET_REASON_TAG);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // getMyTicketReasonDetailList( String reasonId, final boolean isLoad)
    private void getMyTicketReasonDetailList(String reasonId, final boolean isLoad) {
        getTmApiEndPointProvider().addRequest(MyTicketDetailEndPoint.getMyTicketReasonDetailList(reasonId, new Response.Listener<MyTicketReasonDetailObject>() {
            @Override
            public void onResponse(MyTicketReasonDetailObject myTicketReasonDetailObject) {
                if (getActivity() != null) {
                    if (myTicketReasonDetailObject != null && myTicketReasonDetailObject.getSpinnerObjectList() != null) {
                        if (myTicketReasonDetailObject.getSpinnerObjectList().size() > 0) {
                            mSpinnerReasonDetail.setEnabled(true);
                            mReasonDetailList.clear();
                            mReasonDetailList = new ArrayList<>(myTicketReasonDetailObject.getSpinnerObjectList());
                            mReasonDetailList.add(0, mReasonDetailObject);

                            String keyReasonDetail = MY_TICKET_DETAIL_OBJECT != null ? String.valueOf(MY_TICKET_DETAIL_OBJECT.getReasonDetail()) : "";

                            if (isLoad == true) {
                                TmUtils.setSpinnerAdapter(getActivity(), mSpinnerReasonDetail, mReasonDetailList, keyReasonDetail);
                            } else {
                                TmUtils.setSpinnerAdapter(getActivity(), mSpinnerReasonDetail, mReasonDetailList);
                            }
                            CURRENT_DESCRIPTION_REASON_DETAIL_SPINNER_SELECTION = mSpinnerReasonDetail.getSelectedItemPosition();
                        }
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onErrorListener(error);
            }
        }), EXTRA_REQUEST_TICKET_REASON_DETAIL_TAG);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // getMyTicketEffectList(String issueId)
    private void getMyTicketEffectList(String issueId) {
        getTmApiEndPointProvider().addRequest(MyTicketDetailEndPoint.getMyTicketEffectList(issueId, new Response.Listener<MyTicketEffectObject>() {
            @Override
            public void onResponse(MyTicketEffectObject ticketEffectObject) {
                if (getActivity() != null) {
                    if (ticketEffectObject != null && ticketEffectObject.getSpinnerObjectList() != null) {
                        if (ticketEffectObject.getSpinnerObjectList().size() > 0) {
                            mSpinnerDescriptionEffect.setEnabled(true);
                            String keyEffect = MY_TICKET_DETAIL_OBJECT != null ? String.valueOf(MY_TICKET_DETAIL_OBJECT.getEffect()) : "";
                            TmUtils.setSpinnerAdapter(getActivity(), mSpinnerDescriptionEffect, ticketEffectObject.getSpinnerObjectList(), keyEffect);
                            CURRENT_DESCRIPTION_EFFECT_SPINNER_SELECTION = mSpinnerDescriptionEffect.getSelectedItemPosition();
                        }
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onErrorListener(error);
            }
        }), EXTRA_REQUEST_TICKET_EFFECT_TAG);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // getMyTicketServiceTypeList()
    private void getMyTicketServiceTypeList() {
        getTmApiEndPointProvider().addRequest(MyTicketDetailEndPoint.getMyTicketServiceTypeList(new Response.Listener<MyTicketServiceTypeObject>() {
            @Override
            public void onResponse(MyTicketServiceTypeObject serviceTypeObject) {
                if (getActivity() != null) {
                    if (serviceTypeObject != null && serviceTypeObject.getSpinnerObjectList() != null) {
                        if (serviceTypeObject.getSpinnerObjectList().size() > 0) {
                            mSpinnerServiceType.setSpinnerMultiAdapter(mServiceTypeObject, serviceTypeObject.getSpinnerObjectList(), false);
                            mSpinnerServiceType.setSpinnerTitleDialog(getString(R.string.tm_ticket_detail_service_type));
                            if (!MY_TICKET_DETAIL_OBJECT.getServiceType().isEmpty()) {
                                mSpinnerServiceType.SetStringKeyChoice(MY_TICKET_DETAIL_OBJECT.getServiceType());
                                mSpinnerServiceType.setSelectionNew(TmUtils.parseStringToArray(MY_TICKET_DETAIL_OBJECT.getServiceType()));
                                //    mSpinnerServiceType.setSpinnerUpdateCallback(new PrivateSpinnerCallbackInterface());
                                mSpinnerServiceType.setCallback(new DeviceSpinnerCallbackInterface(), true);
                            }
                        }
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onErrorListener(error);
            }
        }), EXTRA_REQUEST_TICKET_SERVICE_TYPE_TAG);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // getMyTicketCusTypeList(String serviceType, final boolean isLoad)
    private void getMyTicketCusTypeList(String serviceType, final boolean isLoad) {
        getTmApiEndPointProvider().addRequest(MyTicketDetailEndPoint.getMyTicketCusTypeList(serviceType, new Response.Listener<MyTicketCusTypeObject>() {
            @Override
            public void onResponse(MyTicketCusTypeObject myTicketCusTypeObject) {
                if (getActivity() != null) {
                    if (myTicketCusTypeObject != null && myTicketCusTypeObject.getSpinnerObjectList() != null) {
                        if (myTicketCusTypeObject.getSpinnerObjectList().size() > 0) {
                            mSpinnerCusType.setSpinnerMultiAdapter(mCusTypeObject, myTicketCusTypeObject.getSpinnerObjectList(), false);
                            mSpinnerCusType.setSpinnerTitleDialog(getString(R.string.tm_ticket_detail_cus_type));
                            if (isLoad == true) {
                                if (!MY_TICKET_DETAIL_OBJECT.getCusType().isEmpty()) {
                                    mSpinnerCusType.SetStringKeyChoice(MY_TICKET_DETAIL_OBJECT.getCusType());
                                    mSpinnerCusType.setSelectionNew(TmUtils.parseStringToArray(MY_TICKET_DETAIL_OBJECT.getCusType()));
                                }
                            }
                        }
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onErrorListener(error);
            }
        }), EXTRA_REQUEST_TICKET_CUS_TYPE_TAG);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // getMyTicketQueueList(final boolean isLoad)
    private void getMyTicketQueueList(final boolean isLoad) {
        getTmApiEndPointProvider().addRequest(MyTicketDetailEndPoint.getMyTicketQueueList(new Response.Listener<MyTicketQueueObject>() {
            @Override
            public void onResponse(MyTicketQueueObject myTicketQueueObject) {
                if (getActivity() != null) {
                    if (myTicketQueueObject != null && myTicketQueueObject.getSpinnerObjectList() != null) {
                        if (myTicketQueueObject.getSpinnerObjectList().size() > 0) {
                            mQueueList.clear();
                            mQueueList = new ArrayList<>(myTicketQueueObject.getSpinnerObjectList());
                            mQueueList.add(0, mQueueObject);
                            if (isLoad == true) {
                                TmUtils.setSpinnerAdapter(getContext(), mSpinnerEmployeeQueue, mQueueList, String.valueOf(MY_TICKET_DETAIL_OBJECT.getQueue()));
                            } else {
                                TmUtils.setSpinnerAdapter(getContext(), mSpinnerEmployeeQueue, mQueueList);
                            }
                            CURRENT_EMPLOYEE_QUEUE_SPINNER_SELECTION = mSpinnerEmployeeQueue.getSelectedItemPosition();
                        }
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onErrorListener(error);
            }
        }), EXTRA_REQUEST_TICKET_QUEUE_TAG);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // getMyTicketStatusList(String userId, String ticketStatusId)
    private void getMyTicketStatusList(String userId, String ticketStatusId, final boolean isLoad) {
        getTmApiEndPointProvider().addRequest(MyTicketDetailEndPoint.getMyTicketStatusList(userId, ticketStatusId, new Response.Listener<MyTicketStatusObject>() {
            @Override
            public void onResponse(MyTicketStatusObject myTicketStatusObject) {
                if (getActivity() != null) {
                    if (myTicketStatusObject != null && myTicketStatusObject.getSpinnerObjectList() != null) {
                        if (myTicketStatusObject.getSpinnerObjectList().size() > 0) {
                            mStatusTypeList.clear();
                            mStatusTypeList = new ArrayList<>(myTicketStatusObject.getSpinnerObjectList());
                            // mStatusTypeList.add(0, mStatusObject);

                            if (isLoad == true) {
                                TmUtils.setSpinnerAdapter(getContext(), mSpinnerEmployeeStatus, mStatusTypeList, String.valueOf(MY_TICKET_DETAIL_OBJECT.getTicketStatus()));
                            } else {
                                TmUtils.setSpinnerAdapter(getContext(), mSpinnerEmployeeStatus, mStatusTypeList);
                            }

                            CURRENT_EMPLOYEE_STATUS_SPINNER_SELECTION = mSpinnerEmployeeStatus.getSelectedItemPosition();

                            //oaoa

                        /*SpinnerObject spinnerObject = (SpinnerObject) mSpinnerEmployeeStatus.getSelectedItem();
                        onSelectedItemSpinnerStatus(spinnerObject);*/


                        /*if (spinnerObject.getKey().equalsIgnoreCase("2")) {

                            mSpinnerEmployeeQueue.setEnabled(true);
                        } else {
                            mSpinnerEmployeeQueue.setEnabled(false);
                        }*/
                        }
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onErrorListener(error);
            }
        }), EXTRA_REQUEST_TICKET_STATUS_TAG);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // getMyTicketOperateStaffList( String queue, String locationId, String branchId, String cricLev , boolean isLoad)
    private void getMyTicketOperateStaffList(String queue, String locationId, String branchId, String cricLev, final boolean isLoad) {
        getTmApiEndPointProvider().addRequest(MyTicketDetailEndPoint.getMyTicketOperateStaffList(queue, locationId, branchId, cricLev, new Response.Listener<MyTicketOperateStaffObject>() {
            @Override
            public void onResponse(MyTicketOperateStaffObject myTicketOperateStaffObject) {
                if (getActivity() != null) {
                    if (myTicketOperateStaffObject != null && myTicketOperateStaffObject.getSpinnerObjectList() != null) {
                        if (myTicketOperateStaffObject.getSpinnerObjectList().size() > 0) {
                            mOperateStaffList.clear();
                            mOperateStaffList = new ArrayList<>(myTicketOperateStaffObject.getSpinnerObjectList());
                            mOperateStaffList.add(0, mOperateStaffObject);

                            String keyOperateStaff = MY_TICKET_DETAIL_OBJECT != null ? MY_TICKET_DETAIL_OBJECT.getOperateStaff() : "";
                            if (isLoad == true) {
                                TmUtils.setSpinnerAdapter(getContext(), mSpinnerEmployeeOperateStaff, mOperateStaffList, keyOperateStaff);
                            } else {
                                TmUtils.setSpinnerAdapter(getContext(), mSpinnerEmployeeOperateStaff, mOperateStaffList);
                                mSpinnerEmployeeOperateStaff.setSelection(1);
                            }
                        } else {
                            //oaoa
                            SpinnerObject OperateStaffObject = new SpinnerObject("-100", "Không có dữ liệu");
                            mOperateStaffList.clear();
                            mOperateStaffList.add(0, OperateStaffObject);
                            TmUtils.setSpinnerAdapter(getContext(), mSpinnerEmployeeOperateStaff, mOperateStaffList);
                        }
                    } else {
                        //oaoa
                        SpinnerObject OperateStaffObject = new SpinnerObject("-100", "Không có dữ liệu");
                        mOperateStaffList.clear();
                        mOperateStaffList.add(0, OperateStaffObject);
                        TmUtils.setSpinnerAdapter(getContext(), mSpinnerEmployeeOperateStaff, mOperateStaffList);
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onErrorListener(error);
            }
        }), EXTRA_REQUEST_TICKET_OPERATE_STAFF_TAG);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // getMyTicketVisorStaffList( String queue, String locationId, String branchId, boolean isLoad)
    private void getMyTicketVisorStaffList(String queue, String locationId, String branchId, final boolean isLoad) {
        getTmApiEndPointProvider().addRequest(MyTicketDetailEndPoint.getMyTicketVisorStaffList(queue, locationId, branchId, new Response.Listener<MyTicketVisorStaffObject>() {
            @Override
            public void onResponse(MyTicketVisorStaffObject myTicketVisorStaffObject) {
                if (getActivity() != null) {
                    if (myTicketVisorStaffObject != null && myTicketVisorStaffObject.getSpinnerObjectList() != null) {
                        if (myTicketVisorStaffObject.getSpinnerObjectList().size() > 0) {
                            mVisorStaffList.clear();
                            mVisorStaffList = new ArrayList<>(myTicketVisorStaffObject.getSpinnerObjectList());
                            mVisorStaffList.add(0, mVisorStaffObject);
                            String keyVisorStaff = MY_TICKET_DETAIL_OBJECT != null ? MY_TICKET_DETAIL_OBJECT.getVisorStaff() : "";
                            if (isLoad == true) {
                                TmUtils.setSpinnerAdapter(getContext(), mSpinnerEmployeeVisorStaff, mVisorStaffList, keyVisorStaff);
                            } else {
                                TmUtils.setSpinnerAdapter(getContext(), mSpinnerEmployeeVisorStaff, mVisorStaffList);
                                mSpinnerEmployeeVisorStaff.setSelection(1);
                            }
                        }
                    } else {
                        SpinnerObject VisorStaffObject = new SpinnerObject("-100", "Không có dữ liệu");
                        mVisorStaffList.clear();
                        mVisorStaffList.add(0, VisorStaffObject);
                        TmUtils.setSpinnerAdapter(getContext(), mSpinnerEmployeeVisorStaff, mVisorStaffList);
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onErrorListener(error);
            }
        }), EXTRA_REQUEST_TICKET_VISOR_STAFF_TAG);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // getMyTicketProcessStaffList(String queue, String locationId, String branchId, boolean isLoad)
    private void getMyTicketProcessStaffList(String queue, String locationId, String branchId, final boolean isLoad) {
        getTmApiEndPointProvider().addRequest(MyTicketDetailEndPoint.getMyTicketProcessStaffList(queue, locationId, branchId, new Response.Listener<MyTicketProcessStaffObject>() {
            @Override
            public void onResponse(MyTicketProcessStaffObject myTicketProcessStaffObject) {
                if (getActivity() != null) {
                    if (myTicketProcessStaffObject != null && myTicketProcessStaffObject.getSpinnerObjectList() != null) {
                        if (myTicketProcessStaffObject.getSpinnerObjectList().size() > 0) {
                            mProcessStaffList.clear();
                            mProcessStaffList = new ArrayList<>(myTicketProcessStaffObject.getSpinnerObjectList());
                            mProcessStaffList.add(0, mProcessStaffObject);
                            if (isLoad == true) {
                                String keyProcessStaff = MY_TICKET_DETAIL_OBJECT != null ? MY_TICKET_DETAIL_OBJECT.getProcessStaff() : "";
                                TmUtils.setSpinnerAdapter(getContext(), mSpinnerEmployeeProcessStaff, mProcessStaffList, keyProcessStaff);
                            } else {
                                TmUtils.setSpinnerAdapter(getContext(), mSpinnerEmployeeProcessStaff, mProcessStaffList);
                            }

                            mSpinnerProcessStaffList = myTicketProcessStaffObject.getSpinnerObjectList();
                            CURRENT_EMPLOYEE_PROCESS_STAFF_SPINNER_SELECTION = mSpinnerEmployeeProcessStaff.getSelectedItemPosition();
                        }
                    } else {
                        //oaoa
                        SpinnerObject ProcessStaffObject = new SpinnerObject("-100", "Không có dữ liệu");
                        mProcessStaffList.clear();
                        mProcessStaffList.add(0, ProcessStaffObject);
                        TmUtils.setSpinnerAdapter(getContext(), mSpinnerEmployeeProcessStaff, mProcessStaffList);

                    }


                    stopRefreshAnimation();
                    mProgressDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onErrorListener(error);
            }
        }), EXTRA_REQUEST_TICKET_PROCESS_STAFF_TAG);
    }


    private void onDialogDateTimePicker(final TextView textView) {
        final View dialogView = View.inflate(getContext(), R.layout.entry_dialog_date_time_pick, null);
        final AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
        referenceSpinnersDaysMonthsYears(dialogView);
        //load data cho spinner;
        loadDataLocalSpinnerDaysMothsYears();
        dialogView.findViewById(R.id.tm_date_time_set).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
//                        //DatePicker datePicker = (DatePicker) dialogView.findViewById(R.id.tm_date_picker);
                        TimePicker timePicker = (TimePicker) dialogView.findViewById(R.id.tm_time_picker);
//                        // them code khu nay
                        String daySelected = sDays.getSelectedItem().toString();
                        String day = Integer.valueOf(daySelected) > 9 ? daySelected + ""
                                : "0" + daySelected;
                        String monthSelected = cutSelectCurrentMonth();
                        String mount = Integer.valueOf(monthSelected) > 9 ? monthSelected + "" : "0" + monthSelected;
                        String minute = timePicker.getCurrentMinute() > 9 ? timePicker.getCurrentMinute() + "" : "0" + timePicker.getCurrentMinute();
                        String time = day.trim() + "/" + mount.trim() + "/" + sYears.getSelectedItem().toString().trim() + " " + timePicker.getCurrentHour() + ":" + minute.trim() + ":00";

                        if (textView != null) {
                            if (textView.getId() == R.id.tm_tv_ticket_detail_employee_issue_date) {

                                if (TmUtils.CompareIssueDateTime(time, textView.getText().toString())) {
                                    textView.setText(time);
                                    alertDialog.dismiss();
                                } else {
                                    TmUtils.choiceTimeDiaLog(getContext(), textView, tvEmployeeIssueDate);
                                }
                            }

                            if (textView.getId() == R.id.tm_tv_ticket_detail_employee_finished_expect_date) {
                                if (TmUtils.CompareDateExpectTime(time)) {
                                    textView.setText(time);
                                    alertDialog.dismiss();
                                } else {
                                    TmUtils.choiceTimeDiaLog(getContext(), textView, null);
                                }
                            }
                        }
                    }
                });
        alertDialog.setView(dialogView);
        alertDialog.show();
    }

    private void extraArguments() {
        if (getArguments() != null) {
            TICKET_ID = getArguments().getString(EXTRA_TICKET_CODE_INTENT_KEY);
        }
    }

    private void onErrorListener(VolleyError error) {
        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
            stopRefreshAnimation();
            getTmToast().makeToast(getString(R.string.tm_error_connect), Toast.LENGTH_SHORT);
        } else if (error instanceof AuthFailureError) {
            Log.d("tmt", "AuthFailureError");
        } else if (error instanceof ServerError) {
            Log.d("tmt", "ServerError");
        } else if (error instanceof NetworkError) {
            Log.d("tmt", "NetworkError");
        } else if (error instanceof ParseError) {
            Log.d("tmt", "ParseError");
        } else {
            Log.d("tmt", "onErrorResponse : " + error.getMessage());
        }

        mProgressDialog.dismiss();
    }

    private boolean onValidateUpdateTicketDetail() {

        if (mSpinnerIssueDescription.getSelectedItemPosition() == 0) {
            getTmToast().makeToast(getString(R.string.tm_toast_input_cus_description_empty), Toast.LENGTH_LONG);
            return false;
        }

        if (mSpinnerReason.getSelectedItemPosition() == 0) {
            getTmToast().makeToast(getString(R.string.tm_toast_input_reason_empty), Toast.LENGTH_LONG);
            return false;
        }

        if (mSpinnerReasonDetail.getSelectedItemPosition() == 0) {
            getTmToast().makeToast(getString(R.string.tm_toast_input_reason_detail_empty), Toast.LENGTH_LONG);
            return false;
        }

        if (mSpinnerServiceType.getSelectedItemsAsString().equalsIgnoreCase("")) {
            getTmToast().makeToast(getString(R.string.tm_toast_input_service_type_empty), Toast.LENGTH_LONG);
            return false;
        }

        if (mSpinnerCusType.getSelectedItemsAsString().equalsIgnoreCase("")) {
            getTmToast().makeToast(getString(R.string.tm_toast_input_cus_type_empty), Toast.LENGTH_LONG);
            return false;
        }


        if (mSpinnerEmployeeQueue.getSelectedItemPosition() == 0) {
            getTmToast().makeToast(getString(R.string.tm_toast_input_queue_empty), Toast.LENGTH_LONG);
            return false;
        }

        if (mSpinnerEmployeeOperateStaff.getSelectedItemPosition() == 0) {
            getTmToast().makeToast(getString(R.string.tm_toast_input_operate_staff_empty), Toast.LENGTH_LONG);
            return false;
        }

        if (mSpinnerEmployeeVisorStaff.getSelectedItemPosition() == 0) {
            getTmToast().makeToast(getString(R.string.tm_toast_input_visor_staff_empty), Toast.LENGTH_LONG);
            return false;
        }


        return true;
    }


    private MyTicketDetailObject getMyTicketDetailObject() {
        MyTicketDetailObject obj = MY_TICKET_DETAIL_OBJECT;
        obj.setOperateStaff("");
        obj.setVisorStaff("");
        obj.setProcessStaff("");

        SpinnerObject objSpinnerTicketDetail = null;
        objSpinnerTicketDetail = (SpinnerObject) mSpinnerIssueDescription.getSelectedItem();
        if (objSpinnerTicketDetail != null) {
            obj.setIssueID(Integer.parseInt(objSpinnerTicketDetail.getKey()));
        }
        objSpinnerTicketDetail = (SpinnerObject) mSpinnerReason.getSelectedItem();
        if (objSpinnerTicketDetail != null) {
            obj.setReason(Integer.parseInt(objSpinnerTicketDetail.getKey()));
        }

        objSpinnerTicketDetail = (SpinnerObject) mSpinnerReasonDetail.getSelectedItem();
        if (objSpinnerTicketDetail != null) {
            obj.setReasonDetail(Integer.parseInt(objSpinnerTicketDetail.getKey()));
        }

        obj.setServiceType(mSpinnerServiceType.GetStringKeyChoice());
        obj.setCusType(mSpinnerCusType.GetStringKeyChoice());

        objSpinnerTicketDetail = (SpinnerObject) mSpinnerDescriptionEffect.getSelectedItem();
        if (objSpinnerTicketDetail != null) {
            obj.setEffect(Integer.parseInt(objSpinnerTicketDetail.getKey()));
        }

        obj.setLevel(tvDescriptionLevel.getText().toString());
        obj.setDescription(tvRegionDescription.getText().toString());

        objSpinnerTicketDetail = (SpinnerObject) mSpinnerEmployeeStatus.getSelectedItem();
        if (objSpinnerTicketDetail != null) {
            obj.setTicketStatus(Integer.parseInt(objSpinnerTicketDetail.getKey()));
        }

        objSpinnerTicketDetail = (SpinnerObject) mSpinnerEmployeeQueue.getSelectedItem();
        if (objSpinnerTicketDetail != null) {
            obj.setQueue(Integer.parseInt(objSpinnerTicketDetail.getKey()));
        }

        obj.setCusQty(tvDescriptionCusQty.getText().toString().isEmpty() ? 0 : Integer
                .parseInt(tvDescriptionCusQty.getText().toString()));
        obj.setPayTVQty(tvDescriptionPlayTvQty.getText().toString().isEmpty() ? 0
                : Integer.parseInt(tvDescriptionPlayTvQty.getText().toString()));
        objSpinnerTicketDetail = (SpinnerObject) mSpinnerEmployeeOperateStaff.getSelectedItem();
        if (objSpinnerTicketDetail != null) {
            obj.setOperateStaff(objSpinnerTicketDetail.getKey());
        }

        objSpinnerTicketDetail = (SpinnerObject) mSpinnerEmployeeVisorStaff.getSelectedItem();
        if (objSpinnerTicketDetail != null) {
            obj.setVisorStaff(objSpinnerTicketDetail.getKey());
        }
        objSpinnerTicketDetail = (SpinnerObject) mSpinnerEmployeeProcessStaff.getSelectedItem();
        if (mSpinnerEmployeeProcessStaff.getSelectedItemPosition() == 0) {
            obj.setProcessStaff("");
        } else if (objSpinnerTicketDetail != null) {
            obj.setProcessStaff(objSpinnerTicketDetail.getKey());
        }


        obj.setProcessMobile(tvEmployeeProcessPhone.getText().toString());
        obj.setProcessIPPhone(tvEmployeeProcessIpPhone.getText().toString());

        obj.setRequiredDate(TmUtils.formatTimeDateUpdateSave(tvEmployeeRequiredDateTicket.getText().toString()));
        obj.setCreatedDate(TmUtils.formatTimeDateUpdateSave(tvEmployeeRequiredDateTicket.getText().toString()));
        obj.setExpectedDate(TmUtils.formatTimeDateUpdateSave(tvEmployeeFinishedExpectDate.getText().toString()));
        obj.setIssueDate(TmUtils.formatTimeDateUpdateSave(tvEmployeeIssueDate.getText().toString()));


        return obj;
    }

    private void onConfirmUpdatedTicketDetail() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        builder.setTitle(getString(R.string.tm_alert_dialog_updated_ticket));
        builder.setMessage(getString(R.string.tm_alert_dialog_want_updated));
        builder.setPositiveButton(getString(R.string.tm_alert_dialog_confirm_updated), new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                MyTicketDetailObject myTicketDetailObject = getMyTicketDetailObject();
                String userId = getTmSharePreferencesUtils().getUserIdLogin();
                String userEmail = getTmSharePreferencesUtils().getEmailUserLogin();
                String queueName = ((SpinnerObject) mSpinnerEmployeeQueue.getSelectedItem()).getValue();
                String keyCode = PRIVATE_KEY_CODE + TICKET_ID + myTicketDetailObject.getIssueID()
                        + myTicketDetailObject.getReason() + myTicketDetailObject.getServiceType() + myTicketDetailObject.getCusType()
                        + myTicketDetailObject.getEffect() + myTicketDetailObject.getLevel() + myTicketDetailObject.getDescription()
                        + myTicketDetailObject.getTicketStatus() + myTicketDetailObject.getQueue()
                        + myTicketDetailObject.getOperateStaff() + myTicketDetailObject.getVisorStaff()
                        + myTicketDetailObject.getProcessStaff() + myTicketDetailObject.getProcessMobile()
                        + myTicketDetailObject.getProcessIPPhone() + myTicketDetailObject.getCusQty() + myTicketDetailObject.getPayTVQty()
                        + userId;

                keyCode = TmUtils.Base64(keyCode);


                mProgressDialog.setMessage(getString(R.string.tm_progress_dialog_updated_ticket_detail));
                mProgressDialog.show();

                getTmApiEndPointProvider().addRequest(MyTicketDetailEndPoint.MyTicketDetailUpdated(userId, userEmail, TICKET_ID, queueName, keyCode, myTicketDetailObject, new Response.Listener<MyResponseObject>() {
                    @Override
                    public void onResponse(MyResponseObject myResponseObject) {
                        getTmToast().makeToast(myResponseObject.getErrorDescription(), Toast.LENGTH_SHORT);

                        mProgressDialog.dismiss();

                        if (myResponseObject.getErrorCode().intValue() == 1) {
                            getActivity().finish();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        onErrorListener(error);
                    }
                }), EXTRA_UPDATE_TICKET_DETAIL_TAG);

                dialog.dismiss();
            }

        });

        builder.setNegativeButton(getString(R.string.tm_alert_dialog_cancel_updated), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onRefresh() {
        autoSynced();
    }

    @Override
    protected void autoSynced() {
        if (!isShowingRefreshAnimation()) {
            showRefreshAnimation();
        }
        setupLoadDataToView();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tm_tv_ticket_detail_employee_issue_date:
                onDialogDateTimePicker(tvEmployeeIssueDate);
                break;
            case R.id.tm_tv_ticket_detail_employee_finished_expect_date:
                onDialogDateTimePicker(tvEmployeeFinishedExpectDate);
                break;

            default:
                break;
        }
    }

    @Subscribe
    public void onTicketDetailSave(TicketDetailSaveEvent ticketDetailSaveEvent) {
        if (onValidateUpdateTicketDetail()) {
            onConfirmUpdatedTicketDetail();
        }
    }

    @Subscribe
    public void onTicketDetailFeedBack(TicketDetailFeedBackEvent ticketDetailFeedBackEvent) {
        if (TICKET_ID != null) {
            getActivity().startActivity(TicketFeedBackActivity.buildIntent(getActivity(), TICKET_ID));
        }
    }

    private final class DeviceSpinnerCallbackInterface implements SpinnerCallbackInterface {

        @Override
        public void UpdateDeviceStatus(String deviceName, String status) {

            SpinnerObject spinnerEffect = (SpinnerObject) mSpinnerDescriptionEffect.getSelectedItem();
            if (spinnerEffect != null && MY_TICKET_DETAIL_OBJECT != null) {
                String userId = String.valueOf(getTmSharePreferencesUtils().getUserLogin().getID());
                String effect = spinnerEffect.getIsEffect();
                String cusQty = tvDescriptionCusQty.getText().toString().isEmpty() ? "0" : tvDescriptionCusQty.getText().toString();

                String keyCode = PRIVATE_KEY_CODE + deviceName + status + userId + TICKET_ID;
                keyCode = TmUtils.Base64(keyCode);
                //   mProgressDialog.setMessage(getString(R.string.tm_progress_dialog_updated_status_device));
                //  mProgressDialog.show();
                getTmApiEndPointProvider().addRequest(MyTicketDetailEndPoint.MyTicketDeviceUpdateData(deviceName, status, TICKET_ID, userId, effect, cusQty, keyCode, new Response.Listener<MyResponseObject>() {
                    @Override
                    public void onResponse(MyResponseObject myResponseObject) {
                        mProgressDialog.dismiss();
                        getTmToast().makeToast(myResponseObject.getErrorDescription(), Toast.LENGTH_LONG);

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        onErrorListener(error);
                    }
                }), EXTRA_UPDATE_TICKET_DEVICE_TAG);
            }

        }

        @Override
        public void UpdateCusType(String serviceType) {
            if (serviceType.equalsIgnoreCase("")) {
                loadDataLocalSpinnerCusType();
                mSpinnerCusType.setEnabled(false);
            } else if (MY_TICKET_DETAIL_OBJECT != null) {
                getMyTicketCusTypeList(serviceType, false);
                mSpinnerCusType.setEnabled(true);
            }
        }
    }

    private String cutSelectCurrentMonth() {
        String tempMonth = sMoths.getSelectedItem().toString();
        int cutCharacter = 0;
        if (tempMonth.length() == 7) {
            cutCharacter = 1;
        } else {
            cutCharacter = 2;
        }
        return tempMonth.substring(tempMonth.length() - cutCharacter);
    }

    private final class onSpinnerTimeListener implements Spinner.OnItemSelectedListener {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            Spinner spinner = (Spinner) parent;
            switch (spinner.getId()) {
                default: {
                    int maxDay = checkDayBigerMoth(cutSelectCurrentMonth(), sYears.getSelectedItem().toString());
                    if (Integer.valueOf(sDays.getSelectedItem().toString()) > maxDay) {
                        for (int i = 30; i >= lDays.size() - 4; i--) {
                            if (Integer.valueOf(lDays.get(i)) == maxDay) {
                                sDays.setSelection(i);
                                break;
                            }
                        }
                    }
                    break;
                }
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

    private final class onSpinnerSelectedListener implements Spinner.OnItemSelectedListener {


        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            Spinner spinner = (Spinner) parent;
            SpinnerObject spinnerObject = (SpinnerObject) parent.getItemAtPosition(position);
            switch (spinner.getId()) {
                case R.id.tm_spinner_ticket_detail_description_issue:
                    if (CURRENT_DESCRIPTION_ISSUE_SPINNER_SELECTION != position) {
                        onSelectedItemIssue(spinnerObject.getKey(), position);
                        CURRENT_DESCRIPTION_ISSUE_SPINNER_SELECTION = position;
                    }
                    break;

                case R.id.tm_spinner_ticket_detail_description_reason:
                    if (CURRENT_DESCRIPTION_REASON_SPINNER_SELECTION != position) {
                        onSelectedItemReason(spinnerObject.getKey(), position);
                        CURRENT_DESCRIPTION_REASON_SPINNER_SELECTION = position;
                    }
                    break;

                case R.id.tm_spinner_ticket_detail_description_reason_detail:
                    if (CURRENT_DESCRIPTION_REASON_DETAIL_SPINNER_SELECTION != position) {
                        onSelectedItemReasonDetail(MY_TICKET_DETAIL_OBJECT, spinnerObject.getKey());
                        CURRENT_DESCRIPTION_REASON_DETAIL_SPINNER_SELECTION = position;
                    }
                    break;

                case R.id.tm_spinner_ticket_detail_employee_queue:
                    if (CURRENT_EMPLOYEE_QUEUE_SPINNER_SELECTION != position) {
                        onSelectedItemSpinnerQueue(MY_TICKET_DETAIL_OBJECT, spinnerObject.getKey(), position);
                        CURRENT_EMPLOYEE_QUEUE_SPINNER_SELECTION = position;
                    }

                    break;


                case R.id.tm_spinner_ticket_detail_description_circle_of_influence:
                    // selected spinner effect (pham vi anh huong)
                    if (CURRENT_DESCRIPTION_EFFECT_SPINNER_SELECTION != position) {
                        onSelectedItemSpinnerEffect(Integer.valueOf(spinnerObject.getIsEffect()), MY_TICKET_DETAIL_OBJECT);
                        CURRENT_DESCRIPTION_EFFECT_SPINNER_SELECTION = position;
                    }
                    break;

                case R.id.tm_spinner_ticket_detail_employee_process_staff:
                    // selected spinner process staff (nhan vien xu ly)
                    if (CURRENT_EMPLOYEE_PROCESS_STAFF_SPINNER_SELECTION != position) {
                        onSelectedItemSpinnerProcessStaff(spinnerObject.getKey());
                        CURRENT_EMPLOYEE_PROCESS_STAFF_SPINNER_SELECTION = position;
                    }
                    break;

                case R.id.tm_spinner_ticket_detail_employee_status:
                    // selected spinner process staff (trang thai)
                    if (CURRENT_EMPLOYEE_STATUS_SPINNER_SELECTION != position) {
                        onSelectedItemSpinnerStatus(spinnerObject);
                        CURRENT_EMPLOYEE_STATUS_SPINNER_SELECTION = position;
                    }
                    break;
            }
        }


        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }

        private void onSelectedItemIssue(String issueId, int position) {
            if (position == 0) {
                mSpinnerReason.setEnabled(false);
                mSpinnerReasonDetail.setEnabled(false);
                loadDataLocalSpinnerReasonType();
                loadDataLocalSpinnerReasonDetailType();
            } else {
                mSpinnerReasonDetail.setEnabled(false);
                mSpinnerReason.setEnabled(true);
                loadDataLocalSpinnerReasonDetailType();

                getMyTicketServiceTypeList();
                getMyTicketReasonList(issueId, false);
            }
        }

        private void onSelectedItemReason(String reasonId, int position) {
            if (position == 0) {
                mSpinnerReasonDetail.setEnabled(false);
                loadDataLocalSpinnerReasonDetailType();
            } else {
                mSpinnerReasonDetail.setEnabled(true);
                getMyTicketReasonDetailList(reasonId, false);
            }
        }

        private void onSelectedItemReasonDetail(MyTicketDetailObject ticketDetailObject, String reasonDetailId) {
            //mProgressDialog.setMessage("Đang cập nhập thời gian dự kiến...");
            // mProgressDialog.show();
            getTmApiEndPointProvider().addRequest(MyTicketDetailEndPoint.getMyTicketEstimatedDate(TmUtils.FormatTimeDateUpdateSave(ticketDetailObject.getCreatedDate()),
                    String.valueOf(ticketDetailObject.getCricLev()), reasonDetailId, new Response.Listener<MyTicketEstimatedDateObject>() {
                        @Override
                        public void onResponse(MyTicketEstimatedDateObject myTicketEstimatedDateObject) {

                            if (myTicketEstimatedDateObject != null) {
                                tvEmployeeRequiredDateTicket.setText(myTicketEstimatedDateObject.getEstimatedDateValue());
                                tvEmployeeFinishedExpectDate.setText(myTicketEstimatedDateObject.getEstimatedDateValue());
                            }
                            mProgressDialog.dismiss();
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            onErrorListener(error);
                        }
                    }));
        }

        private void onSelectedItemSpinnerQueue(MyTicketDetailObject ticketDetailObject, String queueId, int position) {

            //reload process, operate, visor staff
            /*getMyTicketOperateStaffList(queueId, ticketDetailObject.getLocationID(), ticketDetailObject.getBranchID(), String.valueOf(ticketDetailObject.getCricLev()), false);
            getMyTicketVisorStaffList(queueId, ticketDetailObject.getLocationID(), ticketDetailObject.getBranchID(), false);
            getMyTicketProcessStaffList(queueId, ticketDetailObject.getLocationID(), ticketDetailObject.getBranchID(), false);*/

            //oaoa
            if (position == 0) {
                //mSpinnerEmployeeOperateStaff.setEnabled(false);
                //mSpinnerEmployeeVisorStaff.setEnabled(false);
                //mSpinnerEmployeeProcessStaff.setEnabled(false);
                loadDataLocalSpinnerOperateStaff();
                loadDataLocalSpinnerVisorStaff();
                loadDataLocalSpinnerProcessStaff();
            } else {
                mSpinnerEmployeeOperateStaff.setEnabled(true);
                mSpinnerEmployeeVisorStaff.setEnabled(true);
                //if (isEnabledSpinnerProcessStaff()) {
                if (getTmSharePreferencesUtils().getUserLogin().getAssign().equalsIgnoreCase(EXTRA_TICKET_USER_ASSIGN_KEY)) {
                    mSpinnerEmployeeProcessStaff.setEnabled(true);
                } else {
                    loadDataLocalSpinnerProcessStaff();
                    mSpinnerEmployeeProcessStaff.setEnabled(true);
                }


                getMyTicketOperateStaffList(queueId, ticketDetailObject.getLocationID(), ticketDetailObject.getBranchID(), String.valueOf(ticketDetailObject.getCricLev()), false);
                getMyTicketVisorStaffList(queueId, ticketDetailObject.getLocationID(), ticketDetailObject.getBranchID(), false);
                getMyTicketProcessStaffList(queueId, ticketDetailObject.getLocationID(), ticketDetailObject.getBranchID(), false);
            }
        }

        private void onSelectedItemSpinnerEffect(int isEffect, MyTicketDetailObject ticketDetailObject) {
            if (isEffect == 1) {
                setupCusQtyDataTask(String.valueOf(ticketDetailObject.getDeviceType()), getStringListDevice(mMyDeviceObject));
            } else {
                tvDescriptionCusQty.setText("0");
                tvDescriptionPlayTvQty.setText("0");
            }
        }

        private void onSelectedItemSpinnerProcessStaff(String staffEmail) {
            getTmApiEndPointProvider().addRequest(MyTicketDetailEndPoint.getMyTicketProcessStaff(staffEmail, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    if (getActivity() != null) {
                        JsonElement jsonElement = new JsonParser().parse(response);
                        JsonObject jsonObject = jsonElement.getAsJsonObject();
                        if (jsonObject.has("Result")) {
                            jsonObject = jsonObject.getAsJsonObject("Result");
                            if (jsonObject.has("Data")) {
                                jsonElement = jsonObject.get("Data");
                                if (jsonElement.isJsonArray()) {
                                    JsonArray jsonData = jsonElement.getAsJsonArray();
                                    if (jsonData.size() > 0) {
                                        jsonObject = jsonData.get(0).getAsJsonObject();
                                        if (jsonObject.has("IPPhone") && jsonObject.has("Mobile")) {
                                            tvEmployeeProcessIpPhone.setText(jsonObject.get("IPPhone").getAsString().toString());
                                            tvEmployeeProcessPhone.setText(jsonObject.get("Mobile").getAsString().toString());
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    onErrorListener(error);
                }
            }));
        }

        private void setupCusQtyDataTask(String type, String device) {
            getTmApiEndPointProvider().addRequest(MyTicketDetailEndPoint.getMyTicketCusQty(type, device, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    if (getActivity() != null) {
                        JsonElement jsonElement = new JsonParser().parse(response);
                        JsonObject jsonResult = jsonElement.getAsJsonObject();
                        jsonResult = jsonResult.getAsJsonObject("Result");
                        JsonArray jsonData = jsonResult.getAsJsonArray("Data");
                        if (jsonData.size() > 0) {
                            jsonResult = jsonData.get(0).getAsJsonObject();
                            if (jsonResult.has("CusQty") && jsonResult.has("PayTVQty")) {
                                String mCusQty = jsonResult.get("CusQty").toString();
                                String mPayTVQty = jsonResult.get("PayTVQty").toString();
                                tvDescriptionCusQty.setText(mCusQty);
                                tvDescriptionPlayTvQty.setText(mPayTVQty);
                            }
                        }
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    onErrorListener(error);
                }
            }));
        }

        private String getStringListDevice(MyDeviceObject myDeviceObject) {
            String result = "";
            int count = 0;
            if (myDeviceObject.getData() != null) {
                if (myDeviceObject.getData().size() > 0) {
                    for (MyDeviceObject.DeviceObject dv : myDeviceObject.getData()) {
                        count++;
                        if (count == myDeviceObject.getData().size()) {
                            result += dv.getDeviceName();
                        } else {
                            result += dv.getDeviceName();
                            result += ",";
                        }
                    }
                }
            }
            return result;
        }


    }

    private void RoleView() {
        int status = MY_TICKET_DETAIL_OBJECT.getTicketStatus();
        switch (status) {
            case -1://new
            {
                mSpinnerEmployeeQueue.setEnabled(false);
                if (getTmSharePreferencesUtils().getUserLogin().getAssign().equalsIgnoreCase(EXTRA_TICKET_USER_ASSIGN_KEY)) {
                    mSpinnerEmployeeProcessStaff.setEnabled(true);
                } else {
                    mSpinnerEmployeeProcessStaff.setEnabled(false);
                }
                break;
            }
            case 0://inprocess
            {
                mSpinnerEmployeeQueue.setEnabled(false);
                mSpinnerEmployeeProcessStaff.setEnabled(false);
                break;
            }
            case 2: {
                mSpinnerEmployeeQueue.setEnabled(true);
                if (getTmSharePreferencesUtils().getUserLogin().getAssign().equalsIgnoreCase(EXTRA_TICKET_USER_ASSIGN_KEY)) {
                    mSpinnerEmployeeProcessStaff.setEnabled(true);
                } else {
                    mSpinnerEmployeeProcessStaff.setEnabled(false);
                }
                break;
            }
            case 3: {
                mSpinnerEmployeeProcessStaff.setEnabled(true);
                getTmToast().makeToast(String.valueOf(status), Toast.LENGTH_SHORT);
                break;
            }
            default: {
                mSpinnerEmployeeQueue.setEnabled(false);
                mSpinnerEmployeeProcessStaff.setEnabled(false);
                break;
            }
        }
    }

    private void onSelectedItemSpinnerStatus(SpinnerObject spinnerObject) {
        int status = Integer.valueOf(spinnerObject.getKey()).intValue();
        switch (status) {
            case -1: // new

                //disable queue
                mSpinnerEmployeeQueue.setEnabled(false);
                //check if user can assing
                if (getTmSharePreferencesUtils().getUserLogin().getAssign().equalsIgnoreCase(EXTRA_TICKET_USER_ASSIGN_KEY)) {
                    mSpinnerEmployeeProcessStaff.setEnabled(true);
                } else {
                    mSpinnerEmployeeProcessStaff.setEnabled(false);
                }
                //reset process staff
                mSpinnerEmployeeProcessStaff.setSelection(0);
                tvEmployeeProcessIpPhone.setText("");
                tvEmployeeProcessPhone.setText("");

                //oaoa
                /*mSpinnerEmployeeQueue.setEnabled(false);
                mSpinnerEmployeeOperateStaff.setEnabled(true);
                mSpinnerEmployeeVisorStaff.setEnabled(true);
                loadDataLocalSpinnerProcessStaff();
                if (mSpinnerEmployeeQueue.getSelectedItemPosition() != 0 && isEnabledSpinnerProcessStaff()) {
                    mSpinnerEmployeeProcessStaff.setEnabled(true);
                    if (mLoadProcessStaff == false){
                        mLoadProcessStaff = true;
                    }else if (mLoadProcessStaff == true) {
                        SpinnerObject mSpinerQueue = (SpinnerObject) mSpinnerEmployeeQueue.getSelectedItem();
                        getMyTicketProcessStaffList(mSpinerQueue.getKey(), MY_TICKET_DETAIL_OBJECT.getLocationID(), MY_TICKET_DETAIL_OBJECT.getBranchID(), false);
                    }
                }else {
                    mSpinnerEmployeeProcessStaff.setEnabled(false);
                }*/

                break;
            case 0: // Inprogress

                mSpinnerEmployeeQueue.setEnabled(false);
                String assign = getTmSharePreferencesUtils().getUserLogin().getAssign();
                if(assign.equalsIgnoreCase(EXTRA_TICKET_USER_ASSIGN_KEY))
                mSpinnerEmployeeProcessStaff.setEnabled(true);
                else
                    mSpinnerEmployeeProcessStaff.setEnabled(false);
                for(int i =0; i< mProcessStaffList.size();i++){
                    if(mProcessStaffList.get(i).getValue().equals(getTmSharePreferencesUtils().getUserLogin().getName())){
                        mSpinnerEmployeeProcessStaff.setSelection(i);
                        if(getTmSharePreferencesUtils().getUserLogin().getAssign().equalsIgnoreCase(EXTRA_TICKET_USER_ASSIGN_KEY))
                            mSpinnerEmployeeProcessStaff.setEnabled(true);
                        else
                            mSpinnerEmployeeProcessStaff.setEnabled(false);
                        break;
                    }
                }
                //oaoa
                /*mSpinnerEmployeeQueue.setEnabled(false);
                mSpinnerEmployeeOperateStaff.setEnabled(true);
                mSpinnerEmployeeVisorStaff.setEnabled(true);
                if (MY_TICKET_STATE == StateTicket.NEW) {
                    if (isEnabledSpinnerProcessStaff()) {
                        mSpinnerEmployeeProcessStaff.setEnabled(true);
                    } else {
                        mSpinnerEmployeeProcessStaff.setEnabled(false);
                    }
                }else {
                    mSpinnerEmployeeProcessStaff.setEnabled(false);
                }*/

                break;
            case 2: // changed
            {
                //newcode
                mSpinnerEmployeeQueue.setEnabled(true);
                if (getTmSharePreferencesUtils().getUserLogin().getAssign().equalsIgnoreCase(EXTRA_TICKET_USER_ASSIGN_KEY)) {
                    mSpinnerEmployeeProcessStaff.setEnabled(true);
                } else {
                    mSpinnerEmployeeProcessStaff.setEnabled(false);
                }
                tvEmployeeProcessIpPhone.setText("");
                tvEmployeeProcessPhone.setText("");
                mSpinnerEmployeeProcessStaff.setSelection(0);
                break;
            }
            case 3: // forward

                mSpinnerEmployeeProcessStaff.setEnabled(true);

                //oaoa
                /*mSpinnerEmployeeQueue.setEnabled(true);
                mSpinnerEmployeeOperateStaff.setEnabled(true);
                mSpinnerEmployeeVisorStaff.setEnabled(true);
                if (isEnabledSpinnerProcessStaff()) {
                    mSpinnerEmployeeProcessStaff.setEnabled(true);
                } else {
                    mSpinnerEmployeeProcessStaff.setEnabled(false);
                }*/

                break;
            case 4: // rejected
            case 1: // closed
            case 10: // resolved
            {
                mSpinnerEmployeeQueue.setEnabled(false);
                mSpinnerEmployeeProcessStaff.setEnabled(false);
                tvEmployeeProcessIpPhone.setText("");
                tvEmployeeProcessPhone.setText("");
                mSpinnerEmployeeProcessStaff.setSelection(0);
                //oaoa
                /*mSpinnerEmployeeQueue.setEnabled(false);
                mSpinnerEmployeeOperateStaff.setEnabled(false);
                mSpinnerEmployeeVisorStaff.setEnabled(false);
                mSpinnerEmployeeProcessStaff.setEnabled(false);*/

                break;
            }
            default: {
                mSpinnerEmployeeQueue.setEnabled(false);
                mSpinnerEmployeeProcessStaff.setEnabled(false);
                tvEmployeeProcessIpPhone.setText("");
                tvEmployeeProcessPhone.setText("");
                mSpinnerEmployeeProcessStaff.setSelection(0);
                break;
            }
        }
    }

    private boolean isEnabledSpinnerProcessStaff() {
        SpinnerObject mSpinnerQueue = (SpinnerObject) mSpinnerEmployeeQueue.getSelectedItem();
        if (getTmSharePreferencesUtils().getUserLogin().getAssign().equalsIgnoreCase(EXTRA_TICKET_USER_ASSIGN_KEY) && mSpinnerQueue.getKey().equalsIgnoreCase(getTmSharePreferencesUtils().getUserLogin().getQueue())) {
            return true;
        }
        return false;
    }


}
