package app.fpt.ticketmobile.app.ui.lib;

import android.support.v4.widget.SwipeRefreshLayout;

/**
 * Created by Administrator on 6/21/2016.
 */
public class FragmentRefreshBasedSwipeRefreshLayoutHandler implements FragmentRefreshAnimationHandler {

    private final SwipeRefreshLayout mSwipeRefreshLayout;

    public FragmentRefreshBasedSwipeRefreshLayoutHandler(SwipeRefreshLayout mSwipeRefreshLayout, SwipeRefreshLayout.OnRefreshListener onRefreshListener, Integer colorSchemeResource){
        this.mSwipeRefreshLayout = mSwipeRefreshLayout;
        this.mSwipeRefreshLayout.setOnRefreshListener(onRefreshListener);
        if (colorSchemeResource != null){
            this.mSwipeRefreshLayout.setColorSchemeResources(colorSchemeResource.intValue());
        }
    }

    @Override
    public boolean isFragmentShowingRefreshAnimation() {
        return mSwipeRefreshLayout.isRefreshing();
    }


    @Override
    public void showRefreshAnimationInFragment() {
        mSwipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void stopRefreshAnimationInFragment() {
        mSwipeRefreshLayout.setRefreshing(false);
    }
}
