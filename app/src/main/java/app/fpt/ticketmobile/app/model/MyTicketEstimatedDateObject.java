package app.fpt.ticketmobile.app.model;

/**
 * Created by Administrator on 7/5/2016.
 */
public class MyTicketEstimatedDateObject {

    private String EstimatedDateValue;

    public String getEstimatedDateValue() {
        return EstimatedDateValue;
    }

    public void setEstimatedDateValue(String estimatedDateValue) {
        EstimatedDateValue = estimatedDateValue;
    }
}
