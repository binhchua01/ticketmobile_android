package app.fpt.ticketmobile.app.ui.activities.authenticatedActivities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import app.fpt.ticketmobile.app.ui.activities.TmBaseActivity;
import app.fpt.ticketmobile.R;
import app.fpt.ticketmobile.app.ui.fragments.authenticatedFragment.TicketOfUserCreatedFragment;

/**
 * Created by Administrator on 7/1/2016.
 */
public class TicketOfUserCreatedActivity extends TmBaseActivity {

    private static final String EXTRA_FRAGMENT_TICKET_OF_USER_CREATED_KEY = "extra_fragment_ticket_of_user_created_key";


    public TicketOfUserCreatedActivity() {
        super(Integer.valueOf(R.layout.activity_ticket_of_user_created));
    }

    public static Intent buildIntent(Context context){
        Intent intent = new Intent(context, TicketOfUserCreatedActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupIfFragmentIsNeeded();
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    private void setupIfFragmentIsNeeded() {

        TicketOfUserCreatedFragment ticketOfUserCreatedFragment = (TicketOfUserCreatedFragment) getSupportFragmentManager().findFragmentByTag(EXTRA_FRAGMENT_TICKET_OF_USER_CREATED_KEY);
        if (ticketOfUserCreatedFragment == null){
            ticketOfUserCreatedFragment = new TicketOfUserCreatedFragment();
        }
        getSupportFragmentManager().beginTransaction().replace(R.id.content_fragment, ticketOfUserCreatedFragment, EXTRA_FRAGMENT_TICKET_OF_USER_CREATED_KEY).commit();
    }

    @Override
    protected void setupToolbar(Toolbar mToolbar) {
        super.setupToolbar(mToolbar);
        ((TextView) mToolbar.findViewById(R.id.main_toolbar_title)).setText(getString(R.string.tm_ticket_of_me_created));
    }
}
