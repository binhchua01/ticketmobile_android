package app.fpt.ticketmobile.app.ui.adapter.libs.adapterDelegates;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import app.fpt.ticketmobile.app.ui.adapter.libs.utils.ViewUtils;
import app.fpt.ticketmobile.R;
import app.fpt.ticketmobile.app.lib.Provider;
import app.fpt.ticketmobile.app.lib.TmGraphics;
import app.fpt.ticketmobile.app.ui.adapter.libs.GroupTicketPrimaryFunctionInfo;
import app.fpt.ticketmobile.app.ui.lib.TicketPrimaryFunction;
import app.fpt.ticketmobile.widget.views.CircularImageView;

/**
 * Created by ToanMaster on 7/12/2016.
 */
public class DashboardGroupTicketPrimaryFunctionAdapter extends TmAdapterDelegates<GroupTicketPrimaryFunctionInfo, DashboardGroupTicketPrimaryFunctionAdapter.DashboardTicketPrimaryFunctionViewHolder>{

    private final TicketPrimaryFunctionCallback mCallback;
    private final TmGraphics mTmGraphics;
    private Provider<TicketPrimaryFunction> mSelectedPrimaryFunctionProvider = null;


    public interface TicketPrimaryFunctionCallback{
        void onGroupTicketPrimaryFunction(TicketPrimaryFunction ticketPrimaryFunction, String title);
    }

    public DashboardGroupTicketPrimaryFunctionAdapter(int itemViewType, TmGraphics tmGraphics, Provider<TicketPrimaryFunction> selectedPrimaryFunctionProvider, TicketPrimaryFunctionCallback callback) {
        super(itemViewType);
        this.mTmGraphics = tmGraphics;
        this.mSelectedPrimaryFunctionProvider = selectedPrimaryFunctionProvider;
        this.mCallback = callback;
    }

    @Override
    public void onBindViewHolder(GroupTicketPrimaryFunctionInfo groupTicket, DashboardTicketPrimaryFunctionViewHolder holder) {
        holder.bind(groupTicket.getTicketPrimaryFunction(), groupTicket.getTicketPrimaryFunctionTitle());
    }

    @Override
    public DashboardTicketPrimaryFunctionViewHolder onCreateViewHolder(ViewGroup parent) {
        return new DashboardTicketPrimaryFunctionViewHolder(ViewUtils.inflate(R.layout.entry_dashboard_group_ticket_primary_funtion, parent, false));
    }


    public class DashboardTicketPrimaryFunctionViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private final TextView mTicketPrimaryFunctionTitle;
        private final CircularImageView mTicketPrimaryFunctionImage;
        private TicketPrimaryFunction mTicketPrimaryFunction;
        private String mTitle;


        public DashboardTicketPrimaryFunctionViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            mTicketPrimaryFunctionTitle = (TextView) itemView.findViewById(R.id.group_ticket_primary_function_title);
            mTicketPrimaryFunctionImage = (CircularImageView) itemView.findViewById(R.id.group_ticket_primary_function_image);
        }

        public void bind(TicketPrimaryFunction ticketPrimaryFunction, String groupTicketTitle) {
            this.mTicketPrimaryFunction = ticketPrimaryFunction;
            this.mTitle = groupTicketTitle;
            if (mSelectedPrimaryFunctionProvider.provider() != null) {
                boolean isSelectedGroup = ((TicketPrimaryFunction) mSelectedPrimaryFunctionProvider.provider()).equals(ticketPrimaryFunction);
                this.itemView.setActivated(isSelectedGroup);
            }else {
                this.itemView.setActivated(false);
            }
           this.mTicketPrimaryFunctionImage.setImageBitmap(mTmGraphics.getGroupTicketPrimaryFunctionBitmap(ticketPrimaryFunction));
            this.mTicketPrimaryFunctionTitle.setText(groupTicketTitle);
        }

        @Override
        public void onClick(View v) {
            v.setActivated(true);
            mCallback.onGroupTicketPrimaryFunction(mTicketPrimaryFunction, mTitle);
        }
    }



}
