package app.fpt.ticketmobile.app.di.module;

import android.content.Context;

import javax.inject.Provider;

import app.fpt.ticketmobile.app.db.TmSharePreferencesUtils;
import dagger.internal.Factory;

/**
 * Created by ToanMaster on 6/15/2016.
 */
public class AppModule_ProvideTmSharePreferencesUtilFactory implements Factory<TmSharePreferencesUtils> {

    private static final boolean $assertionDisabled = !AppModule_ProvideTmConnectivityInfoFactory.class.desiredAssertionStatus();
    private AppModule appModule;
    private Provider<Context> contextProvider;

    public AppModule_ProvideTmSharePreferencesUtilFactory(AppModule appModule, Provider<Context> contextProvider) {
        if ($assertionDisabled || appModule != null) {
            this.appModule = appModule;
            if ($assertionDisabled || contextProvider != null) {
                this.contextProvider = contextProvider;
                return;
            }
        }
        throw new AssertionError();
    }

    @Override
    public TmSharePreferencesUtils get() {
        TmSharePreferencesUtils provideTmSharePreferencesUtils = appModule.provideTmSharePreferencesUtil(contextProvider.get());
        if (provideTmSharePreferencesUtils != null) {
            return provideTmSharePreferencesUtils;
        }
        throw new NullPointerException("Cannot return null from a non-@Nullable @Provides method");
    }

    public static Factory<TmSharePreferencesUtils> create(AppModule appModule, Provider<Context> contextProvider){
        return new AppModule_ProvideTmSharePreferencesUtilFactory(appModule, contextProvider);
    }
}
