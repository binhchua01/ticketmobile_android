package app.fpt.ticketmobile.app.lib;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.util.HashMap;

import app.fpt.ticketmobile.app.ui.lib.TicketSupport;
import app.fpt.ticketmobile.R;
import app.fpt.ticketmobile.app.ui.lib.TicketSettingInfo;
import app.fpt.ticketmobile.app.ui.lib.TicketPrimaryFunction;

/**
 * Created by Administrator on 7/12/2016.
 */
public class TmGraphics {

    private HashMap<TicketPrimaryFunction, Bitmap> mGroupTicketPrimaryFunctionBitmap;
    private HashMap<TicketSupport, Bitmap> mGroupTicketSupportBitmap;
    private HashMap<TicketSettingInfo, Bitmap> mGroupTicketSettingBitmap;

    public TmGraphics(Context context){
        setupIndividualBitmaps(context);
        setupGroupTicketPrimaryFunctionBitmaps(context);
        setupGroupTicketSupportBitmaps(context);
        setupGroupTicketSettingBitmaps(context);
    }

    private void setupIndividualBitmaps(Context context) {

    }

    private void setupGroupTicketSupportBitmaps(Context context) {
        mGroupTicketPrimaryFunctionBitmap = new HashMap<>();
        mGroupTicketPrimaryFunctionBitmap.put(TicketPrimaryFunction.TICKET_OF_USER, BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_ticket_of_user));
        mGroupTicketPrimaryFunctionBitmap.put(TicketPrimaryFunction.TICKET_OF_USER_CREATE, BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_ticket_of_user_create));
        mGroupTicketPrimaryFunctionBitmap.put(TicketPrimaryFunction.TICKET_INVENTORY_NOT_ASSIGN, BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_ticket));
        mGroupTicketPrimaryFunctionBitmap.put(TicketPrimaryFunction.TICKET_INVENTORY_UNFINISHED, BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_ticket_inventory));
    }



    private void setupGroupTicketPrimaryFunctionBitmaps(Context context) {
        mGroupTicketSupportBitmap = new HashMap<>();
        mGroupTicketSupportBitmap.put(TicketSupport.TICKET_SUPPORT_OF_ME, BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_ticket_of_user));
        mGroupTicketSupportBitmap.put(TicketSupport.TICKET_SUPPORT_CREATE, BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_ticket_of_user));
    }

    private void setupGroupTicketSettingBitmaps(Context context){
        mGroupTicketSettingBitmap = new HashMap<>();
        mGroupTicketSettingBitmap.put(TicketSettingInfo.TICKET_SUPPORT_VERSION, BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_settings_black_24dp));
        mGroupTicketSettingBitmap.put(TicketSettingInfo.TICKET_SUPPORT_EXIT, BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_exit_to_app_black_24dp));
    }

    public Bitmap getGroupTicketPrimaryFunctionBitmap(TicketPrimaryFunction ticketPrimaryFunction){
        return mGroupTicketPrimaryFunctionBitmap.get(ticketPrimaryFunction);
    }

    public Bitmap getGroupTicketSupportBitmap(TicketSupport ticketSupport){
        return mGroupTicketSupportBitmap.get(ticketSupport);
    }

    public Bitmap getGroupTicketSettingBitmap(TicketSettingInfo ticketSettingInfo){
        return mGroupTicketSettingBitmap.get(ticketSettingInfo);
    }

}
