package app.fpt.ticketmobile.app.ui.fragments.authenticatedFragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

import java.util.ArrayList;
import java.util.List;

import app.fpt.ticketmobile.app.model.MyTicketOfUserCreatedObject;
import app.fpt.ticketmobile.app.networking.apiEndpoints.MyTicketOfMeCreatedEndPoint;
import app.fpt.ticketmobile.app.ui.adapter.MyTicketOfMeCreatedAdapter;
import app.fpt.ticketmobile.app.ui.adapter.ticketofusercreated.TicketOfUserCreatedHeader;
import app.fpt.ticketmobile.app.ui.lib.FragmentRefreshBasedSwipeRefreshLayoutHandler;
import app.fpt.ticketmobile.R;
import app.fpt.ticketmobile.app.ui.fragments.TmBaseFragment;

/**
 * Created by Administrator on 7/1/2016.
 */
public class TicketOfUserCreatedFragment extends TmBaseFragment implements SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG_MY_TICKET_OF_USER_CREATED_KEY = "tag_my_ticket_of_me_created_key";
    private RecyclerView mListTicketInventoryNotAssign;
    private MyTicketOfMeCreatedAdapter myTicketOfUserCreatedAdapter;
    private List<TicketOfUserCreatedHeader> mTicketOfUserCreatedHeaderList;

    public interface Callback {

    }

    public TicketOfUserCreatedFragment() {
        super(Callback.class, Integer.valueOf(R.layout.fragment_ticket_inventory), false, true);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mTicketOfUserCreatedHeaderList = new ArrayList<>();
    }

    @Override
    protected void onTmActivityCreated(View parent, Bundle savedInstanceState) {
        super.onTmActivityCreated(parent, savedInstanceState);
        mListTicketInventoryNotAssign = (RecyclerView) parent.findViewById(R.id.list);
        mListTicketInventoryNotAssign.setItemAnimator(new DefaultItemAnimator());
        myTicketOfUserCreatedAdapter = new MyTicketOfMeCreatedAdapter(getActivity(), mTicketOfUserCreatedHeaderList);
        mListTicketInventoryNotAssign.setAdapter(myTicketOfUserCreatedAdapter);

        SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) parent.findViewById(R.id.swipe_refresh_layout);
        setFragmentRefreshAnimationHandler(new FragmentRefreshBasedSwipeRefreshLayoutHandler(swipeRefreshLayout, this, Integer.valueOf(R.color.tm_swipe_refresh_color)));
        if (!isShowingRefreshAnimation()) {
            swipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    showRefreshAnimation();
                }
            }, 500);
        }
        myTicketOfUserCreatedAdapter.onRestoreInstanceState(savedInstanceState);
    }


    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        myTicketOfUserCreatedAdapter.onSaveInstanceState(savedInstanceState);
    }


    @Override
    public void onRefresh() {
        autoSynced();
    }

    @Override
    protected void autoSynced() {
        super.autoSynced();
        if (!isShowingRefreshAnimation()) {
            showRefreshAnimation();
        }

        getTmApiEndPointProvider().addRequest(MyTicketOfMeCreatedEndPoint.getMyCreatedTicket(getTmSharePreferencesUtils().getEmailUserLogin(),

                new Response.Listener<MyTicketOfUserCreatedObject>() {
                    @Override
                    public void onResponse(final MyTicketOfUserCreatedObject myTicketOfUserCreatedObject) {
                        if (getActivity() != null) {
                            if (myTicketOfUserCreatedObject != null && myTicketOfUserCreatedObject.getTicketOfMeCreatedHeaderList() != null) {
                                if (myTicketOfUserCreatedObject.getTicketOfMeCreatedHeaderList().size() > 0) {
                                    myTicketOfUserCreatedAdapter.clear();
                                    mTicketOfUserCreatedHeaderList = myTicketOfUserCreatedObject.getTicketOfMeCreatedHeaderList();
                                    myTicketOfUserCreatedAdapter = new MyTicketOfMeCreatedAdapter(getActivity(), mTicketOfUserCreatedHeaderList);
                                    mListTicketInventoryNotAssign.setAdapter(myTicketOfUserCreatedAdapter);
                                    mListTicketInventoryNotAssign.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
                                } else {
                                    stopRefreshAnimation();
                                    setLayoutNoData(R.layout.fragment_no_data);

                                }
                            } else {
                                stopRefreshAnimation();
                                setLayoutNoData(R.layout.fragment_no_data);

                            }
                            stopRefreshAnimation();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        onErrorListener(error);
                    }
                }), TAG_MY_TICKET_OF_USER_CREATED_KEY);


    }


    @Override
    public void onStop() {
        super.onStop();
        getTmApiEndPointProvider().cancelAllRequest(TAG_MY_TICKET_OF_USER_CREATED_KEY);
    }

    private void onErrorListener(VolleyError error) {
        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
            getTmToast().makeToast(getString(R.string.tm_error_connect), Toast.LENGTH_SHORT);
        } else if (error instanceof AuthFailureError) {
            Log.d("tmt", "AuthFailureError");
        } else if (error instanceof ServerError) {
            Log.d("tmt", "ServerError");
        } else if (error instanceof NetworkError) {
            Log.d("tmt", "NetworkError");
        } else if (error instanceof ParseError) {
            Log.d("tmt", "ParseError");
        } else {
            Log.d("tmt", "onErrorResponse : " + error.getMessage());
        }
        stopRefreshAnimation();
    }

    private void setLayoutNoData(Integer layoutResId) {
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewGroup rootView = (ViewGroup) getView();
        View mainView = inflater.inflate(layoutResId.intValue(), rootView, false);
        rootView.removeAllViews();
        rootView.addView(mainView);
        SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) mainView.findViewById(R.id.swipe_refresh_layout);
        setFragmentRefreshAnimationHandler(new FragmentRefreshBasedSwipeRefreshLayoutHandler(swipeRefreshLayout, this, Integer.valueOf(R.color.tm_swipe_refresh_color)));

    }

}