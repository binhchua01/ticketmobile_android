package app.fpt.ticketmobile.app.ui.fragments.authenticatedFragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

import java.util.ArrayList;
import java.util.List;

import app.fpt.ticketmobile.app.model.MyTicketObject;
import app.fpt.ticketmobile.app.networking.apiEndpoints.MyTicketEndPoint;
import app.fpt.ticketmobile.app.ui.adapter.MyTicketAdapter;
import app.fpt.ticketmobile.app.ui.lib.FragmentRefreshBasedSwipeRefreshLayoutHandler;
import app.fpt.ticketmobile.R;
import app.fpt.ticketmobile.app.ui.fragments.TmBaseFragment;

/**
 * Created by ToanMaster on 6/24/2016.
 */
public class TicketQueueAndStatusFragment extends TmBaseFragment<TicketQueueAndStatusFragment.Callback> implements SwipeRefreshLayout.OnRefreshListener {

    private static final String MY_TICKET_QUEUE_AND_STATUS_REQUEST_TAG = "my_ticket_queue_and_status_request_tag";
    private static final String EXTRA_INTENT_QUEUE_ID_KEY = "extra_intent_queue_id_key";
    private static final String EXTRA_INTENT_QUEUE_STATUS_TITLE_KEY = "extra_intent_queue_status_title_key";
    private static final String EXTRA_INTENT_QUEUE_STATUS_ID_KEY = "extra_intent_queue_status_id_key";
    private String mQueueId = null;
    private String mQueueStatusTitle = null;
    private String mQueueStatusId = null;

    private RecyclerView mListTicketQueueAnStatus;
    private MyTicketAdapter myTicketAdapter;
    private List<MyTicketObject.MyTicket> myTicketObjectList;
    private View mTicketContentLayout;
    private TextView mTicketQueueAndStatusTitleContent;

    public interface Callback {

    }

    public TicketQueueAndStatusFragment() {
        super(Callback.class, Integer.valueOf(R.layout.fragment_ticket_queue_and_status), false, true);
    }

    public static TicketQueueAndStatusFragment getInstances(String queueId, String queueStatusId, String queueStatusTitle) {
        Bundle bundle = new Bundle();
        bundle.putString(EXTRA_INTENT_QUEUE_ID_KEY, queueId);
        bundle.putString(EXTRA_INTENT_QUEUE_STATUS_ID_KEY, queueStatusId);
        bundle.putString(EXTRA_INTENT_QUEUE_STATUS_TITLE_KEY, queueStatusTitle);

        TicketQueueAndStatusFragment fragment = new TicketQueueAndStatusFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        extrasArguments();
    }

    @Override
    protected void onTmActivityCreated(View parent, Bundle savedInstanceState) {
        super.onTmActivityCreated(parent, savedInstanceState);
        setupSwipeRefreshLayoutView(parent);
    }

    private void setupSwipeRefreshLayoutView(View parent) {
        mTicketContentLayout = parent.findViewById(R.id.tm_layout_ticket_content);
        mTicketQueueAndStatusTitleContent = (TextView) parent.findViewById(R.id.tm_ticket_of_queue_status);
        SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) parent.findViewById(R.id.swipe_refresh_layout);
        setFragmentRefreshAnimationHandler(new FragmentRefreshBasedSwipeRefreshLayoutHandler(swipeRefreshLayout, this, Integer.valueOf(R.color.tm_swipe_refresh_color)));
        mListTicketQueueAnStatus = (RecyclerView) parent.findViewById(R.id.list);
        myTicketObjectList = new ArrayList<>();
        myTicketAdapter = new MyTicketAdapter(myTicketObjectList);
        mListTicketQueueAnStatus.setAdapter(myTicketAdapter);
        mListTicketQueueAnStatus.setLayoutManager(new LinearLayoutManager(parent.getContext()));

        if (!isShowingRefreshAnimation()) {
            swipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    showRefreshAnimation();
                }
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!isShowingRefreshAnimation()){
            showRefreshAnimation();
        }
    }

    @Override
    public void onRefresh() {
        autoSynced();
    }

    @Override
    protected void autoSynced() {
        super.autoSynced();
        if (!isShowingRefreshAnimation()) {
            showRefreshAnimation();
        }

        getTmApiEndPointProvider().addRequest(MyTicketEndPoint.getMyTicketQueueAndStatus(getTmSharePreferencesUtils().getUserIdLogin(), mQueueId, mQueueStatusId, new Response.Listener<MyTicketObject>() {
            @Override
            public void onResponse(MyTicketObject myTicketObject) {
                if (getActivity() != null) {
                    if (myTicketObject != null && myTicketObject.getMyTicketList() != null) {
                        if (myTicketObject.getMyTicketList().size() > 0) {
                            myTicketObjectList.clear();
                            myTicketObjectList = myTicketObject.getMyTicketList();
                            myTicketAdapter = new MyTicketAdapter(myTicketObjectList);
                            mListTicketQueueAnStatus.setAdapter(myTicketAdapter);
                            mListTicketQueueAnStatus.setLayoutManager(new LinearLayoutManager(getActivity()));
                            mTicketContentLayout.setVisibility(View.VISIBLE);
                            mTicketQueueAndStatusTitleContent.setText(mQueueStatusTitle + "(" + myTicketObjectList.size() + ")");
                        } else {
                            setLayoutNoData(R.layout.fragment_no_data);
                        }

                    } else {
                        setLayoutNoData(R.layout.fragment_no_data);
                        mTicketContentLayout.setVisibility(View.GONE);
                    }
                    stopRefreshAnimation();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    getTmToast().makeToast(getString(R.string.tm_error_connect), Toast.LENGTH_SHORT);
                } else if (error instanceof AuthFailureError) {
                    Log.d("tmt", "AuthFailureError");
                } else if (error instanceof ServerError) {
                    Log.d("tmt", "ServerError");
                } else if (error instanceof NetworkError) {
                    Log.d("tmt", "NetworkError");
                } else if (error instanceof ParseError) {
                    Log.d("tmt", "ParseError");
                } else {
                    Log.d("tmt", "onErrorResponse : " + error.getMessage());
                }
//                TmUtilsVolleyErrorListener.onErrorListener(getActivity(), getView() ,error);
                stopRefreshAnimation();
            }
        }));
    }

    private void setLayoutNoData(Integer layoutResId) {
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewGroup rootView = (ViewGroup) getView();
        View mainView = inflater.inflate(layoutResId.intValue(), rootView, false);
        rootView.removeAllViews();
        rootView.addView(mainView);
        SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) mainView.findViewById(R.id.swipe_refresh_layout);
        setFragmentRefreshAnimationHandler(new FragmentRefreshBasedSwipeRefreshLayoutHandler(swipeRefreshLayout, this, Integer.valueOf(R.color.tm_swipe_refresh_color)));

    }

    private void extrasArguments() {
        if (getArguments() != null) {
            mQueueId = getArguments().getString(EXTRA_INTENT_QUEUE_ID_KEY);
            mQueueStatusId = getArguments().getString(EXTRA_INTENT_QUEUE_STATUS_ID_KEY);
            mQueueStatusTitle = getArguments().getString(EXTRA_INTENT_QUEUE_STATUS_TITLE_KEY);
        }

    }
}
