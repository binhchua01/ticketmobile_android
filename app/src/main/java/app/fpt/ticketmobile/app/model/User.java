package app.fpt.ticketmobile.app.model;

/**
 * Created by ToanMaster on 6/14/2016.
 */
public class User {

    private String Assign;
    private String Create;
    private Integer DivisionID;
    private String DivisionName;
    private String Email;
    private Integer ID;
    private String IPPhone;
    private String Mobile;
    private String Name;
    private Integer ParentQueue;
    private String Queue;
    private Integer Update;
    private Integer View;

    private String ErrorDescription;

    public String getAssign() {
        return Assign;
    }

    public void setAssign(String assign) {
        Assign = assign;
    }

    public String getCreate() {
        return Create;
    }

    public void setCreate(String create) {
        Create = create;
    }

    public Integer getDivisionID() {
        return DivisionID;
    }

    public void setDivisionID(Integer divisionID) {
        DivisionID = divisionID;
    }

    public String getDivisionName() {
        return DivisionName;
    }

    public void setDivisionName(String divisionName) {
        DivisionName = divisionName;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public String getIPPhone() {
        return IPPhone;
    }

    public void setIPPhone(String IPPhone) {
        this.IPPhone = IPPhone;
    }

    public String getMobile() {
        return Mobile;
    }

    public void setMobile(String mobile) {
        Mobile = mobile;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public Integer getParentQueue() {
        return ParentQueue;
    }

    public void setParentQueue(Integer parentQueue) {
        ParentQueue = parentQueue;
    }

    public String getQueue() {
        return Queue;
    }

    public void setQueue(String queue) {
        Queue = queue;
    }

    public Integer getUpdate() {
        return Update;
    }

    public void setUpdate(Integer update) {
        Update = update;
    }

    public Integer getView() {
        return View;
    }

    public void setView(Integer view) {
        View = view;
    }

    public String getErrorDescription() {
        return ErrorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        ErrorDescription = errorDescription;
    }
}
