package app.fpt.ticketmobile.app.jsonParsers.Fortype.support;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import app.fpt.ticketmobile.app.model.SpinnerObject;
import app.fpt.ticketmobile.app.model.support.MyQueueObject;

/**
 * Created by Administrator on 7/21/2016.
 */
public class MyQueueField {
    public static class Deserializer implements JsonDeserializer<MyQueueObject>{

        @Override
        public MyQueueObject deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            Gson gson = new Gson();
            MyQueueObject myQueueObject = null;
            List<SpinnerObject> spinnerObjectList = new ArrayList<>();

            try{
                JsonObject jsResult = json.getAsJsonObject();
                if (jsResult.has("Result")){
                    jsResult = jsResult.getAsJsonObject("Result");
                    if (jsResult.has("Data")){
                        if (jsResult.get("Data").isJsonArray()) {
                            myQueueObject = gson.fromJson(jsResult, MyQueueObject.class);

                            if (myQueueObject.getQueueList().size() > 0){
                                for (int i = 0; i < myQueueObject.getQueueList().size(); i++){
                                    MyQueueObject.QueueObject queueObject = myQueueObject.getQueueList().get(i);
                                    SpinnerObject spinnerObject = new SpinnerObject(String.valueOf(queueObject.getID()), queueObject.getName());
                                    spinnerObjectList.add(spinnerObject);
                                }
                                myQueueObject.setSpinnerObjectList(spinnerObjectList);
                            }
                        }
                    }
                }

            }catch (Exception e){
                Log.e("tmt", "MyTicketBusinessAreaField : Deserializer : " + e.toString());
            }

            return myQueueObject;
        }
    }
}
