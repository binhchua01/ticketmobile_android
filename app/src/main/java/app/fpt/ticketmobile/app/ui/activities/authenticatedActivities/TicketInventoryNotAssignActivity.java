package app.fpt.ticketmobile.app.ui.activities.authenticatedActivities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import app.fpt.ticketmobile.app.ui.activities.TmBaseActivity;
import app.fpt.ticketmobile.R;
import app.fpt.ticketmobile.app.ui.fragments.authenticatedFragment.TicketInventoryNotAssignFragment;

/**
 * Created by ToanMaster on 7/1/2016.
 */
public class TicketInventoryNotAssignActivity extends TmBaseActivity {
    private static final String EXTRA_FRAGMENT_TICKET_INVENTORY_NOT_ASSIGN_KEY = "extra_fragment_ticket_inventory_not_assign_key";

    public TicketInventoryNotAssignActivity() {
        super(Integer.valueOf(R.layout.activity_ticket_inventory_not_assign));
    }

    public static Intent buildIntent(Context context){
        Intent intent = new Intent(context, TicketInventoryNotAssignActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupIfFragmentIsNeeded();
    }

    private void setupIfFragmentIsNeeded() {
        TicketInventoryNotAssignFragment ticketInventoryNotAssignFragment = (TicketInventoryNotAssignFragment) getSupportFragmentManager().findFragmentByTag(EXTRA_FRAGMENT_TICKET_INVENTORY_NOT_ASSIGN_KEY);
        if (ticketInventoryNotAssignFragment == null){
            ticketInventoryNotAssignFragment = new TicketInventoryNotAssignFragment();
        }
        getSupportFragmentManager().beginTransaction().replace(R.id.content_fragment, ticketInventoryNotAssignFragment, EXTRA_FRAGMENT_TICKET_INVENTORY_NOT_ASSIGN_KEY).commit();
    }

    @Override
    protected void setupToolbar(Toolbar mToolbar) {
        super.setupToolbar(mToolbar);
        ((TextView) mToolbar.findViewById(R.id.main_toolbar_title)).setText(getString(R.string.tm_ticket_not_yet_assigned));
    }
}
