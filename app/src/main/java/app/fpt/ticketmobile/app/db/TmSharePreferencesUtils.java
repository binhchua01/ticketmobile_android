package app.fpt.ticketmobile.app.db;

import android.content.Context;
import android.content.SharedPreferences;

import app.fpt.ticketmobile.app.model.User;

/**
 * Created by ToanMaster on 6/14/2016.
 */
public class TmSharePreferencesUtils {
    private static final String EXTRA_USER_ASSIGN_KEY = "extra_user_assign_key";
    private static final String EXTRA_USER_CREATE_KEY = "extra_pass_login_key";
    private static final String EXTRA_USER_DIVISION_ID_KEY = "extra_user_division_id_key";
    private static final String EXTRA_USER_DIVISION_NAME_KEY = "extra_user_division_name_key";
    private static final String EXTRA_USER_EMAIL_KEY = "extra_email_login_key";
    private static final String EXTRA_USER_ID_KEY = "extra_user_id_key";
    private static final String EXTRA_USER_IPPhone_KEY = "extra_user_ipPhone_key";
    private static final String EXTRA_USER_MOBILE_KEY = "extra_user_mobile_key";
    private static final String EXTRA_USER_NAME_KEY = "extra_user_name_key";
    private static final String EXTRA_USER_PARENT_QUEUE_KEY = "extra_user_parent_queue_key";
    private static final String EXTRA_USER_QUEUE_KEY = "extra_user_queue_key";
    private static final String EXTRA_USER_UPDATE_KEY = "extra_user_update_key";
    private static final String EXTRA_USER_VIEW_KEY = "extra_user_view_key";


    private static final String EXTRA_CHECK_EMAIL_LOGIN_KEY = "extra_check_email_login_key";
    private static final String EXTRA_CHECK_PASS_LOGIN_KEY = "extra_check_pass_login_key";
    private static final String EXTRA_CHECK_REMEMBER_ACCOUNT_LOGIN_KEY = "extra_check_remember_account_login_key";
    private static final String TOKEN_KONG = "token_kong";
    private static TmSharePreferencesUtils mPreferencesUtils;
    private SharedPreferences mPreferences;

    public TmSharePreferencesUtils (Context context){
        mPreferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);

    }

    public synchronized static TmSharePreferencesUtils getInstance(Context context){
        if (mPreferencesUtils == null){
            mPreferencesUtils = new TmSharePreferencesUtils(context);
        }
        return mPreferencesUtils;
    }

    public void saveCheckUserLogin(String email, String pass, boolean checkRememberAccount){
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString(EXTRA_CHECK_EMAIL_LOGIN_KEY, email);
        editor.putString(EXTRA_CHECK_PASS_LOGIN_KEY, pass);
        editor.putBoolean(EXTRA_CHECK_REMEMBER_ACCOUNT_LOGIN_KEY, checkRememberAccount);
        editor.commit();
    }

    public void saveUserLogin(User user){
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString(EXTRA_USER_ASSIGN_KEY, user.getAssign());
        editor.putString(EXTRA_USER_CREATE_KEY, user.getCreate());
        editor.putInt(EXTRA_USER_DIVISION_ID_KEY, user.getDivisionID());
        editor.putString(EXTRA_USER_DIVISION_NAME_KEY, user.getDivisionName());
        editor.putString(EXTRA_USER_EMAIL_KEY, user.getEmail());
        editor.putInt(EXTRA_USER_ID_KEY, user.getID());
        editor.putString(EXTRA_USER_IPPhone_KEY, user.getIPPhone());
        editor.putString(EXTRA_USER_MOBILE_KEY, user.getMobile());
        editor.putString(EXTRA_USER_NAME_KEY, user.getName());
        editor.putInt(EXTRA_USER_PARENT_QUEUE_KEY, user.getParentQueue());
        editor.putString(EXTRA_USER_QUEUE_KEY, user.getQueue());
        editor.putInt(EXTRA_USER_UPDATE_KEY, user.getUpdate());
        editor.putInt(EXTRA_USER_VIEW_KEY, user.getView());
        editor.commit();
    }

    public User getUserLogin(){
        User user = new User();
        user.setAssign(mPreferences.getString(EXTRA_USER_ASSIGN_KEY, ""));
        user.setCreate(mPreferences.getString(EXTRA_USER_CREATE_KEY, ""));
        user.setDivisionID(mPreferences.getInt(EXTRA_USER_DIVISION_ID_KEY, 0));
        user.setDivisionName(mPreferences.getString(EXTRA_USER_DIVISION_NAME_KEY, ""));
        user.setEmail(mPreferences.getString(EXTRA_USER_EMAIL_KEY, ""));
        user.setID(mPreferences.getInt(EXTRA_USER_ID_KEY, 0));
        user.setIPPhone(mPreferences.getString(EXTRA_USER_IPPhone_KEY, ""));
        user.setMobile(mPreferences.getString(EXTRA_USER_MOBILE_KEY, ""));
        user.setName(mPreferences.getString(EXTRA_USER_NAME_KEY, ""));
        user.setParentQueue(mPreferences.getInt(EXTRA_USER_PARENT_QUEUE_KEY, 0));
        user.setQueue(mPreferences.getString(EXTRA_USER_QUEUE_KEY, ""));
        user.setUpdate(mPreferences.getInt(EXTRA_USER_UPDATE_KEY, 0));
        user.setView(mPreferences.getInt(EXTRA_USER_VIEW_KEY, 0));

        return user;
    }

    public void saveKongToken(String kongToken){
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString(TOKEN_KONG, kongToken);
        editor.commit();
    }

    public String getKongToken(){
        String kongToken = "";
        kongToken = mPreferences.getString(TOKEN_KONG, "");
        return  kongToken;
    }

    public void clear(){
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.clear().commit();
//        editor.putString(EXTRA_USER_ASSIGN_KEY, "");
//        editor.putString(EXTRA_USER_CREATE_KEY, "");
//        editor.putInt(EXTRA_USER_DIVISION_ID_KEY, 0);
//        editor.putString(EXTRA_USER_DIVISION_NAME_KEY, "");
//        editor.putString(EXTRA_USER_EMAIL_KEY, "");
//        editor.putInt(EXTRA_USER_ID_KEY, 0);
//        editor.putString(EXTRA_USER_IPPhone_KEY, "");
//        editor.putString(EXTRA_USER_MOBILE_KEY, "");
//        editor.putString(EXTRA_USER_NAME_KEY, "");
//        editor.putInt(EXTRA_USER_PARENT_QUEUE_KEY, 0);
//        editor.putString(EXTRA_USER_QUEUE_KEY, "");
//        editor.putInt(EXTRA_USER_UPDATE_KEY, 0);
//        editor.putInt(EXTRA_USER_VIEW_KEY, 0);
      //  editor.commit();
    }

    public String getEmailUserLogin(){
      return mPreferences.getString(EXTRA_USER_EMAIL_KEY, "");
    }

    public String getPhoneUserLogin(){
       return mPreferences.getString(EXTRA_USER_MOBILE_KEY, "");
    }

    public String getIpPhoneUserLogin(){
        return mPreferences.getString(EXTRA_USER_IPPhone_KEY, "");
    }

    public String getDivisionUserLogin(){
        return mPreferences.getString(EXTRA_USER_DIVISION_NAME_KEY, "");
    }

    public String getUserIdLogin(){
        return String.valueOf(mPreferences.getInt(EXTRA_USER_ID_KEY, -1));
    }

    public String getUserName(){
        return mPreferences.getString(EXTRA_USER_NAME_KEY, "");
    }


    public void setupDeviceTokenGcm(String deviceTokenGcm){
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString("deviceTokenGcm", deviceTokenGcm);
        editor.commit();
    }

    public String getDeviceTokenGcm(){
        String  deviceTokenGcm = mPreferences.getString("deviceTokenGcm", "");
        return deviceTokenGcm;
    }
}
