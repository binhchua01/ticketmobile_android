package app.fpt.ticketmobile.app.model.support;

import java.util.List;

import app.fpt.ticketmobile.app.model.SpinnerObject;

/**
 * Created by Administrator on 7/7/2016.
 */
public class MyTicketReasonSupportObject {

    private List<ReasonObject> Data;
    private List<SpinnerObject> spinnerObjectList;

    public List<ReasonObject> getReasonList() {
        return Data;
    }

    public void setReasonList(List<ReasonObject> data) {
        Data = data;
    }

    public List<SpinnerObject> getSpinnerObjectList() {
        return spinnerObjectList;
    }

    public void setSpinnerObjectList(List<SpinnerObject> spinnerObjectList) {
        this.spinnerObjectList = spinnerObjectList;
    }

    public class ReasonObject{
        private Integer ID;
        private String Description;

        public String getDescription() {
            return Description;
        }

        public void setDescription(String description) {
            Description = description;
        }

        public Integer getID() {
            return ID;
        }

        public void setID(Integer ID) {
            this.ID = ID;
        }
    }
}
