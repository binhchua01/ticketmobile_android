package app.fpt.ticketmobile.app.model.support;

/**
 * Created by Administrator on 7/20/2016.
 */
public class ImportContractObject {

    private String ContractID;
    private String Name;
    private String IPDomain;
    private String Address;

    public String getAddress() {
        return Address;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }

    public String getContract() {
        return ContractID;
    }

    public void setContract(String Contract) {
        this.ContractID = Contract;
    }

    public String getContractName() {
        return Name ;
    }

    public void setContractName(String ContractName) {
        this.Name  = ContractName;
    }

    public String getIpDomain() {
        return IPDomain;
    }

    public void setIpDomain(String IpDomain) {
        this.IPDomain = IpDomain;
    }

    public ImportContractObject(String Contract, String ContractName, String IpDomain, String Address) {
        this.Address = Address;
        this.ContractID = Contract;
        this.Name  = ContractName;
        this.IPDomain = IpDomain;
    }

    @Override
    public String toString() {
        return ContractID + "|" + IPDomain + "|" + Address + "|" + Name;
    }
}
