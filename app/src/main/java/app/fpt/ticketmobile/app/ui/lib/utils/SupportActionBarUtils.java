package app.fpt.ticketmobile.app.ui.lib.utils;

import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import app.fpt.ticketmobile.app.ui.activities.BaseActivity;

/**
 * Created by Administrator on 7/14/2016.
 */
public class SupportActionBarUtils {

    private SupportActionBarUtils() {
    }

    public static boolean disableTitleAndSetCustomView(BaseActivity baseActivity, int resId) {
        ActionBar supportActionBar = baseActivity.getSupportActionBar();
        if (supportActionBar == null || !setCustomView(baseActivity, resId)) {
            return false;
        }
        supportActionBar.setDisplayShowTitleEnabled(false);
        supportActionBar.setDisplayShowCustomEnabled(true);
        return true;
    }

    public static boolean setCustomView(BaseActivity baseActivity, int resId) {
        ActionBar supportActionBar = baseActivity.getSupportActionBar();
        if (supportActionBar != null) {
            if (baseActivity.getToolbar() != null) {
                setCustomView(supportActionBar, baseActivity.getToolbar(), resId);
            } else {
                supportActionBar.setCustomView(resId);
            }
            return true;
        }
        return false;
    }

    private static boolean setCustomView(ActionBar actionBar, Toolbar toolbar, int resId) {
        View view = LayoutInflater.from(actionBar.getThemedContext()).inflate(resId, toolbar, false);
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams == null || !(layoutParams instanceof ActionBar.LayoutParams)) {
            actionBar.setCustomView(view);
        } else {
            actionBar.setCustomView(view, (ActionBar.LayoutParams) layoutParams);
        }
        return true;
    }
}
