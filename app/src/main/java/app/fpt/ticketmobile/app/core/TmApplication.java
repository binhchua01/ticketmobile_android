package app.fpt.ticketmobile.app.core;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import java.io.IOException;

import app.fpt.ticketmobile.app.di.component.AppComponent;
import app.fpt.ticketmobile.app.di.component.DaggerAppComponent;
import app.fpt.ticketmobile.app.di.module.AppModule;
import app.fpt.ticketmobile.R;
import app.fpt.ticketmobile.app.tracker.AnalyticsTrackers;

/**
 * Created by Administrator on 6/13/2016.
 */
public class TmApplication extends MultiDexApplication {

    private static TmApplication mTMAPPLICATION;
    private AppComponent mAppComponent;

    public TmApplication() {
        this.mTMAPPLICATION = this;
    }

    public static TmApplication getmTMAPPLICATION(){
        return mTMAPPLICATION;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(base);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        initAnalytics();
        mAppComponent = DaggerAppComponent.builder().appModule(new AppModule(this, new Handler(Looper.getMainLooper()))).build();

        if (TextUtils.isEmpty(mAppComponent.getTmSharePreferencesUtils().getDeviceTokenGcm())) {
            registrationGcmId();
        }

    }

    private void initAnalytics() {
        AnalyticsTrackers.initialize(this);
        AnalyticsTrackers analyticsTrackers = AnalyticsTrackers.getInstance();
        analyticsTrackers.enableAutoActivityReports(this);
        analyticsTrackers.get(AnalyticsTrackers.Target.APP);

    }

    private void registrationGcmId(){
        new AsyncTask<Void, Void, String>(){
            private String deviceTokenGcm = "";
            @Override
            protected String doInBackground(Void... params) {
                try {
                    InstanceID instanceID = InstanceID.getInstance(getApplicationContext());
                    deviceTokenGcm = instanceID.getToken(getApplicationContext().getResources().getString(R.string.gcm_defaultSenderId) , GoogleCloudMessaging.INSTANCE_ID_SCOPE , null);
                } catch (IOException e) {
                    Log.d("registrationGcmId", e.getMessage());
                }
                return deviceTokenGcm;
            }

            @Override
            protected void onPostExecute(String deviceTokenGcm) {
                super.onPostExecute(deviceTokenGcm);
                mAppComponent.getTmSharePreferencesUtils().setupDeviceTokenGcm(deviceTokenGcm);
                Log.d("registrationGcmId", deviceTokenGcm);
            }
        }.execute();
    }



    public AppComponent getAppComponent() {
        return mAppComponent;
    }

    public static AppComponent staticGetAppcomponent() {
        return mTMAPPLICATION.mAppComponent;
    }
}
