package app.fpt.ticketmobile.app.lib.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import app.fpt.ticketmobile.app.model.SpinnerObject;
import app.fpt.ticketmobile.app.model.User;
import app.fpt.ticketmobile.app.ui.adapter.spinner.MySpinnerAdapter;
import app.fpt.ticketmobile.R;

/**
 * Created by ToanMaster on 6/28/2016.
 */
public class TmUtils {


    ///////////////////////////////////////////////////////////////////////////////////////
    // set



    ///////////////////////////////////////////////////////////////////////////////////////
    // set Adapter Spinner
    public static void setSpinnerAdapter(Context context, Spinner spinner, List<SpinnerObject> listData){
        MySpinnerAdapter adapter = new MySpinnerAdapter(context, R.layout.textview_spinner, listData);
        spinner.setAdapter(adapter);
    }

    public static void setSpinnerAdapter(Context context, Spinner spinner, List<SpinnerObject> listData, String key) {
        MySpinnerAdapter adapter = new MySpinnerAdapter(context, R.layout.textview_spinner, listData);
        spinner.setAdapter(adapter);
        setSelectionAdapter(spinner, listData, key);
    }

    private static <T> void setSelectionAdapter(Spinner spinner, List<T> listData, String key) {
        if (!key.equalsIgnoreCase("")) {
            for (int i = 0; i < listData.size(); i++) {
                SpinnerObject spinnerObject = (SpinnerObject) listData.get(i);
                if (key.equalsIgnoreCase(spinnerObject.getKey())) {
                    spinner.setSelection(i);
                    break;
                }
            }
        }
    }

    public static void setSpinnerAdapterFollowEmail(Context context, Spinner spinner, List<SpinnerObject> listData, String key) {
        MySpinnerAdapter adapter = new MySpinnerAdapter(context, R.layout.textview_spinner, listData);
        spinner.setAdapter(adapter);
        setSelectionAdapterFollowEmail(spinner, listData, key);
    }

    private static <T> void setSelectionAdapterFollowEmail(Spinner spinner, List<T> listData, String key) {
        if (!key.equalsIgnoreCase("")) {
            for (int i = 0; i < listData.size(); i++) {
                SpinnerObject spinnerObject = (SpinnerObject) listData.get(i);
                if (key.equalsIgnoreCase(spinnerObject.getIsEffect())) {
                    spinner.setSelection(i);
                    break;
                }
            }
        }
    }




    public static String[] parseStringToArray(String value) {
        String[] arr = null;
        arr = value.split(",");
        return arr;
    }

    public static String Base64(String data) {
        String result = "";
        String MD5 = "MD5";

        MessageDigest digest = null;
        try {
            digest = java.security.MessageDigest.getInstance(MD5);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        digest.update(data.getBytes());
        byte messageDigest[] = digest.digest();
        result = android.util.Base64.encodeToString(messageDigest, android.util.Base64.DEFAULT);
        return result.toLowerCase();

    }

    public static String DecodeBase64(String dataHTML) {

        String result = "";
        try {
            byte[] b = android.util.Base64.decode(dataHTML, android.util.Base64.DEFAULT);
            result = new String(b, "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static boolean CompareIssueDateTime(String issueDateTime, String createdDateTime) {
        try {
            long epoch1 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(issueDateTime).getTime() / 1000;
            long epoch2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(createdDateTime).getTime() / 1000;

            if (epoch1 <= epoch2) {
                return true;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }


    public static boolean CompareDateExpectTime(String expectDateTime) {
        try {
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            Date date = new Date();
            System.out.println(dateFormat.format(date)); //2014/08/06 15:59:48f

            long epoch1 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(expectDateTime).getTime() / 1000;
            long epoch2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(dateFormat.format(date)).getTime() / 1000;

            if (epoch1 >= epoch2) {
                return true;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void choiceTimeDiaLog(Context context, TextView textView, TextView createDateTime) {

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Thông báo !");
        if (textView.getId() == R.id.tm_tv_ticket_detail_employee_finished_expect_date) {
            builder.setMessage("Thời gian dự kiến hoàn thành lớn hơn hoặc bằng Thời gian hiện tại. " + dateFormat.format(date));
        } else if (textView.getId() == R.id.tm_tv_ticket_detail_employee_issue_date) {
            builder.setMessage("Thời gian phát sinh sự cố nhỏ hơn hoặc bằng Thời gian tạo ticket. " + createDateTime.getText().toString());
        }

        builder.setPositiveButton("Thoát", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }


    public static String formatTimeDateUpdateSave(String smg) {
        String result = null;
        String oldFormat = "dd/MM/yyyy HH:mm:ss";
        String newFormat = "yyyy-MM-dd HH:mm:ss";

        SimpleDateFormat sdf1 = new SimpleDateFormat(oldFormat);
        SimpleDateFormat sdf2 = new SimpleDateFormat(newFormat);


        try {
            result = sdf2.format(sdf1.parse(smg));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String FormatTimeDateUpdateSave(String smg) {
        String result = null;
        String oldFormat = "dd/MM/yyyy HH:mm:ss";
        String newFormat = "yyyy-MM-dd HH:mm:ss";

        SimpleDateFormat sdf1 = new SimpleDateFormat(oldFormat);
        SimpleDateFormat sdf2 = new SimpleDateFormat(newFormat);


        try {
            result = sdf2.format(sdf1.parse(smg));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static final String MD5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest.getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }


    public static void setupHideKeyBoardParent(final View view, final Activity activity) {
        //Set up touch listener for non-text box views to hide keyboard.
        if(!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(view, activity);
                    return false;
                }
            });
        }
        //If a layout container, iterate over children
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupHideKeyBoardParent(innerView, activity);
            }
        }
    }

    private static void hideSoftKeyboard(View view ,  Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }





}
