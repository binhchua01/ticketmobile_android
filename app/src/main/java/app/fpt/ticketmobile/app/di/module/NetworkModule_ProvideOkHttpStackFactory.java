package app.fpt.ticketmobile.app.di.module;

import app.fpt.ticketmobile.app.networking.utils.OkHttpStack;
import dagger.internal.Factory;

/**
 * Created by ToanMaster on 6/14/2016.
 */
public class NetworkModule_ProvideOkHttpStackFactory implements Factory<OkHttpStack> {
    private static final boolean $assertionDisabled = !NetworkModule_ProvideOkHttpStackFactory.class.desiredAssertionStatus();
    private NetworkModule networkModule;

    public NetworkModule_ProvideOkHttpStackFactory(NetworkModule networkModule){
        if ($assertionDisabled || networkModule != null){
            this.networkModule = networkModule;
            return;
        }
        throw new AssertionError();
    }

    @Override
    public OkHttpStack get() {
        OkHttpStack provideOkHttpStack = networkModule.provideOkHttpStack();
        if (provideOkHttpStack != null) {
            return provideOkHttpStack;
        }
        throw new NullPointerException("Cannot return null from a non-@Nullable @Provides method");
    }

    public static Factory<OkHttpStack> create(NetworkModule networkModule){
        return new NetworkModule_ProvideOkHttpStackFactory(networkModule);
    }
}
