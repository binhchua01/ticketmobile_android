package app.fpt.ticketmobile.app.model.support;

import java.util.List;

import app.fpt.ticketmobile.app.ui.adapter.support.TicketSupportOfMeCreatedHeader;

/**
 * Created by Administrator on 7/25/2016.
 */
public class MyTicketSupportOfUserCreatedObject {

    private List<TicketSupportOfMeCreatedHeader> mTicketSupportOfMeCreatedHeaderList;
    private List<TicketSupportOfUserCreated> Data;

    public List<TicketSupportOfUserCreated> getData() {
        return Data;
    }

    public void setData(List<TicketSupportOfUserCreated> data) {
        Data = data;
    }

    public List<TicketSupportOfMeCreatedHeader> getTicketSupportOfUserCreatedHeaderList() {
        return mTicketSupportOfMeCreatedHeaderList;
    }

    public void setTicketSupportOfUserCreatedHeaderList(List<TicketSupportOfMeCreatedHeader> mTicketSupportOfMeCreatedHeaderList) {
        this.mTicketSupportOfMeCreatedHeaderList = mTicketSupportOfMeCreatedHeaderList;
    }

    public class TicketSupportOfUserCreated{
        String TicketID;
        String Title;
        String Queue;
        String TicketStatus;
        String Priority;
        String UpdateDate;
        String Staff;
        String CreatedDate;
        String EstimatedTime;
        String TimeWorked;

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String createdDate) {
            CreatedDate = createdDate;
        }

        public String getEstimatedTime() {
            return EstimatedTime;
        }

        public void setEstimatedTime(String estimatedTime) {
            EstimatedTime = estimatedTime;
        }

        public String getPriority() {
            return Priority;
        }

        public void setPriority(String priority) {
            Priority = priority;
        }

        public String getQueue() {
            return Queue;
        }

        public void setQueue(String queue) {
            Queue = queue;
        }

        public String getStaff() {
            return Staff;
        }

        public void setStaff(String staff) {
            Staff = staff;
        }

        public String getTicketID() {
            return TicketID;
        }

        public void setTicketID(String ticketID) {
            TicketID = ticketID;
        }

        public String getTicketStatus() {
            return TicketStatus;
        }

        public void setTicketStatus(String ticketStatus) {
            TicketStatus = ticketStatus;
        }

        public String getTimeWorked() {
            return TimeWorked;
        }

        public void setTimeWorked(String timeWorked) {
            TimeWorked = timeWorked;
        }

        public String getTitle() {
            return Title;
        }

        public void setTitle(String title) {
            Title = title;
        }

        public String getUpdateDate() {
            return UpdateDate;
        }

        public void setUpdateDate(String updateDate) {
            UpdateDate = updateDate;
        }
    }
}
