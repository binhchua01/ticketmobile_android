package app.fpt.ticketmobile.app.model;

import java.util.List;

/**
 * Created by Administrator on 6/29/2016.
 */
public class MyTicketStatusObject {
    private List<SpinnerObject> mSpinnerObjectList;
    private List<StatusObject> Data;

    public List<StatusObject> getData() {
        return Data;
    }

    public void setData(List<StatusObject> data) {
        Data = data;
    }

    public List<SpinnerObject> getSpinnerObjectList() {
        return mSpinnerObjectList;
    }

    public void setSpinnerObjectList(List<SpinnerObject> mSpinnerObjectList) {
        this.mSpinnerObjectList = mSpinnerObjectList;
    }

    public class StatusObject{
        private Integer Value;
        private String Description;

        public String getDescription() {
            return Description;
        }

        public void setDescription(String description) {
            Description = description;
        }

        public Integer getValue() {
            return Value;
        }

        public void setValue(Integer value) {
            Value = value;
        }
    }
}
