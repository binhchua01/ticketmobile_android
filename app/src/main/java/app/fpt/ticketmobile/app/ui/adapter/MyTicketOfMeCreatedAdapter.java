package app.fpt.ticketmobile.app.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapter;
import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;

import java.util.List;

import app.fpt.ticketmobile.app.ui.adapter.ticketofusercreated.TicketOfUserCreatedChildViewHolder;
import app.fpt.ticketmobile.app.ui.adapter.ticketofusercreated.TicketOfUserCreatedHeader;
import app.fpt.ticketmobile.app.ui.adapter.ticketofusercreated.TicketOfUserCreatedHeaderViewHolder;
import app.fpt.ticketmobile.R;
import app.fpt.ticketmobile.app.ui.adapter.ticketofusercreated.TicketOfUserCreatedChild;

/**
 * Created by Administrator on 7/1/2016.
 */
public class MyTicketOfMeCreatedAdapter  extends ExpandableRecyclerAdapter<TicketOfUserCreatedHeaderViewHolder, TicketOfUserCreatedChildViewHolder> {

    private LayoutInflater mInflater;
    private List<? extends ParentListItem> parentListItems;

    public MyTicketOfMeCreatedAdapter(Context context, @NonNull List<? extends ParentListItem> parentItemList) {
        super(parentItemList);
        parentListItems = parentItemList;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public TicketOfUserCreatedHeaderViewHolder onCreateParentViewHolder(ViewGroup parentViewGroup) {
        View headerView = mInflater.inflate(R.layout.entry_ticket_of_me_created_header, parentViewGroup, false);
        return new TicketOfUserCreatedHeaderViewHolder(headerView);
    }

    @Override
    public TicketOfUserCreatedChildViewHolder onCreateChildViewHolder(ViewGroup childViewGroup) {
        View childView = mInflater.inflate(R.layout.entry_ticket_of_me_created_child, childViewGroup, false);
        return new TicketOfUserCreatedChildViewHolder(childView);
    }

    @Override
    public void onBindParentViewHolder(TicketOfUserCreatedHeaderViewHolder parentViewHolder, int position, ParentListItem parentListItem) {
        TicketOfUserCreatedHeader ticketOfUserCreatedHeader = (TicketOfUserCreatedHeader) parentListItem;
        parentViewHolder.bind(ticketOfUserCreatedHeader);
    }

    @Override
    public void onBindChildViewHolder(TicketOfUserCreatedChildViewHolder childViewHolder, int position, Object childListItem) {
        TicketOfUserCreatedChild ticketOfUserCreatedChild = (TicketOfUserCreatedChild) childListItem;
        childViewHolder.bind(ticketOfUserCreatedChild);
    }

    public void clear() {
        parentListItems.clear();
        notifyDataSetChanged();
    }

    // Add a list of items
    public void addAll(List<? extends  ParentListItem> list) {
        parentListItems = list;
        notifyDataSetChanged();
    }
}