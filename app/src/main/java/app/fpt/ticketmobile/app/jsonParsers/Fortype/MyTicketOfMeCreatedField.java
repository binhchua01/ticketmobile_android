package app.fpt.ticketmobile.app.jsonParsers.Fortype;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import app.fpt.ticketmobile.app.model.MyTicketOfUserCreatedObject;
import app.fpt.ticketmobile.app.ui.adapter.ticketofusercreated.TicketOfUserCreatedChild;
import app.fpt.ticketmobile.app.ui.adapter.ticketofusercreated.TicketOfUserCreatedHeader;

/**
 * Created by Administrator on 7/1/2016.
 */
public class MyTicketOfMeCreatedField {

    public static final class Deserializer implements JsonDeserializer<MyTicketOfUserCreatedObject>{

        @Override
        public MyTicketOfUserCreatedObject deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

            Gson gson = new Gson();
            MyTicketOfUserCreatedObject myTicketOfUserCreatedObject = null;
            List<TicketOfUserCreatedHeader> mTicketOfUserCreatedHeaderList = new ArrayList<>();

            try{

                JsonObject jsObj = json.getAsJsonObject();
                JsonObject jsResult = jsObj.getAsJsonObject("Result");
                myTicketOfUserCreatedObject = gson.fromJson(jsResult, MyTicketOfUserCreatedObject.class);
                if (myTicketOfUserCreatedObject != null && myTicketOfUserCreatedObject.getData().size() > 0) {
                    for (int i = 0; i < myTicketOfUserCreatedObject.getData().size(); i++) {
                        boolean mCheck = true;
                        MyTicketOfUserCreatedObject.TicketOfUserCreated ticketOfUserCreated = myTicketOfUserCreatedObject.getData().get(i);
                        TicketOfUserCreatedChild ticketOfUserCreatedChild = new TicketOfUserCreatedChild(ticketOfUserCreated);
                        if (mTicketOfUserCreatedHeaderList.size() > 0) {
                            for (int j = 0; j < mTicketOfUserCreatedHeaderList.size(); j++) {
                                if (mTicketOfUserCreatedHeaderList.get(j).getQueueName().contains(ticketOfUserCreated.getQueue())) {
                                    mCheck = false;
                                    mTicketOfUserCreatedHeaderList.get(j).getTicketOfUserCreatedChildList().add(ticketOfUserCreatedChild);
                                    break;
                                }
                            }

                            if (mCheck) {
                                List<TicketOfUserCreatedChild> mTicketOfUserCreatedChildList = new ArrayList<>();
                                mTicketOfUserCreatedChildList.add(ticketOfUserCreatedChild);
                                mTicketOfUserCreatedHeaderList.add(new TicketOfUserCreatedHeader(ticketOfUserCreated.getQueue(), mTicketOfUserCreatedChildList));
                            }
                        } else {
                            List<TicketOfUserCreatedChild> mTicketOfUserCreatedChildList = new ArrayList<>();
                            mTicketOfUserCreatedChildList.add(ticketOfUserCreatedChild);
                            mTicketOfUserCreatedHeaderList.add(new TicketOfUserCreatedHeader(ticketOfUserCreated.getQueue(), mTicketOfUserCreatedChildList));
                        }
                    }

                    myTicketOfUserCreatedObject.setTicketOfMeCreatedHeaderList(mTicketOfUserCreatedHeaderList);
                }

            }catch(Exception e){
                Log.e("tmt", "MyTicketInventoryNotAssignField : Deserializer " + e.toString());
            }
            return myTicketOfUserCreatedObject;
        }
    }
}
