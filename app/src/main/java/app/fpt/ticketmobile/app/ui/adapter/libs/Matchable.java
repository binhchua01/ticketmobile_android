package app.fpt.ticketmobile.app.ui.adapter.libs;

import java.util.regex.Pattern;

/**
 * Created by Administrator on 7/12/2016.
 */
public abstract interface Matchable {
    public abstract boolean matches(Pattern pattern);
}
