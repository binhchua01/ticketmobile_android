package app.fpt.ticketmobile.app.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import app.fpt.ticketmobile.app.eventBus.event.DashbroadChangeItemBackground;
import app.fpt.ticketmobile.app.lib.Provider;
import app.fpt.ticketmobile.app.lib.TmGraphics;
import app.fpt.ticketmobile.app.model.User;
import app.fpt.ticketmobile.app.ui.adapter.libs.AdapterItem;
import app.fpt.ticketmobile.app.ui.adapter.libs.DashGroupSettingInfo;
import app.fpt.ticketmobile.app.ui.adapter.libs.GroupTicketPrimaryFunctionInfo;
import app.fpt.ticketmobile.app.ui.adapter.libs.GroupTicketSupportInfo;
import app.fpt.ticketmobile.app.ui.adapter.libs.adapterDelegates.DashGroupTicketSupportAdapter;
import app.fpt.ticketmobile.app.ui.adapter.libs.adapterDelegates.DashboardAccountAdapter;
import app.fpt.ticketmobile.app.ui.adapter.libs.adapterDelegates.DashboardGroupTicketPrimaryFunctionAdapter;
import app.fpt.ticketmobile.app.ui.adapter.libs.adapterDelegates.DashboardSectionHeadersAdapterDelegate;
import app.fpt.ticketmobile.app.ui.adapter.libs.adapterDelegates.DashboardSettingAdapterDelegate;
import app.fpt.ticketmobile.app.ui.lib.TicketPrimaryFunction;
import app.fpt.ticketmobile.app.ui.lib.TicketSettingInfo;
import app.fpt.ticketmobile.app.ui.lib.TicketSupport;
import app.fpt.ticketmobile.R;
import app.fpt.ticketmobile.app.core.TmApplication;

/**
 * Created by Administrator on 7/12/2016.
 */
public class HomeDashboardAdapter extends TmAiAdmRecyclerViewAdapter<AdapterItem<?>, Object, RecyclerView.ViewHolder>
        implements DashboardGroupTicketPrimaryFunctionAdapter.TicketPrimaryFunctionCallback, DashGroupTicketSupportAdapter.TicketSupportCallback, DashboardAccountAdapter.AccountCallback, DashboardSettingAdapterDelegate.TicketSettingCallback {

    private static final int SECTION_ACCOUNT_HEADER_VIER = assignUniqueViewType();
    private static final int SECTION_HEADER_VIEW = assignUniqueViewType();
    private static final int GROUP_TICKET_PRIMARY_FUNCTION = assignUniqueViewType();
    private static final int GROUP_TICKET_SUPPORT = assignUniqueViewType();
    private static final int GROUP_TICKET_SETTING  = assignUniqueViewType();

    private TicketPrimaryFunction mSelectedTicketPrimaryFunction;
    private TicketSupport mSelectedTicketSupport;
    private TicketSettingInfo mSelectedTicketSetting;
    private HomeDashboardCallback mCallback;
    private List<AdapterItem<?>> mAdapterItemList;




    public interface HomeDashboardCallback extends TmRecycleViewAdapter.Callback {
        void onSelectionTicketPrimaryFunction(TicketPrimaryFunction ticketPrimaryFunction, String title);

        void onSelectionTicketSupport(TicketSupport ticketSupport, String title);

        void onSelectionTicketSetting(TicketSettingInfo ticketSettingInfo, String title);

        void onSelectionAccount(User user);
    }

    public HomeDashboardAdapter(Context context, TmGraphics tmGraphics, TicketPrimaryFunction selectedTicketPrimaryFunction, HomeDashboardCallback callback) {
        super(context, callback);
        this.mCallback = callback;
        this.mSelectedTicketPrimaryFunction = selectedTicketPrimaryFunction;
        this.mAdapterItemList = new ArrayList<>();
        this.mAdapterItemList.add(new AdapterItem(SECTION_ACCOUNT_HEADER_VIER, TmApplication.staticGetAppcomponent().getTmSharePreferencesUtils().getUserLogin(), User.class, null));
        this.mAdapterItemList.add(new AdapterItem(SECTION_HEADER_VIEW, context.getString(R.string.tm_ticket_primary_function_group), String.class, null));
        this.mAdapterItemList.add(new AdapterItem(GROUP_TICKET_PRIMARY_FUNCTION, new GroupTicketPrimaryFunctionInfo(TicketPrimaryFunction.TICKET_INVENTORY_NOT_ASSIGN,
                context.getString(R.string.tm_ticket_not_yet_assigned)), GroupTicketPrimaryFunctionInfo.class, null));
        this.mAdapterItemList.add(new AdapterItem(GROUP_TICKET_PRIMARY_FUNCTION, new GroupTicketPrimaryFunctionInfo(TicketPrimaryFunction.TICKET_INVENTORY_UNFINISHED,
                context.getString(R.string.tm_ticket_backoff_unfinished)), GroupTicketPrimaryFunctionInfo.class, null));
        this.mAdapterItemList.add(new AdapterItem(GROUP_TICKET_PRIMARY_FUNCTION, new GroupTicketPrimaryFunctionInfo(TicketPrimaryFunction.TICKET_OF_USER,
                context.getString(R.string.tm_ticket_of_me)), GroupTicketPrimaryFunctionInfo.class, null));
        this.mAdapterItemList.add(new AdapterItem(GROUP_TICKET_PRIMARY_FUNCTION, new GroupTicketPrimaryFunctionInfo(TicketPrimaryFunction.TICKET_OF_USER_CREATE,
                context.getString(R.string.tm_ticket_of_me_created)), GroupTicketPrimaryFunctionInfo.class, null));

        this.mAdapterItemList.add(new AdapterItem(SECTION_HEADER_VIEW, context.getString(R.string.tm_ticket_support), String.class, null));
        this.mAdapterItemList.add(new AdapterItem(GROUP_TICKET_SUPPORT, new GroupTicketSupportInfo(TicketSupport.TICKET_SUPPORT_OF_ME, context.getString(R.string.tm_ticket_support_of_me)), GroupTicketSupportInfo.class, null));
        this.mAdapterItemList.add(new AdapterItem(GROUP_TICKET_SUPPORT, new GroupTicketSupportInfo(TicketSupport.TICKET_SUPPORT_CREATE, context.getString(R.string.tm_ticket_support_created)), GroupTicketSupportInfo.class, null));
        this.mAdapterItemList.add(new AdapterItem(SECTION_HEADER_VIEW, context.getString(R.string.tm_setting), String.class, null));
       // this.mAdapterItemList.add(new AdapterItem(GROUP_TICKET_SETTING, new DashGroupSettingInfo(TicketSettingInfo.TICKET_SUPPORT_VERSION, context.getString(R.string.tm_ticket_setting_version)), DashGroupSettingInfo.class, null));
        this.mAdapterItemList.add(new AdapterItem(GROUP_TICKET_SETTING, new DashGroupSettingInfo(TicketSettingInfo.TICKET_SUPPORT_EXIT, context.getString(R.string.tm_ticket_setting_exit)), DashGroupSettingInfo.class, null));

        addDelegate(new DashboardAccountAdapter(SECTION_ACCOUNT_HEADER_VIER, this));
        addDelegate(new DashboardSectionHeadersAdapterDelegate(SECTION_HEADER_VIEW));
        addDelegate(new DashboardGroupTicketPrimaryFunctionAdapter(GROUP_TICKET_PRIMARY_FUNCTION, tmGraphics, new SelectedGroupTicketPrimaryFunctionProvider(), this));
        addDelegate(new DashGroupTicketSupportAdapter(GROUP_TICKET_SUPPORT, tmGraphics, new SelectedGroupTicketSupportProvider(), this));
        addDelegate(new DashboardSettingAdapterDelegate(GROUP_TICKET_SETTING, tmGraphics, new SelectedGroupSettingProvider(), this));
    }

    public void onSetChangeMenuItemSupport(DashbroadChangeItemBackground changeItemBackground){
        onTicketSupportSelected(changeItemBackground.getTicketSupport(), changeItemBackground.getTitle());
    }

    @Override
    public int onGetItemCount() {
        return mAdapterItemList.size();
    }

    @Override
    public AdapterItem<?> onGetAdapterItem(int position) {
        return mAdapterItemList.get(position);
    }


    @Override
    public void onGroupTicketPrimaryFunction(TicketPrimaryFunction ticketPrimaryFunction, String title) {
        notifyDataSetChanged();
        mSelectedTicketPrimaryFunction = ticketPrimaryFunction;
        mSelectedTicketSupport = null;
        mSelectedTicketSetting = null;
        mCallback.onSelectionTicketPrimaryFunction(ticketPrimaryFunction, title);
    }

    @Override
    public void onTicketSupportSelected(TicketSupport ticketSupport, String title) {
        notifyDataSetChanged();
        mSelectedTicketPrimaryFunction = null;
        mSelectedTicketSetting = null;
        mSelectedTicketSupport = ticketSupport;

        mCallback.onSelectionTicketSupport(ticketSupport, title);

    }

    @Override
    public void onTicketSettingSelected(TicketSettingInfo ticketSettingInfo, String title) {
        notifyDataSetChanged();
        mSelectedTicketPrimaryFunction = null;
        mSelectedTicketSupport = null;
        mSelectedTicketSetting = ticketSettingInfo;
        mCallback.onSelectionTicketSetting(ticketSettingInfo, title);
    }

    private class SelectedGroupTicketPrimaryFunctionProvider implements Provider<TicketPrimaryFunction> {

        @Override
        public TicketPrimaryFunction provider() {
            return mSelectedTicketPrimaryFunction;
        }
    }

    private class SelectedGroupTicketSupportProvider implements Provider<TicketSupport> {

        @Override
        public TicketSupport provider() {
            return mSelectedTicketSupport;
        }
    }

    private class SelectedGroupSettingProvider implements Provider<TicketSettingInfo>{

        @Override
        public TicketSettingInfo provider() {
            return mSelectedTicketSetting;
        }
    }

    @Override
    public void onAccountSelected(User user) {
        mCallback.onSelectionAccount(user);
    }

}
