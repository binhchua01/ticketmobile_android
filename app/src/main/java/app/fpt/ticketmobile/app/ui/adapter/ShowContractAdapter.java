package app.fpt.ticketmobile.app.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import app.fpt.ticketmobile.R;
import app.fpt.ticketmobile.app.model.support.ImportContractObject;

/**
 * Created by Administrator on 7/28/2016.
 */
public class ShowContractAdapter extends RecyclerView.Adapter<ShowContractAdapter.ShowContractViewHolder> {

    private List<ImportContractObject> mImportContractObjectList = null;

    public ShowContractAdapter (List<ImportContractObject> importContractObjectList){
        mImportContractObjectList = new ArrayList<>(importContractObjectList);
    }

    @Override
    public ShowContractViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.entry_show_contract_item, parent, false);
        return new ShowContractViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ShowContractViewHolder holder, int position) {
        ImportContractObject contractObject = mImportContractObjectList.get(position);
        holder.onBind(contractObject);
    }

    @Override
    public int getItemCount() {
        return mImportContractObjectList.size();
    }

    public class ShowContractViewHolder extends RecyclerView.ViewHolder{

        private TextView mTvContract;
        private TextView mTvContractName;
        private TextView mTvIpDomain;
        private TextView mTvAddress;

        public ShowContractViewHolder(View parent) {
            super(parent);
            mTvContract = (TextView) parent.findViewById(R.id.tm_tv_dialog_contracts);
            mTvContractName = (TextView) parent.findViewById(R.id.tm_tv_dialog_contracts_name);
            mTvIpDomain = (TextView) parent.findViewById(R.id.tm_tv_dialog_contracts_ip_domain);
            mTvAddress = (TextView) parent.findViewById(R.id.tm_tv_dialog_contracts_address);
        }

        public void onBind(ImportContractObject importContractObject){
            mTvContract.setText(importContractObject.getContract());
            mTvContractName.setText(importContractObject.getContractName());
            mTvIpDomain.setText(importContractObject.getIpDomain().trim());
            mTvAddress.setText(importContractObject.getAddress());
        }
    }
}
