package app.fpt.ticketmobile.widget.views.libs;

import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

/**
 * Created by Administrator on 7/13/2016.
 */
public class ShowImeRunnable implements Runnable {

    private final View mView;

    public ShowImeRunnable(View view) {
        this.mView = view;
    }

    @Override
    public void run() {
        if (this.mView.requestFocus()) {
            InputMethodManager imm = (InputMethodManager) this.mView.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.showSoftInput(this.mView, 0);
            }
        }
    }

    public View getView() {
        return this.mView;
    }

}