package app.fpt.ticketmobile.widget.views;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Html;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import app.fpt.ticketmobile.app.model.SpinnerObject;
import app.fpt.ticketmobile.app.ui.activities.BaseActivity;
import app.fpt.ticketmobile.app.ui.adapter.spinner.MySpinnerAdapter;
import app.fpt.ticketmobile.R;
import app.fpt.ticketmobile.widget.views.libs.SpinnerCallbackInterface;

/**
 * Created by ToanMaster on 6/27/2016.
 */
public class MultiSelectionObjectSpinner extends Spinner {

    private SpinnerUpdateInterfaceCallback mInterfaceUpdatedCallback;
    private SpinnerCallbackInterface callback;
    boolean flagServiceType;
    String titleDialog = "";
    String[] _items = null;
    boolean[] mSelection = null;
    boolean[] mSelectionOld = null;
    int iCheck = -1;
    MySpinnerAdapter simple_adapter = null;

    ArrayList<SpinnerObject> listItems = new ArrayList<>();
    private List<SpinnerObject> mListItemOriginalDisplay = null;
    private String strKeyChoice = "";
    private SpinnerObject mSpinnerObjectDefault = null;
    private boolean mIsShowTitle = false;
    private AlertDialog mAlertDialog;
    private boolean mIsCheckAll = false;
    private CheckBox checkBox = null;
    private Button mButtonChoose = null;
    private Button mButtonCancel = null;

    public static SpinnerCheckBoxState SPINNER_CHECK_BOX_STATE = SpinnerCheckBoxState.CHECK_ALL;
    public static boolean isState = true;

    public interface SpinnerUpdateInterfaceCallback {
        void onMultiChooseCallback(String locationKey);
    }

    public enum SpinnerCheckBoxState {
        CHECK_ALL,
        CHECK_LIMP,
        UN_CHECK
    }

    public CheckBox getCheckBoxAll() {
        return this.checkBox;
    }
    public void getCheckBoxAll(CheckBox checkBox) {
        this.checkBox = checkBox;
    }
    public MultiSelectionObjectSpinner(Context context) {
        super(context);
    }

    public MultiSelectionObjectSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setSpinnerMultiAdapter(SpinnerObject mSpinnerObjectOriginalDisplay, List<SpinnerObject> listItem, boolean isCustomTitle) {
        mListItemOriginalDisplay = new ArrayList<>();
        mIsShowTitle = isCustomTitle;
        try {
            simple_adapter = new MySpinnerAdapter(getContext(), R.layout.textview_spinner, mListItemOriginalDisplay);
            simple_adapter.setDropDownViewResource(R.layout.simple_spinner_item);
            simple_adapter.add(mSpinnerObjectOriginalDisplay);
            mSpinnerObjectDefault = mSpinnerObjectOriginalDisplay;
            setItems(listItem);
            this.mListItemOriginalDisplay = new ArrayList<>(mListItemOriginalDisplay);
            this.listItems = new ArrayList<>(listItem);
            super.setAdapter(simple_adapter);

        } catch (Exception e) {
            Log.e("tmt", "setSpinnerMultiAdapter " + e.getMessage());
        }
    }


    public void setSpinnerMultiAdapter(List<SpinnerObject> listItem) {
        try {
            simple_adapter = new MySpinnerAdapter(getContext(), R.layout.textview_spinner, listItem);
            simple_adapter.setDropDownViewResource(R.layout.simple_spinner_item);
            setItems(listItem);
            this.listItems = new ArrayList<>(listItem);
            super.setAdapter(simple_adapter);

        } catch (Exception e) {
            Log.e("tmt", "setSpinnerMultiAdapter " + e.getMessage());
        }
    }

    public void setSpinnerTitleDialog(String title) {
        this.titleDialog = title;
    }


    public void setCheckBox() {
        if (checkBox != null) {
            checkBox.setChecked(false);
            Arrays.fill(mSelection, false);
            SPINNER_CHECK_BOX_STATE = SpinnerCheckBoxState.CHECK_ALL;
            for (int i = 0; i < _items.length; i++) {
                mAlertDialog.getListView().setItemChecked(i, mSelection[i]);
            }
        }
    }

    @Override
    public boolean performClick() {
        try {
//            AlertDialog.Builder builder = new AlertDialog.Builder(getContext(), R.style.TmAppAlertDialogTheme);
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            if (mIsShowTitle) {
                View view = setViewTitleDialog(titleDialog, builder);
                builder.setCustomTitle(view);

            } else {
                builder.setTitle(Html.fromHtml("<font color='#00ccff'>" + titleDialog + "</font>"));

            }
            mSelectionOld = mSelection.clone();
            builder.setMultiChoiceItems(_items, mSelection, new PrivateMultiChoice());
            LinearLayout layout = new LinearLayout(getContext());
            LinearLayout.LayoutParams parms = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            layout.setOrientation(LinearLayout.HORIZONTAL);
            layout.setLayoutParams(parms);
            layout.setPadding(10, 10, 10, 10);
            mButtonChoose = new Button(getContext());
            mButtonChoose.setGravity(Gravity.CENTER);
            mButtonChoose.setText("CHỌN");
            mButtonChoose.setTextSize(15);
            mButtonCancel = new Button(getContext());
            mButtonCancel.setGravity(Gravity.CENTER);
            mButtonCancel.setTextSize(15);
            mButtonCancel.setText("HỦY");
            mButtonChoose.setBackgroundColor(getContext().getResources().getColor(R.color.tm_button_grey));
            mButtonCancel.setBackgroundColor(getContext().getResources().getColor(R.color.tm_button_grey));
            LinearLayout.LayoutParams buttonLayoutParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            buttonLayoutParams.setMargins(0, 0, 10, 0);
            layout.addView(mButtonCancel, buttonLayoutParams);
            layout.addView(mButtonChoose, buttonLayoutParams);
            builder.setView(layout);
            mAlertDialog = builder.create();
            mButtonChoose.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    simple_adapter.clear();
                    simple_adapter.add(buildSelectedItemObject());
                    mSelectionOld = mSelection.clone();
                    if (callback != null && flagServiceType) {
                        callback.UpdateCusType(strKeyChoice);
                    }
                    mAlertDialog.dismiss();

                }
            });
            mButtonCancel.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    mSelection = mSelectionOld.clone();
                    mAlertDialog.cancel();
                }
            });
            mAlertDialog.show();
        } catch (Exception e) {
        }

        return true;
    }


    @Override
    public void setAdapter(SpinnerAdapter adapter) {
        throw new RuntimeException("setAdapter is not supported by MultiSelectSpinner.");
    }

    public void setItems(List<SpinnerObject> items) {
        BindingListItemDialog(items);
        mSelection = new boolean[_items.length];
        Arrays.fill(mSelection, false);
    }

    public void setSelectionNew(String[] selection) {
        StringBuilder sb = new StringBuilder();
        boolean foundOne = false;
        for (String cell : selection) {
            for (int j = 0; j < this.listItems.size(); ++j) {
                if (listItems.get(j).getKey().equals(cell)) {
                    mSelection[j] = true;
                    if (foundOne) {
                        sb.append(",");
                    }
                    foundOne = true;
                    sb.append(listItems.get(j).getValue());
                }
            }
        }
        SpinnerObject obj = new SpinnerObject("choose", sb.toString());
        simple_adapter.clear();
        simple_adapter.add(obj);

    }

    private SpinnerObject buildSelectedItemObject() {
        //strKeyChoice = null;
        StringBuilder sbKey = new StringBuilder();
        StringBuilder sbValue = new StringBuilder();
        boolean foundOne = false;
        for (int i = 0; i < this.listItems.size(); ++i) {
            if (mSelection[i]) {
                if (foundOne) {
                    sbKey.append(",");
                    sbValue.append(",");
                }
                foundOne = true;
                SpinnerObject objTmp = this.listItems.get(i);

                sbKey.append(objTmp.getKey());
                sbValue.append(objTmp.getValue());
            }
        }
        strKeyChoice = sbKey.toString();

        if (mInterfaceUpdatedCallback != null) {
            mInterfaceUpdatedCallback.onMultiChooseCallback(strKeyChoice);
        }

        if (!foundOne && strKeyChoice.equalsIgnoreCase("")) {
            return mSpinnerObjectDefault;
        }


        SpinnerObject obj = new SpinnerObject("choose", sbValue.toString());
        return obj;
    }

    public String getSelectedItemsAsString() {
        StringBuilder sb = new StringBuilder();
        boolean foundOne = false;

        for (int i = 0; i < _items.length; ++i) {
            if (mSelection[i]) {
                if (foundOne) {
                    sb.append(",");
                }
                foundOne = true;
                sb.append(_items[i]);
            }
        }
        return sb.toString();
    }

    public String getSelectedKeyItemsAsString() {
        StringBuilder sb = new StringBuilder();
        boolean foundOne = false;

        for (int i = 0; i < this.listItems.size(); ++i) {
            if (mSelection[i]) {
                if (foundOne) {
                    sb.append(",");
                }
                foundOne = true;
                sb.append(this.listItems.get(i).getKey());
            }
        }
        return sb.toString();
    }

    private void BindingListItemDialog(List<SpinnerObject> items) {
        _items = new String[items.size()];
        for (int i = 0; i < items.size(); i++) {
            _items[i] = items.get(i).getValue();
        }
    }

    public String GetStringKeyChoice() {
        return strKeyChoice;
    }

    public void SetStringKeyChoice(String key) {
        strKeyChoice = key;
    }

    public void setCallback(SpinnerCallbackInterface callback, boolean flagServiceType) {
        this.callback = callback;
        this.flagServiceType = flagServiceType;
    }

    public void setSpinnerUpdateCallback(SpinnerUpdateInterfaceCallback callback) {
        this.mInterfaceUpdatedCallback = callback;
    }


    private View setViewTitleDialog(String titleDialog, final AlertDialog.Builder builder) {
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        View parent = layoutInflater.inflate(R.layout.entry_ticket_dialog_title, null, false);
        TextView textView = (TextView) parent.findViewById(R.id.tm_tv_dialog_title);
        checkBox = (CheckBox) parent.findViewById(R.id.tm_cb_dialog_title);
        textView.setText(titleDialog);
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                boolean checked = false;
                if (isChecked) {
                    if (SPINNER_CHECK_BOX_STATE == SpinnerCheckBoxState.CHECK_ALL) {
                        Arrays.fill(mSelection, true);
                        mIsCheckAll = true;
                    } else if (SPINNER_CHECK_BOX_STATE == SpinnerCheckBoxState.CHECK_LIMP) {
                    }
                } else {
                    SPINNER_CHECK_BOX_STATE = SpinnerCheckBoxState.CHECK_ALL;
                    //đánh dấu đang chọn hoặc không chọn checkbox chọn tất cả.
                    if (iCheck == -1)
                        Arrays.fill(mSelection, false);
                    mIsCheckAll = false;
                }
                iCheck = -1;
                for (int i = 0; i < _items.length; i++) {
                    if (mSelection[i]) {
                        checked = true;
                    }
                    mAlertDialog.getListView().setItemChecked(i, mSelection[i]);
                }
                if (checked) {
                    mButtonChoose.setBackgroundColor(getContext().getResources().getColor(R.color.tm_blue_color));
                    mButtonCancel.setBackgroundColor(getContext().getResources().getColor(R.color.tm_blue_color));
                } else {
                    mButtonChoose.setBackgroundColor(getContext().getResources().getColor(R.color.tm_button_grey));
                    mButtonCancel.setBackgroundColor(getContext().getResources().getColor(R.color.tm_button_grey));
                }
            }
        });

        checkBox.setChecked(mIsCheckAll);

        return parent;
    }


    private void checkStateCheckBox() {
        if (SPINNER_CHECK_BOX_STATE != null) {

            int count = 0;
            for (int i = 0; i < mSelection.length; i++) {
                if (mSelection[i] == true) {
                    count++;
                }
            }
            // thay đổi màu sắc 2 button khi có chọn 1 checkbox
            if (count > 0) {
                mButtonChoose.setBackgroundColor(getContext().getResources().getColor(R.color.tm_blue_color));
                mButtonCancel.setBackgroundColor(getContext().getResources().getColor(R.color.tm_blue_color));
            } else {
                mButtonChoose.setBackgroundColor(getContext().getResources().getColor(R.color.tm_button_grey));
                mButtonCancel.setBackgroundColor(getContext().getResources().getColor(R.color.tm_button_grey));
            }
            // nếu có chọn 1 checkbox thì ko check chọn tất cả.
            if (count < mSelection.length) {
                if (checkBox != null) {
                    checkBox.setChecked(false);
                }
            } else if (count == mSelection.length) {
                if (checkBox != null) {
                    checkBox.setChecked(true);
                }

            } else {
                if (checkBox != null) {
                    if (checkBox.isChecked()) {
                        SPINNER_CHECK_BOX_STATE = SpinnerCheckBoxState.CHECK_LIMP;
                        isState = true;
                    } else {
                        SPINNER_CHECK_BOX_STATE = SpinnerCheckBoxState.CHECK_ALL;
                    }
                }
            }


        }
    }

    private final class PrivateMultiChoice implements DialogInterface.OnMultiChoiceClickListener {

        @Override
        public void onClick(DialogInterface dialog, int which, boolean isChecked) {
            // đánh dấu đang chọn 1 trong các checkbox con.
            iCheck = which;
            if (mSelection != null && which < mSelection.length && which > -1) {
                mSelection[which] = isChecked;
            } else {
                throw new IllegalArgumentException("Argument 'which' is out of bounds.");
            }
            checkStateCheckBox();
        }
    }

}
