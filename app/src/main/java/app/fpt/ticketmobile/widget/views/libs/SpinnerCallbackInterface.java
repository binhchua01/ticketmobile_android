package app.fpt.ticketmobile.widget.views.libs;

/**
 * Created by ToanMaster on 6/27/2016.
 */
public interface SpinnerCallbackInterface {

    public void UpdateDeviceStatus(String deviceName , String status);
    public void UpdateCusType(String serviceType );
}
