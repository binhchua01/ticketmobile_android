package app.fpt.ticketmobile.widget.decorators;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import app.fpt.ticketmobile.R;



public class DividerItemDecoration extends RecyclerView.ItemDecoration {
    private static final int[] ATTRS;
    private static final int UNLIMITED = -1;
    private Callback mCallback;
    private int mConfigContainerId;
    private ContainerType mContainerType;
    private Drawable mDividerDrawable;
    private int mHorizontalDividerMaxLength;
    private boolean mIncludeMargins;
    private boolean mUsingCustomDivider;
    private int mVerticalDividerMaxLength;

    public interface Callback {
        boolean shouldDecorateItem(int i);
    }


    static /* synthetic */ class C09431 {
        static final /* synthetic */ int[] f55xb4d32668;

        static {
            f55xb4d32668 = new int[ContainerType.values().length];
            try {
                f55xb4d32668[ContainerType.VERTICAL_LIST.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f55xb4d32668[ContainerType.HORIZONTAL_LIST.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                f55xb4d32668[ContainerType.GRID.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
        }
    }

    private static class ConfigContainer {
        private final HorizontalInsets mHorizontalInsets;
        private boolean mShouldDecorateItem;
        private final VerticalInsets mVerticalInsets;

        public ConfigContainer() {
            this.mShouldDecorateItem = false;
            this.mHorizontalInsets = new HorizontalInsets();
            this.mVerticalInsets = new VerticalInsets();
        }

        public boolean shouldDecorateItem() {
            return this.mShouldDecorateItem;
        }

        public void setShouldDecorateItem(boolean shouldDecorateItem) {
            this.mShouldDecorateItem = shouldDecorateItem;
        }

        public HorizontalInsets getHorizontalInsets() {
            return this.mHorizontalInsets;
        }

        public VerticalInsets getVerticalInsets() {
            return this.mVerticalInsets;
        }
    }

    public enum ContainerType {
        VERTICAL_LIST,
        HORIZONTAL_LIST,
        GRID
    }

    public static class HorizontalInsets {
        private int mLeft;
        private int mRight;

        public void setInsets(int left, int right) {
            this.mLeft = left;
            this.mRight = right;
        }

        public void setEmpty() {
            setInsets(0, 0);
        }
    }

    public static class VerticalInsets {
        private int mBottom;
        private int mTop;

        public void setInsets(int top, int bottom) {
            this.mTop = top;
            this.mBottom = bottom;
        }

        public void setEmpty() {
            setInsets(0, 0);
        }
    }

    static {
        ATTRS = new int[]{16843284};
    }

    public DividerItemDecoration(Context context, ContainerType containerType) {
        this.mUsingCustomDivider = false;
        this.mIncludeMargins = false;
        this.mHorizontalDividerMaxLength = UNLIMITED;
        this.mVerticalDividerMaxLength = UNLIMITED;
        this.mConfigContainerId = R.id.divider_item_decoration_config_container;
        this.mContainerType = containerType;
        sharedConstruction(context, containerType);
    }

    public DividerItemDecoration(Context context, ContainerType containerType, int color) {
        this.mUsingCustomDivider = false;
        this.mIncludeMargins = false;
        this.mHorizontalDividerMaxLength = UNLIMITED;
        this.mVerticalDividerMaxLength = UNLIMITED;
        this.mConfigContainerId = R.id.divider_item_decoration_config_container;
        this.mContainerType = containerType;
        sharedConstruction(context, containerType);
        setColor(color);
    }

    private void sharedConstruction(Context context, ContainerType containerType) {
        TypedArray a = context.obtainStyledAttributes(ATTRS);
        this.mDividerDrawable = a.getDrawable(0);
        a.recycle();
        setContainerType(containerType);
        this.mUsingCustomDivider = false;
    }

    public void setCallback(Callback callback) {
        this.mCallback = callback;
    }

    public void setIncludeMargins(boolean includeMargins) {
        this.mIncludeMargins = includeMargins;
    }

    public void setHorizontalDividerMaxLength(int horizontalDividerMaxLength) {
        this.mHorizontalDividerMaxLength = horizontalDividerMaxLength;
    }

    public void setVerticalDividerMaxLength(int verticalDividerMaxLength) {
        this.mVerticalDividerMaxLength = verticalDividerMaxLength;
    }

    public void setConfigContainerId(int idRes) {
        this.mConfigContainerId = idRes;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        if (this.mDividerDrawable != null) {
            boolean z;
            int position = parent.getChildAdapterPosition(view);
            ConfigContainer configContainer = getConfigContainer(view);
            if (this.mCallback == null || this.mCallback.shouldDecorateItem(position)) {
                z = true;
            } else {
                z = false;
            }
            configContainer.setShouldDecorateItem(z);
            if (configContainer.shouldDecorateItem()) {
                if (shouldOffsetItem(view, parent)) {
                    switch (C09431.f55xb4d32668[this.mContainerType.ordinal()]) {
                        case 1 /*1*/:
                            outRect.set(0, 0, 0, this.mDividerDrawable.getIntrinsicHeight());
                            break;
                        case 2 /*2*/:
                            outRect.set(0, 0, this.mDividerDrawable.getIntrinsicWidth(), 0);
                            break;
                        case 3/*3*/:
                            outRect.set(0, 0, this.mDividerDrawable.getIntrinsicWidth(), this.mDividerDrawable.getIntrinsicHeight());
                            break;
                    }
                }
                boolean getHorizontalInsets = false;
                boolean getVerticalInsets = false;
                switch (C09431.f55xb4d32668[this.mContainerType.ordinal()]) {
                    case 1 /*1*/:
                        getHorizontalInsets = true;
                        break;
                    case 2 /*2*/:
                        getVerticalInsets = true;
                        break;
                    case 3 /*3*/:
                        getHorizontalInsets = true;
                        getVerticalInsets = true;
                        break;
                }
                if (getHorizontalInsets) {
                    getHorizontalInsets(view, parent, configContainer.getHorizontalInsets());
                }
                if (getVerticalInsets) {
                    getVerticalInsets(view, parent, configContainer.getVerticalInsets());
                }
            }
        }
    }

    @Override
    public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
        if (this.mDividerDrawable != null) {
            switch (C09431.f55xb4d32668[this.mContainerType.ordinal()]) {
                case 1/*1*/:
                    drawHorizontalDividers(c, this.mDividerDrawable, parent);
                case 2 /*2*/:
                    drawVerticalDividers(c, this.mDividerDrawable, parent);
                case 3 /*3*/:
                    drawGridDividers(c, this.mDividerDrawable, parent);
                default:
            }
        }
    }


    public void setContainerType(ContainerType containerType) {
        this.mContainerType = containerType;
    }

    public void setColor(int color) {
        if (this.mDividerDrawable != null) {
            if (!this.mUsingCustomDivider) {
                ShapeDrawable divider = new ShapeDrawable(new RectShape());
                divider.setIntrinsicWidth(this.mDividerDrawable.getIntrinsicWidth());
                divider.setIntrinsicHeight(this.mDividerDrawable.getIntrinsicHeight());
                this.mDividerDrawable = divider;
                this.mUsingCustomDivider = true;
            }
            this.mDividerDrawable.setAlpha(Color.alpha(color));
            this.mDividerDrawable.setColorFilter(new LightingColorFilter(Color.BLACK, color));
        }
    }


    protected boolean shouldOffsetItem(View child, RecyclerView parent) {
        return true;
    }
    protected boolean shouldDrawDivider(View child, RecyclerView parent) {
        return true;
    }

    protected void getHorizontalInsets(View child, RecyclerView parent, HorizontalInsets outInsets) {
        outInsets.setEmpty();
    }
    protected void getVerticalInsets(View child, RecyclerView parent, VerticalInsets outInsets) {
        outInsets.setEmpty();
    }

    private void drawHorizontalDividers(Canvas c, Drawable drawable, RecyclerView parent) {
        int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View child = parent.getChildAt(i);
            ConfigContainer configContainer = getConfigContainer(child);
            if (configContainer.shouldDecorateItem() && shouldDrawDivider(child, parent)) {
                RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();
                int left = child.getLeft();
                int top = child.getBottom() + params.bottomMargin;
                int right = child.getRight();
                int bottom = top + drawable.getIntrinsicHeight();
                if (this.mIncludeMargins) {
                    left -= params.leftMargin;
                    right += params.rightMargin;
                }
                HorizontalInsets insets = configContainer.getHorizontalInsets();
                left += insets.mLeft;
                right -= insets.mRight;
                if (this.mHorizontalDividerMaxLength >= 0 && right - left > this.mHorizontalDividerMaxLength) {
                    right = left + this.mHorizontalDividerMaxLength;
                }
                drawDividerIfPossible(c, drawable, left, top, right, bottom);
            }
        }
    }

    private void drawVerticalDividers(Canvas c, Drawable drawable, RecyclerView parent) {
        int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View child = parent.getChildAt(i);
            ConfigContainer configContainer = getConfigContainer(child);
            if (configContainer.shouldDecorateItem() && shouldDrawDivider(child, parent)) {
                RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();
                int left = child.getRight() + params.rightMargin;
                int top = child.getTop();
                int right = left + drawable.getIntrinsicWidth();
                int bottom = child.getBottom();
                if (this.mIncludeMargins) {
                    top -= params.topMargin;
                    bottom += params.bottomMargin;
                }
                VerticalInsets insets = configContainer.getVerticalInsets();
                top += insets.mTop;
                bottom -= insets.mBottom;
                if (this.mVerticalDividerMaxLength >= 0 && bottom - top > this.mVerticalDividerMaxLength) {
                    bottom = top + this.mVerticalDividerMaxLength;
                }
                drawDividerIfPossible(c, drawable, left, top, right, bottom);
            }
        }
    }

    private void drawGridDividers(Canvas c, Drawable drawable, RecyclerView parent) {
        drawHorizontalDividers(c, drawable, parent);
        drawVerticalDividers(c, drawable, parent);
    }

    private void drawDividerIfPossible(Canvas canvas, Drawable drawable, int left, int top, int right, int bottom) {
        if (left <= right && top <= bottom) {
            drawable.setBounds(left, top, right, bottom);
            drawable.draw(canvas);
        }
    }

    private ConfigContainer getConfigContainer(View view) {
        ConfigContainer configContainer = (ConfigContainer) view.getTag(this.mConfigContainerId);
        if (configContainer != null) {
            return configContainer;
        }
        view.setTag(this.mConfigContainerId, new ConfigContainer());
        return (ConfigContainer) view.getTag(this.mConfigContainerId);
    }
}


