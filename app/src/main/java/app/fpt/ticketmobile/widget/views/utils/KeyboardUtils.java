package app.fpt.ticketmobile.widget.views.utils;

import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import app.fpt.ticketmobile.widget.views.libs.ShowImeRunnable;

/**
 * Created by Administrator on 7/13/2016.
 */
public class KeyboardUtils {

    public static ShowImeRunnable showSoftInput(View view) {
        ShowImeRunnable runnable = new ShowImeRunnable(view);
        view.post(runnable);
        return runnable;
    }

    public static void hideSoftInput(View view) {
        InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static void hideSoftInput(ShowImeRunnable showImeRunnable) {
        View view = showImeRunnable.getView();
        view.removeCallbacks(showImeRunnable);
        hideSoftInput(view);
    }

}
