package app.fpt.ticketmobile.widget.views;

import android.content.Context;
import android.content.DialogInterface;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import app.fpt.ticketmobile.R;
import app.fpt.ticketmobile.app.model.SpinnerObject;
import app.fpt.ticketmobile.app.ui.adapter.spinner.MySpinnerAdapter;

/**
 * Created by Administrator on 7/19/2016.
 */
public class MultiSelectionSpinner extends Spinner implements DialogInterface.OnMultiChoiceClickListener {

    private List<SpinnerObject> mListItemsData = new ArrayList<>();
    private List<SpinnerObject> mListItemOriginalDisplay = new ArrayList<>();
    private String mTitleDialog = null;
    String[] _items = null;
    boolean[] mSelection = null;
    boolean[] mSelectionOld = null;
    MySpinnerAdapter simple_adapter = null;

    public MultiSelectionSpinner(Context context) {
        super(context);
    }

    public MultiSelectionSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setSpinnerMultiAdapter(String titleDialog, List<SpinnerObject> ItemOriginalDisplay , List<SpinnerObject> listItemsData){
        this.mTitleDialog = titleDialog;
        this.mListItemsData =  listItemsData;
        this.mListItemOriginalDisplay = ItemOriginalDisplay;
        try {
            simple_adapter = new MySpinnerAdapter(getContext(), R.layout.textview_spinner, mListItemOriginalDisplay);
            simple_adapter.setDropDownViewResource(R.layout.simple_spinner_item);
            setItems(listItemsData);
            super.setAdapter(simple_adapter);

        } catch (Exception e) {
            Log.e("tmt",  "setSpinnerMultiAdapter " +  e.getMessage());
        }
    }

    public void setItems(List<SpinnerObject> items) {
        BindingListItemDialog(items);
        mSelection = new boolean[_items.length];
        Arrays.fill(mSelection, false);
    }


    private void BindingListItemDialog(List<SpinnerObject> items) {
        _items = new String[items.size()];
        for (int i = 0; i < items.size(); i++) {
            _items[i] = items.get(i).getValue();
        }
    }


    @Override
    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
        if (mSelection != null && which < mSelection.length && which > -1) {
            mSelection[which] = isChecked;
        } else {
            throw new IllegalArgumentException("Argument 'which' is out of bounds.");
        }
    }

    @Override
    public boolean performClick() {


        return true;
    }
}
